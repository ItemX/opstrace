package errortracking

import (
	"time"

	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
)

// InsertErrorEventV2 inserts the given error event in the
// error_tracking_error_event_v2 table in the clickhouse database. It then proceeds
// to upsert gl_error_tracking_error_status_v2 as well.
func (db *database) InsertErrorEventV2(e *et.ErrorEventV2) error {
	err := db.insert(e)
	if err != nil {
		return err
	}

	errorStatus := &et.ErrorStatusV2{
		ProjectID:   e.ProjectID,
		EventID:     e.EventID,
		Timestamp:   e.Timestamp,
		IsDeleted:   e.IsDeleted,
		Fingerprint: e.Fingerprint,
		Actor:       e.Actor,
		//Title:     e.Title, // TODO: Title is to be computed.
		//UserID: // TODO: Compute userID if exists from the event.
		Status:    uint8(errorUnresolved),
		UpdatedBy: uint8(2),
		UpdatedAt: time.Now(),
	}
	return db.insert(errorStatus)
}
