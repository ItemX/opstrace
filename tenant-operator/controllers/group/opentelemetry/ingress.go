package opentelemetry

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/config"
	netv1 "k8s.io/api/networking/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getIngressTLS(cr *v1alpha1.Group) []netv1.IngressTLS {
	return []netv1.IngressTLS{
		{
			Hosts: []string{cr.Spec.GetHost()},
		},
	}
}

func getIngressLabels(cr *v1alpha1.Group) map[string]string {
	return map[string]string{}
}

func getIngressAnnotations(cr *v1alpha1.Group, existing map[string]string) map[string]string {
	gatekeeperURL := config.Get().GetGatekeeperURL()
	domain := cr.Spec.GetHostURL()
	annotations := common.MergeMap(
		existing,
		common.GetAuthIngressAnnotations(fmt.Sprint(cr.Spec.ID), gatekeeperURL, domain, "write"),
	)
	// otel-collector's http receiver is hardcoded to /v1/traces, see also
	// https://github.com/open-telemetry/opentelemetry-collector/blob/main/receiver/otlpreceiver/otlp.go#L180
	annotations["nginx.ingress.kubernetes.io/rewrite-target"] = "/v1/traces"
	// By default the limit is 1 MiB, but we got reports that this is not
	// enough and users hit `413 Request Entity Too Large` error. The 10 MiB is
	// an arbitrary value that should be enough for normal traffic but without
	// more detailed metrics it is hard to tell for sure. OTEL collector does
	// not impose any limits ATM.
	annotations["nginx.ingress.kubernetes.io/proxy-body-size"] = "10m"

	return annotations
}

func getIngressSpec(cr *v1alpha1.Group) netv1.IngressSpec {
	pathType := netv1.PathTypePrefix
	serviceName := GetOtelDeploymentName(cr)

	serviceBackendPort := netv1.ServiceBackendPort{
		Name: constants.OtelIngressPortName,
	}

	return netv1.IngressSpec{
		TLS: getIngressTLS(cr),
		Rules: []netv1.IngressRule{
			{
				Host: cr.Spec.GetHost(),
				IngressRuleValue: netv1.IngressRuleValue{
					HTTP: &netv1.HTTPIngressRuleValue{
						Paths: []netv1.HTTPIngressPath{
							{
								Path:     fmt.Sprintf("/v1/traces/%d", cr.Spec.ID),
								PathType: &pathType,
								Backend: netv1.IngressBackend{
									Service: &netv1.IngressServiceBackend{
										Name: serviceName,
										Port: serviceBackendPort,
									},
									Resource: nil,
								},
							},
						},
					},
				},
			},
		},
	}
}

func Ingress(cr *v1alpha1.Group) *netv1.Ingress {
	return &netv1.Ingress{
		ObjectMeta: v1.ObjectMeta{
			Name:        GetOtelDeploymentName(cr),
			Namespace:   cr.Namespace,
			Labels:      getIngressLabels(cr),
			Annotations: getIngressAnnotations(cr, nil),
		},
		Spec: getIngressSpec(cr),
	}
}

func IngressMutator(cr *v1alpha1.Group, current *netv1.Ingress) error {
	currentSpec := &current.Spec
	spec := getIngressSpec(cr)
	// Apply default overrides
	if err := common.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := common.PatchObject(
		currentSpec,
		cr.Spec.Overrides.OpenTelemetry.Components.Ingress.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = common.MergeMap(
		getIngressAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.OpenTelemetry.Components.Ingress.Annotations,
	)
	current.Labels = common.MergeMap(
		getIngressLabels(cr),
		cr.Spec.Overrides.OpenTelemetry.Components.Ingress.Labels,
	)

	return nil
}
func IngressSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      GetOtelDeploymentName(cr),
	}
}
