//go:build linux

package common

import "github.com/rjeczalik/notify"

const (
	eventMask = notify.InCreate | notify.InMovedTo | notify.InModify | notify.InMoveSelf
)
