package models

import (
	"encoding/hex"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/zeebo/blake3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
)

type ErrorEventV2 struct {
	Environment string    `json:"environment,omitempty"`
	EventID     uuid.UUID `json:"event_id,omitempty" ch:"event_id"`
	Level       string    `json:"level,omitempty" ch:"level"`
	Message     string    `json:"message,omitempty" ch:"message"`
	Platform    string    `json:"platform,omitempty" ch:"platform"`
	Timestamp   time.Time `json:"timestamp" ch:"timestamp"`
	Transaction string    `json:"transaction,omitempty" ch:"transaction"`
	// Exception can be an object with the attribute values [] or a flat list of objects.
	// See https://develop.sentry.dev/sdk/event-payloads/exception/.
	// This is field is decoded in a special way
	Exception   []*types.Exception `json:"-"`
	Fingerprint []byte             `ch:"fingerprint"`
	Actor       string             `ch:"actor"`
	IsDeleted   uint8              `ch:"is_deleted"`
	ProjectID   uint64             `ch:"project_id"`
	Payload     string
}

func NewErrorEventV2(projectID uint64, e *types.Event, payload []byte) (*ErrorEventV2, error) {
	// TODO: Tune fingerprinting of the event
	fingerprint := blake3.Sum256([]byte(fmt.Sprintf("%s|%s|%s", e.Name(), e.ExceptionActor(), e.Platform)))

	eventID, err := formatUUID(e.EventID)
	if err != nil {
		return nil, err
	}
	return &ErrorEventV2{
		ProjectID:   projectID,
		Actor:       e.ExceptionActor(),
		Platform:    e.Platform,
		Environment: e.Environment,
		Level:       e.Level,
		Payload:     string(payload),
		Fingerprint: fingerprint[:],
		Timestamp:   e.Timestamp,
		EventID:     eventID,
		Message:     e.Message,
		Transaction: e.Transaction,
		IsDeleted:   uint8(0),
	}, nil
}

func (e *ErrorEventV2) AsInsertStmt(tz *time.Location) string {
	stmt := `
INSERT INTO %s (
	event_id,
	project_id,
	timestamp,
	is_deleted,
	fingerprint,
	actor,
	platform,
	environment,
	level,
	payload,
	message,
	transaction
) VALUES (toUUID(%s), %d, %s, %d, unhex(%s), %s, %s, %s, %s, %s, %s, %s)`

	return fmt.Sprintf(stmt,
		errorEventsTableV2,
		quote(e.EventID.String()),
		e.ProjectID,
		formatTime(e.Timestamp, tz),
		e.IsDeleted,
		quote(hex.EncodeToString(e.Fingerprint)),
		quote(e.Actor),
		quote(e.Platform),
		quote(e.Environment),
		quote(e.Level),
		quote(e.Payload),
		quote(e.Message),
		quote(e.Transaction),
	)
}
