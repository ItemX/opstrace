package gitlabobservabilityexporter

import "github.com/prometheus/client_golang/prometheus"

var (
	tracedataSizeBytes = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "custom",
			Name:      "traces_size_bytes",
			Help:      "size of traces received in bytes",
		},
		[]string{"group"},
	)
	spansReceivedCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "custom",
			Name:      "spans_received",
			Help:      "number of spans received per tenant",
		},
		[]string{"group"},
	)
	spansIngestedCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "custom",
			Name:      "spans_ingested",
			Help:      "number of spans ingested per tenant",
		},
		[]string{"group"},
	)
)

func ConfiguredCollectors() []prometheus.Collector {
	return []prometheus.Collector{
		tracedataSizeBytes,
		spansReceivedCounter,
		spansIngestedCounter,
	}
}
