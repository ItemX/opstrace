/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package group

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"strconv"
	"strings"
	"testing"
	"time"

	ctrl "sigs.k8s.io/controller-runtime"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/envtest"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/argusapi"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	tenantapi "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api"
	tenantapiv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	// +kubebuilder:scaffold:imports
)

const (
	groupID  = 42
	timeout  = time.Second * 10
	interval = time.Millisecond * 500
)

var (
	k8sClient   client.Client
	testEnv     *envtest.Environment
	ctx         context.Context
	cancel      context.CancelFunc
	argusServer *httptest.Server
)

func TestAPIs(t *testing.T) {
	RegisterFailHandler(Fail)
	suiteConfig, reporterConfig := GinkgoConfiguration()
	reporterConfig.FullTrace = true
	RunSpecs(t, "Group Test Suite", suiteConfig, reporterConfig)
}

var _ = BeforeSuite(func() {
	logf.SetLogger(zap.New(zap.WriteTo(GinkgoWriter), zap.UseDevMode(true)))

	ctx, cancel = context.WithCancel(context.TODO())

	By("bootstrapping test environment")
	testEnv = &envtest.Environment{
		CRDDirectoryPaths: []string{
			"../../../scheduler/controllers/cluster/manifests/prometheus-operator/servicemonitors.crd.yaml",
			"../../../scheduler/controllers/cluster/manifests/jaeger-operator/jaeger.crd.yaml",
			filepath.Join("../../", "config", "crd", "bases")},
		ErrorIfCRDPathMissing: true,
	}

	cfg, err := testEnv.Start()
	Expect(err).NotTo(HaveOccurred())
	Expect(cfg).NotTo(BeNil())

	Expect(scheme.AddToScheme(scheme.Scheme)).To(Succeed())
	Expect(tenantapi.AddToScheme(scheme.Scheme)).To(Succeed())

	// +kubebuilder:scaffold:scheme

	k8sClient, err = client.New(cfg, client.Options{Scheme: scheme.Scheme})
	Expect(err).NotTo(HaveOccurred())
	Expect(k8sClient).NotTo(BeNil())

	k8sManager, err := ctrl.NewManager(cfg,
		ctrl.Options{
			Scheme: scheme.Scheme,
			// disable metrics binding to prevent port clashes
			MetricsBindAddress: "0",
		})
	Expect(err).NotTo(HaveOccurred())

	By("set up fake argus http server")
	argusServer = argusServerStub()
	createTenant(argusServer.URL)

	err = (&ReconcileGroup{
		Client:   k8sManager.GetClient(),
		Scheme:   k8sManager.GetScheme(),
		Log:      ctrl.Log.WithName("controllers").WithName("group"),
		Recorder: k8sManager.GetEventRecorderFor("controller"),
		/* #nosec G402 */
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}).SetupWithManager(k8sManager)
	Expect(err).NotTo(HaveOccurred())

	go func() {
		defer GinkgoRecover()
		err = k8sManager.Start(ctx)
		Expect(err).ToNot(HaveOccurred(), "failed to run manager")
	}()
})

var _ = AfterSuite(func() {
	cancel()
	By("tearing down the test environment")
	argusServer.Close()
	Expect(testEnv.Stop()).To(Succeed())
})

// stub out argus endpoints for this controller
func argusServerStub() *httptest.Server {
	writeJSON := func(w http.ResponseWriter, obj interface{}) {
		if err := json.NewEncoder(w).Encode(obj); err != nil {
			panic(err)
		}
	}

	mux := &http.ServeMux{}
	mux.Handle(
		"/api/groups/",
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.URL.Path == "/api/groups/" {
				writeJSON(w, []argusapi.Group{
					{
						ID:   groupID,
						Name: "TEST_GROUP",
					},
				},
				)
			} else {
				// FIXME(prozlach): This is a very naive approach that could
				// use more work/time.
				gID := strings.TrimPrefix(r.URL.Path, "/api/groups/")
				gIDParsed, err := strconv.ParseInt(gID, 10, 64)
				Expect(err).ToNot(HaveOccurred())
				writeJSON(w, argusapi.Group{
					ID:   gIDParsed,
					Name: "TEST_GROUP",
				},
				)
			}
		}))

	return httptest.NewServer(mux)
}

func createTenant(argusURL string) {
	res := &tenantapiv1alpha1.Tenant{
		ObjectMeta: metav1.ObjectMeta{
			Name:       constants.TenantName,
			Namespace:  "default",
			Generation: 1,
		},
	}
	Expect(k8sClient.Create(ctx, res)).To(Succeed())
	condition := metav1.Condition{
		Status:             metav1.ConditionTrue,
		Reason:             common.ReconciliationSuccessReason,
		Message:            "All components are in ready state",
		Type:               common.ConditionTypeReady,
		ObservedGeneration: 1,
	}
	apimeta.SetStatusCondition(&res.Status.Conditions, condition)
	res.Status.Argus = tenantapiv1alpha1.ArgusStatus{
		URL: &argusURL,
	}
	Expect(k8sClient.Status().Update(ctx, res)).To(Succeed())
}
