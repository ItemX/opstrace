package cluster

import (
	"context"
	"fmt"
	"net/url"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ClusterState struct {
	ClickHouse       ClickHouseState
	PostgresEndpoint *v1.Secret
	// All Opstrace tenants
	Tenants *v1.NamespaceList

	LogLevel string
}

func NewClusterState(logLevel string) *ClusterState {
	return &ClusterState{LogLevel: logLevel}
}

func (i *ClusterState) Read(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	err := i.readClickHouseState(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readTenants(ctx, client)
	if err != nil {
		return err
	}

	err = i.readPostgresEndpoint(ctx, client)
	if err != nil {
		return err
	}

	return err
}

// Get ClickHouse endpoints for the scheduler user
func (i *ClusterState) GetPostgresEndpoint() (*url.URL, error) {
	if i.PostgresEndpoint == nil || i.PostgresEndpoint.Data[constants.PostgresEndpointKey] == nil {
		return nil, fmt.Errorf("%s key not set in %s secret in %s namespace. This is needed to connect to the postgres instance",
			constants.PostgresEndpointKey,
			constants.PostgresCredentialName,
			constants.PostgresCredentialNamespace)
	}
	endpoint := string(i.PostgresEndpoint.Data[constants.PostgresEndpointKey])
	return url.Parse(endpoint)
}

func (i *ClusterState) readTenants(ctx context.Context, c client.Client) error {
	currentState := &v1.NamespaceList{}
	selector, err := metav1.LabelSelectorAsSelector(
		&metav1.LabelSelector{
			MatchLabels: map[string]string{
				constants.TenantLabelIdentifier: "true",
			},
		},
	)
	if err != nil {
		return err
	}
	err = c.List(ctx, currentState, &client.ListOptions{
		LabelSelector: selector,
	})
	if err != nil {
		return err
	}
	i.Tenants = currentState.DeepCopy()
	return nil
}

func (i *ClusterState) readPostgresEndpoint(ctx context.Context, c client.Client) error {
	currentState := &v1.Secret{}
	selector := client.ObjectKey{
		Namespace: constants.PostgresCredentialNamespace,
		Name:      constants.PostgresCredentialName,
	}
	err := c.Get(ctx, selector, currentState)
	if err != nil {
		return err
	}
	i.PostgresEndpoint = currentState.DeepCopy()
	return nil
}

func (i *ClusterState) readClickHouseState(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	clickHouseState := NewClickHouseState()
	if err := clickHouseState.Read(ctx, cr, client); err != nil {
		return err
	}

	i.ClickHouse = *clickHouseState
	return nil
}
