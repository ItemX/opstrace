package argus

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getPostgresEndpointName(cr *v1alpha1.Tenant) string {
	return constants.ArgusPostgresSecretName
}

func getClickhouseEndpointData(endpoint []byte) map[string][]byte {
	credentials := map[string][]byte{
		constants.ArgusPostgresURLKey: endpoint,
	}
	return credentials
}

func PostgresEndpointSecret(cr *v1alpha1.Tenant, endpoint []byte) *corev1.Secret {
	data := getClickhouseEndpointData(endpoint)

	return &corev1.Secret{
		ObjectMeta: v1.ObjectMeta{
			Name:      getPostgresEndpointName(cr),
			Namespace: cr.Namespace,
		},
		Data: data,
		Type: corev1.SecretTypeOpaque,
	}
}

func PostgresEndpointSecretMutator(current *corev1.Secret, endpoint []byte) {
	data := getClickhouseEndpointData(endpoint)
	current.Data = data
}

func PostgresEndpointSecretSelector(cr *v1alpha1.Tenant) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getPostgresEndpointName(cr),
	}
}
