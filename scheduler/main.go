/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

//nolint:gochecknoinits
package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"net/http"
	"os"
	"runtime"
	"time"

	"go.uber.org/zap/zapcore"
	"sigs.k8s.io/cli-utils/pkg/kstatus/polling"
	"sigs.k8s.io/cli-utils/pkg/kstatus/polling/engine"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	"sigs.k8s.io/controller-runtime/pkg/log"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	apis "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster"
	clustermanifests "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/manifests"
	ctrlcommon "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/tenant"
	tenantmanifests "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/tenant/manifests"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/version"

	_ "go.uber.org/automaxprocs"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.

	_ "k8s.io/client-go/plugin/pkg/client/auth"

	k8sruntime "k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"

	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/namespace"
	// +kubebuilder:scaffold:imports
)

var (
	scheme                  = k8sruntime.NewScheme()
	setupLog                = ctrl.Log.WithName("setup")
	metricsAddr             string
	enableLeaderElection    bool
	probeAddr               string
	driftPreventionInterval time.Duration
	configFile              string
	err                     error
	logLevel                string
	zapOpts                 zap.Options
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))

	utilruntime.Must(opstracev1alpha1.AddToScheme(scheme))
	// +kubebuilder:scaffold:scheme
}

func printVersion() {
	log.Log.Info(fmt.Sprintf("Go Version: %s", runtime.Version()))
	log.Log.Info(fmt.Sprintf("Go OS/Arch: %s/%s", runtime.GOOS, runtime.GOARCH))
	log.Log.Info(fmt.Sprintf("operator Version: %v", version.Version))
}

func assignOpts() {
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":7070", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":7071", "The address the probe endpoint binds to.")
	flag.DurationVar(
		&driftPreventionInterval,
		"drift-prevention-interval",
		constants.DefaultDriftPreventionInterval,
		"How often should the controller scan for drift (e.g. manual changes) in objects it created.",
	)
	flag.BoolVar(&enableLeaderElection, "leader-elect", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")
	flag.StringVar(&configFile, "config", "",
		"The controller will load its initial configuration from this file. "+
			"Omit this flag to use the default configuration values. "+
			"Command-line flags override configuration from this file.")
	zapOpts = zap.Options{
		Development: true,
		TimeEncoder: zapcore.ISO8601TimeEncoder,
	}
	zapOpts.BindFlags(flag.CommandLine)
	flag.Parse()
}

func main() { //nolint
	printVersion()
	assignOpts()

	ctrlConfig := opstracev1alpha1.ProjectConfig{}
	options := ctrl.Options{
		Scheme:                 scheme,
		MetricsBindAddress:     metricsAddr,
		Port:                   9443,
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         enableLeaderElection,
		LeaderElectionID:       "2c0156f1.opstrace.com",
	}

	if configFile != "" {
		options, err = options.AndFrom(ctrl.ConfigFile().AtPath(configFile).OfKind(&ctrlConfig))
		if err != nil {
			fmt.Printf("msg=unable to load the config file - exiting, error=%v\n", err)
			os.Exit(1)
		}
	}

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), options)
	if err != nil {
		fmt.Printf("msg=unable to start manager - exiting, error=%v\n", err)
		os.Exit(1)
	}

	if logLevel = string(ctrlConfig.LogLevel); logLevel == "" {
		logLevel = "info"
	}
	zapOpts.Level, err = zapcore.ParseLevel(logLevel)
	if err != nil {
		fmt.Printf("msg=unable to parse log level - exiting, error=%v\n", err)
		os.Exit(1)
	}

	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&zapOpts)))

	log.Log.Info("Registering Components")

	// Setup Scheme for all resources
	if err := apis.AddToScheme(mgr.GetScheme()); err != nil {
		log.Log.Error(err, "")
		os.Exit(1)
	}

	if err != nil {
		log.Log.Error(err, "error starting metrics service")
	}

	log.Log.Info("Processing embedded manifests...")
	log.Log.Info("Loading initial manifests for cluster")
	initialClusterManifests, err := ctrlcommon.InitialManifests(clustermanifests.Upstream)
	if err != nil {
		log.Log.Error(err, "error occurred while parsing embedded manifests for the cluster")
		os.Exit(1)
	}
	log.Log.Info("Loading initial manifests for tenants")
	initialTenantManifests, err := ctrlcommon.InitialManifests(tenantmanifests.Upstream)
	if err != nil {
		log.Log.Error(err, "error occurred while parsing embedded manifests for the tenants")
		os.Exit(1)
	}

	log.Log.Info("Starting the Cmd")

	jobStatusReader := common.NewCustomJobStatusReader(mgr.GetRESTMapper())
	pollingOpts := polling.Options{
		CustomStatusReaders: []engine.StatusReader{jobStatusReader},
	}

	if err = (&cluster.ReconcileCluster{
		Client: mgr.GetClient(),
		Scheme: mgr.GetScheme(),
		Log:    ctrl.Log.WithName("controllers").WithName("Cluster"),
		/* #nosec G402 */
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
		Recorder:                mgr.GetEventRecorderFor("Cluster"),
		InitialManifests:        initialClusterManifests,
		StatusPoller:            polling.NewStatusPoller(mgr.GetClient(), mgr.GetRESTMapper(), pollingOpts),
		DriftPreventionInterval: driftPreventionInterval,
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Cluster")
		os.Exit(1)
	}

	if err = (&namespace.ReconcileGitLabNamespace{
		Client: mgr.GetClient(),
		Scheme: mgr.GetScheme(),
		Log:    ctrl.Log.WithName("controllers").WithName("GitLabNamespace"),
		/* #nosec G402 */
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
		Recorder:     mgr.GetEventRecorderFor("GitLabNamespace"),
		StatusPoller: polling.NewStatusPoller(mgr.GetClient(), mgr.GetRESTMapper(), pollingOpts),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "GitLabNamespace")
		os.Exit(1)
	}
	// +kubebuilder:scaffold:builder

	tenantReconciler := tenant.ReconcileTenant{
		Client: mgr.GetClient(),
		Scheme: mgr.GetScheme(),
		Log:    ctrl.Log.WithName("controllers").WithName("GitLabObservabilityTenant"),
		/* #nosec G402 */
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
		Recorder:                mgr.GetEventRecorderFor("GitLabObservabilityTenant"),
		InitialManifests:        initialTenantManifests,
		StatusPoller:            polling.NewStatusPoller(mgr.GetClient(), mgr.GetRESTMapper(), pollingOpts),
		DriftPreventionInterval: 30 * time.Second,
	}
	if err = (&tenantReconciler).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "GitLabObservabilityTenant")
		os.Exit(1)
	}
	// +kubebuilder:scaffold:builder

	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}
	if err := mgr.AddReadyzCheck("readyz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up ready check")
		os.Exit(1)
	}

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}
