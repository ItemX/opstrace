package recorder

import (
	"bytes"
	"compress/gzip"
	"compress/zlib"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
	harnesstypes "gitlab.com/gitlab-org/opstrace/opstrace/test/sentry-sdk/internal/types"
)

type Recorder struct {
	addr      string
	server    *http.Server
	isStarted bool

	output     map[string]harnesstypes.ParsedRequest
	outputLock sync.Mutex
}

func NewRecorder(port int) *Recorder {
	r := &Recorder{
		addr:   fmt.Sprintf(":%v", port),
		output: make(map[string]harnesstypes.ParsedRequest),
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/download", r.downloadHandler)
	mux.HandleFunc("/", r.sentryHandler)

	r.server = &http.Server{
		Addr:        r.addr,
		Handler:     mux,
		ReadTimeout: 5 * time.Second,
	}

	return r
}

func (rec *Recorder) Start() error {
	rec.isStarted = true
	if err := rec.server.ListenAndServe(); err != nil {
		if err != http.ErrServerClosed {
			fmt.Println(err)
		}
		return fmt.Errorf("starting to listen on recorder server: %w", err)
	}
	return nil
}

func (rec *Recorder) sentryHandler(w http.ResponseWriter, r *http.Request) {
	var (
		payload []byte
		err     error
	)

	parsed := harnesstypes.ParsedRequest{
		URL:           r.RequestURI,
		Protocol:      r.Proto,
		Headers:       r.Header,
		ClientVersion: extractSentryClient(r.Header),
	}

	payload, err = io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		//nolint:errcheck
		w.Write([]byte(err.Error()))
		return
	}

	if isGzipEncoded(r) {
		payload, err = uncompressedGzipPayload(payload)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			//nolint:errcheck
			w.Write([]byte(err.Error()))
			return
		}
	}

	parsed.Body = string(payload)
	fmt.Printf("%v", parsed)

	if strings.Contains(parsed.URL, "store") {
		if !json.Valid(payload) {
			payload, err = uncompressPayload(payload)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				//nolint:errcheck
				w.Write([]byte(err.Error()))
				return
			}
		}
	}

	var (
		endpoint string
		item     interface{}
	)

	item, err = types.NewEventFrom(payload)
	if err != nil {
		item, err = types.NewEnvelopeFrom(payload)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			//nolint:errcheck
			w.Write([]byte(err.Error()))
			return
		} else {
			endpoint = "envelope"
		}
	} else {
		endpoint = "store"
	}
	parsed.Endpoint = endpoint

	// if needed, check if we can parse SDK details from the payload itself
	if endpoint == "envelope" {
		envelope, ok := item.(*types.Envelope)
		if ok {
			if envelope.Headers.SDK != nil {
				parsed.ClientVersion = fmt.Sprintf("%s:%s", envelope.Headers.SDK.Name, envelope.Headers.SDK.Version)
			}
		}
	}

	randomKey := uuid.New()
	parsed.UUID = randomKey.String()

	rec.outputLock.Lock()
	rec.output[randomKey.String()] = parsed
	rec.outputLock.Unlock()

	w.WriteHeader(200)
	//nolint:errcheck
	json.NewEncoder(w).Encode(&randomKey)
}

func (rec *Recorder) downloadHandler(w http.ResponseWriter, r *http.Request) {
	rec.outputLock.Lock()
	output := rec.output
	rec.outputLock.Unlock()

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	//nolint:errcheck
	json.NewEncoder(w).Encode(&output)
}

func isGzipEncoded(req *http.Request) bool {
	return req.Header.Get("Content-Encoding") == "gzip"
}

func uncompressedGzipPayload(original []byte) ([]byte, error) {
	reader := bytes.NewReader(original)
	gzreader, err := gzip.NewReader(reader)
	if err != nil {
		return nil, fmt.Errorf("reading gzip'ed payload: %w", err)
	}
	output, err := io.ReadAll(gzreader)
	if err != nil {
		return nil, fmt.Errorf("reading gzip'ed payload: %w", err)
	}
	return output, nil
}

func uncompressPayload(payload []byte) ([]byte, error) {
	raw, err := base64.StdEncoding.DecodeString(string(payload))
	if err != nil {
		return nil, fmt.Errorf("failed to decode base64 payload: %w", err)
	}
	r := bytes.NewReader(raw)
	zr, err := zlib.NewReader(r)
	if err != nil {
		return nil, fmt.Errorf("failed to read compressed payload: %w", err)
	}
	output, err := io.ReadAll(zr)
	if err != nil {
		return nil, fmt.Errorf("failed to read compressed payload: %w", err)
	}
	return output, nil
}

func extractSentryClient(headers map[string][]string) string {
	var sentryClient string
	// from User-Agent
	if _, ok := headers["User-Agent"]; ok {
		sentryClient = headers["User-Agent"][0]
	}
	// from X-Sentry-Auth
	//nolint:nestif
	if sentryClient == "" {
		if _, ok := headers["X-Sentry-Auth"]; ok {
			comps := strings.Split(headers["X-Sentry-Auth"][0], ",")
			if len(comps) > 0 {
				for _, comp := range comps {
					if strings.Contains(comp, "sentry_client") {
						sentryClient = strings.Split(comp, "=")[1]
					}
				}
			}
		}
	}
	// last resort!
	if sentryClient == "" {
		sentryClient = fmt.Sprintf("%+v", headers)
	}
	return sentryClient
}
