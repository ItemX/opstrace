# Sending traces with otel-collector

In this guide we show how to configure an `otel-collector` agent to securely send traces to your GitLab Observability instance.

## Prerequisites

* A GitLab Observability instance.
* A decision: for which GitLab namespace would you like to send data?
* A namespace authentication token created in the GitLab Observability UI for the namespace.

## Example Kubernetes Deployment

This example shows how you could configure an OpenTelemetry Collector instance to send traces to GitLab Observability.

To get started, copy the ConfigMap and Deployment definitions in the following code block, and then make the following changes to the copy:

* Enable any desired `receivers` supported by `otel-collector-contrib`. This example only enables an OTLP receiver.
* Replace stub values:
  * Replace `NAMESPACE` with the namespace name
  * Replace `NAMESPACE_TOKEN` with the namespace auth token
  * Replace `GITLAB_O11Y_ADDRESS` with the GitLab Observability instance domain
* Apply the edited configuration to your cluster as follows:
    `kubectl apply -f filename.yaml`

```yaml
kind: ConfigMap
apiVersion: v1
metadata:
  name: otel-collector
  namespace: kube-system
  labels:
    k8s-app: otel-collector
data:
  otel.yaml: |
    receivers:
      # Receive traces from kubelet
      # See also: https://kubernetes.io/docs/concepts/cluster-administration/system-traces
      otlp:
        protocols:
          grpc: {}

    processors:
      batch: {}

    exporters:
      # Log what's happening
      logging: {}
      # Send spans to GitLab Observability instance
      otlphttp:
        traces_endpoint: GITLAB_O11Y_ADDRESS/v1/traces/NAMESPACE
        headers:
          authorization: Bearer NAMESPACE_TOKEN

    # Set up the pipeline linking together the above components
    service:
      pipelines:
        traces:
          receivers: [otlp]
          processors: [batch]
          exporters: [logging, otlphttp]

---

kind: Deployment
apiVersion: apps/v1
metadata:
  name: otel-collector
  namespace: kube-system
  labels:
    k8s-app: otel-collector
spec:
  replicas: 1
  selector:
    matchLabels:
      k8s-app: otel-collector
  template:
    metadata:
      labels:
        k8s-app: otel-collector
    spec:
      nodeSelector:
        kubernetes.io/arch: amd64 # image is amd64-only
      containers:
      - name: otel-collector
        image: docker.io/otel/opentelemetry-collector-contrib:0.42.0
        command: [/otelcol-contrib, --config, /config/otel.yaml]
        volumeMounts:
        - name: config
          mountPath: /config
      volumes:
      - name: config
        configMap:
          name: otel-collector
```
