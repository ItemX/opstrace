package cluster

import (
	"fmt"
	"strings"

	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

const (
	ingressPath = `/query/(?<groupID>[0-9]+)/(?<projectID>[0-9]+)`
)

type TraceQueryAPIReconciler struct {
	BaseReconciler
	clusterState *ClusterState
}

func NewTraceQueryAPIReconciler(
	initialManifests map[string][]byte,
	teardown bool,
	clusterState *ClusterState) *TraceQueryAPIReconciler {
	res := &TraceQueryAPIReconciler{
		clusterState: clusterState,

		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.TraceQueryAPIInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.TraceQueryAPIInventoryID,
			reconcilerName: "trace-query-api",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.TraceQueryAPI
			},

			serviceAccountName: constants.TraceQueryAPIName,
		},
	}

	res.subclassApplyMethod = res.applyConfiguration
	return res
}

func (r *TraceQueryAPIReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := r.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}
	res.Images = []kustomize.Image{
		{
			Name:    "trace-query-api-image-placeholder",
			NewName: constants.DockerImageName(constants.TraceQueryAPIImageName),
			NewTag:  constants.DockerImageTag,
		},
	}
	err = r.applyDeploymentConfiguration(res)
	if err != nil {
		return nil, err
	}

	gatekeeperURL := fmt.Sprintf("http://gatekeeper.%s.svc.cluster.local:3001", cr.Namespace())

	res.PatchesJson6902 = append(
		res.PatchesJson6902,
		// nginx-ingress auth-url
		// e.g. http://GATEKEEPER_ENDPOINT/v1/auth/both/webhook/GROUP_ID/PROJECT_ID/read?min_accesslevel=0
		types.Patch{
			Patch: fmt.Sprintf(
				`[
						{"op": "replace", "path": "/spec/rules/0/host", "value": "%s" },
						{"op": "replace", "path": "/spec/rules/0/http/paths/0/path", "value": "%s"},
						{"op": "replace", "path": "/metadata/annotations/nginx.ingress.kubernetes.io~1auth-url", "value": "%s/v1/auth/both/webhook/$groupID/$projectID/read?min_accesslevel=0" },
						{"op": "replace", "path": "/metadata/annotations/nginx.ingress.kubernetes.io~1cors-allow-origin", "value": "%s" }
					  ]`,
				cr.Spec.GetHost(),
				ingressPath,
				gatekeeperURL,
				cr.Spec.GitLab.TrimInstanceURL(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Ingress",
					},
					Name: constants.TraceQueryAPIName,
				},
			},
		},
		// PodMonitor
		types.Patch{
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "PodMonitor",
					},
					Name: constants.TraceQueryAPIName,
				},
			},
			Patch: fmt.Sprintf(
				`[
				{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}
			]`,
				cr.Namespace(),
			),
		},
	)

	return res, nil
}

func (r *TraceQueryAPIReconciler) applyDeploymentConfiguration(k *kustomize.Kustomization) error {
	clickHouseEndpoints, err := r.clusterState.ClickHouse.GetEndpoints()
	if err != nil {
		return fmt.Errorf("failed to retrieve clickhouse endpoints: %w", err)
	}
	clickHouseDSN := clickHouseEndpoints.Native.String() + "/" + constants.TraceQueryAPIDBName

	patches := []string{
		fmt.Sprintf(
			`{"op": "replace", "path": "/spec/template/spec/containers/0/env/1/value", "value": "%s" }`,
			clickHouseDSN,
		),
	}
	if r.clusterState.LogLevel != "" {
		patches = append(patches,
			fmt.Sprintf(
				`{"op": "replace", "path": "/spec/template/spec/containers/0/env/0/value", "value": "%s" }`,
				r.clusterState.LogLevel,
			),
		)
	}

	k.PatchesJson6902 = append(
		k.PatchesJson6902,
		types.Patch{
			Patch: fmt.Sprintf(
				`[ %s ]`, strings.Join(patches, ","),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Deployment",
					},
					Name: constants.TraceQueryAPIName,
				},
			},
		},
	)

	return nil
}
