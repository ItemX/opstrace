provider "google" {
  project = var.project_id
  region  = var.region
  zone    = var.zone
}

# GCS-backed disk creation on CH does not support authenticating
# with service account HMAC keys. For now, we resort to creating
# user account HMAC keys out of band and passing it to this module.

# Create a new service account
resource "google_service_account" "service_account" {
  account_id = var.service_account_name
}

# Create the HMAC key for the associated service account
resource "google_storage_hmac_key" "key" {
  service_account_email = google_service_account.service_account.email
}


# Create the GCS bucket
resource "google_storage_bucket" "clickhouse_data" {
  name          = var.bucket_name
  location      = var.bucket_location
  storage_class = var.storage_class

  force_destroy = false

  public_access_prevention = "enforced"

  versioning {
    enabled = true
  }
  // Enforce lifecycle rules of keeping 1 non-current version and deleting non-current objects after 7 days.
  // This is the default configuration that comes while creating the bucket via Console.
  lifecycle_rule {
    action {
      type = "Delete"
    }
    condition {
      num_newer_versions = 1
    }
  }

  lifecycle_rule {
    action {
      type = "Delete"
    }
    condition {
      days_since_noncurrent_time = 7
    }
  }

  // Enforce dual region replication.
  // In order to aid performance between ClickHouse and GCS, it is essential to have the bucket in the same region and zone
  // as the ClickHouse nodes.
  // Choose the other region that has the lowest latency from the default region.
  // Reasons for not using default multi region(s) is to ensure that we keep the buckets in the nearest region
  // and keep the option to enable turbo replication if required.
  custom_placement_config {
    data_locations = var.bucket_regions
  }
}

resource "google_storage_bucket_iam_member" "member" {
  bucket     = google_storage_bucket.clickhouse_data.name
  role       = "roles/storage.objectAdmin"
  member     = google_service_account.service_account.member
  depends_on = [google_storage_bucket.clickhouse_data, google_service_account.service_account]
}
