package main

import (
	"context"
	"crypto/tls"
	"encoding/gob"
	"flag"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"path"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	redisprometheus "github.com/redis/go-redis/extra/redisprometheus/v9"
	redis "github.com/redis/go-redis/v9"
	log "github.com/sirupsen/logrus"
	"go.uber.org/zap/zapcore"
	"golang.org/x/oauth2"

	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	"sigs.k8s.io/controller-runtime/pkg/metrics"

	"gitlab.com/gitlab-org/opstrace/opstrace/gatekeeper/gatekeeper"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	apis "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api"

	_ "go.uber.org/automaxprocs"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.
	_ "k8s.io/client-go/plugin/pkg/client/auth"

	k8sruntime "k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"

	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

var (
	scheme      = k8sruntime.NewScheme()
	setupLog    = ctrl.Log.WithName("setup")
	metricsAddr string
	probeAddr   string
	port        = common.GetEnv("PORT", ":3001")
	// redis addr is typically sentinel address (USE_REDIS_SENTINEL).
	redisAddr     = common.GetEnv("REDIS_ADDRESS", "127.0.0.1:6379")
	redisPassword = common.GetEnv("REDIS_PASSWORD", "")
	// redis HA is the default via sentinel client.
	useRedisSentinel            = getEnvBool("USE_REDIS_SENTINEL", true)
	useSecureCookie             = getEnvBool("USE_SECURE_COOKIE", false)
	cookieName                  = common.GetEnv("COOKIE_NAME", constants.SessionCookieName)
	cookieSecret                = common.GetEnv("COOKIE_SECRET", "")
	oauthClientID               = common.GetEnv("GITLAB_OAUTH_CLIENT_ID", "")
	oauthClientSecret           = common.GetEnv("GITLAB_OAUTH_CLIENT_SECRET", "")
	serverDomain                = common.GetEnv("DOMAIN", "http://localhost:3001")
	gitlabAddr                  = common.GetEnv("GITLAB_INSTANCE_URL", "https://gitlab.com")
	gitlabInternalEndpointToken = common.GetEnv("GITLAB_INTERNAL_ENDPOINT_TOKEN", "")
	redisConnectionPoolSize     = getEnvInt("REDIS_CONNECTION_POOL_SIZE", 100)
)

func assignOpts() {
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")

	opts := zap.Options{
		Development: true,
		// ISO8601TimeEncoder serializes a time.Time to an ISO8601-formatted
		// string with millisecond precision.
		TimeEncoder: zapcore.ISO8601TimeEncoder,
	}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()

	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))
}

//nolint:funlen
func main() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))
	utilruntime.Must(opstracev1alpha1.AddToScheme(scheme))

	assignOpts()

	if gitlabInternalEndpointToken == "" {
		log.Fatal("GITLAB_INTERNAL_ENDPOINT_TOKEN is a required parameter")
	}

	logLevel, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		logLevel = log.InfoLevel
	}
	log.SetLevel(logLevel)

	// avoid [Error] gob: type not registered for interface: oauth2.Token
	gob.Register(oauth2.Token{})

	// Setup prometheus metrics:
	registry := metrics.Registry
	metricsHandler := promhttp.HandlerFor(registry, promhttp.HandlerOpts{
		ErrorHandling:     promhttp.HTTPErrorOnError,
		EnableOpenMetrics: true,
	})
	routerMetrics := http.NewServeMux()
	routerMetrics.Handle("/metrics", metricsHandler)

	routerMain := gin.Default()
	routerMain.Use(gatekeeper.ErrorLogger())
	routerMain.Static("/assets", "./assets")
	// For now, disable all proxies for X-Forwarded-For client IP purposes.
	// This affects context.ClientIP(), which we aren't using yet anyway.
	// See also https://pkg.go.dev/github.com/gin-gonic/gin#readme-don-t-trust-all-proxies
	err = routerMain.SetTrustedProxies(nil)
	if err != nil {
		log.WithError(err).Fatal("failed to set trusted proxies")
	}
	routerMain.LoadHTMLGlob("templates/*.gohtml")

	// Parse gitlab address and set up the internal API endpoint
	internalEndpointAddr, err := url.Parse(gitlabAddr)
	if err != nil {
		log.WithError(err).Fatal("invalid gitlab addr")
	}
	internalEndpointAddr.Path = path.Join(internalEndpointAddr.Path, constants.GitlabInternalErrorTrackingEndpoint)

	namespace, err := GetWatchNamespace()
	if err != nil {
		log.Error(err, "failed to get namespace for watching/creating GitLabNamespace CRs")
		os.Exit(1)
	}

	redisClient := getRedis(registry)

	// This client is a split client that reads objects from the cache and writes to the apiserver.
	// Keeping a cache that is automatically updated objects we care about (GitLabNamespace) reduces
	// load on the apiserver and provides realtime event updates
	k8sClient := StartControllerManager(namespace)

	routerMain.Use(gatekeeper.Config(&gatekeeper.ConfigOptions{
		Port:                        port,
		UseSecureCookie:             useSecureCookie,
		CookieName:                  cookieName,
		CookieSecret:                cookieSecret,
		OauthClientID:               oauthClientID,
		OauthClientSecret:           oauthClientSecret,
		ServerDomain:                serverDomain,
		GitlabAddr:                  gitlabAddr,
		GitlabInternalEndpointAddr:  internalEndpointAddr.String(),
		GitlabInternalEndpointToken: gitlabInternalEndpointToken,
		K8sClient:                   k8sClient,
		Namespace:                   namespace,
	}))

	routerMain.Use(gatekeeper.RouteMetrics(registry))

	// Setup our session middleware to use Redis for session persistence
	routerMain.Use(gatekeeper.Session(redisClient, &gatekeeper.SessionOptions{
		CookieName:      cookieName,
		CookieSecret:    cookieSecret,
		UseSecureCookie: useSecureCookie,
		Registry:        registry,
	})...)
	// Configure auth-metrics and make it available to the auth controllers
	routerMain.Use(gatekeeper.AuthMetrics(registry))
	// Configure OAuth2 and make it available to the auth controllers
	routerMain.Use(gatekeeper.OAuth2(&gatekeeper.AuthOptions{
		ClientID:     oauthClientID,
		ClientSecret: oauthClientSecret,
		ServerDomain: serverDomain,
		GitlabAddr:   gitlabAddr,
	}))
	// Configure cache client and make them available
	// to downstream handlers
	routerMain.Use(gatekeeper.Cache(redisClient, registry))

	gatekeeper.SetRoutes(routerMain)

	// If the request got here, it means that the Gin server along with the
	// middlewares are up. We use this information to let k8s know that we are
	// ready to serve traffic.
	routerMain.GET("/readyz", func(ctx *gin.Context) {
		sts := redisClient.Ping(ctx)
		if err := sts.Err(); err != nil {
			log.WithError(err).Error("readyz redis ping")
			ctx.String(500, "Redis not available")
			return
		}
		ctx.String(200, "Success")
	})

	log.Infof("log level: %s", log.GetLevel())
	log.Infof("starting HTTP server on %s", port)

	srvRouter := &http.Server{
		Addr:    port,
		Handler: routerMain,
		// NOTE(prozlach): Arbitrary amount of time, may need tunning.
		ReadHeaderTimeout: 10 * time.Second,
	}
	srvMetrics := &http.Server{
		Addr:    metricsAddr,
		Handler: routerMetrics,
		// NOTE(prozlach): Arbitrary amount of time, may need tunning.
		ReadHeaderTimeout: 10 * time.Second,
	}

	serveWithGracefulShutdown(srvRouter, srvMetrics)
}

func StartControllerManager(namespace string) client.Client {
	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:                 scheme,
		Port:                   9443,
		HealthProbeBindAddress: probeAddr,
		// we already have a metrics server
		MetricsBindAddress: "0",
		LeaderElection:     false,
		LeaderElectionID:   "",
	})
	if err != nil {
		setupLog.Error(err, "unable to create manager")
		os.Exit(1)
	}

	log.Info("Registering Scheme")

	if err := apis.AddToScheme(mgr.GetScheme()); err != nil {
		log.Error(err, "")
		os.Exit(1)
	}

	log.Info("Starting the controller manager")

	if err = (&gatekeeper.NamespaceWatcher{
		Client:    mgr.GetClient(),
		Scheme:    mgr.GetScheme(),
		Log:       ctrl.Log.WithName("NamespaceWatcher"),
		Namespace: namespace,
		/* #nosec G402 */
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Cluster")
		os.Exit(1)
	}

	// /healthz only checks if pod is up and running, and not if e.g. Redis
	// Sentinel is operational. For that we use /readyz endpoint in Gin server
	// itself.
	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}

	setupLog.Info("starting manager")
	go func() {
		if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
			setupLog.Error(err, "problem running manager")
			os.Exit(1)
		}
	}()

	return mgr.GetClient()
}

func serveWithGracefulShutdown(srvs ...*http.Server) {
	for _, srv := range srvs {
		srv := srv
		go func() {
			// service connections
			if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
				log.Fatalf("failed to start server: %s\n", err)
			}
		}()
	}

	// Wait for interrupt signal to gracefully shutdown the server
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Info("shutting down gracefully")

	// Allow 10 seconds to flush existing connections
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	wg := sync.WaitGroup{}
	wg.Add(len(srvs))
	for _, srv := range srvs {
		srv := srv
		go func() {
			if err := srv.Shutdown(ctx); err != nil {
				log.Fatal("server shutdown error:", err)
			}
			wg.Done()
		}()
	}
	wg.Wait()

	log.Info("server exiting")
}

func getEnvBool(key string, fallback bool) bool {
	v := os.Getenv(key)
	if len(v) == 0 {
		return fallback
	}
	bv, err := strconv.ParseBool(v)
	if err != nil {
		log.Fatalf("failed to convert env var %s into bool: %v", key, err)
	}
	return bv
}

func getEnvInt(key string, fallback int) int {
	v := os.Getenv(key)
	if len(v) == 0 {
		return fallback
	}
	iv, err := strconv.Atoi(v)
	if err != nil {
		log.Fatalf("failed to convert env var %s into bool: %v", key, err)
	}
	return iv
}

func getRedis(registry metrics.RegistererGatherer) *redis.Client {
	var redisClient *redis.Client
	if useRedisSentinel {
		redisClient = redis.NewFailoverClient(&redis.FailoverOptions{
			MasterName:    "mymaster",
			SentinelAddrs: []string{redisAddr},
			Password:      redisPassword,
			DB:            0, // use default DB
			PoolSize:      redisConnectionPoolSize,
		})
	} else {
		redisClient = redis.NewClient(&redis.Options{
			Addr:     redisAddr,
			Password: redisPassword,
			DB:       0,
			PoolSize: redisConnectionPoolSize,
		})
	}
	registry.MustRegister(redisprometheus.NewCollector("redis", "", redisClient))

	return redisClient
}

// GetWatchNamespace returns the Namespace the operator should be watching for changes.
func GetWatchNamespace() (string, error) {
	// WatchNamespaceEnvVar is the constant for env variable NAMESPACE
	// which specifies the Namespace to watch and create resource in.
	// An empty value means the operator is running with cluster scope.
	var watchNamespaceEnvVar = "NAMESPACE"

	ns, found := os.LookupEnv(watchNamespaceEnvVar)
	if !found {
		return "", fmt.Errorf("%s must be set", watchNamespaceEnvVar)
	}
	return ns, nil
}
