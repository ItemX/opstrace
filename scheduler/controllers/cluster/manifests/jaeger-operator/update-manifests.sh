#!/usr/bin/env bash

set -eou pipefail

# https://github.com/jaegertracing/jaeger-operator/releases
VER=v1.45.0
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
README_DIR=$(realpath $SCRIPT_DIR)

# We split the bundle in order to make it possible for tenant ang group tests
# to load crds into envtest.
curl -sSfL \
    https://github.com/jaegertracing/jaeger-operator/releases/download/${VER}/jaeger-operator.yaml \
    -o ${SCRIPT_DIR}/bundle.yaml.tmp
yq e '. | select(.kind == "CustomResourceDefinition" and .metadata.name == "jaegers.jaegertracing.io" )' ${SCRIPT_DIR}/bundle.yaml.tmp > ${SCRIPT_DIR}/jaeger.crd.yaml
yq e '. | select(.kind != "CustomResourceDefinition" or .metadata.name != "jaegers.jaegertracing.io" )' ${SCRIPT_DIR}/bundle.yaml.tmp > ${SCRIPT_DIR}/bundle.yaml
rm ${SCRIPT_DIR}/bundle.yaml.tmp

echo "Please make sure to read the README.md file in the $README_DIR directory as well."
