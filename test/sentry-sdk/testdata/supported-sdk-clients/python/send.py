#!/usr/bin/env python
import sentry_sdk, os, sys, certifi
from sentry_sdk import HttpTransport
from sentry_sdk import Hub
from sentry_sdk.sessions import auto_session_tracking
import time

class InsecureHttpTransport(HttpTransport):
    # INSECURE: Override this to only disable cert verification for self hosted cases
    def _get_pool_options(self, ca_certs):
        # type: (Optional[Any]) -> Dict[str, Any]
        cert_reqs = "CERT_REQUIRED"
        insecure_ok = os.getenv("INSECURE_OK", False).lower() == "true"
        if insecure_ok is True:
            cert_reqs = "CERT_NONE"
        return {
            "num_pools": 2,
            "cert_reqs": cert_reqs,
            "ca_certs": ca_certs or certifi.where(),
        }


def init_sentry(dsn: str) -> None:
    sentry_sdk.init(
        dsn=dsn,
        traces_sample_rate=1.0,
        transport=InsecureHttpTransport,
        debug=True,
        release="v1.2.3",
        environment="dev",
        auto_session_tracking=True
    )


def failing_function() -> None:
    raise Exception("An exception")

if __name__ == "__main__":
    dsn = os.getenv("SENTRY_DSN")
    if dsn is None:
        print("empty dsn; exiting")
        sys.exit(1)

    init_sentry(dsn)
    
    sentry_sdk.capture_message("Testing capture_message in python")
    
    hub = Hub.current
    hub.start_session()

    try:
        with hub.configure_scope() as scope:
            scope.set_user({"id": "42"})
            raise Exception("all is wrong")
    except Exception:
        hub.capture_exception()
    # time.sleep(100)
    hub.end_session()
    hub.flush()
