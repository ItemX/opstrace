package e2e

import (
	"fmt"

	clickhouseclient "github.com/ClickHouse/clickhouse-go/v2"
	"github.com/anthhub/forwarder"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
	"k8s.io/utils/pointer"
)

var _ = Describe("ClickHouse cluster", func() {
	var clickhouseCluster *clickhousev1alpha1.ClickHouse
	beforeEachCreateCluster(&clickhouseCluster, clusterConfig{
		replicas: pointer.Int32(2),
	})

	Context("with 2 replicas", func() {

		BeforeEach(func(ctx SpecContext) {
			By("assert cluster readiness")
			assertClusterReady(ctx, clickhouseCluster)
			assertClusterRunning(ctx, clickhouseCluster)
		})

		It("should allow connection to a replica through named service", func(ctx SpecContext) {
			By("port-forward load balanced CH service")

			ports, close := testutils.PortForward(restConfig, &forwarder.Option{
				RemotePort:  9000,
				ServiceName: clickhouseCluster.Name,
			}, Default)
			defer close()

			By("ping the database with the Golang client")
			db := clickhouseclient.OpenDB(&clickhouseclient.Options{
				Addr: []string{fmt.Sprintf("0.0.0.0:%d", ports[0].Local)},
			})
			Expect(db.Ping()).To(Succeed())
		})
	})
})
