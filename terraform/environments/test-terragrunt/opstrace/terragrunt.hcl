terraform {
  source = "${get_terragrunt_dir()}/../../../..//terraform/modules/opstrace"
}

include {
  path = find_in_parent_folders()
}

locals {
  common_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
}

dependency "gitlab" {
  config_path  = "../gitlab"
  skip_outputs = true
}

dependency "gke" {
  config_path = "../gke"
  mock_outputs = {
    kubernetes_cluster_name = "mock"
    kubeconfig_path         = find_in_parent_folders(".kubeconfig_mock")
    global_labels           = {}
  }
  mock_outputs_allowed_terraform_commands = ["init", "validate", "validate-inputs", "plan"]
}

dependency "sql" {
  config_path = "../sql"
  mock_outputs = {
    postgres_dsn_endpoint = "mock"
  }
  mock_outputs_allowed_terraform_commands = ["init", "validate", "validate-inputs", "plan"]
}

dependencies {
  paths = ["../gke", "../sql"]
}

inputs = {
  ch_nodepool_machine_type = "n1-standard-2"
  project_id               = local.common_vars.inputs.project_id
  gke_cluster_name         = dependency.gke.outputs.kubernetes_cluster_name
  zone                     = local.common_vars.inputs.location
  postgres_dsn_endpoint    = dependency.sql.outputs.postgres_dsn_endpoint
  global_labels = {
    gl_realm              = local.common_vars.inputs.gl_realm
    gl_env_type           = local.common_vars.inputs.gl_env_type
    gl_env_name           = local.common_vars.inputs.gl_env_name
    gl_owner_email_handle = local.common_vars.inputs.gl_owner_email_handle
    gl_dept               = local.common_vars.inputs.gl_dept
    gl_dept_group         = local.common_vars.inputs.gl_dept_group
  }
}