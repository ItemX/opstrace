# Debug tenant-operator

This document is created to help people debug the tenant-operator and it's Custom Resources (CR).

First of all read the documentation, they are not perfect but they will help you allong in most cases.

## Argus instance

After you have setup your argus instance it should at least generate a kubernetes deployment, but it can also generate more depending on how you have configured your argus instance.

If that is not the case you need to debug it.

Start by checking the events and status field of the the Tenant CR.

```shell
$ kubectl describe tenant example-tenant

Status:
  Message:                Ingress.extensions "argus-ingress" is invalid: spec.rules[0].http.paths[0].pathType: Required value: pathType must be specified
  Phase:                  failing
  Previous Service Name:  argus-service
Events:
  Type     Reason           Age               From              Message
  ----     ------           ----              ----              -------
  Warning  ProcessingError  9s (x4 over 27s)  Dashboard  Ingress.extensions "argus-ingress" is invalid: spec.rules[0].http.paths[0].pathType: Required value: pathType must be specified
```

`kubectl get tenant example-tenant -o yaml` will also give you the status output.

For example in this case my Tenant CR is missing the definition of `ingress.pathType`

```.yaml
spec:
  ingress:
    enabled: True
    pathType: Prefix
```

In general if you are having issues setting up your argus instance simply your Tenant CR.
First try [examples/Tenant.yaml](examples/Tenant.yaml) and get that
argus instance up and running.
When that is done add small config changes to start build the config you want.

## Argus Dashboards

First check you are creating a dashboard in the same namespace as the tenant-operator

Another good place to see if the dashboard have been applied to the argus dashboard
is to check the kubernetes events.

```shell
$ kubectl get events

2d16h       Warning   ProcessingError                dashboard/simple-dashboard                           Get "http://admin:***@argus-service.default.svc.cluster.local:3000/api/folders": dial tcp 10.96.171.19:3000: connect: connection refused
2d16h       Normal    Success                        dashboard/simple-dashboard                           dashboard default/simple-dashboard successfully submitted
```

For example the events that you can see above the argus Dashboard CRs was most likley created before
the Tenant CR was up and running and thus coulden't answer the tenant operator when it tried
to apply the dashboard.

## Operator logs

Check the logs from the operator and see what you can find.

Depending on how you have setup the operator you should be able to get the logs running:

```shell
kubectl logs -l control-plane=controller-manager -c manager
```

## Bugs

If you still are unable to deploy the argus instance that you want and you think it's a bug please create an issue.

When creating the issue please add as much information as possible from this debug documentation.
It will make it easier for us to debug the issues faster.
