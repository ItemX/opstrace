package errortracking

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
)

func TestGetSentryDataItemManager(t *testing.T) {
	assert := assert.New(t)

	t.Run("Gets queue handler for error data item ", func(t *testing.T) {
		payload, err := os.ReadFile("testdata/exceptions/python_event.json")
		assert.Nil(err)
		e, err := types.NewEventFrom(payload)
		assert.Nil(err)
		r, err := getQueueItemHandler(types.EventType, e)
		assert.Nil(err)
		assert.Equal("*errortracking.EventErrorHandler", reflect.TypeOf(r).String())
	})

	t.Run("Gets queue handler for message data item ", func(t *testing.T) {
		r, err := getQueueItemHandler(types.EventType, &types.Event{Message: "some message"})
		assert.Nil(err)
		assert.Equal("*errortracking.EventMessageHandler", reflect.TypeOf(r).String())
	})

	t.Run("Gets queue handler for session data item ", func(t *testing.T) {
		r, err := getQueueItemHandler(types.SessionType, nil)
		assert.Nil(err)
		assert.Equal("*errortracking.SessionHandler", reflect.TypeOf(r).String())
	})

	t.Run("Fails if type is not supported", func(t *testing.T) {
		r, err := getQueueItemHandler(types.UnsupportedDataItem, nil)
		assert.Nil(r)
		assert.True(errors.Is(err, errUnknownQueuedItemType))
	})
}

func TestParsePayload(t *testing.T) {
	assert := assert.New(t)
	t.Run("Should fail if argument is not an array of bytes", func(t *testing.T) {
		item, err := parsePayload(3)
		assert.Nil(item)
		assert.Equal(fmt.Errorf("received item is not a byte slice, cannot unmarshal"), err)
	})

	t.Run("Should fail if unmarshal fails", func(t *testing.T) {
		b, err := json.Marshal([]byte{1, 2, 3})
		assert.Nil(err)
		item, err := parsePayload(b)
		assert.Nil(item)
		assert.True(strings.Contains(err.Error(), "unmarshalling received item"))
	})

	t.Run("Should succeed", func(t *testing.T) {
		expectedItem := &QueueItem{
			Type:      types.EventType,
			Payload:   []byte("someeventdata"),
			ProjectID: uint64(1234),
		}
		b, err := json.Marshal(expectedItem)
		assert.Nil(err)

		item, err := parsePayload(b)
		assert.Nil(err)
		assert.Equal(expectedItem, item)
	})
}

func TestParseQueueItem(t *testing.T) {
	assert := assert.New(t)

	t.Run("Should parse as specified type a valid, queued payload", func(t *testing.T) {
		payload, err := os.ReadFile("testdata/exceptions/ruby_event.json")
		assert.Nil(err)

		itemType, item, err := parseQueueItem(&QueueItem{
			Type:      types.EventType,
			Payload:   payload,
			ProjectID: uint64(1234),
		})
		assert.Nil(err)

		assert.Equal(itemType, types.EventType)
		_, ok := item.(*types.Event)
		assert.True(ok)
	})

	t.Run("Should fail for unknown type", func(t *testing.T) {
		itemType, item, err := parseQueueItem(&QueueItem{
			Type:      types.UnsupportedDataItem,
			Payload:   []byte{},
			ProjectID: uint64(1234),
		})
		assert.Equal(itemType, types.UnsupportedDataItem)
		assert.Nil(item)
		assert.Equal(fmt.Errorf("cannot process queued data item"), err)
	})
}
