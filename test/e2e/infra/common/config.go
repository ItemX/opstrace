package common

import (
	"fmt"

	"github.com/gruntwork-io/terratest/modules/logger"
	"github.com/gruntwork-io/terratest/modules/testing"
	. "github.com/onsi/ginkgo/v2"
)

// Some shared environment variable names for infra providers.
const (
	TestEnvSchedulerImage = "TEST_SCHEDULER_IMAGE"
	TestEnvGOUIImage      = "TEST_GOUI_IMAGE"
	TestEnvGitLabImage    = "TEST_GITLAB_IMAGE"
)

// Configuration is shared config between the different infra providers.
type Configuration struct {
	// GOBHost is the host of the GOB instance
	GOBHost string
	// GitLabHost is the host of the GitLab instance
	GitLabHost string
	// InstanceName is the name of the instance
	InstanceName string
	// GitLabImage is the image used for GitLab
	GitLabImage string
	// SchedulerImage is the image used for the scheduler
	SchedulerImage string
	// GOUIImage is the image used for the GOUI.
	GOUIImage string
	// GitLabAdminToken is the admin token for the GitLab instance.
	GitLabAdminToken string
}

type ginkgoLogger struct{}

func (ginkgoLogger) Logf(t testing.TestingT, format string, args ...interface{}) {
	logger.DoLog(t, 3, GinkgoWriter, fmt.Sprintf(format, args...))
}

var GinkgoLogger = logger.New(ginkgoLogger{})
