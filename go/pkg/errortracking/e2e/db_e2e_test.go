package e2e

import (
	"context"
	"fmt"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking"
)

const (
	ClickHousePort     = "9000/tcp"
	ClickhouseHTTPPort = "8123/tcp"
	clickHouseImage    = "clickhouse/clickhouse-server:23.2.6.34-alpine"
)

func CreateClickHouseContainer(ctx context.Context, configPath string) (testcontainers.Container, error) {
	var mounts testcontainers.ContainerMounts
	if configPath != "" {
		mounts = testcontainers.Mounts(testcontainers.BindMount(configPath, "/etc/clickhouse-server/config.d/testconf.xml"))
	}
	chReq := testcontainers.ContainerRequest{
		Image:        clickHouseImage,
		ExposedPorts: []string{ClickHousePort, ClickhouseHTTPPort},
		WaitingFor:   wait.ForHTTP("/").WithPort(ClickhouseHTTPPort),
		Mounts:       mounts,
	}
	return testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: chReq,
		Started:          true,
	})
}

func getClickHouseEndpoint(t *testing.T) string {
	t.Helper()

	workingDir, err := os.Getwd()
	require.NoError(t, err)
	ctx := context.Background()
	// Minimal additional configuration (config.d) to enable cluster mode
	replconf := path.Join(workingDir, "clickhouse-replicated.xml")
	chContainer, err := CreateClickHouseContainer(ctx, replconf)
	require.NoError(t, err)

	t.Cleanup(func() {
		if err := chContainer.Terminate(ctx); err != nil {
			t.Fatalf("failed to terminate container: %s", err)
		}
	})

	port, err := chContainer.MappedPort(ctx, "9000/tcp")
	require.NoError(t, err)
	host, err := chContainer.Host(ctx)
	require.NoError(t, err)
	endpoint := fmt.Sprintf("clickhouse://%s:%s", host, port.Port())
	t.Log(endpoint)

	return endpoint
}

func TestDBMigrationsE2E(t *testing.T) {
	endpoint := getClickHouseEndpoint(t)
	_, err := errortracking.NewDB(endpoint, &errortracking.DatabaseOptions{
		UseCompression: true,
	})
	require.NoError(t, err)
}

func TestDBMigrationsMissingE2E(t *testing.T) {
	endpoint := getClickHouseEndpoint(t)
	_, err := errortracking.NewDB(endpoint, &errortracking.DatabaseOptions{
		UseCompression: true,
	})
	require.NoError(t, err)

	// GCS specific migrations should be older than the general migrations.
	// This should verify that they are applied correctly.
	_, err = errortracking.NewDB(endpoint, &errortracking.DatabaseOptions{
		UseCompression:   true,
		UseRemoteStorage: "gcp",
	})
	require.NoError(t, err)

	// Re-run the migrations to make sure that we don't re-apply the migrations
	_, err = errortracking.NewDB(endpoint, &errortracking.DatabaseOptions{
		UseCompression:   true,
		UseRemoteStorage: "gcp",
	})
	require.NoError(t, err)
}
