package gatekeeper_test

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"path"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/opstrace/opstrace/gatekeeper/gatekeeper"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"

	"github.com/gin-gonic/gin"
)

type Pair struct {
	Key   string
	Value string
}

var _ = Context("Errortracking API authentication", func() {
	var gitlabInternalEndpointToken = "secret"
	var mockGitlab *httptest.Server
	var router *gin.Engine

	var failHandler = func(w http.ResponseWriter, r *http.Request) {
		GinkgoHelper()

		Fail("the gitlab internal endpoint should not have been called")
	}

	var successHandler = func(w http.ResponseWriter, r *http.Request) {
		GinkgoHelper()

		secret := r.Header.Get(constants.GitlabErrorTrackingTokenHeader)
		Expect(gitlabInternalEndpointToken).To(Equal(secret), "invalid gitlab shared secret")
		Expect(r.Method).To(Equal(http.MethodPost), "invalid request method")

		w.WriteHeader(200)
		w.Write([]byte(`{"enabled":true}`))
	}

	BeforeEach(func() {
		gin.SetMode(gin.TestMode)
		gin.DefaultWriter = GinkgoWriter

		mockGitlab = httptest.NewServer(http.NewServeMux())
		DeferCleanup(func() {
			mockGitlab.Close()
		})

		// Parse gitlab address and set up the internal API endpoint
		internalEndpointAddr, err := url.Parse(mockGitlab.URL)
		Expect(err).ToNot(HaveOccurred())
		internalEndpointAddr.Path = path.Join(internalEndpointAddr.Path, constants.GitlabInternalErrorTrackingEndpoint)

		router = gin.Default()
		router.Use(gatekeeper.Config(&gatekeeper.ConfigOptions{
			GitlabAddr:                  mockGitlab.URL,
			GitlabInternalEndpointAddr:  internalEndpointAddr.String(),
			GitlabInternalEndpointToken: gitlabInternalEndpointToken,
		}))
		router.Use(func(ctx *gin.Context) {
			ctx.Set(gatekeeper.CacheClientKey, NewMockRedis())
		})
		gatekeeper.SetRoutes(router)
	})

	DescribeTable("ingestion path using Sentry DSN",
		func(headers []Pair, expected int, gitlabInternalEndpointHandler http.HandlerFunc) {
			w := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "/v1/error_tracking/auth", nil)
			Expect(err).ToNot(HaveOccurred())

			// add headers to test case request
			for _, h := range headers {
				req.Header.Set(h.Key, h.Value)
			}
			req.Header.Set("X-Original-Method", http.MethodPost)

			// set up mock gitlab internal endpoint handler for this test case
			mux := http.NewServeMux()
			mux.HandleFunc(constants.GitlabInternalErrorTrackingEndpoint, gitlabInternalEndpointHandler)
			mockGitlab.Config.Handler = mux

			router.ServeHTTP(w, req)
			Expect(expected).To(Equal(w.Code))
		},
		Entry(
			"should fail with no header and no param set",
			[]Pair{},
			http.StatusUnauthorized,
			failHandler,
		),
		Entry(
			"should fail with random headers and params",
			[]Pair{{Key: "foo", Value: "bar"}},
			http.StatusUnauthorized,
			failHandler,
		),
		Entry(
			"should fail with invalid header",
			[]Pair{{Key: "X-Sentry-Auth", Value: "foo=bar"}},
			http.StatusUnauthorized,
			failHandler,
		),
		Entry("should succeed with valid header",
			[]Pair{
				{Key: "X-Sentry-Auth", Value: "sentry_key=glsec_randomword"},
				{Key: "X-Original-Url", Value: "/errortracking/api/v1/projects/api/123/store?sentry_key=gl_randomword"},
			},
			http.StatusOK,
			successHandler,
		),
		Entry("should succeed with valid header, per-group deployment",
			[]Pair{
				{Key: "X-Sentry-Auth", Value: "sentry_key=glsec_randomword"},
				{Key: "X-Original-Url", Value: "/v1/errortracking/123/projects/api/123/store?sentry_key=gl_randomword"},
			},
			http.StatusOK,
			successHandler,
		),
		Entry("should succeed with valid header plus other keys",
			[]Pair{
				{Key: "X-Sentry-Auth", Value: "sentry_verion=7,sentry_key=glsec_randomword123456789,sentry_timestamp=0"},
				{Key: "X-Original-Url", Value: "/errortracking/api/v1/projects/api/123/envelope?sentry_key=gl_randomword"},
			},
			http.StatusOK,
			successHandler,
		),
		Entry("should succeed with valid header plus other keys, per-group deployment",
			[]Pair{
				{Key: "X-Sentry-Auth", Value: "sentry_verion=7,sentry_key=glsec_randomword123456789,sentry_timestamp=0"},
				{Key: "X-Original-Url", Value: "/v1/errortracking/123/projects/api/123/envelope?sentry_key=gl_randomword"},
			},
			http.StatusOK,
			successHandler,
		),
		Entry("should fail with empty header",
			[]Pair{
				{Key: "X-Sentry-Auth", Value: "sentry_key="},
			},
			http.StatusUnauthorized,
			failHandler,
		),
		Entry("should fail with empty header and other keys",
			[]Pair{
				{Key: "X-Sentry-Auth", Value: "sentry_timestamp=0,sentry_key=,sentry_version=7"},
			},
			http.StatusUnauthorized,
			failHandler,
		),
		Entry("should succeed with valid query param in original url for envelope endpoint",
			[]Pair{
				{Key: "X-Original-Url", Value: "/errortracking/api/v1/projects/api/123/envelope?sentry_key=gl_randomword"},
			},
			http.StatusOK,
			successHandler,
		),
		Entry("should succeed with valid query param in original url for envelope endpoint, per-group deployment",
			[]Pair{
				{Key: "X-Original-Url", Value: "/v1/errortracking/123/projects/api/123/envelope?sentry_key=gl_randomword"},
			},
			http.StatusOK,
			successHandler,
		),
		Entry("should succeed with valid query param in original url for store endpoint",
			[]Pair{
				{Key: "X-Original-Url", Value: "/errortracking/api/v1/projects/api/123/store?sentry_key=gl_randomword"},
			},
			http.StatusOK,
			successHandler,
		),
		Entry("should succeed with valid query param in original url for store endpoint, per-group deployment",
			[]Pair{
				{Key: "X-Original-Url", Value: "/v1/errortracking/123/projects/api/123/store?sentry_key=gl_randomword"},
			},
			http.StatusOK,
			successHandler,
		),
		Entry("should succeed with valid query param in original url",
			[]Pair{
				{
					Key:   "X-Original-Url",
					Value: "/errortracking/api/v1/projects/api/123/store?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			http.StatusOK,
			successHandler,
		),
		Entry("should succeed with valid query param in original url, per-group deployment",
			[]Pair{
				{
					Key:   "X-Original-Url",
					Value: "/v1/errortracking/123/projects/api/123/store?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			http.StatusOK,
			successHandler,
		),
		Entry("should fail with invalid project ID",
			[]Pair{
				{
					Key:   "X-Original-Url",
					Value: "/errortracking/api/v1/projects/api/abc/store/?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			http.StatusUnauthorized,
			failHandler,
		),
		Entry("should fail with invalid group ID, per-group deployment",
			[]Pair{
				{
					Key:   "X-Original-Url",
					Value: "/v1/errortracking/abc/projects/api/123/store/?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			http.StatusUnauthorized,
			failHandler,
		),
		Entry("should fail with invalid project ID, per-group deployment",
			[]Pair{
				{
					Key:   "X-Original-Url",
					Value: "/v1/errortracking/123/projects/api/abc/store/?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			http.StatusUnauthorized,
			failHandler,
		),
		Entry("should fail with empty key in original url",
			[]Pair{
				{Key: "X-Original-Url", Value: "/errortracking/api/v1/projects/api/123/store/?sentry_key="},
			},
			http.StatusUnauthorized,
			failHandler,
		),
		Entry("should fail with empty key in original url, per-group deployment",
			[]Pair{
				{Key: "X-Original-Url", Value: "/v1/errortracking/123/projects/api/123/store/?sentry_key="},
			},
			http.StatusUnauthorized,
			failHandler,
		),
		Entry("should fail with empty key in original url plus other keys",
			[]Pair{
				{
					Key:   "X-Original-Url",
					Value: "/errortracking/api/v1/projects/api/123/store?sentry_timestamp=0&sentry_key=&sentry_version=7"},
			},
			http.StatusUnauthorized,
			failHandler,
		),
		Entry("should fail with empty key in original url plus other keys, per-group deployment",
			[]Pair{
				{
					Key:   "X-Original-Url",
					Value: "/v1/errortracking/123/projects/api/123/store?sentry_timestamp=0&sentry_key=&sentry_version=7"},
			},
			http.StatusUnauthorized,
			failHandler,
		),
		Entry("should fail with disabled sentry key",
			[]Pair{
				{
					Key:   "X-Original-Url",
					Value: "/errortracking/api/v1/projects/api/123/store/?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			http.StatusUnauthorized,
			http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				w.Write([]byte(`{"enabled": false`))
			}),
		),
		Entry("should fail with disabled sentry key, per-group deployment",
			[]Pair{
				{
					Key:   "X-Original-Url",
					Value: "/v1/errortracking/123/projects/api/123/store/?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			http.StatusUnauthorized,
			http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				w.Write([]byte(`{"enabled": false`))
			}),
		),
		Entry("should fail with invalid gitlab shared secret",
			[]Pair{
				{
					Key:   "X-Original-Url",
					Value: "/errortracking/api/v1/projects/api/123/store/?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			http.StatusUnauthorized,
			http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				// an invalid shared secret returns a 401 Unauthorized
				w.WriteHeader(http.StatusUnauthorized)
			}),
		),
		Entry("should fail with invalid gitlab shared secret, per-group deployment",
			[]Pair{
				{
					Key:   "X-Original-Url",
					Value: "/v1/errortracking/123/projects/api/123/store/?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			http.StatusUnauthorized,
			http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				// an invalid shared secret returns a 401 Unauthorized
				w.WriteHeader(http.StatusUnauthorized)
			}),
		),
	)
})

var _ = Context("Errortracking API authentication", func() {
	var router *gin.Engine
	var gitlabInternalEndpointToken = "secret"
	var gitlabPHS *testutils.ProgrammableHTTPServer
	var logOutput *bytes.Buffer

	var glatDummyTokens = map[string]struct{}{
		"glpat-rwrwrwrwrwrwrwrwrwrw": {},
		"glpat-rrrrrrrrrrrrrrrrrrrr": {},
		"glpat-wwwwwwwwwwwwwwwwwwww": {},
	}

	BeforeEach(func() {
		gitlabPHS = testutils.NewProgrammableHTTPServer(logger, "")
		configurePHSforGitlabTokenAuthnz(gitlabPHS, glatDummyTokens, testTokenTypeProject)
		gitlabPHS.Start()

		// logs capture
		logOutput = new(bytes.Buffer)
		GinkgoWriter.TeeTo(logOutput)

		gin.SetMode(gin.TestMode)
		gin.DefaultWriter = GinkgoWriter

		router = gin.Default()
		router.Use(gatekeeper.ErrorLogger())
		router.Use(gatekeeper.Config(&gatekeeper.ConfigOptions{
			GitlabInternalEndpointToken: gitlabInternalEndpointToken,
			GitlabAddr:                  gitlabPHS.ServerURL(),
		}))
		router.Use(func(ctx *gin.Context) {
			ctx.Set(gatekeeper.CacheClientKey, NewMockRedis())
		})
		gatekeeper.SetRoutes(router)
	})

	AfterEach(func() {
		gitlabPHS.Stop()
		GinkgoWriter.ClearTeeWriters()
	})

	DescribeTable("read path using Gitlab Shared Secret",
		func(headers []Pair, expected int) {
			w := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "/v1/error_tracking/auth", nil)
			Expect(err).ToNot(HaveOccurred())

			// add headers to test case request
			for _, h := range headers {
				req.Header.Set(h.Key, h.Value)
			}

			// run tests for PUT and GET methods
			for _, method := range []string{http.MethodPut, http.MethodGet} {
				req.Header.Set("X-Original-Method", method)
				router.ServeHTTP(w, req)
				Expect(expected).To(Equal(w.Code))
			}
		},
		Entry(
			"should fail with no auth header set",
			[]Pair{
				{Key: "X-Original-Url", Value: "/v1/errortracking/projects/123/errors"},
			},
			http.StatusUnauthorized,
		),
		Entry(
			"should fail with empty auth header set",
			[]Pair{
				{Key: constants.GitlabErrorTrackingTokenHeader, Value: ""},
				{Key: "X-Original-Url", Value: "/v1/errortracking/123/projects/123/errors"},
			},
			http.StatusUnauthorized,
		),
		Entry(
			"should fail with invalid token",
			[]Pair{
				{Key: constants.GitlabErrorTrackingTokenHeader, Value: "foo"},
				{Key: "X-Original-Url", Value: "/v1/errortracking/123/projects/123/errors"},
			},
			http.StatusUnauthorized,
		),
		Entry(
			"should accept request with valid token",
			[]Pair{
				{Key: constants.GitlabErrorTrackingTokenHeader, Value: gitlabInternalEndpointToken},
				{Key: "X-Original-Url", Value: "/v1/errortracking/123/projects/123/errors"},
			},
			http.StatusOK,
		),
	)

	Context("read path using Gitlab Access Token", func() {
		DescribeTable("should 200 if token is valid - Group",
			func(requestPath string) {
				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					fmt.Sprintf("/v1/error_tracking/auth?request_path=%s", requestPath),
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusOK))

				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/namespaces/13")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/user")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/groups/13/members/all/61")

				// And finally - check if log message is produced
				Expect(logOutput.String()).To(ContainSubstring("HandleGroupAccessAuth decision OK"))
			},
			Entry(nil, "/errortracking/api/v1/api/0/organizations/13/issues/"),
			Entry(nil, "/errortracking/api/v1/api/0/organizations/13/projects/"),
			Entry(nil, "/errortracking/api/v1/api/0/organizations/13/stats_v2"),
		)

		DescribeTable("should 200 if token is valid - Project",
			func(requestPath string) {
				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					fmt.Sprintf("/v1/error_tracking/auth?request_path=%s", requestPath),
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusOK))

				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/user")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/projects/115/members/all/61")

				// And finally - check if log message is produced
				Expect(logOutput.String()).To(ContainSubstring("HandleProjectAccessAuth decision OK"))
			},
			Entry(nil, "/errortracking/api/v1/projects/115/errors"),
			Entry(nil, "/errortracking/api/v1/projects/115/errors/aaa-fingerprint-123"),
			Entry(nil, "/errortracking/api/v1/projects/api/115/store"),
			Entry(nil, "/errortracking/api/v1/projects/api/115/envelope"),
			Entry(nil, "/errortracking/api/v1/projects/115/errors/aaa-fingerprint-123/events"),
			Entry(nil, "/errortracking/api/v1/projects/115/messages"),
			Entry(nil, "/errortracking/api/v1/projects/115"),
		)

		DescribeTable("should 403 if gitlab namespace does not exist - Group",
			func(requestPath string) {
				// Namespace info query
				nsReply := func(r *http.Request) (int, string) {
					if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
						return 404, ``
					}
					return http.StatusForbidden, GitlabReplyUnauthorized
				}
				gitlabPHS.SetResponseFunc(
					http.MethodGet, "/api/v4/namespaces/13",
					nsReply,
				)

				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					fmt.Sprintf("/v1/error_tracking/auth?request_path=%s", requestPath),
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusForbidden))

				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/namespaces/13")
			},
			Entry(nil, "/errortracking/api/v1/api/0/organizations/13/issues/"),
			Entry(nil, "/errortracking/api/v1/api/0/organizations/13/projects/"),
			Entry(nil, "/errortracking/api/v1/api/0/organizations/13/stats_v2"),
		)

		DescribeTable("should 403 if gitlab project does not exist - Project",
			func(requestPath string) {
				// Namespace info query
				httpReply := func(r *http.Request) (int, string) {
					if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
						return 404, ``
					}
					return http.StatusForbidden, GitlabReplyUnauthorized
				}
				gitlabPHS.SetResponseFunc(
					http.MethodGet, "/api/v4/projects/115/members/all/61",
					httpReply,
				)

				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					fmt.Sprintf("/v1/error_tracking/auth?request_path=%s", requestPath),
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusForbidden))

				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/projects/115/members/all/61")
			},
			Entry(nil, "/errortracking/api/v1/projects/115/errors"),
			Entry(nil, "/errortracking/api/v1/projects/115/errors/aaa-fingerprint-123"),
			Entry(nil, "/errortracking/api/v1/projects/api/115/store"),
			Entry(nil, "/errortracking/api/v1/projects/api/115/envelope"),
			Entry(nil, "/errortracking/api/v1/projects/115/errors/aaa-fingerprint-123/events"),
			Entry(nil, "/errortracking/api/v1/projects/115/messages"),
			Entry(nil, "/errortracking/api/v1/projects/115"),
		)

		DescribeTable("should 403 if token group membership is inactive - Group",
			func(requestPath string) {
				membershipReply := func(r *http.Request) (int, string) {
					if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
						return http.StatusOK, `{
			                                "state": "inactive",
			                                "access_level": 30
			                              }`
					}
					return http.StatusForbidden, GitlabReplyUnauthorized
				}
				gitlabPHS.SetResponseFunc(
					http.MethodGet, "/api/v4/groups/13/members/all/61",
					membershipReply,
				)

				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					fmt.Sprintf("/v1/error_tracking/auth?request_path=%s", requestPath),
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusForbidden))

				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/namespaces/13")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/user")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/groups/13/members/all/61")
			},
			Entry(nil, "/errortracking/api/v1/api/0/organizations/13/issues/"),
			Entry(nil, "/errortracking/api/v1/api/0/organizations/13/projects/"),
			Entry(nil, "/errortracking/api/v1/api/0/organizations/13/stats_v2"),
		)

		DescribeTable("should 403 if token project membership is inactive - Project",
			func(requestPath string) {
				projectGroups := func(r *http.Request) (int, string) {
					if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
						return http.StatusOK, `{
                                "state": "inactive",
                                "access_level": 30
                            }`
					}
					return http.StatusForbidden, GitlabReplyUnauthorized
				}
				gitlabPHS.SetResponseFunc(
					http.MethodGet, "/api/v4/projects/115/members/all/61",
					projectGroups,
				)

				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					fmt.Sprintf("/v1/error_tracking/auth?request_path=%s", requestPath),
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusForbidden))

				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/user")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/projects/115/members/all/61")
			},
			Entry(nil, "/errortracking/api/v1/projects/115/errors"),
			Entry(nil, "/errortracking/api/v1/projects/115/errors/aaa-fingerprint-123"),
			Entry(nil, "/errortracking/api/v1/projects/api/115/store"),
			Entry(nil, "/errortracking/api/v1/projects/api/115/envelope"),
			Entry(nil, "/errortracking/api/v1/projects/115/errors/aaa-fingerprint-123/events"),
			Entry(nil, "/errortracking/api/v1/projects/115/messages"),
			Entry(nil, "/errortracking/api/v1/projects/115"),
		)

		DescribeTable("should 403 if token does not have correct scopes - Group",
			func(requestPath string) {
				patSelfReply := func(r *http.Request) (int, string) {
					if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
						return http.StatusOK, GitlabTokenRegistryScopes
					}
					return http.StatusForbidden, GitlabReplyUnauthorized
				}
				gitlabPHS.SetResponseFunc(
					http.MethodGet, "/api/v4/personal_access_tokens/self",
					patSelfReply,
				)

				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					fmt.Sprintf("/v1/error_tracking/auth?request_path=%s", requestPath),
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusForbidden))

				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
			},
			Entry(nil, "/errortracking/api/v1/api/0/organizations/13/issues/"),
			Entry(nil, "/errortracking/api/v1/api/0/organizations/13/projects/"),
			Entry(nil, "/errortracking/api/v1/api/0/organizations/13/stats_v2"),
		)

		DescribeTable("should 403 if token does not have correct scopes - Project",
			func(requestPath string) {
				patSelfReply := func(r *http.Request) (int, string) {
					if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
						return http.StatusOK, GitlabTokenRegistryScopes
					}
					return http.StatusForbidden, GitlabReplyUnauthorized
				}
				gitlabPHS.SetResponseFunc(
					http.MethodGet, "/api/v4/personal_access_tokens/self",
					patSelfReply,
				)

				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					fmt.Sprintf("/v1/error_tracking/auth?request_path=%s", requestPath),
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusForbidden))

				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
			},
			Entry(nil, "/errortracking/api/v1/projects/115/errors"),
			Entry(nil, "/errortracking/api/v1/projects/115/errors/aaa-fingerprint-123"),
			Entry(nil, "/errortracking/api/v1/projects/api/115/store"),
			Entry(nil, "/errortracking/api/v1/projects/api/115/envelope"),
			Entry(nil, "/errortracking/api/v1/projects/115/errors/aaa-fingerprint-123/events"),
			Entry(nil, "/errortracking/api/v1/projects/115/messages"),
			Entry(nil, "/errortracking/api/v1/projects/115"),
		)
	})
})
