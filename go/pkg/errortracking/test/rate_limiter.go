// Code generated by mockery v2.26.1. DO NOT EDIT.

package test

import (
	redis_rate "github.com/go-redis/redis_rate/v10"
	mock "github.com/stretchr/testify/mock"

	types "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
)

// RateLimiterMock is an autogenerated mock type for the RateLimiter type
type RateLimiterMock struct {
	mock.Mock
}

// IsAllowed provides a mock function with given fields: _a0, _a1, _a2
func (_m *RateLimiterMock) IsAllowed(_a0 uint64, _a1 string, _a2 string) (*redis_rate.Result, error) {
	ret := _m.Called(_a0, _a1, _a2)

	var r0 *redis_rate.Result
	var r1 error
	if rf, ok := ret.Get(0).(func(uint64, string, string) (*redis_rate.Result, error)); ok {
		return rf(_a0, _a1, _a2)
	}
	if rf, ok := ret.Get(0).(func(uint64, string, string) *redis_rate.Result); ok {
		r0 = rf(_a0, _a1, _a2)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*redis_rate.Result)
		}
	}

	if rf, ok := ret.Get(1).(func(uint64, string, string) error); ok {
		r1 = rf(_a0, _a1, _a2)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// SetLimits provides a mock function with given fields: _a0
func (_m *RateLimiterMock) SetLimits(_a0 *types.LimitsConfig) {
	_m.Called(_a0)
}

type mockConstructorTestingTNewRateLimiterMock interface {
	mock.TestingT
	Cleanup(func())
}

// NewRateLimiterMock creates a new instance of RateLimiterMock. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewRateLimiterMock(t mockConstructorTestingTNewRateLimiterMock) *RateLimiterMock {
	mock := &RateLimiterMock{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
