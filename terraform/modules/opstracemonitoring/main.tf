# -------------------------------------------------------------------------------------------
# utils - BlackBox-exporter, smoke tests
# -------------------------------------------------------------------------------------------

# BlackBox Exporter secret holding grafana API key used for auth
# for the GOUI probe
resource "kubernetes_secret" "bbe-api-key" {
  metadata {
    name = "bbe-api-key"
  }

  data = {
    apikey = var.blackbox_exporter_api_key
  }
}

resource "kubernetes_secret" "smoketests-errortracking-secret" {
  metadata {
    name = "smoketests-errortracking-secret"
  }

  data = {
    gitlabobservabilityapitoken = var.gitlab_observability_api_token
    grafanaapitoken             = var.blackbox_exporter_api_key
    sentrydsn                   = var.sentry_dsn
  }
}

resource "kubernetes_config_map" "smoketests-errortracking-config" {
  metadata {
    name = "smoketests-errortracking-config"
  }

  data = {
    group_error_tracking_endpoint      = var.group_error_tracking_endpoint
    error_tracking_ingestion_delay_sec = "10"
    log_level                          = "INFO"
    test_period_sec                    = "10"
    retries                            = "15"
  }

}



data "kustomization_build" "alerting" {
  path = "${path.module}/alerting/overlays/${var.environment}"
}

resource "kustomization_resource" "alerting" {
  for_each = data.kustomization_build.alerting.ids
  manifest = data.kustomization_build.alerting.manifests[each.value]
}

data "kustomization_overlay" "blackbox-exporter-manifest" {
  resources = ["${path.module}/blackbox-exporter"]
  patches {

    target {
      kind = "ServiceMonitor"
      name = "gatekeeper-probe"
    }
    patch = <<-EOF
      - op: replace
        path: /spec/endpoints/0/params/target/0
        value: "${var.gatekeeper_probe_url}"
    EOF

  }
}

resource "kustomization_resource" "blackbox-exporter-manifest" {
  for_each = data.kustomization_overlay.blackbox-exporter-manifest.ids
  manifest = data.kustomization_overlay.blackbox-exporter-manifest.manifests[each.value]
  depends_on = [
    kubernetes_secret.bbe-api-key,
  ]
}

data "kustomization_build" "smoketests-manifest" {
  path = "${path.module}/smoketests"
}

resource "kustomization_resource" "smoketests-manifest" {
  for_each = data.kustomization_build.smoketests-manifest.ids
  manifest = data.kustomization_build.smoketests-manifest.manifests[each.value]
  depends_on = [
    kubernetes_secret.smoketests-errortracking-secret,
  ]
}
