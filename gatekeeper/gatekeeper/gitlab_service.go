package gatekeeper

import (
	"context"
	"crypto/rand"
	"crypto/sha256"
	"crypto/tls"
	"errors"
	"fmt"
	"math/big"
	"net/http"
	"sort"
	"time"

	"github.com/gin-gonic/gin"
	cache "github.com/go-redis/cache/v9"
	log "github.com/sirupsen/logrus"
	gitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"golang.org/x/oauth2"
)

type GitLabService struct {
	gitlabClient      *gitlab.Client
	requestContext    *gin.Context
	oauthTokenSource  oauth2.TokenSource
	invalidateSession bool

	// cacheKey is a uniqe string derived from either oauth token or bearer
	// token and is used to find data in cache.
	cacheKey string
}

func calculateSHASum(in string) string {
	h := sha256.New()
	h.Write([]byte(in))
	return fmt.Sprintf("%x", h.Sum(nil))
}

// Returns an instance of GitLabService that uses an http.Client configured
// with the access token. All GET requests against GitLab are cached.
func NewGitLabServiceFromAccessToken(requestCtx *gin.Context, bearerToken string) (*GitLabService, error) {
	serverConfig := GetConfig(requestCtx)
	opts := []gitlab.ClientOptionFunc{
		// Wire up the base URL for the Gitlab instance
		gitlab.WithBaseURL(serverConfig.GitlabAddr),
	}

	if common.LookupEnvOrBool("TLS_SKIP_INSECURE_VERIFY", false) {
		transport := &http.Transport{
			// #nosec
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		httpClient := &http.Client{Transport: transport}
		opts = append(opts,
			// The HTTP Client returned by auth.Client is supposed to automatically
			// refresh the authToken if it has expired
			gitlab.WithHTTPClient(httpClient),
		)
	}

	client, err := gitlab.NewClient(bearerToken, opts...)
	if err != nil {
		return nil, fmt.Errorf("failed to create gitlab client: %w", err)
	}

	return &GitLabService{
		gitlabClient:      client,
		requestContext:    requestCtx,
		invalidateSession: false,
		cacheKey:          calculateSHASum(bearerToken),
	}, nil
}

// Returns an instance of GitLabService that uses an http.Client
// configured with the Oauth token taken from the request context.
// The http.Client will handle token.access_token expiry and request a new
// access_token using token.refresh_token.
// All GET requests against GitLab are cached.
func NewGitLabServiceFromOauthToken(ctx *gin.Context) (*GitLabService, error) {
	auth := GetAuthConfig(ctx)
	token := GetAuthToken(ctx)
	c := context.TODO()

	if common.LookupEnvOrBool("TLS_SKIP_INSECURE_VERIFY", false) {
		t := &http.Transport{
			// #nosec
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		tc := &http.Client{Transport: t}
		c = context.WithValue(c, oauth2.HTTPClient, tc)
	}

	serverConfig := GetConfig(ctx)

	tokenSource := auth.TokenSource(c, token)

	client, err := gitlab.NewOAuthClient(
		token.AccessToken,

		// Wire up the base URL for the Gitlab instance
		gitlab.WithBaseURL(serverConfig.GitlabAddr),

		// The HTTP Client returned by auth.Client is supposed to automatically
		// refresh the authToken if it has expired
		gitlab.WithHTTPClient(oauth2.NewClient(c, tokenSource)),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create gitlab client: %w", err)
	}

	return &GitLabService{
		gitlabClient:      client,
		requestContext:    ctx,
		oauthTokenSource:  tokenSource,
		invalidateSession: false,
		cacheKey:          calculateSHASum(token.AccessToken),
	}, nil
}

// Returns latest issued token. A new token may have been issued
// through a refresh request and we need to store it.
// Returns nil if token is invalid (i.e. issued a refresh request and it failed).
func (s *GitLabService) Token() (*oauth2.Token, error) {
	return s.oauthTokenSource.Token()
}

// TODO: Investigate usage.
func (s *GitLabService) HandleError(err error) error {
	if s.oauthTokenSource == nil {
		return err
	}

	if _, err2 := s.Token(); err2 != nil {
		return ErrUnauthorized
	} else {
		return err
	}
}

// Get the current user.
func (s *GitLabService) CurrentUser() (*gitlab.User, error) {
	key := fmt.Sprintf("currentuser:%s", s.cacheKey)

	var user gitlab.User
	if hit := s.cacheGet(key, &user); hit {
		return &user, nil
	}

	u, _, err := s.gitlabClient.Users.CurrentUser()
	if err != nil {
		return nil, s.HandleError(fmt.Errorf("failed to retrieve currentuser: %w", err))
	}
	s.cacheSet(key, u, 30*time.Second)

	return u, nil
}

// Check if current user can access the namespace by Id.
func (s *GitLabService) CanAccessProject(pid, minAccessLevelKey int) (bool, error) {
	membership, err := s.getProjectMembership(pid)
	if err != nil {
		return false, fmt.Errorf("failed to determine project membership: %w", err)
	}
	res := membership.State == "active" && membership.AccessLevel > gitlab.AccessLevelValue(minAccessLevelKey)
	return res, nil
}

// IsProjectInGroup checks if given project belongs to the given namespace id.
func (s *GitLabService) IsProjectInGroup(pid, gid int) (bool, error) {
	pgs, err := s.ListProjectsGroups(pid)
	if err != nil {
		return false, err
	}
	for _, pg := range pgs {
		if pg.ID == gid {
			return true, nil
		}
	}

	return false, nil
}

// IsProjectInGroup checks if given project is in the user-namespace.
// NOTE(prozlach) The way we do it is a bit hacky. According to the
// documentation:
//
// https://docs.gitlab.com/ee/api/namespaces.html
//
// ```
// A personal namespace, which is based on your username and provided to you when you create your account.
//   - You cannot create subgroups in a personal namespace.
//   - Groups in your namespace do not inherit your namespace permissions and group features.
//   - All the projects you create are under the scope of this namespace.
//
// ```
//
// The way I interpret it is that even if we have groups in the user NS
// (haven't found a way to create them), you will be only to create projects in
// the namespace itself, not it in the group. Creation of subgroups does not
// apply here either, nor the permissions inheritance, so no need to check
// hierarchy.
//
// So the idea is simple - fetch the project and see the namespace it is
// sitting in. If it is the same as the one we are looking for - return true,
// return false otherwise.
func (s *GitLabService) IsProjectInUserNamespace(pid, nsid int) (bool, error) {
	p, err := s.GetProject(pid)
	if err != nil {
		return false, err
	}

	return p.Namespace.ID == nsid, nil
}

// Get Project
func (s *GitLabService) GetProject(pid interface{}) (*gitlab.Project, error) {
	key := fmt.Sprintf("project:%d", pid)

	p := new(gitlab.Project)
	if hit := s.cacheGet(key, p); hit {
		return p, nil
	}

	p, _, err := s.gitlabClient.Projects.GetProject(pid, nil)
	if err != nil {
		return nil, s.HandleError(fmt.Errorf("failed to retrieve project %d: %w", pid, err))
	}
	s.cacheSet(key, p, 30*time.Second)

	return p, nil
}

// Get the current user's group membership by group Id.
func (s *GitLabService) getProjectMembership(pid int) (*gitlab.ProjectMember, error) {
	u, err := s.CurrentUser()
	if err != nil {
		return nil, err
	}

	g, err := s.GetInheritedProjectMember(pid, u.ID)
	if err != nil {
		return nil, s.HandleError(fmt.Errorf("failed to retrieve project membership: %w", err))
	}

	// membership can return nil without an error
	if g == nil {
		return nil, s.HandleError(errors.New("GetInheritedProjectMember returned nil"))
	}

	return g, nil
}

func (s *GitLabService) GetInheritedProjectMember(pid, user int) (*gitlab.ProjectMember, error) {
	key := fmt.Sprintf("projectmember:%d:%d", pid, user)

	var projectMember gitlab.ProjectMember
	if hit := s.cacheGet(key, &projectMember); hit {
		return &projectMember, nil
	}

	u := fmt.Sprintf("projects/%d/members/all/%d", pid, user)

	req, err := s.gitlabClient.NewRequest(http.MethodGet, u, nil, nil)
	if err != nil {
		return nil, err
	}

	pm := new(gitlab.ProjectMember)
	_, err = s.gitlabClient.Do(req, pm)
	if err != nil {
		return nil, err
	}

	s.cacheSet(key, pm, 30*time.Second)

	return pm, err
}

// Check if current user can access the namespace by Id.
func (s *GitLabService) CanAccessNamespace(nid string) (bool, gitlab.AccessLevelValue, error) {
	// A namespace could be a group or a user.
	ns, err := s.GetNamespace(nid)
	if err != nil || ns == nil {
		return false, gitlab.NoPermissions, err
	}

	switch ns.Kind {
	case GroupNamespaceType:
		membership, err := s.getGroupMembership(ns.ID)
		if err != nil {
			return false, gitlab.NoPermissions, err
		}
		return membership.State == "active" && membership.AccessLevel > gitlab.NoPermissions,
			membership.AccessLevel,
			nil
	case UserNamespaceType:
		// User must have at least `Owner` permissions level to see the user
		// namespace.
		return true, gitlab.OwnerPermissions, nil
	default:
		// Don't think there are any other Kinds, so this code path shouldn't
		// execute but good to be safe.
		return false, gitlab.NoPermissions, nil
	}
}

// Get GitLab namespace by Id.
func (s *GitLabService) GetNamespace(nid string) (*GitLabNamespace, error) {
	key := fmt.Sprintf("ns:%s", nid)

	var ns *gitlab.Namespace
	if hit := s.cacheGet(key, &ns); hit {
		return &GitLabNamespace{ns}, nil
	}

	namespace, _, err := s.gitlabClient.Namespaces.GetNamespace(nid)
	if err != nil {
		return nil, s.HandleError(fmt.Errorf("failed to retrieve namespace: %w", err))
	}
	s.cacheSet(key, namespace, 30*time.Second)

	return &GitLabNamespace{namespace}, nil
}

// Get Projects ancestor groups, sorted in descending order, top-level NS first
func (s *GitLabService) ListProjectsGroups(pid interface{}) ([]*gitlab.ProjectGroup, error) {
	key := fmt.Sprintf("projectancestors:%d", pid)

	pgs := make([]*gitlab.ProjectGroup, 0)
	if hit := s.cacheGet(key, &pgs); hit {
		return pgs, nil
	}

	pgs, _, err := s.gitlabClient.Projects.ListProjectsGroups(pid, nil)
	if err != nil {
		return nil, s.HandleError(fmt.Errorf("failed to retrieve project's %d ancestor groups: %w", pid, err))
	}

	// NOTE(prozlach): The documentation does not say if the result of the API
	// call is always sorted. Hence we sort the result so that the top-level NS
	// goes first. The FullPath attribute will be the shortest for the
	// top-level namespace, longest for the namespace in which project resides.
	lessFunc := func(i, j int) bool {
		return len(pgs[i].FullPath) < len(pgs[j].FullPath)
	}
	sort.Slice(pgs, lessFunc)

	s.cacheSet(key, pgs, 30*time.Second)

	return pgs, nil
}

// GetAccessTokenScopes returns the list of scopes assigned to the access token
// that this instance of GitlabService uses.
//
// This method is added here because it doesn't exist in the underlying library
//
// Docs:
// https://docs.gitlab.com/ee/api/personal_access_tokens.html#using-a-request-header
func (s *GitLabService) GetAccessTokenScopes() ([]string, error) {
	key := fmt.Sprintf("tokenscopes:%s", s.cacheKey)

	var scopes struct {
		Scopes []string `json:"scopes"`
	}
	if hit := s.cacheGet(key, &scopes); hit {
		return scopes.Scopes, nil
	}

	req, err := s.gitlabClient.NewRequest(
		http.MethodGet, "/personal_access_tokens/self", nil, nil,
	)
	if err != nil {
		return nil, fmt.Errorf("unable to create request to fetch access token scopes: %w", err)
	}

	_, err = s.gitlabClient.Do(req, &scopes)
	if err != nil {
		return nil, fmt.Errorf("request to fetch access token scopes failed: %w", err)
	}

	s.cacheSet(key, &scopes, 30*time.Second)

	return scopes.Scopes, nil
}

// Get the current user's group membership by group Id.
func (s *GitLabService) getGroupMembership(gid int) (*gitlab.GroupMember, error) {
	u, err := s.CurrentUser()
	if err != nil {
		return nil, err
	}

	g, err := s.GetInheritedGroupMember(gid, u.ID)
	// membership can return nil without an error
	if err != nil || g == nil {
		return nil, s.HandleError(fmt.Errorf("failed to retrieve groupmembership: %w", err))
	}

	return g, nil
}

// GetGroupMember gets a member of a group, including inherited membership.
// This method is added here because it doesn't exist in the underlying library
//
// GitLab API docs:
// https://docs.gitlab.com/ee/api/members.html#get-a-member-of-a-group-or-project-including-inherited-and-invited-members
//
//nolint:lll
func (s *GitLabService) GetInheritedGroupMember(
	gid int, user int,
) (*gitlab.GroupMember, error) {
	key := fmt.Sprintf("groupmember:%d:%d", gid, user)

	var groupMember gitlab.GroupMember
	if hit := s.cacheGet(key, &groupMember); hit {
		return &groupMember, nil
	}

	u := fmt.Sprintf("groups/%d/members/all/%d", gid, user)

	req, err := s.gitlabClient.NewRequest(http.MethodGet, u, nil, nil)
	if err != nil {
		return nil, err
	}

	gm := new(gitlab.GroupMember)
	_, err = s.gitlabClient.Do(req, gm)
	if err != nil {
		return nil, err
	}

	s.cacheSet(key, gm, 30*time.Second)

	return gm, err
}

// Get key from cache and return true if the cache was hit.
func (s *GitLabService) cacheGet(key string, value interface{}) bool {
	c := GetCache(s.requestContext)
	ctx := context.TODO()

	err := c.Get(ctx, key, &value)
	if err == nil || errors.Is(err, cache.ErrCacheMiss) {
		log.Trace(fmt.Sprintf("CacheGet: '%s', err: %v", key, err))
		return err == nil && value != nil
	}
	log.Errorf("cache retrieval failure for key [%s]: %+v", key, err)
	return false
}

// Set key/value in cache with TTL.
func (s *GitLabService) cacheSet(key string, value interface{}, ttl time.Duration) {
	c := GetCache(s.requestContext)
	ctx := context.TODO()

	err := c.Set(&cache.Item{
		Ctx:   ctx,
		Key:   key,
		Value: value,
		TTL:   ttl,
	})
	if err != nil {
		log.Errorf("cache set failure for key [%s]: %+v", key, err)
	}
}

const projectCacheTTL = 1 * time.Minute

// GetGroupProjects return all the projects available under passed Group ID.
func (s *GitLabService) GetGroupProjects(gid interface{}) ([]*gitlab.BasicProject, error) {
	var (
		key          = fmt.Sprintf("group:%s:projects", gid)
		baseProjects []*gitlab.BasicProject
	)

	if hit := s.cacheGet(key, &baseProjects); hit {
		return baseProjects, nil
	}

	// include projects from the subgroups will have a performance cost.
	// See discussion on https://gitlab.com/gitlab-org/opstrace/opstrace/-/merge_requests/1824#note_1189263082
	includeSubGroups := false

	// TODO(Arun): Handle pagination if the number of projects won't fit in a single page.
	projects, _, err := s.gitlabClient.Groups.ListGroupProjects(gid, &gitlab.ListGroupProjectsOptions{
		IncludeSubGroups: &includeSubGroups,
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
		},
	})
	if err != nil {
		return nil, s.HandleError(fmt.Errorf("failed to retrieve group projects: %w", err))
	}

	baseProjects = make([]*gitlab.BasicProject, len(projects))
	for i, proj := range projects {
		baseProjects[i] = &gitlab.BasicProject{
			ID:                proj.ID,
			Name:              proj.Name,
			NameWithNamespace: proj.NameWithNamespace,
			PathWithNamespace: proj.PathWithNamespace,
		}
	}
	jitter, err := rand.Int(rand.Reader, big.NewInt(5000))
	if err != nil {
		return nil, s.HandleError(fmt.Errorf("failed to retrieve random jitter: %w", err))
	}
	ttl := projectCacheTTL + time.Duration(jitter.Int64())*time.Millisecond
	s.cacheSet(key, baseProjects, ttl)
	return baseProjects, nil
}
