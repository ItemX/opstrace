provider "google" {
  project = var.project_id
  region  = var.region
  zone    = var.zone
}

resource "random_id" "db_name_suffix" {
  byte_length = 16
}

resource "google_compute_global_address" "private_ip_address" {
  name          = "google-managed-services-${var.vpc_name}"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  address       = "192.168.64.0"
  prefix_length = 19
  network       = var.vpc_id
}

resource "google_service_networking_connection" "private_vpc_connection" {
  network                 = var.vpc_id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}

# TODO: noticed it created the sql instance with a public ip
resource "google_sql_database_instance" "postgres" {
  depends_on = [
    google_compute_global_address.private_ip_address,
    google_service_networking_connection.private_vpc_connection,
  ]

  name                = "${var.instance_name}-${random_id.db_name_suffix.hex}"
  database_version    = "POSTGRES_11"
  deletion_protection = false

  settings {
    user_labels = {
      opstrace_cluster_name = var.instance_name
    }

    database_flags {
      name  = "max_connections"
      value = var.db_max_connections
    }

    activation_policy = "ALWAYS"
    availability_type = "ZONAL"
    backup_configuration {
      enabled                        = true
      start_time                     = "11:00"
      transaction_log_retention_days = 7

      backup_retention_settings {
        retained_backups = 7
        retention_unit   = "COUNT"
      }
    }

    disk_autoresize = true
    disk_size       = 10
    disk_type       = "PD_SSD"
    ip_configuration {
      private_network = var.vpc_id
      ipv4_enabled    = false
    }

    location_preference {
      zone = var.zone
    }

    pricing_plan = "PER_USE"
    tier         = "db-custom-2-3840"
  }
}

resource "google_sql_database" "opstrace" {
  name     = "opstrace"
  instance = google_sql_database_instance.postgres.name
}

# Generate a random password string for use with database access
resource "random_password" "sql_password" {
  length  = 75
  special = false
}

# The terraform provider does not support setting the root_password:
# https://github.com/hashicorp/terraform-provider-google/issues/10122
# so we define a sql user instead
resource "google_sql_user" "opstrace" {
  project  = var.project_id
  name     = "opstrace"
  instance = google_sql_database_instance.postgres.name
  # TODO: move to IAM roles instead?
  password = random_password.sql_password.result

  # deletion_policy - (Optional) The deletion policy for the user. Setting
  # ABANDON allows the resource to be abandoned rather than deleted. This is
  # useful for Postgres, where users cannot be deleted from the API if they have
  # been granted SQL roles.
  deletion_policy = "ABANDON"
}
