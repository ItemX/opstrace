describe("login", function () {
  it("the home page should redirect for authorisation for non logged-in users", () => {
    cy.request({
      url: "/",
      followRedirect: false,
    }).then((resp) => {
      expect(resp.status).to.eq(302);
      expect(resp.redirectedToUrl).to.eq(
        `https://${Cypress.env("GITLAB_DOMAIN")}/users/sign_in`
      );
    });
  });

  it("successfully logs in", () => {
    cy.loginViaUi(
      Cypress.env("GITLAB_USERNAME"),
      Cypress.env("GITLAB_PASSWORD")
    );
  });
});
