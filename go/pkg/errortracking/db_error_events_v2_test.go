package errortracking

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors_v2"
)

func TestBuildListEventV2Query(t *testing.T) {
	for _, tc := range []struct {
		name     string
		params   errors_v2.ListErrorsV2Params
		expected string
		args     []interface{}
		err      error
	}{
		{
			name: "should build a simple query",
			params: errors_v2.ListErrorsV2Params{
				GroupID: 1,
				Project: []uint64{1, 2},
				Limit:   int64Pointer(20),
				Sort:    stringPointer("date"),
				Status:  stringPointer("unresolved"),
				Start:   stringPointer("2022-12-23T06:47:26"),
				End:     stringPointer("2022-12-23T07:47:26"),
			},
			expected: `SELECT
    project_id,
    lower(hex(id)) as _id,
    sum(error_count) AS count,
    argMaxMerge(status) AS status,
    any(actor) as actor,
    uniqMerge(approximated_user_count) AS user_count,
    max(last_seen_at) AS _last_seen_at,
    min(first_seen_at) AS _first_seen_at
FROM gl_error_tracking_group
WHERE project_id IN ( $1 ) 
GROUP BY
project_id,
id
 HAVING status = $2 AND _last_seen_at >= $3 AND _last_seen_at <= $4 ORDER BY _last_seen_at DESC LIMIT $5`,
			args: []interface{}{[]uint64{1, 2}, uint8(0), time.Date(2022, time.December, 23, 6, 47, 26, 0, time.UTC), time.Date(2022, time.December, 23, 7, 47, 26, 0, time.UTC), int64(20)},
			err:  nil,
		},
		{
			name: "should build a query with resolved status",
			params: errors_v2.ListErrorsV2Params{
				GroupID: 1,
				Project: []uint64{1, 2},
				Limit:   int64Pointer(20),
				Sort:    stringPointer("date"),
				Status:  stringPointer("resolved"),
			},
			expected: `SELECT
    project_id,
    lower(hex(id)) as _id,
    sum(error_count) AS count,
    argMaxMerge(status) AS status,
    any(actor) as actor,
    uniqMerge(approximated_user_count) AS user_count,
    max(last_seen_at) AS _last_seen_at,
    min(first_seen_at) AS _first_seen_at
FROM gl_error_tracking_group
WHERE project_id IN ( $1 ) 
GROUP BY
project_id,
id
 HAVING status = $2 ORDER BY _last_seen_at DESC LIMIT $3`,
			args: []interface{}{[]uint64{1, 2}, uint8(1), int64(20)},
			err:  nil,
		},
		{
			name: "should build query with sort of first_seen_at",
			params: errors_v2.ListErrorsV2Params{
				GroupID: 1,
				Project: []uint64{1, 2},
				Limit:   int64Pointer(20),
				Sort:    stringPointer("new"),
				Status:  stringPointer("unresolved"),
			},
			expected: `SELECT
    project_id,
    lower(hex(id)) as _id,
    sum(error_count) AS count,
    argMaxMerge(status) AS status,
    any(actor) as actor,
    uniqMerge(approximated_user_count) AS user_count,
    max(last_seen_at) AS _last_seen_at,
    min(first_seen_at) AS _first_seen_at
FROM gl_error_tracking_group
WHERE project_id IN ( $1 ) 
GROUP BY
project_id,
id
 HAVING status = $2 ORDER BY _first_seen_at DESC LIMIT $3`,
			args: []interface{}{[]uint64{1, 2}, uint8(0), int64(20)},
			err:  nil,
		},
		{
			name: "should build query with sort of count",
			params: errors_v2.ListErrorsV2Params{
				GroupID: 1,
				Project: []uint64{1, 2},
				Limit:   int64Pointer(20),
				Sort:    stringPointer("freq"),
				Status:  stringPointer("unresolved"),
			},
			expected: `SELECT
    project_id,
    lower(hex(id)) as _id,
    sum(error_count) AS count,
    argMaxMerge(status) AS status,
    any(actor) as actor,
    uniqMerge(approximated_user_count) AS user_count,
    max(last_seen_at) AS _last_seen_at,
    min(first_seen_at) AS _first_seen_at
FROM gl_error_tracking_group
WHERE project_id IN ( $1 ) 
GROUP BY
project_id,
id
 HAVING status = $2 ORDER BY count DESC LIMIT $3`,
			args: []interface{}{[]uint64{1, 2}, uint8(0), int64(20)},
			err:  nil,
		},
		{
			name: "should build query with sort of user",
			params: errors_v2.ListErrorsV2Params{
				GroupID: 1,
				Project: []uint64{1, 2},
				Limit:   int64Pointer(20),
				Sort:    stringPointer("user"),
				Status:  stringPointer("unresolved"),
			},
			expected: `SELECT
    project_id,
    lower(hex(id)) as _id,
    sum(error_count) AS count,
    argMaxMerge(status) AS status,
    any(actor) as actor,
    uniqMerge(approximated_user_count) AS user_count,
    max(last_seen_at) AS _last_seen_at,
    min(first_seen_at) AS _first_seen_at
FROM gl_error_tracking_group
WHERE project_id IN ( $1 ) 
GROUP BY
project_id,
id
 HAVING status = $2 ORDER BY user_count DESC LIMIT $3`,
			args: []interface{}{[]uint64{1, 2}, uint8(0), int64(20)},
			err:  nil,
		},
		{
			name: "should build query with default sort of date",
			params: errors_v2.ListErrorsV2Params{
				GroupID: 1,
				Project: []uint64{1, 2},
				Limit:   int64Pointer(20),
				Status:  stringPointer("unresolved"),
			},
			expected: `SELECT
    project_id,
    lower(hex(id)) as _id,
    sum(error_count) AS count,
    argMaxMerge(status) AS status,
    any(actor) as actor,
    uniqMerge(approximated_user_count) AS user_count,
    max(last_seen_at) AS _last_seen_at,
    min(first_seen_at) AS _first_seen_at
FROM gl_error_tracking_group
WHERE project_id IN ( $1 ) 
GROUP BY
project_id,
id
 HAVING status = $2 ORDER BY _last_seen_at DESC LIMIT $3`,
			args: []interface{}{[]uint64{1, 2}, uint8(0), int64(20)},
			err:  nil,
		},
		{
			name: "should fail because start param is incorrect formatted",
			params: errors_v2.ListErrorsV2Params{
				GroupID: 1,
				Project: []uint64{1, 2},
				Limit:   int64Pointer(20),
				Sort:    stringPointer("date"),
				Status:  stringPointer("unresolved"),
				Start:   stringPointer("2022-12-23-06:47:26"),
				End:     stringPointer("2022-12-23T07:47:26"),
			},
			expected: "",
			err:      fmt.Errorf("failed to parse `start` param"),
		},
		{
			name: "should fail because end param is incorrect formatted",
			params: errors_v2.ListErrorsV2Params{
				GroupID: 1,
				Project: []uint64{1, 2},
				Limit:   int64Pointer(20),
				Sort:    stringPointer("date"),
				Status:  stringPointer("unresolved"),
				Start:   stringPointer("2022-12-23T06:47:26"),
				End:     stringPointer("2022-12-23-07:47:26"),
			},
			expected: "",
			err:      fmt.Errorf("failed to parse `end` param"),
		},
	} {
		t.Log(tc.name)
		sql, args, err := buildListErrorsV2Query(tc.params)
		if tc.err != nil {
			assert.Error(t, err)
		} else {
			assert.Nil(t, err)
		}
		assert.Equal(t, tc.expected, sql)
		assert.EqualValues(t, tc.args, args)
	}
}
