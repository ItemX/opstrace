package models

import (
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"
)

const (
	errorEventsTableV2 = "gl_error_tracking_events_v2"
	errorStatusTableV2 = "gl_error_tracking_error_status_v2"
)

func quote(v string) string {
	return "'" + strings.NewReplacer(`\`, `\\`, `'`, `\'`).Replace(v) + "'"
}

func formatTime(value time.Time, tz *time.Location) string {
	switch value.Location().String() {
	case "Local":
		return fmt.Sprintf("toDateTime64('%d', 6)", value.UnixMicro())
	case tz.String():
		return fmt.Sprintf("toDateTime64('%s', 6,'%s')", value.Format("2006-01-02 15:04:05.000000"), tz.String())
	}
	return fmt.Sprintf("toDateTime64('%s', 6, '%s')",
		value.Format("2006-01-02 15:04:05.000000"),
		value.Location().String(),
	)
}

func formatUUID(id string) (uuid.UUID, error) {
	uid, err := uuid.Parse(id)
	if err != nil {
		return uuid.Nil, fmt.Errorf("failed to parse as UUID %w", err)
	}
	return uid, nil
}
