package gatekeeper

// Based upon https://github.com/penglongli/gin-metrics

import (
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
)

// Use set gin metrics middleware.
func RouteMetrics(registry prometheus.Registerer) gin.HandlerFunc {
	ginMetricHandle := initGinMetrics(registry)

	return func(ctx *gin.Context) {
		startTime := time.Now()
		ctx.Next()
		latency := time.Since(startTime)

		ginMetricHandle(ctx, latency)
	}
}

// initGinMetrics used to init gin metrics.
func initGinMetrics(registry prometheus.Registerer) func(*gin.Context, time.Duration) {
	metricURIRequestTotal := prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "gin_uri_request_total",
			Help: "all the server received request num with every uri.",
		},
		[]string{"uri", "method", "code"},
	)
	registry.MustRegister(metricURIRequestTotal)

	metricRequestBody := prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "gin_request_body_total",
			Help: "the server received request body size, unit byte",
		},
		[]string{"uri", "method", "code"},
	)
	registry.MustRegister(metricRequestBody)

	metricResponseBody := prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "gin_response_body_total",
			Help: "the server send response body size, unit byte",
		},
		[]string{"uri", "method", "code"},
	)
	registry.MustRegister(metricResponseBody)

	metricRequestDuration := prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "gin_request_duration",
			Help:    "the time server took to handle the request.",
			Buckets: prometheus.DefBuckets,
		},
		[]string{"uri"},
	)
	registry.MustRegister(metricRequestDuration)

	return func(ctx *gin.Context, latency time.Duration) {
		r := ctx.Request
		w := ctx.Writer

		// set uri request total
		metricURIRequestTotal.
			WithLabelValues(ctx.FullPath(), r.Method, strconv.Itoa(w.Status())).
			Inc()

		// set request body size
		// since r.ContentLength can be negative (in some occasions) guard the operation
		if r.ContentLength >= 0 {
			metricRequestBody.
				WithLabelValues(ctx.FullPath(), r.Method, strconv.Itoa(w.Status())).
				Add(float64(r.ContentLength))
		}

		// set response size
		if w.Size() > 0 {
			metricResponseBody.
				WithLabelValues(ctx.FullPath(), r.Method, strconv.Itoa(w.Status())).
				Add(float64(w.Size()))
		}

		// set request duration
		metricRequestDuration.
			WithLabelValues(ctx.FullPath()).
			Observe(latency.Seconds())
	}
}
