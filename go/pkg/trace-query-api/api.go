package query

import (
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/metrics"
	"go.uber.org/zap"

	models "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/trace-query-api/models"
)

func SetRoutes(controller *Controller, router *gin.Engine) {
	router.Use(metrics.InstrumentHandlerFuncWithOpts(stripProjectIDFromPath, metrics.NoOpAdditionalMetricsFunc))

	query := router.Group("/query/:group_id/:project_id")
	v1 := query.Group("/v1")
	v1.GET("/traces", controller.TraceHandler)
}

type Controller struct {
	Q      QueryDB
	Logger *zap.Logger
}

type Result struct {
	ProjectID string `json:"project_id"`

	Traces      []*models.TraceItem `json:"traces"`
	TotalTraces int64               `json:"totalTraces"`
}

func (c *Controller) TraceHandler(ctx *gin.Context) {
	groupID := ctx.Param("group_id")
	projectID := ctx.Param("project_id")

	if groupID == "" || projectID == "" {
		ctx.String(400, "Bad Request - Missing groupID or projectID")
		return
	}

	pID, err := strconv.ParseInt(projectID, 10, 64)
	if err != nil {
		c.Logger.Error("failed to parse projectID", zap.Error(err))
		ctx.String(400, "Bad Request")
		return
	}

	results, err := c.Q.GetTraces(ctx, pID)
	if err != nil {
		c.Logger.Error("failed to get traces", zap.Error(err))
		ctx.String(500, "internal error")
		return
	}

	resp := &Result{
		ProjectID:   projectID,
		TotalTraces: int64(len(results)),
		Traces:      results,
	}
	ctx.JSON(200, resp)
}

// Helper to register metrics for traces Handler
// In order to avoid cardinality issues with metrics additionally sliced with path that have groupID/projectID
// just return the handler path.
func stripProjectIDFromPath(path string) string {
	if strings.Contains(path, "/v1/traces") {
		return "/v1/traces"
	}
	return path
}
