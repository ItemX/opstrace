package errortracking

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/go-openapi/strfmt"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors_v2"
	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
)

const baseQueryErrorsV2 = `SELECT
    project_id,
    lower(hex(id)) as _id,
    sum(error_count) AS count,
    argMaxMerge(status) AS status,
    any(actor) as actor,
    uniqMerge(approximated_user_count) AS user_count,
    max(last_seen_at) AS _last_seen_at,
    min(first_seen_at) AS _first_seen_at
FROM gl_error_tracking_group
`

const groupByErros = `
GROUP BY
project_id,
id
`

func (db *database) ListErrorsV2(params errors_v2.ListErrorsV2Params) ([]*models.ErrorV2, error) {
	ctx := context.Background()
	var result []*models.ErrorV2

	query, args, err := buildListErrorsV2Query(params)
	if err != nil {
		return nil, err
	}

	rows, err := db.conn.Query(ctx, query, args...)
	if err != nil {
		return nil, fmt.Errorf("failed to list events: %w", err)
	}
	for rows.Next() {
		// Scan the row as a ErrorTrackingError struc....
		e := &et.ErrorV2{}
		err := rows.ScanStruct(e)
		if err != nil {
			return nil, fmt.Errorf("failed to scan struct into ErrorV2: %w", err)
		}
		status, ok := errorStatusToStr[errorStatus(e.Status)]
		if !ok {
			return nil, fmt.Errorf("unexpected error status value %v", e.Status)
		}
		result = append(result, &models.ErrorV2{
			Actor:     e.Actor,
			UserCount: e.UserCount,
			Count:     strconv.Itoa(int(e.Count)),
			FirstSeen: strfmt.DateTime(e.FirstSeenAt),
			ID:        e.ID,
			LastSeen:  strfmt.DateTime(e.LastSeenAt),
			Project: &models.Project{
				ID: strconv.Itoa(int(e.ProjectID)),
			},
			Status: string(status),
			//Title:                 "",
		})
	}
	err = rows.Close()
	if err != nil {
		return nil, fmt.Errorf("failed to close connection while reading from clickhouse: %w", err)
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("failed to read events from clickhouse: %w", err)
	}
	return result, nil
}

func buildListErrorsV2Query(params errors_v2.ListErrorsV2Params) (string, []interface{}, error) {
	q := &queryBuilder{}
	q.reset(baseQueryErrorsV2)

	q.build("WHERE project_id IN ( ? ) ", params.Project)

	q.build(groupByErros)
	// Status default value is unresolved so we can skip the nil check.
	status, ok := errorStatusToInt[errorStatusStr(*params.Status)]
	if !ok {
		return "", nil, fmt.Errorf("unexpected error status %v", *params.Status)
	}
	q.build(
		" HAVING status = ?",
		uint8(status),
	)
	if params.Start != nil {
		start, err := time.Parse("2006-01-02T15:04:05", *params.Start)
		if err != nil {
			return "", nil, fmt.Errorf("failed to parse `start` param: %w", err)
		}
		q.build(" AND _last_seen_at >= ?", start)
	}

	if params.End != nil {
		end, err := time.Parse("2006-01-02T15:04:05", *params.End)
		if err != nil {
			return "", nil, fmt.Errorf("failed to parse `end` param: %w", err)
		}
		q.build(" AND _last_seen_at <= ?", end)
	}

	// TODO: query param handling
	// See https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2002
	// if params.Query != nil && len(*params.Query) > 2 {
	//	// clickhouse ILIKE search operator uses percentage character % to match
	//	// any byte. golang requires %% to escape the percentage % character.
	//	wildcard := fmt.Sprintf("%%%s%%", *params.Query)
	//	q.build(
	//		" AND (gl_error_tracking_errors.name ILIKE ? OR gl_error_tracking_errors.description ILIKE ?)",
	//		wildcard,
	//		wildcard,
	//	)
	// }

	orderBy := ""
	const (
		orderByDate = " ORDER BY _last_seen_at DESC"
		orderByNew  = " ORDER BY _first_seen_at DESC"
		orderByFreq = " ORDER BY count DESC"
		orderByUser = " ORDER BY user_count DESC"
	)
	if params.Sort != nil {
		switch *params.Sort {
		case "date":
			orderBy = orderByDate
		case "new":
			orderBy = orderByNew
		case "freq":
			orderBy = orderByFreq
		case "user":
			orderBy = orderByUser
		default:
			orderBy = orderByDate
		}
	} else {
		orderBy = orderByDate
	}
	q.build(orderBy)

	if params.Limit != nil {
		q.build(" LIMIT ?", *params.Limit)
	} else {
		q.build(" LIMIT ?", defaultListIssueLimit)
	}

	return q.sql, q.args, nil
}
