package common

import (
	"context"
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	"github.com/fluxcd/pkg/ssa"
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/inventory"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"sigs.k8s.io/cli-utils/pkg/object"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// We use closures, as Actions are type-agnostic and the cr object that is
// accessible in the runner is of type Object.
// TODO(prozlach): Is there a better way to approach this?
type GetInventoryFunc func() *v1beta2.ResourceInventory
type SetInventoryFunc func(*v1beta2.ResourceInventory)

type ManifestsPruneAction struct {
	ComponentName string
	GetInventory  GetInventoryFunc
	Msg           string
	Log           logr.Logger
}

func (r ManifestsPruneAction) Run(runner ActionRunner) (string, ActionOperation, error) {
	if r.GetInventory() == nil {
		return r.Msg, OperationLog, fmt.Errorf("unable to prune manifests as the inventory is empty")
	}

	objects, err := inventory.List(r.GetInventory())
	if err != nil {
		return r.Msg, OperationLog, fmt.Errorf("unable to parse inventory: %w", err)
	}

	r.Log.V(1).Info("pruning manifests", "manifests_count", len(objects))

	opts := ssa.DeleteOptions{
		PropagationPolicy: metav1.DeletePropagationBackground,
		Inclusions:        runner.resourceManager.GetOwnerLabels(runner.cr.GetName(), runner.cr.GetNamespace()),
		// NOTE(prozlach): Worth keeping it visible/around, may be useful someday
		Exclusions: map[string]string{},
	}

	changeSet, err := runner.resourceManager.DeleteAll(runner.ctx, objects, opts)
	if err != nil {
		return r.Msg, OperationLog, fmt.Errorf("deleting objects failed: %w", err)
	}

	if changeSet != nil && len(changeSet.Entries) > 0 {
		for _, e := range changeSet.Entries {
			r.Log.V(1).Info(fmt.Sprintf("%s deleted", e.String()))
		}
		return r.Msg, OperationDeleted, nil
	}

	return r.Msg, OperationNoop, nil
}

type ManifestsEnsureAction struct {
	HealthcheckTimeout time.Duration
	ComponentName      string
	Manifests          []*unstructured.Unstructured
	GetInventory       GetInventoryFunc
	SetInventory       SetInventoryFunc
	Msg                string
	Log                logr.Logger
}

func (r ManifestsEnsureAction) Run(runner ActionRunner) (string, ActionOperation, error) {
	runner.resourceManager.SetOwnerLabels(r.Manifests, runner.cr.GetName(), runner.cr.GetNamespace())

	r.Log.Info("ensuring manifests", "manifests_count", len(r.Manifests))

	// Create a snapshot of the current inventory.
	oldInventory := inventory.New()
	if r.GetInventory() != nil {
		r.GetInventory().DeepCopyInto(oldInventory)
	}

	// Validate and apply resources in stages.
	changeSet, err := r.apply(runner.ctx, runner.resourceManager, r.Manifests)
	if err != nil {
		return r.Msg, OperationLog, fmt.Errorf("reconciling manifests failed: %w", err)
	}

	// Create an inventory from the reconciled resources.
	newInventory := inventory.New()
	err = inventory.AddChangeSet(newInventory, changeSet)
	if err != nil {
		return r.Msg, OperationLog, fmt.Errorf("updating inventory with changeset failed: %w", err)
	}

	// Set last applied inventory in status.
	r.SetInventory(newInventory)

	// Detect stale resources which are subject to garbage collection.
	staleObjects, err := inventory.Diff(oldInventory, newInventory)
	if err != nil {
		return r.Msg, OperationLog, fmt.Errorf("detecting stale objects failed: %w", err)
	}

	// Run garbage collection for stale resources.
	objectsPruned, err := r.prune(runner.ctx, runner.resourceManager, runner.cr, staleObjects)
	if err != nil {
		return r.Msg, OperationLog, fmt.Errorf("pruning old objects failed: %w", err)
	}

	// Run the health checks for the last applied resources.
	err = r.checkHealth(runner.resourceManager, changeSet.ToObjMetadataSet())
	if err != nil {
		return r.Msg, OperationLog, fmt.Errorf("checking health of deployed object failed: %w", err)
	}

	if objectsPruned {
		return r.Msg, OperationUpdated, nil
	}
	if len(changeSet.Entries) > 0 {
		for _, e := range changeSet.Entries {
			if e.Action != "unchanged" {
				return r.Msg, OperationUpdated, nil
			}
		}
	}
	return r.Msg, OperationNoop, nil
}

func (r *ManifestsEnsureAction) apply(
	ctx context.Context,
	manager *ssa.ResourceManager,
	objects []*unstructured.Unstructured,
) (*ssa.ChangeSet, error) {
	if err := ssa.SetNativeKindsDefaults(objects); err != nil {
		return nil, fmt.Errorf("failed to set native kinds defaults: %w", err)
	}

	applyOpts := ssa.DefaultApplyOptions()
	applyOpts.Force = true
	applyOpts.Cleanup = ssa.ApplyCleanupOptions{
		Annotations: []string{
			// remove the kubectl annotation
			corev1.LastAppliedConfigAnnotation,
		},
		FieldManagers: []ssa.FieldManager{
			{
				// to undo changes made with 'kubectl apply --server-side --force-conflicts'
				Name:          "kubectl",
				OperationType: metav1.ManagedFieldsOperationApply,
			},
			{
				// to undo changes made with 'kubectl apply'
				Name:          "kubectl",
				OperationType: metav1.ManagedFieldsOperationUpdate,
			},
			{
				// to undo changes made with 'kubectl apply'
				Name:          "before-first-apply",
				OperationType: metav1.ManagedFieldsOperationUpdate,
			},
		},
	}

	// contains only CRDs and Namespaces
	var defStage []*unstructured.Unstructured

	// contains only Kubernetes Class types e.g.: RuntimeClass, PriorityClass,
	// StorageClas, VolumeSnapshotClass, IngressClass, GatewayClass, ClusterClass, etc
	var classStage []*unstructured.Unstructured

	// contains all objects except for CRDs, Namespaces and Class type objects
	var resStage []*unstructured.Unstructured

	// contains the objects' metadata after apply
	resultSet := ssa.NewChangeSet()

	for _, u := range objects {
		switch {
		case ssa.IsClusterDefinition(u):
			defStage = append(defStage, u)
		case strings.HasSuffix(u.GetKind(), "Class"):
			classStage = append(classStage, u)
		default:
			resStage = append(resStage, u)
		}
	}

	var changeSetLog strings.Builder

	if len(defStage) > 0 {
		if err := r.applyDefStage(ctx, applyOpts, manager, defStage, &changeSetLog, resultSet); err != nil {
			return nil, err
		}
	} else {
		r.Log.V(1).Info("defStage empty, no objects were applied")
	}

	if len(classStage) > 0 {
		if err := r.applyClassStage(ctx, applyOpts, manager, classStage, &changeSetLog, resultSet); err != nil {
			return nil, err
		}
	} else {
		r.Log.V(1).Info("classStage empty, no objects were applied")
	}

	if len(resStage) > 0 {
		if err := r.applyResStage(ctx, applyOpts, manager, resStage, &changeSetLog, resultSet); err != nil {
			return nil, err
		}
	} else {
		r.Log.V(1).Info("resStage empty, no objects were applied")
	}

	return resultSet, nil
}

func (r *ManifestsEnsureAction) applyDefStage(
	ctx context.Context,
	applyOpts ssa.ApplyOptions,
	manager *ssa.ResourceManager,
	defStage []*unstructured.Unstructured,
	changeSetLog *strings.Builder,
	resultSet *ssa.ChangeSet,
) error {
	// validate, apply and wait for CRDs and Namespaces to register
	changeSet, err := manager.ApplyAll(ctx, defStage, applyOpts)
	if err != nil {
		return fmt.Errorf("defStage ApplyAll failed: %w", err)
	}

	if changeSet != nil && len(changeSet.Entries) > 0 {
		for _, e := range changeSet.Entries {
			r.Log.V(1).Info(e.String())
		}
		r.Log.Info("server-side apply for cluster definitions completed")

		resultSet.Append(changeSet.Entries)

		for _, change := range changeSet.Entries {
			if change.Action != string(ssa.UnchangedAction) {
				changeSetLog.WriteString(change.String() + "\n")
			}
		}

		if err := manager.WaitForSet(changeSet.ToObjMetadataSet(), ssa.WaitOptions{
			Interval: 2 * time.Second,
			Timeout:  r.HealthcheckTimeout,
		}); err != nil {
			return fmt.Errorf("WaitForSet in defStage failed: %w", err)
		}
	} else {
		r.Log.V(1).Info("defStage applied, but changeSate is empty")
	}

	return nil
}

func (r *ManifestsEnsureAction) applyClassStage(
	ctx context.Context,
	applyOpts ssa.ApplyOptions,
	manager *ssa.ResourceManager,
	classStage []*unstructured.Unstructured,
	changeSetLog *strings.Builder,
	resultSet *ssa.ChangeSet,
) error {
	// validate, apply and wait for Class type objects to register
	changeSet, err := manager.ApplyAll(ctx, classStage, applyOpts)
	if err != nil {
		return fmt.Errorf("classStage ApplyAll failed: %w", err)
	}

	if changeSet != nil && len(changeSet.Entries) > 0 {
		for _, e := range changeSet.Entries {
			r.Log.V(1).Info(e.String())
		}
		r.Log.Info("server-side apply for cluster class types completed")

		resultSet.Append(changeSet.Entries)

		for _, change := range changeSet.Entries {
			if change.Action != string(ssa.UnchangedAction) {
				changeSetLog.WriteString(change.String() + "\n")
			}
		}

		if err := manager.WaitForSet(changeSet.ToObjMetadataSet(), ssa.WaitOptions{
			Interval: 2 * time.Second,
			Timeout:  r.HealthcheckTimeout,
		}); err != nil {
			return fmt.Errorf("WaitForSet in classStage failed: %w", err)
		}
	} else {
		r.Log.V(1).Info("classStage applied, but changeSate is empty")
	}

	return nil
}

func (r *ManifestsEnsureAction) applyResStage(
	ctx context.Context,
	applyOpts ssa.ApplyOptions,
	manager *ssa.ResourceManager,
	resStage []*unstructured.Unstructured,
	changeSetLog *strings.Builder,
	resultSet *ssa.ChangeSet,
) error {
	// sort by kind, validate and apply all the others objects
	sort.Sort(ssa.SortableUnstructureds(resStage))
	changeSet, err := manager.ApplyAll(ctx, resStage, applyOpts)
	if err != nil {
		return fmt.Errorf("%w\n%s", err, changeSetLog.String())
	}

	if changeSet != nil && len(changeSet.Entries) > 0 {
		for _, e := range changeSet.Entries {
			r.Log.V(1).Info(e.String())
		}
		r.Log.Info("server-side apply completed")

		resultSet.Append(changeSet.Entries)

		for _, change := range changeSet.Entries {
			if change.Action != string(ssa.UnchangedAction) {
				changeSetLog.WriteString(change.String() + "\n")
			}
		}
	} else {
		r.Log.V(1).Info("resStage applied, but changeSate is empty")
	}

	return nil
}

func (r *ManifestsEnsureAction) prune(
	ctx context.Context,
	manager *ssa.ResourceManager,
	obj client.Object,
	objects []*unstructured.Unstructured,
) (bool, error) {
	if len(objects) == 0 {
		return false, nil
	}

	r.Log.Info("performing garbage collection")

	opts := ssa.DeleteOptions{
		PropagationPolicy: metav1.DeletePropagationBackground,
		Inclusions:        manager.GetOwnerLabels(obj.GetName(), obj.GetNamespace()),
	}

	changeSet, err := manager.DeleteAll(ctx, objects, opts)
	if err != nil {
		return false, fmt.Errorf("DeleteAll failed: %w", err)
	}

	// emit event only if the prune operation resulted in changes
	if changeSet != nil && len(changeSet.Entries) > 0 {
		for _, e := range changeSet.Entries {
			r.Log.V(1).Info(fmt.Sprintf("%s removed", e.String()))
		}
		return true, nil
	}

	return false, nil
}

func (r *ManifestsEnsureAction) checkHealth(
	manager *ssa.ResourceManager,
	toCheck object.ObjMetadataSet,
) error {
	if len(toCheck) == 0 {
		return nil
	}

	checkStart := time.Now()

	r.Log.V(1).Info("starting wait for manifests to reconcile", "timeout", r.HealthcheckTimeout.String())
	// Check the health with a default timeout of 30sec shorter than the reconciliation interval.
	err := manager.WaitForSet(
		toCheck,
		ssa.WaitOptions{
			Interval: 5 * time.Second,
			Timeout:  r.HealthcheckTimeout,
		},
	)
	if err != nil {
		return fmt.Errorf("health check failed after %s: %w", time.Since(checkStart).String(), err)
	}
	r.Log.Info("manifests are healthy", "reconciliation_time", time.Since(checkStart).String())

	return nil
}
