use std::env;

fn main() {
  let dsn = match env::var_os("SENTRY_DSN") {
    Some(v) => v.into_string().unwrap(),
    None => panic!("$SENTRY_DSN is not set")
  };
  println!("Got SENTRY_DSN: {}", dsn);

  let _guard = sentry::init((dsn, sentry::ClientOptions {
    release: sentry::release_name!(),
    attach_stacktrace: true,
    send_default_pii: true,
    debug: true,
    ..Default::default()
  }));

  let err = "NaN".parse::<usize>().unwrap_err();
  sentry::capture_error(&err);

  sentry::capture_event(sentry::protocol::Event {
    message: Some("test event".into()),
    ..Default::default()
  });

  sentry::capture_message("test message", sentry::Level::Fatal);

  // session
  sentry::start_session();
  sentry::end_session_with_status( sentry::types::protocol::v7::SessionStatus::Crashed);

  // Sentry should capture this
  panic!("Everything is on fire!");
}