package e2e

import (
	"reflect"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/kubectl/pkg/polymorphichelpers"
	"k8s.io/utils/pointer"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	adminUserName = "admin"
	adminPassword = "passw0rd"
	// by default use latest-alpine to test against so we integrate against current release.
	defaultClickHouseServer = "clickhouse/clickhouse-server:latest-alpine"
)

// test cluster config
type clusterConfig struct {
	image         string
	generateName  string
	ssl           bool
	replicas      *int32
	objectStorage *clickhousev1alpha1.ClickHouseObjectStorage
}

// init cluster with 3 replicas and typical settings
// deletes cluster after the test and verifies cleanup
func beforeEachCreateCluster(cluster **clickhousev1alpha1.ClickHouse, config clusterConfig) {
	BeforeEach(func(ctx SpecContext) {
		By("create cluster definition")
		generateName := config.generateName
		if generateName == "" {
			generateName = "cluster-e2e-test-"
		}
		replicas := config.replicas
		if replicas == nil {
			replicas = pointer.Int32(3)
		}
		image := config.image
		if image == "" {
			image = defaultClickHouseServer
		}
		adminSecret := createAdminSecret(ctx)
		adminUsers := []clickhousev1alpha1.ClickHouseAdmin{
			{
				Name: adminUserName,
				SecretKeyRef: corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: adminSecret.Name,
					},
					Key: "password",
				},
			},
		}
		c := &clickhousev1alpha1.ClickHouse{
			ObjectMeta: metav1.ObjectMeta{
				// generates a name for our e2e test
				GenerateName: generateName,
				Namespace:    "default",
			},
			Spec: clickhousev1alpha1.ClickHouseSpec{
				// TODO(joe): run these tests with different image versions
				// need to define a support matrix of images for this operator
				Image:    image,
				Replicas: replicas,
				// GKE storage provisioner seems to provision volumes with size
				// starting with 1Gi. Everything lower than that is rounded to
				// 1Gi causing Cluster to never transition to `Complete` state
				// as the desired storage size (e.g. 512Mi) and actual size
				// (1Gi) of PVC differ.
				StorageSize:   resource.MustParse("1Gi"),
				InternalSSL:   pointer.Bool(config.ssl),
				AdminUsers:    adminUsers,
				ObjectStorage: config.objectStorage,
			},
		}

		Expect(k8sClient.Create(ctx, c)).To(Succeed())
		DeferCleanup(func(ctx SpecContext) {
			By("delete cluster definition")
			Expect(k8sClient.Delete(ctx, c)).To(Succeed())
			assertClusterDeleted(ctx, c)
		})
		*cluster = c
	})
}

// creates admin secret and deletes it after the test
func createAdminSecret(ctx SpecContext) *corev1.Secret {
	By("create admin secret")
	s := &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			GenerateName: "admin-secret-",
			Namespace:    "default",
		},
		StringData: map[string]string{
			"password": adminPassword,
		},
	}
	Expect(k8sClient.Create(ctx, s)).To(Succeed())
	DeferCleanup(func(ctx SpecContext) {
		By("delete admin secret")
		Expect(k8sClient.Delete(ctx, s)).To(Succeed())
	})
	return s
}

// assert that the cluster CR is ready
func assertClusterReady(ctx SpecContext, cluster *clickhousev1alpha1.ClickHouse) {
	By("check that clickhouse status ready is set")
	Eventually(ctx, func(g Gomega) {
		g.Expect(
			k8sClient.Get(ctx,
				types.NamespacedName{
					Namespace: cluster.Namespace,
					Name:      cluster.Name,
				},
				cluster)).
			Should(Succeed())
		g.Expect(cluster.Status.Status).To(Equal(clickhousev1alpha1.StatusCompleted))
	}).Should(Succeed())
}

func stsReady(s *appsv1.StatefulSet) (bool, string, error) {
	viewer := polymorphichelpers.StatefulSetStatusViewer{}
	u, err := runtime.DefaultUnstructuredConverter.ToUnstructured(s)
	if err != nil {
		return false, "", err
	}

	msg, ready, err := viewer.Status(&unstructured.Unstructured{Object: u}, 0)
	return ready, msg, err
}

// assert that the cluster is running properly
func assertClusterRunning(ctx SpecContext, cluster *clickhousev1alpha1.ClickHouse) {
	By("check statefulsets exist for our cluster and are ready")
	Eventually(ctx, func(g Gomega) {
		replicas := 0
		walkClusterStatefulSets(ctx, cluster, func(s *appsv1.StatefulSet) {
			replicas++
			r, _, err := stsReady(s)
			g.Expect(err).NotTo(HaveOccurred())
			g.Expect(r).To(BeTrue())
		})

		g.Expect(replicas).To(Equal(int(*cluster.Spec.Replicas)))
	}).Should(Succeed())
}

func walkClusterStatefulSets(ctx SpecContext, cluster *clickhousev1alpha1.ClickHouse, f func(s *appsv1.StatefulSet)) {
	ownedStatefulSets := &appsv1.StatefulSetList{}
	Expect(k8sClient.List(ctx, ownedStatefulSets,
		client.InNamespace(cluster.Namespace),
		client.MatchingLabels{
			"name": cluster.Name,
		})).Should(Succeed())

	for _, is := range ownedStatefulSets.Items {
		s := is
		f(&s)
	}
}

func assertClusterDeleted(ctx SpecContext, cluster *clickhousev1alpha1.ClickHouse) {
	By("all managed resources should be deleted")
	Eventually(func(g Gomega) {
		managed := []client.ObjectList{
			&appsv1.StatefulSetList{},
			&corev1.ServiceList{},
			&corev1.SecretList{},
			&corev1.ConfigMapList{},
		}
		for _, m := range managed {
			g.Expect(k8sClient.List(ctx, m, client.InNamespace(cluster.Namespace))).To(Succeed())
			v := reflect.ValueOf(m)
			// Note(joe): I can't find a nice way of doing this without reflection or lots of boilerplate for each type.
			items := reflect.Indirect(v).FieldByName("Items")
			// all cleared up.
			if items.IsNil() {
				continue
			}
			// anything with an owner reference should not be owned by the cluster resource.
			for i := 0; i < items.Len(); i++ {
				meta := items.Index(i).Addr().Interface().(metav1.Object)
				for _, o := range meta.GetOwnerReferences() {
					g.Expect(o.UID).NotTo(Equal(cluster.UID))
				}
			}
		}
	}).Should(Succeed())
}
