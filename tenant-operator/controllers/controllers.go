package controllers

import (
	"errors"
)

var ErrGrafanaURLTenantNotReady = errors.New("tenant is not ready")
var ErrGrafanaURLTenantURLEmpty = errors.New("GOUI URL is not set")
