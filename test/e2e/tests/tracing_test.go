package tests

import (
	"context"
	"crypto/tls"
	"encoding/hex"
	"errors"
	"fmt"
	"math/rand"
	"net/url"
	"time"

	"github.com/anthhub/forwarder"
	"github.com/gogo/protobuf/types"
	jaegerapiv3 "github.com/jaegertracing/jaeger/proto-gen/api_v3"
	tracev1 "github.com/jaegertracing/jaeger/proto-gen/otel/trace/v1"
	. "github.com/onsi/ginkgo/v2"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"sigs.k8s.io/controller-runtime/pkg/client"

	jaegercommonv1 "github.com/jaegertracing/jaeger/proto-gen/otel/common/v1"

	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/argusapi"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
)

// TODO(joe): this needs rewriting when we have shared tenant tracing deployed.
var _ = PDescribe("Tracing", Ordered, Serial, func() {
	cluster := &schedulerv1alpha1.Cluster{}
	beforeAllEnsureCluster(cluster)

	namespace := &schedulerv1alpha1.GitLabNamespace{}
	beforeAllEnsureNamespace(namespace)

	var namespaceID int
	// api key for the wrong org
	var adminOrgAPIKey string
	// api key for the correct org
	var apiKey string
	// servicename for jaeger compatible query
	const serviceName = "test_otlp_tracing_api"
	// tracer configured with serviceName to create spans
	var tracer trace.Tracer

	// send a test span to the otel trace http endpoint
	sendSpans := func(ctx SpecContext, groupID int, apiKey string, spans ...trace.Span) error {
		opts := []otlptracehttp.Option{
			// #nosec G402
			otlptracehttp.WithTLSClientConfig(&tls.Config{
				InsecureSkipVerify: true,
			}),
			otlptracehttp.WithEndpoint(infraConfig.GOBHost),
			otlptracehttp.WithURLPath(fmt.Sprintf("/v1/traces/%d", groupID)),
			otlptracehttp.WithHeaders(
				map[string]string{
					"Authorization": fmt.Sprintf("Bearer %s", apiKey),
				},
			),
		}
		exporter, err := otlptracehttp.New(ctx, opts...)
		Expect(err).NotTo(HaveOccurred())
		defer func() { Expect(exporter.Shutdown(ctx)).To(Succeed()) }()
		return exporter.ExportSpans(ctx, readSpans(spans...))
	}

	// creates a span from the tracer and returns the span context and span.
	// span properties are randomly assigned.
	getSingleSpan := func(ctx SpecContext) (context.Context, trace.Span) {
		spanLabel := getRandomSpanLabel()

		kind := randItem(
			trace.SpanKindUnspecified,
			trace.SpanKindInternal,
			trace.SpanKindServer,
			trace.SpanKindClient,
			trace.SpanKindProducer,
			trace.SpanKindConsumer,
		)

		status := randItem(
			codes.Unset,
			codes.Ok,
			codes.Error,
		)

		// provide a unique name for lookup convenience
		operationName := "test_" + spanLabel
		spanCtx, span := tracer.Start(ctx, operationName,
			trace.WithSpanKind(kind),
			trace.WithAttributes(attribute.String("testing", spanLabel)))
		span.AddEvent("emitting test span", trace.WithAttributes(attribute.String("testing", spanLabel)))
		span.SetStatus(status, "test status")

		By(fmt.Sprintf("created span %s for trace %s",
			span.SpanContext().SpanID(), span.SpanContext().TraceID()))

		return spanCtx, span
	}

	// jaeger client opens grpc connection through proxy.
	// returns client and closer function.
	jaegerClient := func(ctx SpecContext) (jaegerapiv3.QueryServiceClient, func()) {
		ports, close := testutils.PortForward(restConfig, &forwarder.Option{
			Namespace:   namespace.Namespace(),
			ServiceName: fmt.Sprintf("jaeger-%d-query", namespace.Spec.ID),
			RemotePort:  16685,
		}, Default)

		conn, err := grpc.DialContext(ctx, fmt.Sprintf("localhost:%d", ports[0].Local),
			grpc.WithTransportCredentials(insecure.NewCredentials()))
		Expect(err).NotTo(HaveOccurred())

		closer := func() {
			defer close()
			defer conn.Close()
		}
		return jaegerapiv3.NewQueryServiceClient(conn), closer
	}

	// use jaeger GRPC API to find the span and verify its properties.
	// returns async assertion that must be checked from the caller.
	queryForSpan := func(ctx SpecContext, span trace.Span, matcher OmegaMatcher) {
		client, close := jaegerClient(ctx)
		defer close()

		rs := readSpan(span)
		attr := map[string]string{}
		for _, kv := range rs.Attributes() {
			attr[string(kv.Key)] = kv.Value.AsString()
		}
		query := &jaegerapiv3.TraceQueryParameters{
			ServiceName:   serviceName,
			OperationName: rs.Name(),
			Attributes:    attr,
			StartTimeMin: &types.Timestamp{
				Seconds: rs.StartTime().Unix() - 1,
			},
			StartTimeMax: &types.Timestamp{
				Seconds: rs.EndTime().Unix() + 1,
			},
			NumTraces: 1,
		}
		// wait for up to ~60s for span to appear in queries.
		// There should normally be up to a 5s delay for the next batch from the collector to Jaeger,
		// plus another <5s or so for the trace to appear in queries, but waiting longer should avoid flakes.
		Eventually(func(g Gomega) {
			rcv, err := client.FindTraces(ctx, &jaegerapiv3.FindTracesRequest{
				Query: query,
			})
			g.Expect(err).NotTo(HaveOccurred())
			chunk, err := rcv.Recv()
			g.Expect(err).NotTo(HaveOccurred())
			g.Expect(chunk.ResourceSpans).To(HaveLen(1))
			r1 := chunk.ResourceSpans[0]
			g.Expect(r1.InstrumentationLibrarySpans).To(HaveLen(1))
			s1 := r1.InstrumentationLibrarySpans[0]
			g.Expect(s1.Spans).To(HaveLen(1))

			verifySpan(rs, s1.Spans[0])
		}).WithTimeout(time.Minute).Should(matcher)
	}

	BeforeAll(func(ctx SpecContext) {
		namespaceID = int(namespace.Spec.ID)

		By("set up a shared tracer for our tests")
		r, err := resource.Merge(
			resource.Default(),
			resource.NewWithAttributes(
				"",
				semconv.ServiceNameKey.String(serviceName),
				semconv.ServiceVersionKey.String("v0.1.0"),
				attribute.String("environment", "e2e"),
			))
		Expect(err).NotTo(HaveOccurred())
		tp := sdktrace.NewTracerProvider(sdktrace.WithResource(r))
		tracer = tp.Tracer("otel_tracer")

		By("creating argus API token for tracing requests")
		argusCreds := &corev1.Secret{}
		Expect(k8sClient.Get(ctx, client.ObjectKey{
			Namespace: namespace.Namespace(),
			Name:      constants.ArgusAdminSecretName,
		}, argusCreds)).Should(Succeed())

		adminUser := argusCreds.Data[constants.ArgusAdminUserEnvVar]
		adminPass := argusCreds.Data[constants.ArgusAdminPasswordEnvVar]

		ports, close := testutils.PortForward(restConfig, &forwarder.Option{
			Namespace:   namespace.Namespace(),
			ServiceName: constants.ArgusServiceName,
			RemotePort:  constants.ArgusHTTPPort,
		}, Default)
		defer close()
		client, err := argusapi.New(fmt.Sprintf("http://localhost:%d", ports[0].Local),
			argusapi.Config{
				BasicAuth: url.UserPassword(string(adminUser), string(adminPass)),
			})
		Expect(err).NotTo(HaveOccurred())

		// NOTE(joe): using Argus org 1 as incorrect entry org may become obsolete.
		// In this case we would want to create another GitLabNamespace
		// and test API keys from there to make sure they are not valid here.
		Expect(client.UserSwitchContext(1)).To(Succeed())
		key, err := client.CreateAPIKey(argusapi.CreateAPIKeyRequest{
			Name: common.RandStringRunes(10),
			Role: "Viewer",
		})
		Expect(err).NotTo(HaveOccurred())
		adminOrgAPIKey = key.Key

		// switch to GitLab namespace org and create an api key
		Expect(client.UserSwitchContext(int(namespace.Spec.ID))).To(Succeed())
		key, err = client.CreateAPIKey(argusapi.CreateAPIKeyRequest{
			Name: common.RandStringRunes(10),
			Role: "Viewer",
		})
		Expect(err).NotTo(HaveOccurred())
		apiKey = key.Key
	})

	Context("Auth", func() {
		It("should not accept an empty api key", func(ctx SpecContext) {
			_, span := getSingleSpan(ctx)
			span.End()
			Expect(sendSpans(ctx, namespaceID, "", span)).NotTo(Succeed())
			queryForSpan(ctx, span, Not(Succeed()))
		})

		It("should not accept an invalid api key", func(ctx SpecContext) {
			_, span := getSingleSpan(ctx)
			span.End()
			Expect(sendSpans(ctx, namespaceID, "bad"+apiKey, span)).NotTo(Succeed())
			queryForSpan(ctx, span, Not(Succeed()))
		})

		It("should not accept a different org api key", func(ctx SpecContext) {
			_, span := getSingleSpan(ctx)
			span.End()
			Expect(sendSpans(ctx, namespaceID, adminOrgAPIKey, span)).NotTo(Succeed())
			queryForSpan(ctx, span, Not(Succeed()))
		})
	})

	Context("Spans", func() {
		It("can save and retrieve single span", func(ctx SpecContext) {
			_, span := getSingleSpan(ctx)
			span.End()
			Expect(sendSpans(ctx, namespaceID, apiKey, span)).To(Succeed())
			queryForSpan(ctx, span, Succeed())
		})

		It("can save and retrieve fragmented spans", func(ctx SpecContext) {
			By("create chain of nested spans")
			rootContext, rootSpan := getSingleSpan(ctx)
			rootSpan.SetAttributes(attribute.String("fragment", "root"))
			rootSpan.AddEvent("emitting root test span")

			childContext, childSpan := tracer.Start(rootContext,
				"test_"+getRandomSpanLabel(),
				trace.WithSpanKind(trace.SpanKindServer))
			childSpan.SetAttributes(attribute.String("fragment", "child"))
			childSpan.AddEvent("emitting child test span")

			_, childSiblingSpan := tracer.Start(rootContext,
				"test"+getRandomSpanLabel(),
				trace.WithLinks(trace.Link{SpanContext: childSpan.SpanContext()}),
				trace.WithSpanKind(trace.SpanKindProducer))

			_, grandchildSpan := tracer.Start(childContext, "test_"+getRandomSpanLabel())
			grandchildSpan.SetAttributes(attribute.String("fragment", "grandchild"))
			grandchildSpan.AddEvent("emitting grandchild test span")
			grandchildSpan.RecordError(errors.New("error"))
			grandchildSpan.SetStatus(codes.Error, "error")

			grandchildSpan.End()
			childSpan.End()
			rootSpan.End()
			childSiblingSpan.End()

			getTraceID := func(span trace.Span) trace.TraceID {
				return readSpan(span).SpanContext().TraceID()
			}

			traceID := getTraceID(rootSpan)
			// ensure all the trace ids are identical
			Expect(
				[]trace.TraceID{getTraceID(rootSpan), getTraceID(childSpan), getTraceID(grandchildSpan)}).
				Should(HaveEach(traceID))

			By("sending spans separately")
			Expect(sendSpans(ctx, namespaceID, apiKey, rootSpan)).To(Succeed())
			Expect(sendSpans(ctx, namespaceID, apiKey, childSpan, childSiblingSpan)).To(Succeed())
			Expect(sendSpans(ctx, namespaceID, apiKey, grandchildSpan)).To(Succeed())

			client, close := jaegerClient(ctx)
			defer close()

			By("query by trace id")
			var resourceSpans []*tracev1.ResourceSpans
			// allow 1m for spans to appear
			Eventually(func(g Gomega) {
				rcv, err := client.GetTrace(ctx, &jaegerapiv3.GetTraceRequest{
					TraceId: traceID.String(),
				})
				g.Expect(err).NotTo(HaveOccurred())
				chunk, err := rcv.Recv()
				g.Expect(err).NotTo(HaveOccurred())
				g.Expect(chunk.ResourceSpans).To(HaveLen(4))
				resourceSpans = chunk.ResourceSpans
			}).WithTimeout(time.Minute).Should(Succeed())

			By("check returned spans match ours and have correct parent references")
			spans := map[string]*tracev1.Span{}
			for _, s := range resourceSpans {
				Expect(s.InstrumentationLibrarySpans).To(HaveLen(1))
				Expect(s.InstrumentationLibrarySpans[0].Spans).To(HaveLen(1))
				span := s.InstrumentationLibrarySpans[0].Spans[0]
				spans[hex.EncodeToString(span.SpanId)] = span
			}

			// expected span chain to verify parent id references
			spanChain := readSpans(rootSpan, childSpan, grandchildSpan)
			for _, s := range spanChain {
				sID := s.SpanContext().SpanID().String()
				gotSpan, ok := spans[sID]
				Expect(ok).To(BeTrue(), "span map lookup")
				if s.Parent().HasSpanID() {
					Expect(hex.EncodeToString(gotSpan.ParentSpanId)).To(Equal(s.Parent().SpanID().String()))
				}
				verifySpan(s, gotSpan)
			}
		})
	})

})

func readSpans(span ...trace.Span) []sdktrace.ReadOnlySpan {
	spans := make([]sdktrace.ReadOnlySpan, len(span))
	for i, s := range span {
		spans[i] = readSpan(s)
	}
	return spans
}

func readSpan(span trace.Span) sdktrace.ReadOnlySpan {
	ro, ok := span.(sdktrace.ReadOnlySpan)
	Expect(ok).To(BeTrue(), "convert to readonly span")
	return ro
}

func getRandomSpanLabel() string {
	return common.RandStringRunes(10)
}

func randItem[A any](items ...A) A {
	//#nosec
	return items[rand.Intn(len(items))]
}

// verify a generated OTEL span against one from the jaeger grpc API.
func verifySpan(a sdktrace.ReadOnlySpan, b *tracev1.Span) {
	By("verify span properties")
	tid := a.SpanContext().TraceID()
	Expect(tid[:]).To(BeEquivalentTo(b.TraceId), "trace id")
	Expect(a.Name()).To(Equal(b.Name), "span name")

	sid := a.SpanContext().SpanID()
	Expect(sid[:]).To(BeEquivalentTo(b.SpanId), "span id")

	Expect(a.SpanKind()).To(BeNumerically("==", b.Kind), "span kind")

	if a.Parent().IsValid() {
		pid := a.Parent().SpanID()
		Expect(pid[:]).To(BeEquivalentTo(b.ParentSpanId), "parent span id")
	}

	Expect(a.StartTime().UnixNano()).To(BeEquivalentTo(b.StartTimeUnixNano), "start time")
	Expect(a.EndTime().UnixNano()).To(BeEquivalentTo(b.EndTimeUnixNano), "end time")

	compareAttributes := func(a []attribute.KeyValue, b []*jaegercommonv1.KeyValue) {
		By("verify attributes")
		attrA := []string{}
		for _, kv := range b {
			attrA = append(attrA, fmt.Sprintf("%s=%s", kv.Key, kv.Value.GetStringValue()))
		}
		attrB := []string{}
		for _, kv := range a {
			attrB = append(attrB, fmt.Sprintf("%s=%s", kv.Key, kv.Value.AsString()))
		}
		Expect(attrA).To(ContainElements(attrB), "attributes")
	}

	compareAttributes(a.Attributes(), b.Attributes)

	// jaeger storage doesn't store a main event name so we just compare attributes.
	// the OTEL event name appears as a an attribute on the span.
	jaegerAttributes := []*jaegercommonv1.KeyValue{}
	for _, e := range b.Events {
		jaegerAttributes = append(jaegerAttributes, e.Attributes...)
	}

	otelAttributes := []attribute.KeyValue{}
	for _, e := range a.Events() {
		otelAttributes = append(otelAttributes, attribute.String("event", e.Name))
		otelAttributes = append(otelAttributes, e.Attributes...)
	}

	compareAttributes(otelAttributes, jaegerAttributes)

	type linkKey struct {
		traceID    string
		spanID     string
		traceState string
	}
	links := map[linkKey][]*jaegercommonv1.KeyValue{}
	for _, l := range b.Links {
		links[linkKey{
			traceID:    hex.EncodeToString(l.TraceId),
			spanID:     hex.EncodeToString(l.SpanId),
			traceState: l.TraceState,
		}] = l.Attributes
	}
	for _, l := range a.Links() {
		k := linkKey{
			traceID:    l.SpanContext.TraceID().String(),
			spanID:     l.SpanContext.SpanID().String(),
			traceState: l.SpanContext.TraceState().String(),
		}
		Expect(links).To(HaveKey(k))
		compareAttributes(l.Attributes, links[k])
	}

	if b.Status != nil {
		// otel and jaeger codes are not the same
		switch a.Status().Code {
		case codes.Unset:
			Expect(b.Status.Code).To(Equal(tracev1.Status_STATUS_CODE_UNSET), "status code")
		case codes.Ok:
			Expect(b.Status.Code).To(Equal(tracev1.Status_STATUS_CODE_OK), "status code")
		case codes.Error:
			Expect(b.Status.Code).To(Equal(tracev1.Status_STATUS_CODE_ERROR), "status code")
		}
	}

	if a.Status().Code > codes.Unset {
		Expect(b.Status).NotTo(BeNil(), "status should be set")
	}

	Expect(a.SpanContext().TraceState().String()).To(Equal(b.TraceState))
}
