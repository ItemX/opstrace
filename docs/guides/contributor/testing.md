# Testing

Each project should have a sensible unit test coverage, integration tests and end-to-end tests where sensible.

Unit tests are a mix of typical Go tests, [testify](https://github.com/stretchr/testify) assertions/suites
and for the operators we have some [Ginkgo](https://onsi.github.io/ginkgo/) [controller tests](https://book.kubebuilder.io/cronjob-tutorial/writing-tests.html).

End-to-end tests use Ginkgo as it provides some very useful constructs to check for declarative state.
We use Ginkgo V2 with the CLI as it provides more robust [interrupt and timeout handling](https://onsi.github.io/ginkgo/MIGRATING_TO_V2#interrupt-behavior).

Each project `Makefile` has a `unit-tests` target to run project specific unit tests.

## Running E2E tests

You can choose to run e2e tests against a GCP account, which will set up all the infra for you, or via your own existing Kubernetes cluster (`kind` mode).

See [e2e test README](../../../test/e2e/README.md) for more detailed instructions.

## Test cycle

When updating the various components you can run `make docker-build kind-load-docker-images deploy` to build all containers,
load them into your `kind` cluster and then redeploy the scheduler.

You may then proceed to manually test changes and run/update the e2e tests.

> NOTE: On M1 Mac loading images to Kind cluster may not work, see [this](https://github.com/kubernetes-sigs/kind/issues/2772) upstream issue.

The local kind cluster is set up to route ingress to localhost.
