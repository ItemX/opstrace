package tests

import (
	"os"
	"os/exec"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

var _ = Describe("frontend", Ordered, Serial, func() {

	cluster := &schedulerv1alpha1.Cluster{}
	beforeAllEnsureCluster(cluster)

	beforeAllConfigureGitLab()

	It("should run the frontend test suite", func(ctx SpecContext) {
		cmd := exec.CommandContext(ctx, "make", "-C", "../frontend", "run-suite")
		cmd.Env = os.Environ()
		cmd.Env = append(cmd.Env,
			"GITLAB_DOMAIN="+infraConfig.GitLabHost,
			"GITLAB_USERNAME="+gitLab.user.Username,
			"GITLAB_PASSWORD="+gitLab.userPassword,
			"SENTRY_DSN="+gitLab.errorTrackingConfig.clientKey.SentryDsn,
			"ERROR_TRACKING_PROJECT_PATH="+gitLab.errorTrackingConfig.project.Path,
		)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		Expect(cmd.Run()).To(Succeed())
	})
})
