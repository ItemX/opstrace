package errortracking

import (
	"errors"
	"fmt"
	"os"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
	"sigs.k8s.io/yaml"
)

/*
---
ApiVersion: 1
sizings:
  default:
    et_api_reads:
    et_api_writes:
    ...
  xl:
    ...
  xxl:
    ...
bindings:
  groups:
    22: xl
  projects:
    1: xxl
*/

var ErrInvalidLimitsConfiguration = errors.New("limits configuration is invalid")

func verifyConfig(in *types.LimitsConfig) error {
	if in.APIVersion != 1 {
		return fmt.Errorf("%w: usupported format of the limits config: %d", ErrInvalidLimitsConfiguration, in.APIVersion)
	}

	if _, ok := in.Sizings["default"]; !ok {
		return fmt.Errorf("%w: default limits configuration is not defined", ErrInvalidLimitsConfiguration)
	}

	for k, v := range in.Sizings {
		if v.APIReadsLimit == 0 || v.APIWritesLimit == 0 {
			return fmt.Errorf("%w: invalid limits for sizing %s", ErrInvalidLimitsConfiguration, k)
		}
	}

	for k, v := range in.Bindings.Groups {
		if _, ok := in.Sizings[v]; !ok {
			return fmt.Errorf(
				"%w: binding for group %d does not point to existing sizing %s",
				ErrInvalidLimitsConfiguration, k, v,
			)
		}
	}

	for k, v := range in.Bindings.Projects {
		if _, ok := in.Sizings[v]; !ok {
			return fmt.Errorf(
				"%w: binding for project %d does not point to existing sizing %s",
				ErrInvalidLimitsConfiguration, k, v,
			)
		}
	}

	return nil
}

func ReadConfig(path string) (*types.LimitsConfig, error) {
	blob, err := os.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("error while reading limits file: %w", err)
	}
	res := new(types.LimitsConfig)

	err = yaml.Unmarshal(blob, res)
	if err != nil {
		return nil, fmt.Errorf("error occurred while parsing limits yaml: %w", err)
	}

	err = verifyConfig(res)
	if err != nil {
		return nil, fmt.Errorf("limits configuration is invalid: %w", err)
	}

	return res, nil
}
