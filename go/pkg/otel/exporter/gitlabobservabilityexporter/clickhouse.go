package gitlabobservabilityexporter

import (
	"fmt"

	"github.com/ClickHouse/clickhouse-go/v2"
)

type database struct {
	conn clickhouse.Conn
}

func newDB(cfg *config) (*database, error) {
	dbOpts, err := clickhouse.ParseDSN(cfg.ClickHouseDSN)
	if err != nil {
		return nil, fmt.Errorf("parsing clickhouse DSN: %w", err)
	}

	dbOpts.MaxOpenConns = cfg.ClickHouseMaxOpenConns
	dbOpts.MaxIdleConns = cfg.ClickHouseMaxIdleConns
	dbOpts.ConnMaxLifetime = cfg.ClickHouseConnMaxLifetime
	dbOpts.Compression = &clickhouse.Compression{Method: clickhouse.CompressionLZ4}

	conn, err := clickhouse.Open(dbOpts)
	if err != nil {
		return nil, fmt.Errorf("clickhouse db open: %w", err)
	}

	return &database{conn: conn}, nil
}
