//go:build darwin && cgo

package common

import "github.com/rjeczalik/notify"

const (
	eventMask = notify.Create | notify.Write | notify.Rename | notify.FSEventsInodeMetaMod
)
