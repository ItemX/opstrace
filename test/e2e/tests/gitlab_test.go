package tests

import (
	"context"
	"crypto/tls"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/davecgh/go-spew/spew"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	gogitlab "github.com/xanzy/go-gitlab"
	gocommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
)

// gitLab specific configuration for the test run
var gitLab struct {
	once sync.Once
	// random string used for naming GitLab resources
	randStr string
	// client from go-gitlab configured with the test instance and credentials
	client *gogitlab.Client
	// httpClient is configured for general http access
	httpClient *http.Client
	// user is set up with the credentials for the test run
	user         *gogitlab.User
	userPassword string
	// error tracking configuration for the test run
	errorTrackingConfig struct {
		project      *gogitlab.Project
		clientKey    *gogitlab.ErrorTrackingClientKey
		groupToken   *gogitlab.GroupAccessToken
		listEndpoint string
	}
}

// beforeAllConfigureGitLab sets up the GitLab client other elements needed for
// all our feature tests.
// Lazy loading is used to avoid setting up the client if it's not needed.
func beforeAllConfigureGitLab() {
	BeforeAll(func(ctx SpecContext) {
		gitLab.once.Do(func() {
			initGitLab(ctx)
			initErrorTracking()
		})
	})
}

// Init GitLab group/project and sentry related settings.
func initGitLab(ctx context.Context) {
	Expect(infraConfig.GitLabAdminToken).NotTo(BeEmpty())
	// Test asserts actions using gitlab API like enabling error tracking which shouldn't be done on prod env yet.
	Expect(strings.Index(infraConfig.GitLabHost, "gitlab.com")).To(Equal(-1))

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true, //nolint:gosec
		},
	}
	By("Creating a GitLab API client")
	httpClient := &http.Client{
		Transport: tr,
		Timeout:   time.Second * 10,
	}
	glClient, err := gogitlab.NewClient(
		infraConfig.GitLabAdminToken,
		gogitlab.WithBaseURL("https://"+infraConfig.GitLabHost),
		gogitlab.WithHTTPClient(httpClient),
		gogitlab.WithRequestOptions(gogitlab.WithContext(ctx)),
		gogitlab.WithCustomLogger(&glClientLogger{}),
	)
	Expect(err).ToNot(HaveOccurred())

	By("Enabling error tracking on GitLab via application settings")
	req, err := glClient.NewRequest(http.MethodPut, "application/settings", nil, nil)
	Expect(err).ToNot(HaveOccurred())

	q := req.URL.Query()
	q.Add("error_tracking_enabled", "true")
	q.Add("error_tracking_api_url", fmt.Sprintf("https://%s:443/", infraConfig.GOBHost))
	req.URL.RawQuery = q.Encode()

	_, err = glClient.Do(req, nil)
	Expect(err).ToNot(HaveOccurred())

	// used for a name for all the things
	randStr, err := gocommon.RandStringASCIIBytes(5)
	Expect(err).ToNot(HaveOccurred())

	By("Creating a GitLab user")
	// generate a password that GitLab will accept.
	// it doesn't like pure alpha passwords, even if random.
	userPassword := ""
	for i := 0; i < len(randStr); i++ {
		userPassword += fmt.Sprintf("%d%s", i, string(randStr[i]))
	}
	user, _, err := glClient.Users.CreateUser(&gogitlab.CreateUserOptions{
		Email:            gogitlab.String(fmt.Sprintf("test-%s@%s", randStr, infraConfig.GitLabHost)),
		Password:         gogitlab.String(userPassword),
		Username:         gogitlab.String(randStr),
		Name:             gogitlab.String(randStr),
		SkipConfirmation: gogitlab.Bool(true),
	})
	Expect(err).ToNot(HaveOccurred())

	gitLab.randStr = randStr
	gitLab.client = glClient
	gitLab.httpClient = httpClient
	gitLab.user = user
	gitLab.userPassword = userPassword
}

// Init error tracking related settings.
// Requires initGitLab to be called first.
func initErrorTracking() {
	randStr := gitLab.randStr
	glClient := gitLab.client
	By("Setting up GitLab Error Tracking")
	By("Create a new group")
	groupName := fmt.Sprintf("test-group-%s", randStr)
	groupPath := fmt.Sprintf("group-%s", randStr)
	group, _, err := glClient.Groups.CreateGroup(&gogitlab.CreateGroupOptions{
		Name: &groupName,
		Path: &groupPath,
	})
	Expect(err).ToNot(HaveOccurred())

	By("Adding the user to the group")
	_, _, err = glClient.GroupMembers.AddGroupMember(group.ID, &gogitlab.AddGroupMemberOptions{
		UserID:      &gitLab.user.ID,
		AccessLevel: gogitlab.AccessLevel(gogitlab.DeveloperPermissions),
	})
	Expect(err).ToNot(HaveOccurred())

	By("Creating a new project and enabling error tracking on it")
	Expect(err).ToNot(HaveOccurred())
	projectName := fmt.Sprintf("errortracking-test-%s", randStr)
	project, _, err := glClient.Projects.CreateProject(&gogitlab.CreateProjectOptions{
		Name:        &projectName,
		NamespaceID: &group.ID,
	})
	Expect(err).ToNot(HaveOccurred())

	// Enable per-project error tracking and get the Sentry URL from settings and test against it.
	// The go-gitlab client doesn't support this API yet, so we use the raw client.
	req, err := glClient.NewRequest(
		http.MethodPut,
		fmt.Sprintf("projects/%d/error_tracking/settings", project.ID), nil, nil,
	)
	Expect(err).ToNot(HaveOccurred())
	q := req.URL.Query()
	q.Add("active", "true")
	q.Add("integrated", "true")
	req.URL.RawQuery = q.Encode()

	_, err = glClient.Do(req, nil)
	Expect(err).ToNot(HaveOccurred())

	By("Creating project SENTRY_DSN key")
	key, _, err := glClient.ErrorTracking.CreateClientKey(project.ID, nil)
	Expect(err).ToNot(HaveOccurred())

	By("Setting up Observability Token for the group")

	scopes := []string{"api", "read_api", "read_observability", "write_observability"}
	expiresAt := gogitlab.ISOTime(time.Now().Add(time.Hour * 24))
	tokenName := fmt.Sprintf("test-e2e-%s", randStr)
	token, _, err := glClient.GroupAccessTokens.CreateGroupAccessToken(
		group.ID,
		&gogitlab.CreateGroupAccessTokenOptions{
			Name:      &tokenName,
			Scopes:    &scopes,
			ExpiresAt: &expiresAt,
		},
	)
	Expect(err).ToNot(HaveOccurred())

	listErrorEndpoint := fmt.Sprintf("https://%s:443/errortracking/api/v1/projects/%d/errors", infraConfig.GOBHost, project.ID)

	gitLab.errorTrackingConfig.project = project
	gitLab.errorTrackingConfig.clientKey = key
	gitLab.errorTrackingConfig.groupToken = token
	gitLab.errorTrackingConfig.listEndpoint = listErrorEndpoint

	s := spew.Sdump(struct {
		ProjectPath        string
		SentryDSN          string
		ListErrorsEndpoint string
	}{
		ProjectPath:        project.PathWithNamespace,
		SentryDSN:          key.SentryDsn,
		ListErrorsEndpoint: listErrorEndpoint,
	})
	By("using error tracking configuration: " + s)
}

type glClientLogger struct{}

func (l *glClientLogger) Printf(format string, v ...interface{}) {
	GinkgoWriter.Printf(format+"\n", v...)
}
