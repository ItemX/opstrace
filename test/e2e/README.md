# E2E Tests Suite

## Summary

The E2E test-suite aims to provide test-coverage for all application-features provided as part of the Gitlab Observability Component. It allows for:

* testing platform and/or subsystem-specific features.
* testing provisioning & deprovisioning of the infrastructure that runs the platform via `terraform`/`terragrunt`.
* testing GitLab-specific integrations via a dedicated GitLab instance.

These tests are written using [ginkgo](https://onsi.github.io/ginkgo/).

Most tests have also been written through a single entrypoint - the creation of `Cluster`/`GitlabNamespace` custom resources similar to how our backend is intended to be used.

The environment variable `TEST_IDENTIFIER` is used to identify a given test-run. It is used to name all resources provisioned by the test-suite. It is also used to identify the `terraform` state path in the GCS bucket. This variable is set to the `CI_COMMIT_SHORT_SHA` by default when running inside CI, or `tag-$CI_COMMIT_TAG` when running against a tagged release. The e2e code normalizes this variable, replacing any non DNS compatible characters with `-`.

## Target Environments

The test-suite can be run against multiple infrastructure providers, namely:

### kind (aka bring your own kubeconfig)

This is the default provider.

> When `kind` is used as the target provider, the test-suite does NOT install the `CRD`s required for the scheduler OR the `scheduler-manager` controller itself. These must be installed onto a target cluster beforehand.

The reason for this is to allow the user to decide how the cluster is set up, where the manager runs (could be on host machine, or in CI).

Prior to running tests locally, one must follow instructions in [quickstart](../../docs/quickstart.md) to configure the OAuth application against your GitLab instance.

> Alternatively, use [devvm](https://gitlab.com/gitlab-org/opstrace/devvm) which will set this up for you, 
then [set the KUBECONFIG variable](https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/#set-the-kubeconfig-environment-variable).

If you cannot set "Trusted" in the GitLab OAuth config, you must follow the auth flow at least once
to authorize the application.

Set these environment variables:

```bash
export gitlab_oauth_client_id="$your_oauth_application_client_id"
export gitlab_oauth_client_secret="$your_oauth_application_secret"
```

and optionally:

```bash
export TEST_GOB_HOST="$custom_gob_host" # default localhost
export TEST_GITLAB_HOST="$custom_gitlab_host" # default gitlab.com
```

When running the tests locally at the root of the project, run:

```bash
make kind deploy e2e-test
```

This sets up a `kind` cluster, deploys the scheduler and starts the tests.

If running against an existing cluster, with everything deployed, just do `make e2e-test`.

### GCP

> When `GCP` is used as the target provider, the test-suite does provision all the necessary infrastructure, i.e DNS entries, GCP cloud instance to run a self-managed version of Gitlab & a GKE cluster to host all platform resources with a bunch of additional resources as well.

[Application Default Credentials](https://cloud.google.com/docs/authentication/application-default-credentials) will be assumed when running the tests.

The test-suite uses the same `terraform`/`terragrunt` setup as the one
we use for provisioning our own `prod`/`staging` environments. This helps us test the correctness of our provisioning machinery in addition to the tests targeting application-level correctness.

When running against GCP, the setup needs access to the underlying
credentials/environment variables. At the root of the project, run:

```bash
export TF_VAR_registry_username="$secret"
export TF_VAR_registry_auth_token="$secret"
export TEST_IDENTIFIER="$testSHA"
export TEST_TARGET_PROVIDER="gcp"
make e2e-test-suite
```

## Writing (new) tests

In principle, we try to write tests through external interfaces (e.g. ingress) only when testing user-facing features.

Use the custom resources of the scheduler (`Cluster` and `GitLabNamespace`) to create necessary cluster/tenant state. In cases where applicable, this allows us to set up tenant resources without actually exercising the provisioning step via GitLab OAuth.

## E2E Helpers

The E2E test-suite is built around a series of helpers that abstracts a lot of details around resource provisioning, state management and testing utilities.
It allows for a developer to only focus on the logic that's being tested and the involved test-code.

[Namespace tests](./tests/namespace_test.go) are a good example of how to use these helpers.

## Infrastructure

When using a non-local environment provider, e.g. GCP, the test-suite spins up infrastructure resources (mostly) managed via `terraform`/`terragrunt` right now. The backing `terraform` state is also stored remotely inside a GCS bucket with each test-run using a dedicated path in the bucket, for instance:

```
gs://$BUCKET_NAME/$TEST_IDENTIFIER/$COMPONENT/terraform.tfstate/
```

Therefore, state specific to a given test-run is managed in complete isolation providing system debuggability should one need to access an inventory of all provisioned resources. Note, both `$BUCKET_NAME` and `$PATH_PREFIX` (`TEST_IDENTIFIER` above) are configurable via environment variables as seen [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/main/    terraform/environments/integration/terragrunt.hcl).

Leveraging `terraform` also ensures managed provisioning & deprovisioning of all resources backing our tests. In some cases though, the test-suite can be interrupted abruptly leaving resources orphaned. To mitigate against it, the test-suite has been written in a way to be retried at any point in time.

Since underlying resources are tagged with the `CI_COMMIT_SHORT_SHA` the tests were first run against, when a test run is re-run against the same `CI_COMMIT_SHORT_SHA` (via `TEST_IDENTIFIER`) but with a non-breaking `scheduler` image, it attempts to setup & teardown the same set of resources again. This ensures that the suite runs to completion and eventually cleanup any orphaned resources. This makes it trivial for this feature to be leveraged inside an "out-of-band reaper script" to ensure that the project stays in good hygiene at all times.

This also allows for a non-local provider to be targeted outside of your CI infrastructure by simply providing the right environment variables and/or project credentials to run the tests against. Refer to using [GCP](#gcp) section above.

> Set `TEST_SKIP_CLEANUP` env var to any value to skip cleanup of resources after a test run.

## Manual Destroy

It's common for the cleanup phase to fail due to broken `Cluster` CR state, if a bug has been introduced,
or if for some reason the kustomize resource state is out of sync with the actual state of the cluster.

In this case, you can manually destroy the cluster by running:

```bash
make destroy
```

This is also available as a GitLab pipeline job, which can be triggered through the web UI.
Set `TEST_IDENTIFIER_OVERRIDE` to override the default identifier in the pipeline job.

Set `TEST_IDENTIFIER` when running locally to target a particular test run.

## Debugging Terraform

To use `terraform` or `terragrunt` to inspect the state of the infrastructure, you'll first need all the relevant environment variables.

You can use the `make` target `printenv` to print all the relevant environment variables, assuming you have first set the correct `TEST_IDENTIFIER` var.

You can export all the environment variables to your shell by running:

```bash
export $(make printenv | xargs)
```
