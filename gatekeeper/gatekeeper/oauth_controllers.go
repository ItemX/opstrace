package gatekeeper

import (
	"context"
	"crypto/rand"
	"crypto/tls"
	"fmt"
	"math/big"
	"net/http"
	"net/url"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"golang.org/x/oauth2"
)

const (
	letterBytes          = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	sessionStateKey      = "oauth_state"
	redirectOnSuccessKey = "redirect_on_success"
)

// Create a random string to be used for the state parameter in oauth2.
func generateOpaqueStateParameter() (string, error) {
	b := make([]byte, 36)
	for i := range b {
		num, err := rand.Int(rand.Reader, big.NewInt(int64(len(letterBytes))))
		if err != nil {
			return "", err
		}
		b[i] = letterBytes[num.Int64()]
	}
	return string(b), nil
}

// Initiates the oauth2 authentication with GitLab.
func HandleAuthStart(ctx *gin.Context) {
	metrics := GetAuthMetrics(ctx)
	metrics.LoginStarts.Inc()

	session := sessions.Default(ctx)
	params := ctx.Request.URL.Query()
	redirectOnSuccess := params.Get("rt")
	isLegacyGOUIAuth := false
	// Remain backwards compatible with GOUI for now. New OAuth doesn't use this parameter
	if redirectOnSuccess != "" {
		isLegacyGOUIAuth = true
	}
	if isLegacyGOUIAuth {
		// Check rt is a valid URI and set it on the session so we can navigate
		// the user back to rt once auth has successfully completed
		redirectTo, err := url.ParseRequestURI(redirectOnSuccess)
		if err != nil {
			ctx.AbortWithError(400, fmt.Errorf("invalid rt parameter in request, must be a valid URI: %w", err))
			return
		}
		// Save the redirect-to so we can redirect there upon successful auth completion
		session.Set(redirectOnSuccessKey, redirectTo.String())
	} else {
		// If a users navigates to legacy GOUI and then in the same session
		// initiates this route with isLegacyGOUIAuth = false,
		// we need to clear the redirect from the session
		session.Delete(redirectOnSuccess)
	}

	// Exit early if we can
	token := GetAuthToken(ctx)
	if token.Valid() {
		handleAuthSuccess(ctx, isLegacyGOUIAuth)
		return
	}

	// Generate an opaque state parameter that we can use
	// in the callback part of the oauth2 flow to validate
	// the callback request matches with this initiation
	state, err := generateOpaqueStateParameter()
	if err != nil {
		if isLegacyGOUIAuth {
			ctx.AbortWithError(500, fmt.Errorf("generate opaque state parameter: %w", err))
		} else {
			log.Error(fmt.Errorf("HandleAuthStart generate opaque state parameter: %w", err))
			negotiateError(ctx, 500, "internal error")
		}
		return
	}
	// Save the oauth state so we can validate it in the callback controller
	session.Set(sessionStateKey, state)

	if err := session.Save(); err != nil {
		if isLegacyGOUIAuth {
			ctx.AbortWithError(500, fmt.Errorf("save session: %w", err))
		} else {
			log.Error(fmt.Errorf("HandleAuthStart save session: %w", err))
			negotiateError(ctx, 500, "internal error")
		}
		return
	}
	auth := GetAuthConfig(ctx)
	// Redirect to the gitlab instance where the trusted
	// oauth2 application is configured
	ctx.Redirect(302, auth.AuthCodeURL(state))
}

// Handles the callback from GitLab during oauth2.
//
// If user already has an active session, this handler will
// update it with the new auth token (in the case where the user
// changed identity in the GitLab instance).
func HandleAuthFinish(ctx *gin.Context) {
	c := context.Background()
	metrics := GetAuthMetrics(ctx)
	auth := GetAuthConfig(ctx)
	session := sessions.Default(ctx)
	redirect := session.Get(redirectOnSuccessKey)
	isLegacyGOUIAuth := false
	if redirect != nil {
		isLegacyGOUIAuth = true
	}
	// Use the authorization code that is pushed to the redirect
	// URL. Exchange will do the handshake to retrieve the
	// initial access token. The HTTP Client returned by
	// conf.Client will refresh the token as necessary.
	params := ctx.Request.URL.Query()
	if !params.Has("code") {
		log.Error("HandleAuthFinish no code parameter in request")
		negotiateError(ctx, 400, "no code parameter in request")
		return
	}

	// Very important to make sure the returned state parameter is equal
	// to the one we created when starting the oauth2 flow
	if !params.Has("state") {
		log.Error("HandleAuthFinish no state parameter in request")
		negotiateError(ctx, 400, "no state parameter in request")
		return
	}

	if params.Get("state") != session.Get(sessionStateKey) {
		log.Error("HandleAuthFinish invalid state parameter")
		negotiateError(ctx, 400, "invalid state parameter")
		return
	}

	if common.LookupEnvOrBool("TLS_SKIP_INSECURE_VERIFY", false) {
		t := &http.Transport{
			// #nosec
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		tc := &http.Client{Transport: t}
		c = context.WithValue(c, oauth2.HTTPClient, tc)
	}

	tok, err := auth.Exchange(c, params.Get("code"))
	if err != nil {
		metrics.LoginFailures.Inc()
		if isLegacyGOUIAuth {
			ctx.AbortWithError(401, err)
		} else {
			log.Error(fmt.Errorf("HandleAuthStart auth exchange: %w", err))
			negotiateError(ctx, 401, "Unauthorized")
		}
		return
	}

	// Set the authToken in the session
	err = SetAuthToken(ctx, tok)
	if err != nil {
		if isLegacyGOUIAuth {
			ctx.AbortWithError(401, fmt.Errorf("set auth token: %w", err))
		} else {
			log.Error(fmt.Errorf("HandleAuthStart set auth token: %w", err))
			negotiateError(ctx, 401, "Unauthorized")
		}
		return
	}

	handleAuthSuccess(ctx, isLegacyGOUIAuth)
}

func handleAuthSuccess(ctx *gin.Context, isLegacyGOUIAuth bool) {
	metrics := GetAuthMetrics(ctx)
	metrics.LoginSuccesses.Inc()

	if isLegacyGOUIAuth {
		session := sessions.Default(ctx)
		// Get the new value
		redirect := session.Get(redirectOnSuccessKey)
		redirectString := ""
		if redirect != nil {
			//nolint:errcheck
			redirectString = redirect.(string)
		}
		ctx.Redirect(302, redirectString)
	} else {
		ctx.HTML(200, "success.gohtml", gin.H{"title": "success", "statuscode": 200})
	}
}
