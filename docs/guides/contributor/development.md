# Development

This document is intended to serve as a general guide for developers contributing to this repo.

## Subcomponents

By `subcomponents` it is assumed here a separate entity that Gitlab Observability Platform (GOP) is composed of.
The list of subcomponents can be obtained by:

```bash
$ grep '\- GOMOD_PATH: ' .gitlab-ci.yml | cut -d: -f 2 | tr -d ' '
go
tenant-operator
scheduler
gatekeeper
clickhouse-operator
```

Each subcomponent is also the name of the directory inside the repository.

## Release/tagging model

Before diving into development and makefiles, it is important to understand how artifacts are released and thus made available.

* we define the `next version of observability stack` in `.gitlab-ci.yml` file, variable `NEXT_RELEASE_TAG`.
  * the purpose of this variable is to help humans quickly identify what +/- given artifact contains
  * we bump this version every time there is a backwards-incompatible change or a milestone was reached
  * the format of this version is SemVer-compatible
* the `version string` used to release observability stack's artifacts depends on several factors:
  * if build was triggered by making a tag in a repo, we use this tag
  * during plain build we append to `the next version of observability stack` the git commit SHA that was used to build artifacts
    * `0.1.0-aafdaaac`
    * unfortunately we can't be here fully SemVer compatible due to [docker limitations](https://github.com/distribution/distribution/issues/1201), hence `-` instead of `+`
    * in case when artifacts were build from repo with uncommitted changes, we add `-dirty` suffix. For example: `0.1.0-aafdaaac.dirty`
  * the `version string` is used for both docker images and artifacts like k8s manifests
* every time an MR is merged to main, all artifacts are build and released using the `next-version`+`git SHA` format

There are few reasons for this schema:

* using git commit SHA:
  * we outsource determining if docker images need rebuilding to git, by using the git commit SHA and `dirty` tag. It is much simpler schema than calculating SHAsums of the source files.
  * the version of manifests is determined easily by the git repo history, no need to maintain a file with image versions inside the repo.
* we use the same `version string` for all artifacts, as GOP is a distribution in a way - we ship everything together as a single product. This simplifies the schema too as there is only one pattern of the version string.
* we want to be able to always use `main` branch's artifacts to launch a cluster, hence we release for every merged MR.
* appending `next version of observability stack` for docker images is meant to simplify trimming images in docker registries. Using git commit SHAsums alone would require correlation with git repository commit history in the long run. Same applies for quickly determining the contents of the artifacts by humans.

## Makefile targets

Below is the list of the Makefile targets that you will most frequently be using:

* global targets - valid only for the root of the repo:
  * `make kind` - create a kind cluster and optionally installs the `telepresence` traffic manager.
* global+local targets - valid both for the root of the repo as well as subcomponents (i.e. can be executed only for specific subcomponent)
  * `make docker-ensure` - pull docker images, and if there is no such image yet - build image for the given `version string`
  * `make docker-build` - rebuild docker images for the given `version string`
  * `make docker-push` - push to upstream docker registry the docker images for the given `version string`
  * `make kind-load-docker-images` - load docker images for the given `version string` from local docker to all kind nodes
  * `make build` - build binaries
  * `make lint` - lint the code
  * `make print-docker-images` - print the `version string` that will be used during next build
* local targets - valid only for the subcomponents
  * `make run` - Run the operator locally, configured to watch resources in the default namespace
  * `make build` - build binaries
  * `make lint` - lint the code
  * `make vet` - vet the code

## Example: Local deployment with scheduler running locally

In this setup, the controller is running locally, on your workstation, and `telepresence` proxies cluster DNS.
See [telepresence](./telepresence.md) doc for more usage details.

> Note: Not all operators will need `telepresence`. Kubernetes API access happens via the kubecontext autodetected from the environment.
> Be sure to only run one instance of each operator, otherwise you'll have two processes competing to reconcile the same resources!

* create a Kind cluster (uses kind.yaml for configuration)

```shell
make kind
```

* install CRDs into Kind cluster

```shell
make install
```

* create the GOP cluster CR for the operator to reconcile. This will also deploy an HA Postgres deployment in the `postgres` namespace for local development

```bash
make deploy-without-manager
```

* run the operator locally, configured to watch resources in the default namespace. Note that resources have already been added by make deploy-without-manager so the operator should have work to do on startup.

```bash
make run
```

You can of course run the deployment using a debugger or similar tools.


## Local deployment with scheduler running in a docker container on the k8s cluster

If you want a solution closer to reality you can instead build your own containers.
There are two options for doing it:

* Pushing docker images to the registry you own
* Using local kind cluster and pre-loading docker-images

First option is suitable for any kind of deployments (cloud, kind, etc...), second one works only for kind clusters.

In both cases it is also necessary to adjust deployments using given image and set `imagePullPolicy` to `Always` so that pods are always using the newest image.
In case of kind one may also opt for simply re-creating kind cluster at this is pretty cheap operation.

### Known Issues on Apple M1/M2 chips

* If you are using `Docker Desktop` alternatives you might face [issues](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1877#note_1066220087) to get Clickhouse Cluster to get into a ready state.
     Currently, the only available fix is to move to `Docker Desktop` as it handles the argv0 [correctly](https://github.com/docker/for-mac/issues/6127).

### Building Platform Specific Images

We use Docker Buildkit by default, set in the imported `Makefile.go.mk` with `export DOCKER_BUILDKIT=1`.

Buildkit provides ample support for multi-platform images.
By default all images will be built to target your host architecture, which is automatically inferred.
See [Docker builder ref](https://docs.docker.com/engine/reference/builder/#automatic-platform-args-in-the-global-scope)
for the args we use in images to perform platform specific builds (e.g. for `go build`).

To override this, set/export `DOCKER_DEFAULT_PLATFORM = your_target_platform` to set a target platform for all the docker builds.
See [docker cli environment variables](https://docs.docker.com/engine/reference/commandline/cli/#environment-variables).
This variable automatically sets `--platform` in the relevant docker commands.

### Ensuring images

There are few things that one needs to keep in mind when developing locally with regard to building docker images, independent of the way one chooses to handle containers.

First, the docker image tag will change every time you make a commit.
This is intentional as we want to be always sure that the images that are used for running the GOP are the ones that were build given source code - better be accurate than fast.
But it may also be annoying as one will need to constantly rebuild all the images, independent of which subcomponent really changed.
This behaviour can be changed by simply setting environment variable `CI_COMMIT_TAG`.
Once set, Makefile scripts will use its value instead, independent of the current Git commit SHA.
From this point on it will be responsibility of the developer to make sure that images are rebuild in case when it is necessary.

Secondly, rebuilding all images takes time.
In order to speed things up when developing, one can checkout current `main` and freeze the `version string` using `CI_COMMIT_TAG` environment variable.
For example:

* obtain the current version string


```bash
$ make -sC gatekeeper print-docker-images
registry.gitlab.com/gitlab-org/opstrace/opstrace/gatekeeper:0.1.0-dbef3085
```

* set the `CI_COMMIT_TAG` environment variable

```bash
export CI_COMMIT_TAG=0.1.0-dbef3085
```

Once this is done, one can pull pre-built images for main

```bash
make docker-ensure
```

All docker images for the this `version string` are going to be downloaded.
Once it is done, one can rebuild image for the given subcomponent by:

```bash
make -C <subcomponent-name> docker-build
```

### Your own registry

This is a simplest approach - one simply uses a docker registry for which they have write-access.
We can instruct GOP to use a custom registry by setting the `DOCKER_IMAGE_REGISTRY` environment variable.

```bash
export DOCKER_IMAGE_REGISTRY=vespian
make ...
```

This results in all the docker images now being pushed to `docker.io/vespian` registry:

```bash
$ make -C gatekeeper -s print-docker-images
vespian/gatekeeper:0.1.0-4cef18c5
```

In order to rebuild image of the given subcomponent, one needs simply to:

```bash
make -C <subcomponent> docker
```

This will build and push the image to registry.

* roll-out deployments that use given container

### Kind load docker-image

This solution assumes that you are using kind in your development environment.

* create a Kind cluster with ingress controller installed

```shell
make kind
```

* perform initial pre-load of all docker containers, if you want to.

```shell
make docker-ensure kind-load-docker-images
```

* do local development/testing, for example:

```bash
make install
nvim ./scheduler/something/foo/bar.go
make deploy
```

* upload the new version of docker image to kind cluster:

```bash
make -C <name of the subcomponent> docker-build kind-load-docker-images
```

* roll-out deployments that use given container

* to be 100% sure that we only use our locally built container, one can edit the kustomize file to never pull the image from a external source. Remember to not commit these changes.

```shell
cat <<EOF >> config/manager/kustomization.yaml

patchesJson6902:
  - target:
      version: v1
      kind: Deployment
      name: scheduler-controller-manager
    patch: |-
      - op: add
        path: /spec/template/spec/containers/0/imagePullPolicy
        value: Never
EOF
```

### Bootstrap `Cluster` and `GitLabNamespace`

If you're using gitlab.com and you have access to our [Opstrace group](https://gitlab.com/gitlab-org/opstrace)
you can use this for local authentication without having to run a GitLab instance.

Create an application in the [GitLab OAuth applications](https://gitlab.com/-/profile/applications) section.
Set the `Callback URL` to `http://localhost/v1/auth/callback`.

Set the following environment variables:

```bash
export gitlab_oauth_client_id=your_client_id
export gitlab_oauth_client_secret=your_client_secret
```

You could use a `.envrc` file with [direnv](https://direnv.net/) to keep these handy.

Then when deploying the scheduler via `make deploy` the correct secrets will be created.
See also the kustomize files in `scheduler/config/examplesecrets`.

Use `make dev-namespace` to create an example `Cluster` CR and `GitLabNamespace` CR for Opstrace.

Once everything is deployed and ready you can visit `localhost/-/14485840` to see the dashboard.

## Skaffold

An alternative to the approaches outlined above is [Skaffold](https://skaffold.dev/).
There is a separate document in the documentation explaining the workflow.
This tool is highly experimental and unsupported though - use at your own risk!
