package main

import (
	"context"
	"fmt"
	"os"

	schedulerapi "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/tests"
	"go.uber.org/zap"
	"k8s.io/client-go/rest"
	"k8s.io/kubectl/pkg/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// Set up infra for the test suite and call all destroy functions.
func main() {
	exitCode := 0
	log, err := zap.NewDevelopment()
	if err != nil {
		panic(err)
	}
	defer func() {
		log.Sync()
		os.Exit(exitCode)
	}()
	logger := log.Sugar()

	c, err := tests.LoadConfig()
	if err != nil {
		logger.Fatalf("failed to load tests config: %v", err)
	}

	logger.Infof("destroying %s test infra for %s", c.TestTarget, c.TestIdentifier)

	infra, err := infra.NewInfrastructure(c.TestTarget, c.TestIdentifier)
	if err != nil {
		logger.Fatalf("failed to init infrastructure: %v", err)
	}

	if _, err := infra.LoadConfiguration(); err != nil {
		logger.Fatalf("failed to load infra config: %v", err)
	}

	// attempt to destroy all resources without returning for errors

	if err := infra.DestroyGitLabInstance(); err != nil {
		exitCode = 1
		logger.Errorf("failed to destroy gitlab instance: %v", err)
	}

	if r, err := infra.GetRestConfig(context.TODO()); err != nil {
		logger.Infof("cannot get rest config: %v", err)
		logger.Info("assume k8s environment is already clean.")
	} else if err = forceDeleteCluster(r); err != nil {
		logger.Errorf("delete cluster CRs: %v", err)
		exitCode = 1
	}

	if err := infra.DestroyK8sCluster(); err != nil {
		exitCode = 1
		logger.Errorf("failed to destroy k8s cluster: %v", err)
	}
}

// find any `Cluster` CRs and delete them forcefully
// by first deleting the finalizers.
func forceDeleteCluster(r *rest.Config) error {
	if err := schedulerapi.AddToScheme(scheme.Scheme); err != nil {
		return fmt.Errorf("add scheduler scheme: %w", err)
	}

	c, err := client.New(r, client.Options{
		Scheme: scheme.Scheme,
	})
	if err != nil {
		return fmt.Errorf("create client: %w", err)
	}

	clusters := &schedulerv1alpha1.ClusterList{}
	if err := c.List(context.TODO(), clusters, client.InNamespace("default")); err != nil {
		return fmt.Errorf("list clusters: %w", err)
	}

	for _, cluster := range clusters.Items {
		cluster := cluster
		cr := &cluster
		cr.SetFinalizers([]string{})
		if err := c.Patch(context.TODO(), cr, client.MergeFrom(cr)); err != nil {
			return fmt.Errorf("patch cluster: %w", err)
		}
		if err := c.Delete(context.TODO(), cr); err != nil {
			return fmt.Errorf("delete cluster: %w", err)
		}
	}
	return nil
}
