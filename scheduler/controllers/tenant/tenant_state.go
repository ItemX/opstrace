package tenant

import (
	"context"
	"fmt"
	"net/url"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type GitLabObservabilityTenantState struct {
	Cluster                *v1alpha1.Cluster
	SchedulerCHCredentials *corev1.Secret
	TenantNamespace        *corev1.Namespace
	TenantCHCredentials    *corev1.Secret
	TenantCollectorConfig  *corev1.ConfigMap
}

type ClickHouseEndpoints struct {
	Http   url.URL
	Native url.URL
}

func NewGitLabObservabilityTenantState() *GitLabObservabilityTenantState {
	return &GitLabObservabilityTenantState{}
}

func (g *GitLabObservabilityTenantState) Read(
	ctx context.Context,
	cr *opstracev1alpha1.GitLabObservabilityTenant,
	client client.Client,
) error {
	if err := g.readCluster(ctx, client); err != nil {
		return err
	}

	if err := g.readSchedulerCHCredentials(ctx, client); err != nil {
		return err
	}

	if err := g.readTenantNamespace(ctx, client, cr); err != nil {
		return err
	}

	if err := g.readTenantCHCredentials(ctx, client, cr); err != nil {
		return err
	}

	if err := g.readTenantCollectorConfig(ctx, client, cr); err != nil {
		return err
	}

	return nil
}

func (g *GitLabObservabilityTenantState) readCluster(
	ctx context.Context,
	c client.Client,
) error {
	// For a tenant, we do not *yet* configure a cluster to which
	// it might belong to with the assumption that there will
	// only be one GOB cluster on a given Kubernetes cluster.
	// Therefore, we workaround by listing clusters and picking
	// the one available for now.
	clusters := new(v1alpha1.ClusterList)
	err := c.List(ctx, clusters)
	if err != nil {
		return err
	}
	if len(clusters.Items) != 1 {
		return fmt.Errorf("unexpected number of Cluster CRs found: %d, expecting 1", len(clusters.Items))
	}
	g.Cluster = &clusters.Items[0]
	return nil
}

func (g *GitLabObservabilityTenantState) readSchedulerCHCredentials(
	ctx context.Context,
	c client.Client,
) error {
	currentState := &corev1.Secret{}
	selector := client.ObjectKey{
		Name:      constants.ClickHouseSchedulerCredentialsSecretName,
		Namespace: "default",
	}
	if err := c.Get(ctx, selector, currentState); err != nil {
		return err
	}
	g.SchedulerCHCredentials = currentState.DeepCopy()
	return nil
}

func (g *GitLabObservabilityTenantState) getSchedulerCHCredentials() *ClickHouseEndpoints {
	return parseCredentialSecret(g.SchedulerCHCredentials)
}

func (g *GitLabObservabilityTenantState) readTenantNamespace(
	ctx context.Context,
	c client.Client,
	cr *opstracev1alpha1.GitLabObservabilityTenant,
) error {
	currentState := &corev1.Namespace{}
	selector := client.ObjectKey{
		Name: cr.Namespace(),
	}
	if err := c.Get(ctx, selector, currentState); err != nil {
		// if error is not found, return early. We'll likely create a new
		// one soon in the next reconcile loop!
		if apierrors.IsNotFound(err) {
			return nil
		}
		return err
	}
	g.TenantNamespace = currentState.DeepCopy()
	return nil
}

func (g *GitLabObservabilityTenantState) readTenantCHCredentials(
	ctx context.Context,
	c client.Client,
	cr *opstracev1alpha1.GitLabObservabilityTenant,
) error {
	currentState := &corev1.Secret{}
	selector := client.ObjectKey{
		Name:      constants.GitLabObservabilityTenantCHCredentialsSecretName,
		Namespace: cr.Namespace(),
	}
	if err := c.Get(ctx, selector, currentState); err != nil {
		// if error is not found, return early. We'll likely create a new
		// one soon in the next reconcile loop!
		if apierrors.IsNotFound(err) {
			return nil
		}
		return err
	}
	g.TenantCHCredentials = currentState.DeepCopy()
	return nil
}

func (g *GitLabObservabilityTenantState) getTenantCHCredentials() *ClickHouseEndpoints {
	return parseCredentialSecret(g.TenantCHCredentials)
}

func (g *GitLabObservabilityTenantState) readTenantCollectorConfig(
	ctx context.Context,
	c client.Client,
	cr *opstracev1alpha1.GitLabObservabilityTenant,
) error {
	currentState := &corev1.ConfigMap{}
	selector := client.ObjectKey{
		Name:      constants.OtelCollectorComponentName,
		Namespace: cr.Namespace(),
	}
	if err := c.Get(ctx, selector, currentState); err != nil {
		// if error is not found, return early. We'll likely create a new
		// one soon in the next reconcile loop!
		if apierrors.IsNotFound(err) {
			return nil
		}
		return err
	}
	g.TenantCollectorConfig = currentState.DeepCopy()
	return nil
}

func parseCredentialSecret(secret *corev1.Secret) *ClickHouseEndpoints {
	if secret == nil {
		return nil
	}

	var (
		httpURL   *url.URL
		nativeURL *url.URL
	)
	if _, ok := secret.Data[constants.ClickHouseCredentialsHTTPEndpointKey]; ok {
		httpURL = common.MustParse(
			string(secret.Data[constants.ClickHouseCredentialsHTTPEndpointKey]),
		)
	}
	if _, ok := secret.Data[constants.ClickHouseCredentialsNativeEndpointKey]; ok {
		nativeURL = common.MustParse(
			string(secret.Data[constants.ClickHouseCredentialsNativeEndpointKey]),
		)
	}

	if httpURL == nil || nativeURL == nil {
		return nil
	}

	return &ClickHouseEndpoints{
		Http:   *httpURL,
		Native: *nativeURL,
	}
}
