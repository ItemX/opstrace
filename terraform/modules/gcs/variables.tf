variable "project_id" {
  type = string
}

# region and zone used for google provider, see also https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference#provider-default-values-configuration
variable "region" {
  type        = string
  default     = ""
  description = "The region to manage resources in. If not set, the zone will be used instead."
}

variable "zone" {
  type        = string
  default     = ""
  description = "The zone referencing the region to manage resources in. If not set, the region will be used instead."
}

variable "bucket_name" {
  type        = string
  description = "name of our bucket"
}

variable "bucket_location" {
  type        = string
  description = "location of our bucket"
  default     = "us"
}

variable "storage_class" {
  type        = string
  default     = "STANDARD"
  description = "Storage class for the bucket objects."
}

variable "service_account_name" {
  type        = string
  description = "SA name that will be used by ClickHouse to manage GCS resources"
}

variable "bucket_regions" {
  type        = list(string)
  description = "List of *2* regions to host the bucket. It should include the region that host the ClickHouse nodes and other with the lowest latency to it."
}
