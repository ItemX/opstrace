package types

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"
)

// DateTimeFormats is the list of supported error event timestamp formats.
var DateTimeFormats = []string{
	time.RFC3339,
	"2006-01-02T15:04:05",
}

// UnixOrRFC3339Time is a helper struct to parse a timestamp sent by the sentry sdk.
// Sentry sdk can inconsistently send timestamps as unix timestamp in milliseconds or
// RFC3339 (ISO 8601) timestamp.
// See https://develop.sentry.dev/sdk/event-payloads/#required-attributes (timestamp).
type UnixOrRFC3339Time struct {
	t time.Time
}

// UnmarshalJSON loads either an RFC3339-formatted time (e.g.
// '"2020-11-03T15:15:09Z"') or unix epoch timestamp (e.g. '1498827360.000').
// However few sdks are missing full RFC3339 format so handles those as well.
func (u *UnixOrRFC3339Time) UnmarshalJSON(data []byte) error {
	var (
		placeholder interface{}
		lastErr     error
	)
	lastErr = json.Unmarshal(data, &placeholder)
	if lastErr != nil {
		return fmt.Errorf("failed to unmarshal timestamp: %w", lastErr)
	}
	switch val := placeholder.(type) {
	case string:
		for _, layout := range DateTimeFormats {
			parsed, err := time.Parse(layout, val)
			if err == nil {
				u.t = parsed
				return nil
			}
			lastErr = err
		}
		if lastErr != nil {
			return fmt.Errorf("failed to parse timestamp: %w", lastErr)
		}
	case float64:
		f, err := strconv.ParseFloat(string(data), 64)
		if err != nil {
			return fmt.Errorf("failed to parse timestamp as float: %w", err)
		}

		i := int64(f * 1000)

		u.t = time.Unix(0, i*int64(time.Millisecond))
	}

	return nil
}

func (u *UnixOrRFC3339Time) Time() time.Time {
	return u.t
}
