.SHELLFLAGS = -euc
SHELL=/bin/bash

# use buildkit for better multi-platform variable support.
# see https://docs.docker.com/engine/reference/builder/#automatic-platform-args-in-the-global-scope
export DOCKER_BUILDKIT=1

# Registry address for pushing images to
export DOCKER_IMAGE_REGISTRY ?= registry.gitlab.com/gitlab-org/opstrace/opstrace/test-with-sentry-sdk

ifdef CI_COMMIT_TAG
  # We are doing a release!
  export DOCKER_IMAGE_TAG := ${CI_COMMIT_TAG}
else
  # Ordinary build
  # We need to use here only the most basic tools, as CI uses lots of different
  # containers, that provide different versions of tool, if at all.
  DOCKER_IMAGE_TAG := $(shell git rev-parse --short=8 HEAD | tr -d '\n')
  ifneq ($(shell git status --porcelain),)
	DOCKER_IMAGE_TAG := $(DOCKER_IMAGE_TAG).dirty
  endif
endif

# DOCKER_IMAGE_NAME is lazily evaluated while traversing the per-dir Makefiles
export DOCKER_IMAGE_NAME = sentry-nodejs
export DOCKER_IMAGE = ${DOCKER_IMAGE_REGISTRY}/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}

.PHONY: docker-build
docker-build:
	docker build \
		-f Dockerfile \
		-t ${DOCKER_IMAGE} \
		.

.PHONY: docker-push
docker-push:
	docker push ${DOCKER_IMAGE}

.PHONY: docker-pull
docker-pull:
	docker pull ${DOCKER_IMAGE}

.PHONY: docker-run
docker-run:
	docker run -e SENTRY_DSN=${SENTRY_DSN} ${DOCKER_IMAGE}
