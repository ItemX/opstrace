CREATE TABLE IF NOT EXISTS gl_jaeger_spans_local ON CLUSTER '{cluster}'
(
    `tenant` LowCardinality(String),
    `timestamp` DateTime64(6, 'UTC') CODEC(Delta(4), ZSTD(1)), -- Precision till microseconds
    `traceID` FixedString(16),
    -- model has the full encoded span
    `model` String CODEC(ZSTD(3))
)
ENGINE = ReplicatedMergeTree('/clickhouse/{cluster}/tables/{shard}/tracing.gl_jaeger_spans_local', '{replica}')
PARTITION BY toYYYYMM(timestamp)
PRIMARY KEY(tenant, traceID)
ORDER BY (tenant, traceID, timestamp)
TTL toDateTime(timestamp) + toIntervalDay(30)
{{- if .RemoteStorageBackend }}
SETTINGS storage_policy = 'gcs_main'
{{- end}}
;
