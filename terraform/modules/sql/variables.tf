variable "project_id" {
  type        = string
  description = "project id"
}

# region and zone used for google provider, see also https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference#provider-default-values-configuration
variable "region" {
  type        = string
  default     = ""
  description = "The region to manage resources in. If not set, the zone will be used instead."
}

variable "zone" {
  type        = string
  default     = ""
  description = "The zone referencing the region to manage resources in. If not set, the region will be used instead."
}

variable "instance_name" {
  type        = string
  default     = "example"
  description = "the Opstrace instance name"
}

variable "vpc_name" {
  type        = string
  description = "Name of the vpc that will be used by the SQL database"
}

variable "vpc_id" {
  type        = string
  description = "Id of the vpc that will be used by the SQL databse"
}

variable "db_max_connections" {
  type        = number
  default     = 300
  description = "SQL databse connection limit"
}
