terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.63.1"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.20.0"
    }

    kustomization = {
      source  = "kbst/kustomization"
      version = "0.9.2"
    }
  }

  required_version = ">= 1.4.0"
}
