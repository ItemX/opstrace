variable "project_id" {
  type        = string
  description = "project id"
}

# region and zone used for google provider, see also https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference#provider-default-values-configuration
variable "region" {
  type        = string
  default     = ""
  description = "The region to manage resources in. If not set, the zone will be used instead."
}

variable "zone" {
  type        = string
  default     = ""
  description = "The zone referencing the region to manage resources in. If not set, the region will be used instead."
}

variable "scheduler_image" {
  type    = string
  default = "SCHEDULER_IMAGE_NAME"
}

variable "postgres_dsn_endpoint" {
  type        = string
  sensitive   = true
  description = "Postgres DSN endpoint to put in a secret"
}

variable "global_labels" {
  default     = {}
  description = "Map consisting key=value pairs added as labels to all resources provisioned by this module"
  type        = map(any)
}

variable "gke_cluster_name" {
  type        = string
  description = "Name of the cluster to which the nodes should be attached to"
}

variable "provision_clickhouse_node_pool" {
  default     = true
  type        = bool
  description = "flag to provision dedicated node pool for clickhouse"
}

variable "ch_nodepool_nodes_number" {
  description = "Number of nodes the dedicated clikhouse node pool should have"
  type        = number
  # minimum quorum size is 3
  default = 3
}

variable "ch_nodepool_machine_type" {
  description = "GCP machine type to use ClickHouse node pools"
  type        = string
  default     = "c2d-highcpu-16"
}

variable "et_ratelimits_config" {
  type        = string
  default     = ""
  description = "Error tracking rate limits config. If not specified the scheduler/config/rate-limits/limits.yaml file is used"
}
