package common

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"math/big"
	"os"
	"regexp"
	"strconv"

	jsonpatch "github.com/evanphx/json-patch/v5"
	rbac "k8s.io/api/rbac/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func generateRandomBytes(n int) []byte {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		panic(err)
	}
	return b
}

func RandStringRunes(s int) string {
	b := generateRandomBytes(s)
	return base64.URLEncoding.EncodeToString(b)
}

func RandStringRunesRaw(s int) string {
	b := generateRandomBytes(s)
	return base64.RawStdEncoding.EncodeToString(b)
}

func ParseFeatureAsBool(features map[string]string, k string) (bool, error) {
	if features != nil {
		if _, ok := features[k]; ok {
			parsed, err := strconv.ParseBool(features[k])
			if err != nil {
				return false, fmt.Errorf("parsing feature as bool: %w", err)
			}
			return parsed, nil
		}
	}
	return false, nil
}

func MergeMap(base map[string]string, overrides map[string]string) map[string]string {
	merged := map[string]string{}
	if overrides == nil {
		if base == nil {
			return map[string]string{}
		}
		return base
	}

	for k, v := range base {
		merged[k] = v
	}
	for k, v := range overrides {
		merged[k] = v
	}
	return merged
}

func ValidateServiceName(string string) bool {
	// a DNS-1035 label must consist of lower case alphanumeric
	//    characters or '-', start with an alphabetic character, and end with an
	//    alphanumeric character (e.g. 'my-name',  or 'abc-123', regex used for
	//    validation is '[a-z]([-a-z0-9]*[a-z0-9])?
	b, err := regexp.MatchString("[a-z]([-a-z0-9]*[a-z0-9])?", string)
	if err != nil {
		return false
	}
	return b
}

func PatchObject(base, overrides interface{}) error {
	if overrides == nil {
		return nil
	}
	overridesSpec, err := json.Marshal(overrides)
	if err != nil {
		// TODO: Add error wrapping to satisfy wrapcheck
		//nolint:wrapcheck
		return err
	}

	baseBytes, err := json.Marshal(base)
	if err != nil {
		//nolint:wrapcheck
		return err
	}
	// Turn the patch into a patch that can be applied to the defaults
	patch, err := jsonpatch.CreateMergePatch([]byte(`{}`), overridesSpec)
	if err != nil {
		//nolint:wrapcheck
		return err
	}
	// Apply the overrides patch to the base
	withOverrides, err := jsonpatch.MergePatch(baseBytes, patch)
	if err != nil {
		//nolint:wrapcheck
		return err
	}
	// Unmarshall into original base
	if err := json.Unmarshal(withOverrides, base); err != nil {
		//nolint:wrapcheck
		return err
	}

	return nil
}

// patch the default spec (as defined in code) with the overrides (as provided by an SRE).
func PatchBytes(defaults, overrides []byte) ([]byte, error) {
	if overrides == nil {
		return defaults, nil
	}
	// Turn the patch into a patch that can be applied to the defaults
	patch, err := jsonpatch.CreateMergePatch([]byte(`{}`), overrides)
	if err != nil {
		// TODO: Add error wrapping to satisfy wrapcheck
		//nolint:wrapcheck
		return defaults, err
	}
	// Apply the overrides patch to the base
	// TODO: Add error wrapping to satisfy wrapcheck
	//nolint:wrapcheck
	return jsonpatch.MergePatch(defaults, patch)
}

func RBACObjectMutator(current, desired client.Object) error {
	switch o := current.(type) {
	case *rbac.Role:
		o.Rules = desired.(*rbac.Role).Rules
	case *rbac.RoleBinding:
		o.RoleRef = desired.(*rbac.RoleBinding).RoleRef
		o.Subjects = desired.(*rbac.RoleBinding).Subjects
	case *rbac.ClusterRole:
		o.Rules = desired.(*rbac.ClusterRole).Rules
	case *rbac.ClusterRoleBinding:
		o.RoleRef = desired.(*rbac.ClusterRoleBinding).RoleRef
		o.Subjects = desired.(*rbac.ClusterRoleBinding).Subjects
	default:
		return fmt.Errorf("unsuported kind: %s", o.GetObjectKind().GroupVersionKind().Kind)
	}
	return nil
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandStringASCIIBytes(n int) (string, error) {
	totalNum := int64(len(letterBytes))
	b := make([]byte, n)
	for i := range b {
		r, err := rand.Int(rand.Reader, big.NewInt(totalNum))
		if err != nil {
			return "", fmt.Errorf("failed to read random integers %w", err)
		}
		b[i] = letterBytes[r.Int64()]
	}

	return string(b), nil
}

func GetEnv(key, fallback string) string {
	v := os.Getenv(key)
	if len(v) == 0 {
		return fallback
	}
	return v
}
