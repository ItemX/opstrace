
const Sentry = require('@sentry/node');


Sentry.init({
  dsn: process.env.SENTRY_DSN,
  debug: true,
  release:"my-javascriprt-project@1.0.0",
  environment: "dev"
});

Sentry.captureException(new Error('Hello from Sentry Javascript SDK'));

