package controllers

import (
	"testing"

	"aqwari.net/xml/xmltree"
	"github.com/stretchr/testify/assert"
	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const defaultKeeperImage = "clickhouse/clickhouse-keeper:latest"
const defaultClientConfig = `<config>
  <user>default</user>
</config>
`

const defaultOpenSSLConfig = `
<openSSL>
  <server>
    <!-- openssl req -subj "/CN=localhost" -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout /etc/clickhouse-server/server.key -out /etc/clickhouse-server/server.crt -->
    <certificateFile>/etc/clickhouse-server/ssl/tls.crt</certificateFile>
    <privateKeyFile>/etc/clickhouse-server/ssl/tls.key</privateKeyFile>
    <caConfig>/etc/clickhouse-server/ssl/ca.crt</caConfig>
    <!-- openssl dhparam -out /etc/clickhouse-server/dhparam.pem 4096 -->
    <dhParamsFile>/etc/clickhouse-server/ssl/dhparam.pem</dhParamsFile>
    <verificationMode>relaxed</verificationMode>
    <loadDefaultCAFile>true</loadDefaultCAFile>
    <cacheSessions>true</cacheSessions>
    <disableProtocols>sslv2,sslv3,tlsv1,tlsv1_1</disableProtocols>
    <preferServerCiphers>true</preferServerCiphers>
  </server>
  <client>
    <loadDefaultCAFile>true</loadDefaultCAFile>
    <caConfig>/etc/clickhouse-server/ssl/ca.crt</caConfig>
    <cacheSessions>true</cacheSessions>
    <disableProtocols>sslv2,sslv3,tlsv1,tlsv1_1</disableProtocols>
    <preferServerCiphers>true</preferServerCiphers>
    <!-- Use for self-signed: <verificationMode>none</verificationMode> -->
    <verificationMode>relaxed</verificationMode>
    <invalidCertificateHandler>
      <!-- Use for self-signed: <name>AcceptCertificateHandler</name> -->
      <name>RejectCertificateHandler</name>
    </invalidCertificateHandler>
  </client>
</openSSL>
`

var (
	s3Endpoint  string = "https://clickhouse-data.s3.eu-west-3.amazonaws.com/root/"
	s3Region    string = "eu-west-3"
	gcsEndpoint string = "https://storage.googleapis.com/clickhouse-gob-dev/dbdata/"
	gcsRegion   string = "eu-west-3"
)

func mkCR(name string, replicas int32, ssl bool, backendType string) *clickhousev1alpha1.ClickHouse {
	var (
		objStorage *clickhousev1alpha1.ClickHouseObjectStorage
	)
	switch backendType {
	case string(clickhousev1alpha1.BackendS3):
		objStorage = &clickhousev1alpha1.ClickHouseObjectStorage{
			Backend:     clickhousev1alpha1.BackendS3,
			EndpointURL: s3Endpoint,
			Region:      &s3Region,
		}
	case string(clickhousev1alpha1.BackendGCS):
		objStorage = &clickhousev1alpha1.ClickHouseObjectStorage{
			Backend:     clickhousev1alpha1.BackendGCS,
			EndpointURL: gcsEndpoint,
			Region:      &gcsRegion,
		}
	default:
	}
	return &clickhousev1alpha1.ClickHouse{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: "foo",
		},
		Spec: clickhousev1alpha1.ClickHouseSpec{
			Image:         "clickhouse/clickhouse-server:latest",
			Replicas:      &replicas,
			InternalSSL:   &ssl,
			ObjectStorage: objStorage,
		},
	}
}

type args struct {
	cr *clickhousev1alpha1.ClickHouse
}

func Test_newClickHouseCluster(t *testing.T) {
	tests := []struct {
		name string
		args args
		// xml parts are not directly compared as the maintenance of this test is too high
		wantCluster *clickHouseCluster
		// checks ConfigXML contains these strings
		wantConfigs []xmltree.Element
		// checks UsersXML contains these strings
		wantUsers []xmltree.Element
		// checks KeeperXML contains these strings
		wantKeeperConfigs []xmltree.Element
		wantErr           bool
	}{
		{
			name: "image name is required",
			args: args{
				&clickhousev1alpha1.ClickHouse{},
			},
			wantErr: true,
		},
		{
			"single replica results in single host",
			args{
				mkCR("test1", 1, false, ""),
			},
			&clickHouseCluster{
				ClickHouse:      mkCR("test1", 1, false, ""),
				KeeperImage:     defaultKeeperImage,
				Shards:          1,
				Replicas:        1,
				Hosts:           []string{"test1-0-0"},
				ClientConfigXML: defaultClientConfig,
			},
			// test entire configs here just as a base case
			[]xmltree.Element{
				// remote servers
				{Content: []byte(`
<remote_servers>
  <test1>
    <shard>
      <internal_replication>true</internal_replication>
      <replica>
        <host>test1-0-0</host>
        <port>9000</port>
        <user>replica</user>
      </replica>
    </shard>
  </test1>
</remote_servers>
`),
				},
				// macros
				{Content: []byte(`
<macros>
  <cluster>test1</cluster>
    <shard>0</shard>
    <!-- Replica is full replica service name, not just index -->
    <replica from_env="HOST"/>
</macros>
`),
				},
				// default_replica_name
				{Content: []byte(`<default_replica_name>{replica}</default_replica_name>`)},
				// default_replica_path
				{Content: []byte(`<default_replica_path>/clickhouse/{cluster}/tables/{shard}/{database}/{table}/{uuid}</default_replica_path>`)},
				// keeper_map_path_prefix for tables that use keeper map engine
				{Content: []byte(`<keeper_map_path_prefix>/keeper_map_tables</keeper_map_path_prefix>`)},
				// listen_host
				{Content: []byte(`<listen_host>::</listen_host>`)},
				// listen_host
				{Content: []byte(`<listen_host>0.0.0.0</listen_host>`)},
				// listen_try
				{Content: []byte(`<listen_try>1</listen_try>`)},
				// logger
				{Content: []byte(`
<logger>
  <!-- Possible levels: https://github.com/pocoproject/poco/blob/develop/Foundation/include/Poco/Logger.h#L105 -->
  <level>information</level>
  <log>/var/log/clickhouse-server/clickhouse-server.log</log>
  <errorlog>/var/log/clickhouse-server/clickhouse-server.err.log</errorlog>
  <size>1000M</size>
  <count>2</count>
  <!-- Default behavior is autodetection (log to console if not daemon mode and is tty) -->
  <console>false</console>
</logger>
`),
				},
				// prometheus
				{Content: []byte(`
<prometheus>
  <endpoint>/metrics</endpoint>
  <port>8001</port>
  <metrics>true</metrics>
  <events>true</events>
  <asynchronous_metrics>true</asynchronous_metrics>
</prometheus>
`),
				},
				// query_log
				{Content: []byte(`
<query_log replace="1">
  <database>system</database>
  <table>query_log</table>
  <engine>Engine = MergeTree PARTITION BY event_date ORDER BY event_time TTL event_date + interval 30 day</engine>
  <flush_interval_milliseconds>7500</flush_interval_milliseconds>
</query_log>
`),
				},
				// query_thread_log
				{Content: []byte(`<query_thread_log remove="1" />`)},
				// part_log
				{Content: []byte(`
<part_log replace="1">
  <database>system</database>
  <table>part_log</table>
  <engine>Engine = MergeTree PARTITION BY event_date ORDER BY event_time TTL event_date + interval 30 day</engine>
  <flush_interval_milliseconds>7500</flush_interval_milliseconds>
</part_log>
`),
				},
				// query_views_log
				{Content: []byte(`
<query_views_log replace="1">
  <table>query_views_log</table>
  <partition_by>toStartOfWeek(event_date)</partition_by>
  <ttl>toStartOfWeek(event_date) + INTERVAL 30 DAY DELETE</ttl>
</query_views_log>
`),
				},
				// metric_log
				{Content: []byte(`
<metric_log replace="1">
  <table>metric_log</table>
  <partition_by>toStartOfWeek(event_date)</partition_by>
  <ttl>toStartOfWeek(event_date) + INTERVAL 30 DAY DELETE</ttl>
</metric_log>
`),
				},
				// asynchronous_metric_log
				{Content: []byte(`
<asynchronous_metric_log replace="1">
  <table>asynchronous_metric_log</table>
  <partition_by>toStartOfWeek(event_date)</partition_by>
  <ttl>toStartOfWeek(event_date) + INTERVAL 30 DAY DELETE</ttl>
</asynchronous_metric_log>
`),
				},
				// trace_log
				{Content: []byte(`
<trace_log replace="1">
  <table>trace_log</table>
  <partition_by>toStartOfWeek(event_date)</partition_by>
  <ttl>toStartOfWeek(event_date) + INTERVAL 30 DAY DELETE</ttl>
</trace_log>
`),
				},
				// zookeeper
				{Content: []byte(`
<zookeeper>
  <node>
    <host>127.0.0.1</host>
    <port>2181</port>
  </node>
  <session_timeout_ms>30000</session_timeout_ms>
  <operation_timeout_ms>10000</operation_timeout_ms>
</zookeeper>
`),
				},
				// distributed_ddl
				{Content: []byte(`
<distributed_ddl>
  <path>/clickhouse/cluster/task_queue/ddl</path>
</distributed_ddl>
`),
				},
			},
			[]xmltree.Element{
				// users
				{Content: []byte(`
<users>
  <!-- default user local pod access only -->
  <default>
    <networks>
      <ip>::1</ip>
      <ip>127.0.0.1</ip>
    </networks>
    <profile>default</profile>
    <quota>default</quota>
  </default>

  <!-- replica user for distributed queries -->
  <replica>
    <no_password/>
    <networks>
      <host>test1-0-0.foo.svc.cluster.local</host>
    </networks>
    <profile>default</profile>
    <quota>default</quota>
  </replica>

  <!--
      FIXME: If we want to allow client access from the operator for e.g. DB maintenance:
      1. Uncomment this section.
      2. Generate a random password and store it in a Secret. Do not regenerate if Secret already exists.
      3. Configure a CLICKHOUSE_OPERATOR_PASSWORD envvar to use that generated Secret.
      4. (optional) Figure out how to restrict access to only the clickhouse-operator pod?
          But this doesn't work if the operator is being run directly from a workstation.
  -->
  <!--
  <clickhouse_operator>
    <networks>
      <ip>127.0.0.1</ip>
      <ip>0.0.0.0/0</ip>
    </networks>
    <password from_env="CLICKHOUSE_OPERATOR_PASSWORD"/>
    <profile>clickhouse_operator</profile>
    <quota>default</quota>
  </clickhouse_operator>
  -->
</users>
`),
				},
				// profiles
				{Content: []byte(`
<profiles>
  <clickhouse_operator>
    <log_queries>0</log_queries>
    <skip_unavailable_shards>1</skip_unavailable_shards>
    <http_connection_timeout>10</http_connection_timeout>
  </clickhouse_operator>

  <default>
    <log_queries>1</log_queries>
    <connect_timeout_with_failover_ms>1000</connect_timeout_with_failover_ms>
    <distributed_aggregation_memory_efficient>1</distributed_aggregation_memory_efficient>
    <parallel_view_processing>1</parallel_view_processing>
  </default>
</profiles>
`),
				},
			},
			[]xmltree.Element{
				// listen_host
				{Content: []byte(`<listen_host>::</listen_host>`)},
				// listen_host
				{Content: []byte(`<listen_host>0.0.0.0</listen_host>`)},
				// listen_try
				{Content: []byte(`<listen_try>1</listen_try>`)},
				// logger
				{Content: []byte(`
<logger>
  <level>information</level>
  <log>/var/log/clickhouse-keeper/clickhouse-keeper.log</log>
  <errorlog>/var/log/clickhouse-keeper/clickhouse-keeper.err.log</errorlog>
  <size>1000M</size>
  <count>2</count>
  <console>false</console>
</logger>
`),
				},
				// max_connections
				{Content: []byte(`<max_connections>4096</max_connections>`)},
				// keeper_servers
				{Content: []byte(`
<keeper_server>
  <tcp_port>2181</tcp_port>
  <server_id from_env="REPLICA"/>

  <log_storage_path>/var/lib/clickhouse/coordination/log</log_storage_path>
  <snapshot_storage_path>/var/lib/clickhouse/coordination/snapshots</snapshot_storage_path>

  <coordination_settings>
    <operation_timeout_ms>10000</operation_timeout_ms>
    <min_session_timeout_ms>10000</min_session_timeout_ms>
    <session_timeout_ms>100000</session_timeout_ms>
    <raft_logs_level>information</raft_logs_level>
    <!-- All settings listed in https://github.com/ClickHouse/ClickHouse/blob/master/src/Coordination/CoordinationSettings.h -->
  </coordination_settings>

  <raft_configuration>
    <server>
      <id>0</id>
      <hostname>test1-0-0</hostname>
      <port>9444</port>
    </server>
  </raft_configuration>
</keeper_server>
`),
				},
			},
			false,
		},
		{
			"multiple replicas results in same number of hosts with fixed shard",
			args{
				mkCR("test2", 3, false, ""),
			},
			&clickHouseCluster{
				ClickHouse:      mkCR("test2", 3, false, ""),
				KeeperImage:     defaultKeeperImage,
				Shards:          1,
				Replicas:        3,
				Hosts:           []string{"test2-0-0", "test2-0-1", "test2-0-2"},
				ClientConfigXML: defaultClientConfig,
			},
			[]xmltree.Element{
				// remote_servers
				{Content: []byte(`
<remote_servers>
  <test2>
    <shard>
      <internal_replication>true</internal_replication>
      <replica>
        <host>test2-0-0</host>
        <port>9000</port>
        <user>replica</user>
      </replica>
      <replica>
        <host>test2-0-1</host>
        <port>9000</port>
        <user>replica</user>
      </replica>
      <replica>
        <host>test2-0-2</host>
        <port>9000</port>
        <user>replica</user>
      </replica>
    </shard>
  </test2>
</remote_servers>
`),
				},
			},
			[]xmltree.Element{
				// networks
				{Content: []byte(`
<networks>
  <host>test2-0-0.foo.svc.cluster.local</host>
  <host>test2-0-1.foo.svc.cluster.local</host>
  <host>test2-0-2.foo.svc.cluster.local</host>
</networks>
`),
				},
			},
			[]xmltree.Element{
				// raft configuration
				{Content: []byte(`
<raft_configuration>
  <server>
    <id>0</id>
    <hostname>test2-0-0</hostname>
    <port>9444</port>
  </server>
  <server>
    <id>1</id>
    <hostname>test2-0-1</hostname>
    <port>9444</port>
  </server>
  <server>
    <id>2</id>
    <hostname>test2-0-2</hostname>
    <port>9444</port>
  </server>
</raft_configuration>
`),
				},
			},
			false,
		},
		{
			"multiple replicas results in same number of hosts with fixed shard and ssl enabled",
			args{
				mkCR("test2", 3, true, ""),
			},
			&clickHouseCluster{
				ClickHouse:  mkCR("test2", 3, true, ""),
				KeeperImage: defaultKeeperImage,
				Shards:      1,
				Replicas:    3,
				Hosts:       []string{"test2-0-0", "test2-0-1", "test2-0-2"},
				InternalSSL: true,
				DHParams: `-----BEGIN DH PARAMETERS-----
MIICCAKCAgEAk/L1+jJH7UjtmnnHvhTPbtTxUpx6E1UWPMq0EfLyprtDNSNd3VT0
6XvPJKg8I1XNzYGyh1WUl8kFWNc8s9WLuIRQVxa2CFt82spTgryU3p8T+gHh9xGM
KfRzcZ+1pqT+QOz4gqtyOjF6+U3Vw82x+ZhU6PGWyRTsm1nLHYPm+OLK+jU/bZmI
2Tt88jNLrfKwFVn85+47adEolBK5gInCbmBiVVHkK+m0CvONMZFrE887M8zsJ4Vd
zlJc9Ef9rbPtLTaa6AEFv2xv0mxqU0c0MolnrtaNoG1g8oB0HMMJic+iQ6zOgEjE
dBhJ70RF5t0P/zlc+uZKyTA2kwvBIhNVNcL4fVg3l8b3Wzra17YahMqtlU+CH8Qh
27P58PRh1EvO91nVXYYiWgfxt6Tb3/XPhE2R7SciNYykhQaP3KmuS5RIx41w2M4B
WQ0ugG5JiSOvco5eHtvC3u9aJxDQ1T4ftdB+LVnFF121egC5tyRVro9KPqziVvmM
PBj/2cxmKM8js8LH7ut0tVDkJDxpb2pT261G3PH7SjX4+PrCBrT664jHcrhrQgYx
SEKYR5eYxt/I1o9B9XH3i/QB1X7bpowCKy508pG0uLYUIEM/DexetI4Jh3oTLv0L
Q/J1TW1SxajqurgRsXSvxqXtsMTm5L6CYmRrtEXueRARIpNZPUW7ISsCAQI=
-----END DH PARAMETERS-----
`,
				ClientConfigXML: `<config>
  <user>default</user>
  <secure>true</secure>
  <openSSL>
    <client>
      <caConfig>/etc/clickhouse-server/ssl/ca.crt</caConfig>
      <loadDefaultCAFile>true</loadDefaultCAFile>
      <cacheSessions>true</cacheSessions>
      <disableProtocols>sslv2,sslv3,tlsv1,tlsv1_1</disableProtocols>
      <preferServerCiphers>true</preferServerCiphers>
    </client>
  </openSSL>
</config>
`,
			},
			[]xmltree.Element{
				// remote_servers
				{Content: []byte(`
<remote_servers>
  <test2>
    <shard>
      <internal_replication>true</internal_replication>
      <replica>
        <host>test2-0-0</host>
        <port>9440</port>
        <secure>1</secure>
        <user>replica</user>
      </replica>
      <replica>
        <host>test2-0-1</host>
        <port>9440</port>
        <secure>1</secure>
        <user>replica</user>
      </replica>
      <replica>
        <host>test2-0-2</host>
        <port>9440</port>
        <secure>1</secure>
        <user>replica</user>
      </replica>
    </shard>
  </test2>
</remote_servers>
`),
				},
				// https_port
				{Content: []byte(`<https_port>8443</https_port>`)},
				// tcp_port_secure
				{Content: []byte(`<tcp_port_secure>9440</tcp_port_secure>`)},
				// interserver_https_port
				{Content: []byte(`<interserver_https_port>9010</interserver_https_port>`)},
				// interserver_http_port
				{Content: []byte(`<interserver_http_port remove="1"/>`)},
				// openSSL
				{Content: []byte(defaultOpenSSLConfig)},
			},
			[]xmltree.Element{
				// replica
				{Content: []byte(`
<replica>
  <no_password/>
  <networks>
    <host>test2-0-0.foo.svc.cluster.local</host>
    <host>test2-0-1.foo.svc.cluster.local</host>
    <host>test2-0-2.foo.svc.cluster.local</host>
  </networks>
  <profile>default</profile>
  <quota>default</quota>
</replica>
`),
				},
			},
			[]xmltree.Element{
				// tcp_port_secure
				{Content: []byte(`<tcp_port_secure>9281</tcp_port_secure>`)},
				// raft configuration
				{Content: []byte(`
<raft_configuration>
  <server>
    <id>0</id>
    <hostname>test2-0-0</hostname>
    <port>9444</port>
    <secure>1</secure>
  </server>
  <server>
    <id>1</id>
    <hostname>test2-0-1</hostname>
    <port>9444</port>
    <secure>1</secure>
  </server>
  <server>
    <id>2</id>
    <hostname>test2-0-2</hostname>
    <port>9444</port>
    <secure>1</secure>
  </server>
</raft_configuration>
`),
				},
				// openSSL
				{Content: []byte(defaultOpenSSLConfig)},
			},
			false,
		},
		{
			"single replica results with s3 support enabled",
			args{
				mkCR("test1", 1, false, string(clickhousev1alpha1.BackendS3)),
			},
			&clickHouseCluster{
				ClickHouse:           mkCR("test1", 1, false, string(clickhousev1alpha1.BackendS3)),
				KeeperImage:          defaultKeeperImage,
				ObjectStorageEnabled: true,
				Shards:               1,
				Replicas:             1,
				Hosts:                []string{"test1-0-0"},
				ClientConfigXML:      defaultClientConfig,
			},
			[]xmltree.Element{
				{Content: []byte(`
<storage_configuration>
  <disks>
    <disk_s3>
      <type>s3</type>
      <endpoint>https://clickhouse-data.s3.eu-west-3.amazonaws.com/root/</endpoint>
      <use_environment_credentials>false</use_environment_credentials>
      <access_key_id from_env="ACCESS_KEY_ID"/>
      <secret_access_key from_env="SECRET_ACCESS_KEY"/>
      <region from_env="STORAGE_REGION"/>
      <!-- <server_side_encryption_customer_key_base64 from_env="SSE_CUSTOMER_KEY"/> -->
      <connect_timeout_ms>10000</connect_timeout_ms>
      <request_timeout_ms>5000</request_timeout_ms>
      <retry_attempts>10</retry_attempts>
      <single_read_retries>4</single_read_retries>
      <min_bytes_for_seek>1000</min_bytes_for_seek>
      <metadata_path>/var/lib/clickhouse/disks/s3/</metadata_path>
      <skip_access_check>false</skip_access_check>
    </disk_s3>
    <s3_cache>
      <type>cache</type>
      <disk>disk_s3</disk>
      <path>/var/lib/clickhouse/disks/s3_cache/</path>
      <max_size>10Gi</max_size>
    </s3_cache>
  </disks>
  <policies>
    <policy_s3_only>
      <volumes>
        <volume_s3>
          <disk>s3_cache</disk>
        </volume_s3>
      </volumes>
    </policy_s3_only>
  </policies>
</storage_configuration>
`),
				},
			},
			nil,
			nil,
			false,
		},
		{
			"keeper image name can be explicitly set",
			args{
				func() *clickhousev1alpha1.ClickHouse {
					ch := mkCR("test", 1, false, "")
					i := "myspecialkeeperimage"
					ch.Spec.KeeperImage = &i
					return ch
				}(),
			},
			&clickHouseCluster{
				ClickHouse: func() *clickhousev1alpha1.ClickHouse {
					ch := mkCR("test", 1, false, "")
					i := "myspecialkeeperimage"
					ch.Spec.KeeperImage = &i
					return ch
				}(),
				KeeperImage:     "myspecialkeeperimage",
				Shards:          1,
				Replicas:        1,
				Hosts:           []string{"test-0-0"},
				ClientConfigXML: defaultClientConfig,
			},
			nil,
			nil,
			nil,
			false,
		},
		{
			"single replica results with gcs support enabled",
			args{
				mkCR("test1", 1, false, string(clickhousev1alpha1.BackendGCS)),
			},
			&clickHouseCluster{
				ClickHouse:           mkCR("test1", 1, false, string(clickhousev1alpha1.BackendGCS)),
				KeeperImage:          defaultKeeperImage,
				ObjectStorageEnabled: true,
				Shards:               1,
				Replicas:             1,
				Hosts:                []string{"test1-0-0"},
				ClientConfigXML:      defaultClientConfig,
			},
			[]xmltree.Element{
				{Content: []byte(`
<storage_configuration>
  <disks>
    <gcs>
      <type>s3</type>
      <endpoint>https://storage.googleapis.com/clickhouse-gob-dev/dbdata/</endpoint>
      <access_key_id from_env="ACCESS_KEY_ID"/>
      <secret_access_key from_env="SECRET_ACCESS_KEY"/>
      <region from_env="STORAGE_REGION"/>
      <metadata_path>/var/lib/clickhouse/disks/gcs/</metadata_path>
      <support_batch_delete>false</support_batch_delete>
    </gcs>
    <gcs_cache>
      <type>cache</type>
      <disk>gcs</disk>
      <path>/var/lib/clickhouse/disks/gcs_cache/</path>
      <max_size>10Gi</max_size>
    </gcs_cache>
  </disks>
  <policies>
    <gcs_main>
      <volumes>
        <default>
          <disk>default</disk>
          <max_data_part_size_bytes>10000000</max_data_part_size_bytes>
          <!-- keep_free_space_bytes !-->
        </default>
        <main>
          <disk>gcs_cache</disk>
        </main>
      </volumes>
      <move_factor>0.99</move_factor>
    </gcs_main>
  </policies>
</storage_configuration>
`),
				},
			},
			nil,
			nil,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := newClickHouseCluster(tt.args.cr)
			if (err == nil) == tt.wantErr {
				t.Errorf("wantErr: %v, err: %v", tt.wantErr, err)
			}

			if err != nil {
				assert.Nil(t, got)
				return
			}

			checkXML := func(got string, want []xmltree.Element, message string) {
				root, err := xmltree.Parse([]byte(got))
				if err != nil {
					t.Fatalf("[%s] got invalid xml: %s", message, got)
				}
				// t.Logf("[%s] parsed got xml: %s\n", message, root.Content)
				for _, element := range want {
					el, _ := xmltree.Parse(element.Content)
					// t.Logf("[%s] searching for %s\n", message, el.Name.Local)
					searched := root.SearchFunc(func(e *xmltree.Element) bool {
						return e.Name.Local == el.Name.Local
					})
					if len(searched) == 0 {
						t.Logf("[%s] found no elements when searching for %s\n", message, el.Name.Local)
						continue
					}
					if len(searched) > 1 {
						t.Logf("[%s] found multiple elements when searching for %s\n", message, el.Name.Local)
						continue
					}
					if !xmltree.Equal(searched[0], el) {
						t.Fatalf("[%s] xml element different\nexpected: %s\ngot: %s", message, el.Content, searched[0].Content)
					}
				}
			}
			checkXML(got.ConfigXML, tt.wantConfigs, "Config XML")
			checkXML(got.UsersXML, tt.wantUsers, "Users XML")
			checkXML(got.KeeperXML, tt.wantKeeperConfigs, "Keeper XML")
			got.ConfigXML = ""
			got.UsersXML = ""
			got.KeeperXML = ""
			assert.Equal(t, tt.wantCluster, got)
		})
	}
}
