# Error Tracking

Error Tracking allows developers to ingest, view & discover the errors that their application may be generating during their execution.

## Introduction

This guide is meant to provide you with basics of setting up error tracking for your project using examples from different languages.

Error Tracking provided by Gitlab Observability is based on [Sentry SDK](https://docs.sentry.io/). Check [Sentry SDK documentation](https://docs.sentry.io/platforms/) for more thorough examples of how you can use Sentry SDK in your application.

According to Sentry [data model](https://develop.sentry.dev/sdk/envelopes/#data-model), various item types are:

* [Event](https://develop.sentry.dev/sdk/event-payloads/)
* [Transactions](https://develop.sentry.dev/sdk/event-payloads/transaction/)
* [Attachments](https://develop.sentry.dev/sdk/envelopes/#attachment)
* [Session](https://develop.sentry.dev/sdk/sessions/)
* [Sessions](https://develop.sentry.dev/sdk/sessions/)
* [User feedback](https://develop.sentry.dev/sdk/envelopes/#user-feedback) (also known as user report)
* [Client report](https://develop.sentry.dev/sdk/client-reports/)

## Current status

> Beware that this feature assumes the following feature flag - `integrated_error_tracking` to be enabled on your GitLab installation.

For example, on a GDK install:

```shell
cd <PATH_TO_GDK>
gdk rails console
Feature.enable(:integrated_error_tracking)
```

Right now, there's full support for both capturing exceptions (usually provided with `capture_exception` from supported SDKs) and viewing/reading them from inside the GitLab UI.

There's only ingestion support for a few item-types - `exception`, `message` & `session` for all supported SDKs while we're actively working on adding the ability to consume these item-types within the GitLab UI.

Additional feature requests can be added and/or discussed within our project-repository, see this [tracking issue](https://gitlab.com/gitlab-org/gitlab/-/issues/340178) for more details.

## How to(s)

## Enable error tracking for your project

Regardless of the programming language you use, you'll first need to enable error tracking for a given GitLab project.

We will use `gitlab.com` instance in this guide.

> This guide assumes that you already have a project for which you want to enable error tracking. Please refer to Gitlab's documentation in case you need to create a new one.

Please follow the steps below:

* In the given project, go to Settings->Monitor. Expand `Error Tracking` tab:

![MonitorTabPreEnable](../../assets/Monitor_tab-pre-enable.png)

* Enable Error Tracking with Gitlab as backend:

![MonitorTabPostEnable](../../assets/Monitor_tab-post-enable.png)

* Click on `Save Changes` button.

* Copy the DSN string, you can use it for configuring your SDK implementation.

## List errors

Once your application has emitted errors to the Error Tracking API via the Sentry SDK, they should be available under Monitor > Error Tracking tab/section.

![MonitorListErrors](../../assets/Monitor-list_errors.png)

For more detailed documentation please refer to [GitLab's ErrorTracking documentation](https://gitlab.com/help/operations/error_tracking#error-tracking-list).

## Emit errors

### Supported language SDKs & sentry types

In the table below you can see a list of all event types available via Sentry SDK, and if it is currently supported by Gitlab Error Tracking.

| LANGUAGE | TESTED SDK CLIENT/VERSION | ENDPOINT | SUPPORTED ITEM TYPES |
| --- | --- | --- | --- |
| Go | sentry-go/0.20.0 | store | exception, message |
| Java | sentry.java:6.18.1 | envelope | exception, message |
| NodeJS | sentry.javascript.node:7.38.0 | envelope | exception, message |
| PHP | sentry.php/3.18.0 | store | exception, message |
| Python | sentry.python/1.21.0 | envelope | exception, message, session |
| Ruby | sentry.ruby:5.9.0 | envelope | exception, message |
| Rust | sentry.rust/0.31.0 | envelope | exception, message, session |

A detailed version of this matrix can be found [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1737).

## Usage examples

You can also find working samples for all supported language SDKs [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/test/sentry-sdk/testdata/supported-sdk-clients). Each listed program shows a basic example of how to capture exceptions, events or messages with the respective SDK.

For more in-depth documentation please refer to [Sentry SDK's documentation](https://docs.sentry.io/) specific to the used language.

## Rotate generated DSN

Sentry DSN (client key) is a secret and it should not be exposed to the public. In case of a leak you can rotate the Sentry DSN by following these steps:

* [Create an access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) by clicking your profile picture in Gitlab.com. Then choose Preferences and then Access Token. Make sure you add api scope.
* Using the [error tracking api](https://docs.gitlab.com/ee/api/error_tracking.html), create a new Sentry DSN:

```bash
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --header "Content-Type: application/json" \
     "https://gitlab.example.com/api/v4/projects/<your_project_number>/error_tracking/client_keys"
```

* Get the avaialble client keys (Sentry DSNs). Ensure that the newly created Sentry DSN is in place. Then note down the key id of the old client key:

```bash
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/<your_project_number>/error_tracking/client_keys"
```

* Delete the old client key.

```bash
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/<your_project_number>/error_tracking/client_keys/<key_id>"
```

## Debug SDK issues

Majority of languages supported by Sentry expose `debug` option as part of initialization. This can be helpful when debugging issues with sending errors. Apart from that, there are options that could allow outputting JSON before it is send to the API.
