package harness

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
)

const (
	SentryGoExampleImage     = "registry.gitlab.com/gitlab-org/opstrace/opstrace/test-with-sentry-sdk/sentry-go:latest"
	SentryRubyExampleImage   = "registry.gitlab.com/gitlab-org/opstrace/opstrace/test-with-sentry-sdk/sentry-ruby:latest"
	SentryPythonExampleImage = "registry.gitlab.com/gitlab-org/opstrace/opstrace/test-with-sentry-sdk/sentry-py:latest"
	SentryNodeJSExampleImage = "registry.gitlab.com/gitlab-org/opstrace/opstrace/test-with-sentry-sdk/sentry-nodejs:latest"
	SentryJavaExampleImage   = "registry.gitlab.com/gitlab-org/opstrace/opstrace/test-with-sentry-sdk/sentry-java:latest"
	SentryRustExampleImage   = "registry.gitlab.com/gitlab-org/opstrace/opstrace/test-with-sentry-sdk/sentry-rust:latest"
	SentryPHPExampleImage    = "registry.gitlab.com/gitlab-org/opstrace/opstrace/test-with-sentry-sdk/sentry-php:latest"
)

func GetTestContainers(env map[string]string) ([]testcontainers.ContainerRequest, error) {
	defaultContainers := []testcontainers.ContainerRequest{
		{
			Name:       "go",
			Image:      SentryGoExampleImage,
			Env:        env,
			WaitingFor: wait.ForExit().WithExitTimeout(time.Second * 30),
		},
		{
			Name:       "ruby",
			Image:      SentryRubyExampleImage,
			Env:        env,
			WaitingFor: wait.ForExit().WithExitTimeout(time.Second * 30),
		},
		{
			Name:       "python",
			Image:      SentryPythonExampleImage,
			Env:        env,
			WaitingFor: wait.ForExit().WithExitTimeout(time.Second * 30),
		},
		{
			Name:       "nodejs",
			Image:      SentryNodeJSExampleImage,
			Env:        env,
			WaitingFor: wait.ForExit().WithExitTimeout(time.Second * 30),
		},
		{
			Name:       "java",
			Image:      SentryJavaExampleImage,
			Env:        env,
			WaitingFor: wait.ForExit().WithExitTimeout(time.Second * 30),
		},
		{
			Name:       "rust",
			Image:      SentryRustExampleImage,
			Env:        env,
			WaitingFor: wait.ForExit().WithExitTimeout(time.Second * 30),
		},
		{
			Name:       "php",
			Image:      SentryPHPExampleImage,
			Env:        env,
			WaitingFor: wait.ForExit().WithExitTimeout(time.Second * 30),
		},
	}

	// unless we have an override to limit which SDKs to test, return the
	// default list as is
	if _, ok := os.LookupEnv("TEST_THESE_SDKS_ONLY"); !ok {
		return defaultContainers, nil
	}

	fmt.Println("using TEST_THESE_SDKS_ONLY override to filter testcontainers...")
	defaultContainersMap := make(map[string]testcontainers.ContainerRequest)
	for _, c := range defaultContainers {
		defaultContainersMap[c.Name] = c
	}

	containersToRun := make([]testcontainers.ContainerRequest, 0)
	containersWhitelistStr := os.Getenv("TEST_THESE_SDKS_ONLY")
	containersWhitelisted := strings.Split(containersWhitelistStr, ",")
	for _, c := range containersWhitelisted {
		cname := strings.TrimSpace(c) // cleanup provided names, just in case
		if _, ok := defaultContainersMap[cname]; !ok {
			return nil, fmt.Errorf("we do not yet have testcontainers configuration for <%s>", cname)
		}
		containersToRun = append(containersToRun, defaultContainersMap[cname])
	}
	return containersToRun, nil
}
