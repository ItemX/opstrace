variable "project_id" {
  type        = string
  description = "project id"
}

variable "global_labels" {
  default     = {}
  type        = map(any)
  description = "Map consisting key=value pairs added as labels to all resources provisioned by this module"
}


# region and zone used for google provider, see also https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference#provider-default-values-configuration
variable "region" {
  type        = string
  default     = ""
  description = "The region to manage resources in. If not set, the zone will be used instead."
}

variable "zone" {
  type        = string
  default     = ""
  description = "The zone referencing the region to manage resources in. If not set, the region will be used instead."
}

variable "instance_name" {
  type        = string
  default     = "example"
  description = "the Opstrace instance name"
}

variable "num_nodes" {
  type        = number
  default     = 3
  description = "number of gke nodes"
}

variable "provision_clickhouse_node_pool" {
  default     = false
  type        = bool
  description = "flag to provision dedicated node pool for clickhouse"
}

variable "clickhouse_num_nodes" {
  type        = number
  default     = 3
  description = "number of nodes in clickhouse dedicated node pool"
}

variable "kubeconfig_path" {
  type        = string
  default     = ".kubeconfig"
  description = "Path to kubeconfig with temporary credentials"
}

variable "primary_gke_ip_cidr_range" {
  default     = "10.20.0.0/20"
  type        = string
  description = "Primary subnet range to be used with GKE nodes. See https://cloud.google.com/kubernetes-engine/docs/concepts/alias-ips"
}

variable "secondary_gke_ip_cidr_range_pods" {
  type        = string
  default     = "10.16.0.0/14"
  description = "Secondary subnet range to be used with GKE pods. See https://cloud.google.com/kubernetes-engine/docs/concepts/alias-ips"
}

variable "secondary_gke_ip_cidr_range_services" {
  type        = string
  default     = "10.8.0.0/20"
  description = "Secondary subnet range to be used with GKE services. See https://cloud.google.com/kubernetes-engine/docs/concepts/alias-ips"
}

# We rely on consistent egress IPs as they are expected to firewalled against another clusters.
# As such, we use manual mode for IP allocation in Cloud NAT.
# 64,512 ports are available per address and will be divided among `num_nodes` (VM count) of the cluster.
# Maximum number of ports available per VM can be configured via code (currently 4096).
variable "num_manual_ip_addresses_nat" {
  type        = number
  default     = 1
  description = "Number of static IPs that will be reserved for manual allocation for the Cloud NAT. See https://cloud.google.com/nat/docs/ports-and-addresses#port-reservation-examples for details."
}

variable "gke_machine_type" {
  type        = string
  default     = "c2d-highcpu-16"
  description = "Machine type for the node pool of GKE cluster. Should be available in the referenced zone"
}

# A release channel allows organizations to better set their expectation of what is stable.
# GKE’s release channel options include “rapid,” “regular,” and “stable.” This allows you to
# opt for the alpha releases as part of the “rapid” option, “regular” for standard release
# needs and “stable” when the tried-and-tested version becomes available.
#
# See: https://cloud.google.com/kubernetes-engine/docs/concepts/release-channels
variable "release_channel" {
  type        = string
  default     = "REGULAR"
  description = "Release channel specified for managing upgrades for GKE cluster"
}

# We use maintenance windows and maintenance exclusions to control when automatic cluster
# maintenance, such as auto-upgrades, can and cannot occur on GKE clusters.
#
# Default here: Every week, Mon-Thurs, 1000-1600 in your configured TZ
#
# See: https://cloud.google.com/kubernetes-engine/docs/how-to/maintenance-windows-and-exclusions
# See: https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_cluster#nested_maintenance_policy
variable "maintenance_policy_recurring_window" {
  type = map(any)
  default = {
    "start_time" = "2023-06-01T10:00:00Z"
    "end_time"   = "2050-06-01T16:00:00Z"
    "recurrence" = "FREQ=WEEKLY;BYDAY=MO,TU,WE,TH"
  }
  description = "maintenance policy recurring window to control when cluster maintenance can occur"
}