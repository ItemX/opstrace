output "public_ip" {
  value = google_compute_address.gitlab.address
}

output "ssh_username" {
  value     = split("@", data.google_client_openid_userinfo.me.email)[0]
  sensitive = false
}

output "ssh_private_key" {
  value     = tls_private_key.ssh.private_key_openssh
  sensitive = true
}