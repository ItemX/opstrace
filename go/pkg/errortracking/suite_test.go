package errortracking

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	log "github.com/sirupsen/logrus"
)

func TestCommon(t *testing.T) {
	RegisterFailHandler(Fail)
	suiteConfig, reporterConfig := GinkgoConfiguration()

	RunSpecs(t, "Shared code tests", suiteConfig, reporterConfig)
}

var _ = BeforeSuite(func() {
	log.SetLevel(log.DebugLevel)
	log.SetOutput(GinkgoWriter)
})
