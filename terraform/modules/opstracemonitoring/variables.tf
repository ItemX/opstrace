variable "project_id" {
  type        = string
  description = "project id"
}

# region and zone used for gke_auth, one must be set
variable "region" {
  type        = string
  default     = ""
  description = "The region to manage resources in. If not set, the zone will be used instead."
}

variable "zone" {
  type        = string
  default     = ""
  description = "The zone referencing the region to manage resources in. If not set, the region will be used instead."
}

variable "gke_cluster_name" {
  type        = string
  description = "Name of the GKE cluster"
}

variable "gitlab_observability_api_token" {
  description = "GitLab observability API token"
  type        = string
  default     = ""
  sensitive   = true
}

variable "blackbox_exporter_api_key" {
  description = "Grafana generated api key needed for the GOUI probe auth header"
  type        = string
  default     = ""
  sensitive   = true
}

variable "sentry_dsn" {
  description = "Sentry DSN url"
  type        = string
  default     = ""
  sensitive   = true
}

variable "group_error_tracking_endpoint" {
  description = "Group error tracking endpoint"
  type        = string
  default     = ""
}

variable "gatekeeper_probe_url" {
  description = "Gatekeeper probe url"
  type        = string
  default     = ""
}

variable "goui_probe_url" {
  description = "GOUI probe url (DEPRECATED)"
  type        = string
  default     = ""
}

variable "environment" {
  description = "Environment. Example staging, prod etc"
  type        = string
  default     = "prod"
}
