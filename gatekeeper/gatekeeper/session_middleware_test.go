package gatekeeper

// session store tests use github.com/gin-contrib/sessions/tester which creates
// a gin dev server and tests various session setting scenarios.

import (
	"bytes"
	"encoding/gob"
	"testing"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/tester"
	"github.com/go-redis/redismock/v9"
	redis "github.com/redis/go-redis/v9"
)

const (
	sessionKeyRegexp  = sessionKeyPrefix + "([A-Z|0-9])+"
	defaultExpiration = defaultMaxSessionAge * time.Second
)

func storeFactory(c *redis.Client) func(*testing.T) sessions.Store {
	return func(*testing.T) sessions.Store {
		return newRedisSessionStore(c, []byte("secret"))
	}
}

func gobEncode(t *testing.T, v map[interface{}]interface{}) []byte {
	t.Helper()
	buf := new(bytes.Buffer)
	if err := gob.NewEncoder(buf).Encode(v); err != nil {
		t.Fatal(err)
	}
	return buf.Bytes()
}

func TestSessionStore_GetSet(t *testing.T) {
	db, mock := redismock.NewClientMock()

	// session values asserted in the GetSet test
	expectedValues := gobEncode(t, map[interface{}]interface{}{
		"key": "ok",
	})

	mock.MatchExpectationsInOrder(true)
	// new session, sets on first request with key
	mock.Regexp().ExpectSetEx(sessionKeyRegexp, expectedValues, defaultExpiration).SetVal("")
	// gets on second request
	mock.Regexp().ExpectGet(sessionKeyPrefix).SetVal(string(expectedValues))

	tester.GetSet(t, storeFactory(db))

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Error(err)
	}
}

func TestSessionStore_DeleteKey(t *testing.T) {
	db, mock := redismock.NewClientMock()

	// session values asserted in the DeleteKey test
	expectedValues := gobEncode(t, map[interface{}]interface{}{
		"key": "ok",
	})

	// key is deleted as part of test
	emptyValues := gobEncode(t, make(map[interface{}]interface{}))

	mock.MatchExpectationsInOrder(true)
	// sets session on first request with key
	mock.Regexp().ExpectSetEx(sessionKeyRegexp, expectedValues, defaultExpiration).SetVal("")
	// get session on second request
	mock.Regexp().ExpectGet(sessionKeyPrefix).SetVal(string(expectedValues))
	// remove existing key
	mock.Regexp().ExpectSetEx(sessionKeyRegexp, emptyValues, defaultExpiration).SetVal("")
	// get session on third request
	mock.Regexp().ExpectGet(sessionKeyRegexp).SetVal(string(emptyValues))
	// note there is no save here as the session was not modified

	tester.DeleteKey(t, storeFactory(db))

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Error(err)
	}
}

// Flashes are session flash messages.
// We don't use these, but we'll run the test anyway since it is part of the Session interface.
func TestSessionStore_Flashes(t *testing.T) {
	db, mock := redismock.NewClientMock()

	expectedFlashes := gobEncode(t, map[interface{}]interface{}{
		"_flash": []interface{}{"ok"},
	})

	emptyFlashes := gobEncode(t, make(map[interface{}]interface{}))

	mock.MatchExpectationsInOrder(true)
	// set flash on first request
	mock.Regexp().ExpectSetEx(sessionKeyRegexp, expectedFlashes, defaultExpiration).SetVal("")
	// get existing flashes on first request
	mock.Regexp().ExpectGet(sessionKeyRegexp).SetVal(string(expectedFlashes))
	// getting flashes clears them, so we save the empty values
	mock.Regexp().ExpectSetEx(sessionKeyPrefix, emptyFlashes, defaultExpiration).SetVal("")
	// final request gets session and asserts they are empty
	mock.Regexp().ExpectGet(sessionKeyRegexp).SetVal(string(emptyFlashes))

	tester.Flashes(t, storeFactory(db))

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Error(err)
	}
}

func TestSessionStore_Clear(t *testing.T) {
	db, mock := redismock.NewClientMock()

	emptyValues := gobEncode(t, make(map[interface{}]interface{}))

	mock.MatchExpectationsInOrder(true)
	// tester creates values but immediately clears them
	mock.Regexp().ExpectSetEx(sessionKeyPrefix, emptyValues, defaultExpiration).SetVal("")
	// get and verify empty session
	mock.Regexp().ExpectGet(sessionKeyRegexp).SetVal(string(emptyValues))

	tester.Clear(t, storeFactory(db))

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Error(err)
	}
}

func TestSessionStore_Options(t *testing.T) {
	db, mock := redismock.NewClientMock()

	// no cookies are saved between these requests, so redis GET is never called
	// also default MaxAge is always set to zero, so we never persist any cookies with SETEX
	// but we always try to delete each one with DEL
	mock.MatchExpectationsInOrder(true)
	// first samesite test causes session delete to be called as MaxAge <= 0
	mock.Regexp().ExpectDel(sessionKeyPrefix).SetVal(0)
	// tester sets options that we don't persist, and overrides MaxAge to zero
	mock.Regexp().ExpectDel(sessionKeyPrefix).SetVal(0)
	// tester stops setting options explicitly, but still isn't copying cookies
	mock.Regexp().ExpectDel(sessionKeyPrefix).SetVal(0)
	// same
	mock.Regexp().ExpectDel(sessionKeyPrefix).SetVal(0)
	// session is created with empty values and then immediately expired
	mock.Regexp().ExpectDel(sessionKeyPrefix).SetVal(0)

	tester.Options(t, storeFactory(db))

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Error(err)
	}
}

func TestSessionStore_Many(t *testing.T) {
	db, mock := redismock.NewClientMock()

	sessionAValues := gobEncode(t, map[interface{}]interface{}{
		"hello": "world",
	})
	sessionBValues := gobEncode(t, map[interface{}]interface{}{
		"foo": "bar",
	})

	mock.MatchExpectationsInOrder(true)
	// two sessions are created in one request here
	mock.Regexp().ExpectSetEx(sessionKeyRegexp, sessionAValues, defaultExpiration).SetVal("")
	mock.Regexp().ExpectSetEx(sessionKeyRegexp, sessionBValues, defaultExpiration).SetVal("")
	// get both sessions in the next request
	mock.Regexp().ExpectGet(sessionKeyRegexp).SetVal(string(sessionAValues))
	mock.Regexp().ExpectGet(sessionKeyRegexp).SetVal(string(sessionBValues))

	tester.Many(t, storeFactory(db))
}
