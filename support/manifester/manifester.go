package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/manifests"
	"go.uber.org/zap/zapcore"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/yaml"
)

type Reconciler interface {
	ApplyOverrides(*kustomize.Kustomization, *v1alpha1.Cluster)
}

func main() {
	var subcomponent string
	var clusterCRpath string

	flag.StringVar(&subcomponent, "subcomponent", "", "The subcomponent for which generate manifests.")
	flag.StringVar(&clusterCRpath, "cluster-cr-path", "", "The path to Cluster CR for which generate manifests.")

	opts := zap.Options{
		Development: true,
		TimeEncoder: zapcore.ISO8601TimeEncoder,
	}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()

	if subcomponent == "" {
		fmt.Println("subcomponent needs to be defined")
		os.Exit(1)
	}

	initialManifests, err := cluster.InitialManifests(manifests.Upstream)
	if err != nil {
		fmt.Println("error occurred while parsing embedded manifests")
		os.Exit(1)
	}

	if _, ok := initialManifests[subcomponent]; !ok {
		fmt.Printf("subcomponent %s has not been defined\n", subcomponent)
		os.Exit(1)
	}

	//fmt.Println("=== Embedded manifests:")
	//fmt.Println(string(initialManifests[subcomponent]))

	clusterCR := new(opstracev1alpha1.Cluster)
	blob, err := os.ReadFile(clusterCRpath)
	if err != nil {
		fmt.Printf("unable to read file %s: %v\n", clusterCRpath, err)
		os.Exit(1)
	}
	err = yaml.Unmarshal(blob, clusterCR)
	if err != nil {
		fmt.Println("error occurred while loading Cluster CR")
		os.Exit(1)
	}

	var reconciler Reconciler
	var overrideKustomization *kustomize.Kustomization

	switch subcomponent {
	case "gatekeeper":
		r := cluster.NewGatekeeperReconciler(initialManifests[subcomponent], false, new(cluster.ClusterState))
		overrideKustomization, err = r.ApplyConfiguration(clusterCR, "psql://fake:pass@localhost:5321")
		reconciler = r
	case "external-dns":
		r := cluster.NewExternalDNSReconciler(initialManifests[subcomponent], false)
		overrideKustomization, err = r.ApplyConfiguration(clusterCR)
		reconciler = r
	}
	if err != nil {
		fmt.Printf("unable to apply configuration: %v\n", err)
		os.Exit(1)
	}

	reconciler.ApplyOverrides(overrideKustomization, clusterCR)

	kbytes, err := yaml.Marshal(overrideKustomization)
	if err != nil {
		fmt.Printf("unable to marshal kustomization: %v\n", err)
		os.Exit(1)
	}
	fmt.Println(string(kbytes))

	manifests, err := cluster.ApplyKustomization(initialManifests[subcomponent], overrideKustomization)
	if err != nil {
		fmt.Printf("unable to apply overrides: %v\n", err)
		os.Exit(1)
	}

	fmt.Println("=== Parsed manifests:")
	for _, v := range manifests {
		jsonBlob, err := v.MarshalJSON()
		if err != nil {
			fmt.Printf("error occurred while parsing unstructured to JSON: %v\n", err)
			os.Exit(1)
		}
		yamlBlob, err := yaml.JSONToYAML(jsonBlob)
		if err != nil {
			fmt.Printf("error occurred while converting JSON to YAML: %v\n", err)
			os.Exit(1)
		}
		fmt.Println("---")
		fmt.Println(string(yamlBlob))
	}
}
