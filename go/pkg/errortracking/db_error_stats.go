package errortracking

import (
	"context"
	"fmt"
	"time"
)

type FrequencyPair [2]uint64

/*
Example aggregation query:
SELECT

	fingerprint,
	toUInt64(occurred_at) AS t,
	count(*)

FROM gl_error_tracking_error_events
WHERE fingerprint IN ('2698336012')
GROUP BY

	fingerprint,
	t

ORDER BY

	fingerprint ASC
	t DESC

LIMIT 10
*/
const baseQueryErrorFrequencyTmpl = `
SELECT
	fingerprint,
	toUInt64(%s(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
`

const baseQueryTimeSeriesGeneratorTmpl = `
WITH
    %s(toDateTime('%d')) AS start,
    %s(toDateTime('%d')) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), %d))) AS t
`

func (db *database) getErrorStats(
	ctx context.Context,
	fingerprints []uint32,
	statsPeriod string,
) (map[uint32][]FrequencyPair, error) {
	// the reference point in time around which we process statsPeriod against
	// to generate error stats data, for now, the current time.
	refTime := time.Now()

	query, args := buildErrorStatsQuery(fingerprints, refTime, statsPeriod)

	rows, err := db.conn.Query(ctx, query, args...)
	if err != nil {
		return nil, fmt.Errorf("querying error stats: %w", err)
	}

	type queryColumns struct {
		Fingerprint uint32 `ch:"fingerprint"`
		Timestamp   uint64 `ch:"t"`
		Count       uint64 `ch:"cnt"`
	}

	queriedData := make(map[uint32]map[uint64]uint64)
	for rows.Next() {
		q := new(queryColumns)
		err := rows.ScanStruct(q)
		if err != nil {
			return nil, fmt.Errorf("reading stats output, scan struct: %w", err)
		}
		if _, ok := queriedData[q.Fingerprint]; !ok {
			queriedData[q.Fingerprint] = make(map[uint64]uint64)
		}
		queriedData[q.Fingerprint][q.Timestamp] = q.Count
	}

	result := make(map[uint32][]FrequencyPair)
	timeWindow, err := db.getQueryTimeWindow(ctx, refTime, statsPeriod)
	if err != nil {
		return nil, fmt.Errorf("getting query time window: %w", err)
	}
	// for all fingerprints we queried data for...
	for _, fp := range fingerprints {
		result[fp] = make([]FrequencyPair, 0)
		// generate non-sparse data for the entire time window we queried data for
		for _, ts := range timeWindow {
			if _, ok := queriedData[fp][ts]; ok {
				result[fp] = append(result[fp], FrequencyPair{ts, queriedData[fp][ts]})
			} else {
				result[fp] = append(result[fp], FrequencyPair{ts, 0})
			}
		}
	}

	return result, nil
}

func (db *database) getQueryTimeWindow(ctx context.Context, refTime time.Time, statsPeriod string) ([]uint64, error) {
	query, args := buildTimeSeriesQuery(refTime, statsPeriod)

	rows, err := db.conn.Query(ctx, query, args...)
	if err != nil {
		return nil, fmt.Errorf("querying error stats: %w", err)
	}

	type timeSeriesColumns struct {
		Timestamp uint64 `ch:"t"`
	}

	window := []uint64{}
	for rows.Next() {
		t := new(timeSeriesColumns)
		err := rows.ScanStruct(t)
		if err != nil {
			return nil, fmt.Errorf("reading timeseries output, scan struct: %w", err)
		}
		window = append(window, t.Timestamp)
	}
	return window, nil
}

func buildTimeSeriesQuery(refTime time.Time, statsPeriod string) (string, []interface{}) {
	startTime, endTime, stepSeconds, timeFunc := inferTimelines(refTime, statsPeriod)
	baseQueryTimeSeriesGenerator := fmt.Sprintf(
		baseQueryTimeSeriesGeneratorTmpl,
		timeFunc, startTime,
		timeFunc, endTime,
		stepSeconds,
	)
	q := queryBuilder{}
	q.reset(baseQueryTimeSeriesGenerator)
	return q.sql, q.args
}

func buildErrorStatsQuery(fingerprints []uint32, refTime time.Time, statsPeriod string) (string, []interface{}) {
	startTime, endTime, _, timeFunc := inferTimelines(refTime, statsPeriod)
	baseQueryErrorFrequency := fmt.Sprintf(baseQueryErrorFrequencyTmpl, timeFunc)

	q := &queryBuilder{}
	q.reset(baseQueryErrorFrequency)
	q.build(" WHERE fingerprint IN ?", fingerprints)
	q.build(" AND t >= ?", startTime)
	q.build(" AND t <= ?", endTime)
	q.build(" GROUP BY fingerprint, t")
	q.build(" ORDER BY fingerprint ASC, t DESC")
	return q.sql, q.args
}
