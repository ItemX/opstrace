# Project makefile with shared CI targets and generic dev commands.

# Include all support/makefiles/*.mk files
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir := $(dir $(mkfile_path))

include $(mkfile_dir)$/support/makefiles/*.mk

# Load all go projects to be used for target generation
GO_PROJECTS ?= $(shell grep '\- GOMOD_PATH: ' .gitlab-ci.yml | cut -d: -f 2 | tr -d ' ')

# Public interface. Below are the important targets, used on a daily basis by
# people and CI. These targets need to stabilize (first the concepts, then the names).

# Standard targets to be implemented in each Go project Makefile.
# Used for convenience and to allow parameterised CI jobs.
# Each target can be used for a specific project, e.g. build-scheduler to build just the scheduler binaries.
# lint-code: code linting for go code.
# build: build go code and output binaries.
# regenerate: update any auto-generated code or manifests.
# unit-tests: run unit tests.
# docker-build: build all relevant docker containers.
# docker: build and push all relevant docker containers.
# print-docker-images: print the name and tag of all relevant docker images.
STANDARD_GO_TARGETS = lint-code build regenerate unit-tests docker-build docker-push docker-ensure docker print-docker-images kind-load-docker-images
define make-go-targets
$1_targets = $(addprefix $1-, $(GO_PROJECTS))
.PHONY: $1
$1: $$($1_targets)

$(foreach project,$(GO_PROJECTS),$(eval $(call make-go-target,$1,$(project))))
endef
# Build a go target for a specific project.
define make-go-target
.PHONY: $1-$2
$1-$2:
	make -C $2 $1
endef

$(foreach target,$(STANDARD_GO_TARGETS),$(eval $(call make-go-targets,$(target))))

# Use this target locally to run the markdown linter.
.PHONY: lint-docs
lint-docs:
	docker run -v $(shell pwd):/workdir ghcr.io/igorshubovych/markdownlint-cli:latest "docs/**/*.md"

# Lint all the terraform/grunt environments and modules.
.PHONY: lint-terraform
lint-terraform:
	make -C terraform lint-modules lint-terragrunt

# go mod tidy for the main go projects.
.PHONY: go-mod-tidy
go-mod-tidy:
	@for d in $(GO_PROJECTS) test/e2e; do \
		echo "--- go mod tidy in $$d"; \
		(cd $$d && go mod tidy); \
	done

.PHONY: go-mod-download
go-mod-download:
	@for d in $(GO_PROJECTS) test/e2e; do \
		echo "--- go mod download in $$d"; \
		(cd $$d && go mod download); \
	done

# build and push api images in go/ directory
.PHONY: api-images
api-images:
	make -C go docker

# build and push image for the scheduler only
.PHONY: scheduler-image
scheduler-image:
	make -C scheduler docker

# build and push image for the tenant-operator only
.PHONY: tenant-operator-image
tenant-operator-image:
	make -C tenant-operator docker

# build and push image for the gatekeeper only
.PHONY: gatekeeper-image
gatekeeper-image:
	make -C gatekeeper docker

# install CRDs and deploy scheduler
.PHONY: deploy
deploy:
	make -C scheduler install
	make -C scheduler deploy

.PHONY: dev-namespace
dev-namespace:
	make -C scheduler dev-namespace

# create a local kind cluster
.PHONY: kind
kind:
	make -C scheduler kind

.PHONY: e2e-test
e2e-test: e2e-test-suite

.PHONY: e2e-test-suite
e2e-test-suite:
	TEST_SCHEDULER_IMAGE=$(shell make -s -C scheduler print-docker-images) make -C test/e2e e2e-test

# destroy a local kind cluster
.PHONY: destroy
destroy:
	make -C scheduler kind-delete


# CI image used as a base image for most of the CI jobs.
CI_IMAGE ?= ci:latest

.PHONY: rebuild-ci-container-image
rebuild-ci-container-image: BUILD_ARGS=
rebuild-ci-container-image:
	docker pull "${CI_IMAGE}" || echo "${CI_IMAGE} image is not available. Will not use remote cache."
	docker build --cache-from "${CI_IMAGE}" ${BUILD_ARGS} --build-arg CIUID=$(shell id -u) --build-arg CIGID=$(shell id -g) \
		-t "${CI_IMAGE}"  \
		. -f containers/ci/opstrace-ci.Dockerfile

.PHONY: push-ci-container-image
push-ci-container-image:
	docker push "${CI_IMAGE}"
