package tests

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/davecgh/go-spew/spew"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"

	gocommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	schedulerapi "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/common"
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
)

// Shared test components that are initialized in BeforeSuite.
// These should not be updated after initialisation.
var (
	// testIdentifier is a unique identifier for the test run
	testIdentifier string
	// testTarget is the target environment for the test run
	testTarget gocommon.EnvironmentTarget
	// k8sClient for accessing created cluster
	k8sClient client.Client
	// defaultClient is a client scoped to the default namespace
	defaultNSClient client.Client
	// restConfig is the rest config for the clients
	restConfig *rest.Config
	// clientset is the kubernetes clientset
	clientset *kubernetes.Clientset
	// testInfra is the infrastructure builder for the suite.
	// This is not intended to be used in tests, but is used in helpers.
	testInfra infra.InfrastructureBuilder
	// common config for the infra
	infraConfig common.Configuration
)

func TestE2E(t *testing.T) {
	RegisterFailHandler(Fail)

	SetDefaultEventuallyTimeout(time.Minute * 15)
	SetDefaultEventuallyPollingInterval(time.Second)

	suiteConfig, reporterConfig := GinkgoConfiguration()
	reporterConfig.Verbose = true
	reporterConfig.FullTrace = true
	suiteConfig.Timeout = 2 * time.Hour
	suiteConfig.GracePeriod = 1 * time.Hour
	RunSpecs(t, "E2E Test Suite", suiteConfig, reporterConfig)
}

var _ = BeforeSuite(func(ctx context.Context) {
	By("init client schemes")
	initSchemes()

	By("initializing test variables")
	c, err := LoadConfig()
	Expect(err).ToNot(HaveOccurred())
	setTestTarget(c)
	setTestIdentifier(c)

	testInfra, err = infra.NewInfrastructure(testTarget, testIdentifier)
	Expect(err).ToNot(HaveOccurred())

	By("loading infrastructure configuration")
	infraConfig, err = testInfra.LoadConfiguration()
	Expect(err).ToNot(HaveOccurred())
	byUsingConfig(infraConfig)

	By("creating GitLab Instance")
	Expect(testInfra.CreateGitLabInstance()).To(Succeed())
	deferCleanupOptional(testInfra.DestroyGitLabInstance)

	By("creating k8s cluster")
	Expect(testInfra.CreateK8sCluster()).To(Succeed())
	deferCleanupOptional(testInfra.DestroyK8sCluster)

	By("ensuring we can construct clients for the underlying cluster")
	//
	// This is done inside an `Eventually` block because freshly-minted
	// clusters, esp. inside cloud environments can undergo repairs OR
	// auto-scaling events which causes them to be unreachable for a
	// brief period of time. As soon the clients can be successfully
	// built, we move on downstream.
	//
	Eventually(buildK8sClients, 5*time.Minute, 10*time.Second).WithContext(ctx).Should(Succeed())
	deferCleanupOptional(teardownCluster)

	By("checking that the API server can be consistently queried")
	nsObjectLookupKey := types.NamespacedName{
		Name:      v1.NamespaceDefault,
		Namespace: v1.NamespaceDefault,
	}
	nsObject := &v1.Namespace{}
	Consistently(ctx, func(g Gomega) {
		g.Expect(k8sClient.Get(ctx, nsObjectLookupKey, nsObject)).To(Succeed())
	}, 10*time.Second, 3*time.Second).Should(Succeed())

	By("checking that the scheduler-manager pod is ready & running")
	Eventually(ctx, func(g Gomega) {
		pods, err := clientset.CoreV1().Pods(v1.NamespaceDefault).List(
			ctx,
			metav1.ListOptions{LabelSelector: "control-plane=controller-manager"},
		)
		g.Expect(err).NotTo(HaveOccurred())
		g.Expect(pods.Items).NotTo(BeEmpty(), "no scheduler-manager pod found")
		g.Expect(pods.Items).To(HaveLen(1), "more than one scheduler-manager pod found")

		g.Expect(pods.Items[0].Status.Phase).To(Equal(v1.PodRunning))
	}, 10*time.Second, 3*time.Second).Should(Succeed())
})

func setTestTarget(c Configuration) {
	testTarget = c.TestTarget
	By("using test target: " + string(testTarget))
}

func setTestIdentifier(c Configuration) {
	testIdentifier = c.TestIdentifier
	By("using test identifier: " + testIdentifier)
}

func initSchemes() {
	Expect(scheme.AddToScheme(scheme.Scheme)).To(Succeed())
	Expect(apiextensionsv1.AddToScheme(scheme.Scheme)).To(Succeed())
	Expect(schedulerapi.AddToScheme(scheme.Scheme)).To(Succeed())
}

func buildK8sClients(g Gomega, ctx SpecContext) {
	var err error
	restConfig, err = testInfra.GetRestConfig(ctx)
	g.Expect(err).ToNot(HaveOccurred())
	k8sClient, err = client.New(restConfig, client.Options{
		Scheme: scheme.Scheme,
	})
	g.Expect(err).NotTo(HaveOccurred())

	defaultNSClient = client.NewNamespacedClient(k8sClient, "default")

	clientset, err = kubernetes.NewForConfig(restConfig)
	g.Expect(err).NotTo(HaveOccurred())
}

func byUsingConfig(c common.Configuration) {
	c.GitLabAdminToken = "**********"
	s := spew.Sdump(c)
	By("using configuration: " + s)
}

func teardownCluster(ctx SpecContext) {
	if testTarget == gocommon.KIND {
		return // don't clean up Cluster in kind, so we can inspect it after tests.
	}

	By("clean up cluster CR if it exists")
	// need to clean up cluster so terragrunt can tear down any CRDs it uses
	cs := &schedulerv1alpha1.ClusterList{}
	Expect(defaultNSClient.List(ctx, cs)).To(Succeed())
	for _, c := range cs.Items {
		cr := c
		deleteCustomResourceAndVerify(ctx, &cr)
	}
}

// deferCleanupOptional registers a DeferCleanup unless the environment variable
// TEST_SKIP_CLEANUP is set.
func deferCleanupOptional(args ...interface{}) {
	if _, skip := os.LookupEnv("TEST_SKIP_CLEANUP"); skip {
		return
	}
	DeferCleanup(args...)
}
