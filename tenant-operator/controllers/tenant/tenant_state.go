package tenant

import (
	"context"

	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type TenantState struct {
	Argus  *ArgusState
	Groups *GroupsState
}

func NewTenantState() TenantState {
	return TenantState{}
}

func (i *TenantState) Read(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	err := i.readArgusState(ctx, cr, client)
	if err != nil {
		return err
	}
	err = i.readGroupsState(ctx, cr, client)

	return err
}

func (i *TenantState) readArgusState(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	i.Argus = NewArgusState()
	return i.Argus.Read(ctx, cr, client)
}

func (i *TenantState) readGroupsState(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	i.Groups = NewGroupsState()
	return i.Groups.Read(ctx, cr, client)
}
