package gatekeeper

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// Sets up the gatekeeper routes.
//
//nolint:funlen // These are mostly comments!
func SetRoutes(router *gin.Engine) {
	v1 := router.Group("/v1")
	{
		auth := v1.Group("/auth")
		{
			// Start the oauth2 flow
			auth.GET("/start", HandleAuthStart)
			// Finish the oauth2 flow
			auth.GET("/callback", HandleAuthFinish)
			// Handle the external auth webhook request from nginx-ingress.
			// https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/#external-authentication
			//
			// The API endpoint below checks if the user is an active member of
			// the given group/namespace with high-enough access-level and has
			// the following syntax/arguments:
			// * :action - one of `read`, `write` and `readwrite`. It defines the
			// scopes of the auth token that are required to get permission to
			// access. Do not apply to oauth-based clients.
			// * :namespace_id - the id of the group/namespace the user is
			// trying to access. Only non-negative Integers.
			// * `support_legacy_goui` - url parameter, optional, defaults to
			// false, defines whether the goui user creation/update should be
			// performed on each login. Either `true` or `false.`
			// * `min_accesslevel` - url parameter, required, defines the
			// minimum access level that user needs to have to be permitted
			// access. Integer, in the range of 0-50 inclusive.
			auth.GET(
				"/group/webhook/:namespace_id/:action",
				// Do not set autoAuth on the webhook because nginx-ingress is
				// the direct client. This request from nginx-ingress expects a
				// 401 or 403 if auth fails. nginx-ingress can then redirect to
				// the /v1/auth/start flow.
				GroupAuthHandlerFactory(false),
			)
			// The API endpoint below checks if the user is an active member of
			// the project with high-enough access-level and that the project
			// belongs to the given namespace. It has the following syntax/arguments:
			// * :action - one of `read`, `write` and `readwrite`. It defines the
			// scopes of the auth token that are required to get permission to
			// access. Do not apply to oauth-based clients.
			// * :namespace_id - the id of the group/namespace the user is
			// trying to access. Only non-negative Integers.
			// * :project_id - the id of the project the user is
			// trying to access. Only non-negative Integers.
			// * `min_accesslevel` - url parameter, required, defines the
			// minimum access level that user needs to have to be permitted
			// access. Integer, in the range of 0-50 inclusive.
			auth.GET(
				"/both/webhook/:namespace_id/:project_id/:action",
				// Do not set autoAuth on the webhook because nginx-ingress is
				// the direct client. This request from nginx-ingress expects a
				// 401 or 403 if auth fails. nginx-ingress can then redirect to
				// the /v1/auth/start flow.
				ProjectAuthHandlerFactory(false, true),
			)
			// The API endpoint below checks if the user is an active member of
			// the project with high-enough access-level. It has the following
			// syntax/arguments:
			// * :action - one of `read`, `write` and `readwrite`. It defines the
			// scopes of the auth token that are required to get permission to
			// access. Do not apply to oauth-based clients.
			// * :project_id - the id of the project the user is
			// trying to access. Only non-negative Integers.
			// * `min_accesslevel` - url parameter, required, defines the
			// minimum access level that user needs to have to be permitted
			// access. Integer, in the range of 0-50 inclusive.
			auth.GET(
				"/project/webhook/:project_id/:action",
				// Do not set autoAuth on the webhook because nginx-ingress is
				// the direct client. This request from nginx-ingress expects a
				// 401 or 403 if auth fails. nginx-ingress can then redirect to
				// the /v1/auth/start flow.
				ProjectAuthHandlerFactory(false, false),
			)
			// Specific route for Argus because the :namespace_id is always the topLevelNamespace and we have to handle
			// auth a little differently
			auth.GET(
				"/argus/webhook/:root_namespace_id",
				// Do not set autoAuth on the webhook because nginx-ingress is the direct client.
				// This request from nginx-ingress expects a 401 or 403 if auth fails.
				// nginx-ingress can then redirect to the /v1/auth/start flow.
				ArgusAuthHandlerFactory(false),
			)
		}
		// Provision the namespace
		v1.GET(
			"/provision/:namespace_id/*action",
			// Configure `AuthenticatedSessionRequired` to autoAuth since the web browser is the direct client
			AuthenticatedSessionRequired(true, HandleNamespaceProvisioning),
		)

		// Add internal API to list project of a group
		// don't auto-auth redirect, assume we must already have a session.
		v1.GET("/:group_id/projects", AuthenticatedSessionRequired(false, ListGroupProjectsHandler))
	}
	h := NewErrorTrackingAuthHandler()
	errortracking := v1.Group("/error_tracking")
	{
		errortracking.GET("/auth", h.HandleErrorTrackingAuth)
	}

	catchAll := router.Group("/-")
	{
		// Catches any gitlab URL that is prepended with /-/, e.g. /-/<gitlab_namespace>/<action>.
		catchAll.GET(
			"/:namespace_id/*action",
			// Configure `AuthenticatedSessionRequired` to autoAuth since the web browser is the direct client
			AuthenticatedSessionRequired(true, HandlePath),
		)
	}

	if gin.IsDebugging() {
		router.GET("/teststatus/:code", func(ctx *gin.Context) {
			type status struct {
				Code int `uri:"code" binding:"required"`
			}
			s := &status{}
			if err := ctx.ShouldBindUri(s); err != nil {
				negotiateError(ctx, http.StatusBadRequest, "invalid status code")
			}

			ctx.Status(s.Code)
		})
	}

	// nginx default-backend error handling.
	// This route is not configurable in nginx-ingress.
	router.GET("/", errorsHandler)

	router.NoRoute(func(ctx *gin.Context) {
		ctx.AbortWithStatus(404)
	})
}
