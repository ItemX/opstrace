package tests

import (
	"encoding/json"
	"io"
	"net/http"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

const (
	SentryGoExampleImage     = "registry.gitlab.com/gitlab-org/opstrace/opstrace/test-with-sentry-sdk/go-exceptions-test:0.2.0-d4744d98"
	SentryRubyExampleImage   = "registry.gitlab.com/gitlab-org/opstrace/opstrace/test-with-sentry-sdk/rb-exceptions-test:0.2.0-d4744d98"
	SentryPythonExampleImage = "registry.gitlab.com/gitlab-org/opstrace/opstrace/test-with-sentry-sdk/py-exceptions-test:0.2.0-d4744d98"
	SentryJSExampleImage     = "registry.gitlab.com/gitlab-org/opstrace/opstrace/test-with-sentry-sdk/js-exceptions-test:0.2.0-d4744d98"
)

type errorResp struct {
	Status      string `json:"status,omitempty"`
	ProjectID   int    `json:"project_id,omitempty"`
	Fingerprint int    `json:"fingerprint,omitempty"`
	EventCount  int    `json:"event_count,omitempty"`
	Description string `json:"description,omitempty"`
	Actor       string `json:"actor,omitempty"`
}

var _ = Describe("sentry sdk", Ordered, Serial, func() {
	cluster := &schedulerv1alpha1.Cluster{}
	beforeAllEnsureCluster(cluster)

	beforeAllConfigureGitLab()

	Context("on running the language specific harness", func() {

		initialErrorCount := 0
		BeforeEach(func() {
			initialErrorCount = countProjectErrors()
		})

		It("sample program should successfully send error to GOB", func(ctx SpecContext) {
			env := map[string]string{
				"SENTRY_DSN":  gitLab.errorTrackingConfig.clientKey.SentryDsn,
				"INSECURE_OK": "true",
			}
			containerReqs := []testcontainers.ContainerRequest{
				{
					Image:      SentryGoExampleImage,
					Env:        env,
					WaitingFor: wait.ForExit().WithExitTimeout(time.Second * 30),
				},
				{
					Image:      SentryRubyExampleImage,
					Env:        env,
					WaitingFor: wait.ForExit().WithExitTimeout(time.Second * 30),
				},
				{
					Image:      SentryPythonExampleImage,
					Env:        env,
					WaitingFor: wait.ForExit().WithExitTimeout(time.Second * 30),
				},
				{
					Image:      SentryJSExampleImage,
					Env:        env,
					WaitingFor: wait.ForExit().WithExitTimeout(time.Second * 40),
				},
			}

			By("Sending Errors to GOB")
			for _, req := range containerReqs {
				container, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
					ContainerRequest: req,
				})
				Expect(err).ToNot(HaveOccurred())

				err = container.Start(ctx)
				Expect(err).ToNot(HaveOccurred())

				logs, errLogs := container.Logs(ctx)
				Expect(errLogs).ToNot(HaveOccurred())
				all, errLogs := io.ReadAll(logs)
				Expect(errLogs).ToNot(HaveOccurred())
				GinkgoWriter.Printf("container logs:\n---->\n%s<----\n\n", string(all))
				state, errState := container.State(ctx)
				Expect(errState).ToNot(HaveOccurred())
				GinkgoWriter.Printf("state : %v\n", state)
				By("Ensuring that the container exited cleanly")
				Expect(state.ExitCode).To(Equal(0), "exit code should be 0")
				container.Terminate(ctx)
			}

			By("Verifying that the errors were sent to GOB")

			Eventually(func(g Gomega) {
				// TODO(joe): actually verify error contents here, not just the number.
				g.Expect(countProjectErrors()).To(Equal(initialErrorCount + len(containerReqs)))
			}, time.Second*10, time.Second).Should(Succeed())
		})
	})
})

func countProjectErrors() int {
	By("Listing Errors From GOB")
	GinkgoWriter.Println("List Errors Endpoint", gitLab.errorTrackingConfig.listEndpoint)
	req, err := http.NewRequest(http.MethodGet, gitLab.errorTrackingConfig.listEndpoint, nil)
	Expect(err).ToNot(HaveOccurred())
	req.Header.Add("Private-Token", gitLab.errorTrackingConfig.groupToken.Token)
	resp, err := gitLab.httpClient.Do(req)
	Expect(err).ToNot(HaveOccurred())
	defer resp.Body.Close()
	Expect(resp.StatusCode).To(Equal(http.StatusOK))

	var errors []errorResp
	err = json.NewDecoder(resp.Body).Decode(&errors)
	Expect(err).ToNot(HaveOccurred())

	GinkgoWriter.Printf("list error response: %v\n", errors)

	return len(errors)
}
