package gatekeeper

import (
	"errors"
	"fmt"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/cache/v9"
	log "github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
)

const (
	authConfigKey     = "gatekeeper/authConfig"
	gitLabServiceKey  = "gatekeeper/gitLabService"
	authTokenKey      = "gatekeeper/authToken"
	namespaceKey      = "gatekeeper/namespace"
	projectKey        = "gatekeeper/namespace"
	actionKey         = "gatekeeper/action"
	minAccessLevelKey = "gatekeeper/minAccessLevel"
	authMetricsKey    = "gatekeeper/authMetrics"
	randIDKey         = "gatekeeper/randSessionID"
)

// Helper to set the oauth2 client config on the context.
func SetAuthConfig(ctx *gin.Context, cfg *oauth2.Config) {
	ctx.Set(authConfigKey, cfg)
}

// Helper to get the oauth2 client config from the context.
func GetAuthConfig(ctx *gin.Context) *oauth2.Config {
	auth, exists := ctx.Get(authConfigKey)
	if !exists {
		log.Fatal("must use Auth() middleware")
	}
	return auth.(*oauth2.Config)
}

// Helper to get the metrics from the context.
func GetAuthMetrics(ctx *gin.Context) *AuthMetricsData {
	metrics, exists := ctx.Get(authMetricsKey)
	if !exists {
		log.Fatal("auth metrics not found in the context")
	}
	return metrics.(*AuthMetricsData)
}

// Helper to set the gitLabService on the context.
func SetGitLabService(ctx *gin.Context, us *GitLabService) {
	ctx.Set(gitLabServiceKey, us)
}

// Helper to get the gitLabService from the context.
func GetGitLabService(ctx *gin.Context) *GitLabService {
	us, exists := ctx.Get(gitLabServiceKey)
	if !exists {
		log.Fatal("must use AuthenticatedSessionRequired() middleware")
	}
	return us.(*GitLabService)
}

// Helper to set the authToken in the session.
func SetAuthToken(ctx *gin.Context, token *oauth2.Token) error {
	session := sessions.Default(ctx)
	session.Set(authTokenKey, *token)
	return session.Save()
}

// Helper to get the authToken from the session.
func GetAuthToken(ctx *gin.Context) *oauth2.Token {
	session := sessions.Default(ctx)
	token := session.Get(authTokenKey)
	if token == nil {
		return nil
	}
	//nolint:errcheck
	tok := token.(oauth2.Token)
	return &tok
}

// Helper to clear the authToken in the session.
func ClearAuthToken(ctx *gin.Context) error {
	session := sessions.Default(ctx)
	session.Delete(authTokenKey)
	return session.Save()
}

// Helper to set the target namespaceID in the ctx.
func SetNamespace(ctx *gin.Context, namespaceID string) {
	ctx.Set(namespaceKey, namespaceID)
}

// Helper to get the target namespaceID from the ctx.
func GetNamespace(ctx *gin.Context) string {
	us, exists := ctx.Get(namespaceKey)
	if !exists {
		log.Fatal("must use AuthMiddleware() middleware")
	}
	return us.(string)
}

type LastNamespace struct {
	ID string
}

func lastUserNamespaceKey(userID int, rootLevelNamespaceID string) string {
	return fmt.Sprintf("u:%d:%s:lastns", userID, rootLevelNamespaceID)
}

// Caches the last used namespace for the user within the rootlevel namespace (i.e Tenant).
// This allows us to look up the last used namespace within a specific Argus instance.
func SetLastUsedNamespaceForUser(ctx *gin.Context, rootLevelNamespaceID string, namespace LastNamespace) error {
	c := GetCache(ctx)
	g := GetGitLabService(ctx)
	u, err := g.CurrentUser()
	if err != nil {
		return fmt.Errorf("failed to retrieve currentUser: %w", err)
	}
	key := lastUserNamespaceKey(u.ID, rootLevelNamespaceID)
	err = c.Set(&cache.Item{
		Ctx:   ctx,
		Key:   key,
		Value: namespace,
		// If someone refreshes a day later, then it is probably ok
		// to send back a 401/403 and be forced back through the flow
		TTL: 24 * time.Hour,
	})
	if err != nil {
		return fmt.Errorf("cache set failure for key [%s]: %w", key, err)
	}
	return nil
}

// Get the last used namespace within a specific Argus instance.
// Returns true if value was found.
func GetLastUsedNamespaceForUser(ctx *gin.Context, rootLevelNamespaceID string, value *LastNamespace) bool {
	c := GetCache(ctx)
	g := GetGitLabService(ctx)
	u, err := g.CurrentUser()
	if err != nil {
		log.Errorf("failed to retrieve currentUser: %+v", err)
		return false
	}
	key := lastUserNamespaceKey(u.ID, rootLevelNamespaceID)
	err = c.Get(ctx, key, value)
	if err == nil || errors.Is(err, cache.ErrCacheMiss) {
		return err == nil && value.ID != ""
	}
	log.Errorf("cache retrieval failure for key [%s]: %+v", key, err)
	return false
}
