package models

import (
	"encoding/hex"
	"fmt"
	"time"

	"github.com/google/uuid"
)

type ErrorStatusV2 struct {
	ProjectID   uint64    `ch:"project_id"`
	EventID     uuid.UUID `json:"event_id,omitempty" ch:"event_id"`
	Fingerprint []byte    `ch:"fingerprint"`
	IsDeleted   uint8     `ch:"is_deleted"`
	Timestamp   time.Time `json:"timestamp" ch:"timestamp"`
	// Status is a code:
	//   0 - unresolved
	//   1 - resolved
	Status uint8  `ch:"status"`
	UserID string `ch:"user_id"`
	Actor  string `ch:"actor"`
	// TODO: Title needs to be computed
	Title string `ch:"title"`

	UpdatedAt time.Time `ch:"updated_at"`
	// UpdatedBy is a code:
	//   0 - status changed by user
	//   1 - status changed by system (new event happened after resolve)
	//   2 - status changed by computer (not a user)
	UpdatedBy uint8 `ch:"updated_by"`
}

func (e ErrorStatusV2) AsInsertStmt(tz *time.Location) string {
	//nolint:lll
	stmt := `INSERT INTO %s (event_id, project_id, timestamp, is_deleted, fingerprint, status, actor, updated_by, updated_at)
	VALUES (
		toUUID(%s),
	    %d,
	    %s,
	    %d,
	    unhex(%s),
	    %d,
	    %s,
	    %d,
	    %s)
`
	query := fmt.Sprintf(stmt,
		errorStatusTableV2,
		quote(e.EventID.String()),
		e.ProjectID,
		formatTime(e.Timestamp, tz),
		e.IsDeleted,
		quote(hex.EncodeToString(e.Fingerprint)),
		e.Status,
		quote(e.Actor),
		e.UpdatedBy,
		formatTime(e.UpdatedAt, tz),
	)

	return query
}
