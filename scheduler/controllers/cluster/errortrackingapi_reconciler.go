package cluster

import (
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type ErrortrackingAPIReconciler struct {
	BaseReconciler
	clusterState *ClusterState
}

func NewErrorTrackingAPIReconciler(
	initialManifests map[string][]byte,
	teardown bool,
	clusterState *ClusterState,
) *ErrortrackingAPIReconciler {
	res := &ErrortrackingAPIReconciler{
		clusterState: clusterState,

		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.ErrortrackingAPIInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.ErrortrackingAPIInventoryID,
			reconcilerName: "errortracking-api",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.ErrorTrackingAPI
			},

			serviceAccountName: constants.ErrorTrackingAPIName,
		},
	}

	res.subclassApplyMethod = res.applyConfiguration

	return res
}

//nolint:funlen // already refactored out from the bigger function, further refactoring will decrease readability
func (i *ErrortrackingAPIReconciler) applyStsConfiguration(
	gatekeeperURL string,
	cr *v1alpha1.Cluster,
	k *kustomize.Kustomization,
) error {
	clickhouseEndpoints, err := i.clusterState.ClickHouse.GetEndpoints()
	if err != nil {
		return fmt.Errorf("failed to retrieve clickhouse endpoints: %w", err)
	}
	// TODO: check how to create a user/pwd for error tracking
	clickhouseDSN := clickhouseEndpoints.Native.String() + "/" + constants.ErrorTrackingAPIDatabaseName

	apiBaseURL := cr.Spec.GetHostURL()
	if cr.Spec.DNS.Domain != nil {
		apiBaseURL = "https://errortracking." + *cr.Spec.DNS.Domain
	}

	stsPatches := []string{
		fmt.Sprintf(
			`{"op": "replace", "path": "/spec/template/spec/containers/0/env/3/value", "value": "rfs-%s.%s.svc.cluster.local:26379" }`,
			constants.RedisName, cr.Namespace(),
		),
		fmt.Sprintf(
			`{"op": "replace", "path": "/spec/template/spec/containers/0/env/4/value", "value": "%s" }`,
			gatekeeperURL,
		),
		fmt.Sprintf(
			`{"op": "replace", "path": "/spec/template/spec/containers/0/env/5/value", "value": "%s" }`,
			apiBaseURL,
		),
		fmt.Sprintf(
			`{"op": "replace", "path": "/spec/template/spec/containers/0/env/6/value", "value": "%s" }`,
			clickhouseDSN,
		),
	}

	if i.clusterState.LogLevel != "" {
		stsPatches = append(stsPatches,
			fmt.Sprintf(
				`{"op": "replace", "path": "/spec/template/spec/containers/0/env/2/value", "value": "%s" }`,
				i.clusterState.LogLevel,
			),
		)
	}

	useRemoteStorage, err := common.ParseFeatureAsBool(cr.Spec.Features, "ET_USE_REMOTE_STORAGE_BACKEND")
	if err != nil {
		return fmt.Errorf("unable to parse feature ET_USE_REMOTE_STORAGE_BACKEND: %w", err)
	}
	if useRemoteStorage {
		stsPatches = append(
			stsPatches,
			fmt.Sprintf(
				`{"op": "add", "path": "/spec/template/spec/containers/0/env/-", "value": { "name": "USE_REMOTE_STORAGE", "value": "%s" }}`,
				cr.Spec.Target,
			),
		)
	}

	useClientSideBuffering, err := common.ParseFeatureAsBool(cr.Spec.Features, "ET_USE_CLIENT_SIDE_BUFFERING")
	if err != nil {
		return fmt.Errorf("unable to parse feature ET_USE_CLIENT_SIDE_BUFFERING: %w", err)
	}
	if useClientSideBuffering {
		stsPatches = append(
			stsPatches,
			`{"op": "add", "path": "/spec/template/spec/containers/0/env/-", "value": { "name": "CLIENT_SIDE_BUFFERING_ENABLED", "value": "true" }}`,
			`{"op": "add", "path": "/spec/template/spec/containers/0/env/-", "value": { "name": "MAX_BATCH_SIZE", "value": "1000" }}`,
			`{"op": "add", "path": "/spec/template/spec/containers/0/env/-", "value": { "name": "MAX_PROCESSOR_COUNT", "value": "1" }}`,
		)
	}

	useDBClientCompression, err := common.ParseFeatureAsBool(cr.Spec.Features, "ET_USE_DB_CLIENT_COMPRESSION")
	if err != nil {
		return fmt.Errorf("unable to parse feature ET_USE_DB_CLIENT_COMPRESSION: %w", err)
	}
	if useDBClientCompression {
		stsPatches = append(
			stsPatches,
			`{"op": "add", "path": "/spec/template/spec/containers/0/env/-", "value": { "name": "DB_USE_COMPRESSION", "value": "true" }}`,
		)
	}

	useDebugServer, err := common.ParseFeatureAsBool(cr.Spec.Features, "ET_USE_DEBUG_SERVER")
	if err != nil {
		return fmt.Errorf("unable to parse feature ET_USE_DEBUG_SERVER: %w", err)
	}
	if useDebugServer {
		stsPatches = append(
			stsPatches,
			`{"op": "add", "path": "/spec/template/spec/containers/0/env/-", "value": { "name": "DEBUG_ENABLED", "value": "true" }}`,
		)
	}

	k.PatchesJson6902 = append(
		k.PatchesJson6902,
		types.Patch{
			Patch: fmt.Sprintf(
				`[ %s ]`, strings.Join(stsPatches, ","),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "StatefulSet",
					},
					Name: constants.ErrorTrackingAPIName,
				},
			},
		},
	)

	return nil
}

func (i *ErrortrackingAPIReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := i.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}

	gatekeeperURL := fmt.Sprintf("http://gatekeeper.%s.svc.cluster.local:3001", cr.Namespace())

	res.Images = []kustomize.Image{
		{
			Name:    "errortracking-api-image-placeholder",
			NewName: constants.DockerImageName(constants.ErrorTrackingImageName),
			NewTag:  constants.DockerImageTag,
		},
	}

	err = i.applyStsConfiguration(gatekeeperURL, cr, res)
	if err != nil {
		return nil, err
	}

	res.PatchesJson6902 = append(
		res.PatchesJson6902,
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "ServiceMonitor",
					},
					Name: constants.ErrorTrackingAPIName,
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[
						{"op": "replace", "path": "/spec/rules/0/host", "value": "%s" },
						{"op": "replace", "path": "/metadata/annotations/nginx.ingress.kubernetes.io~1auth-url", "value": "%s/v1/error_tracking/auth?request_path=$request_uri" }
					  ]`,
				cr.Spec.GetHost(),
				gatekeeperURL,
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Ingress",
					},
					Name: constants.ErrorTrackingAPIName,
				},
			},
		},
	)

	return res, nil
}
