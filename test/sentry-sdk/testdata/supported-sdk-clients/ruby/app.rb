require "sentry-ruby"

Sentry.init do |config|
  config.dsn =  ENV["SENTRY_DSN"]
  config.debug = true
  config.release = "v1.0.0"
  config.environment = "dev"
  config.breadcrumbs_logger = [:sentry_logger, :http_logger]
  # INSECURE: Only disable ssl verification when using self-signed certs
  ssl_verification = true
  insecure_ok = ENV["INSECURE_OK"].to_s.downcase
  if insecure_ok == "true"
    ssl_verification = false
  end
  config.transport.ssl_verification = ssl_verification
end

class RubySdkTestError < StandardError
    def message
      "Hello from Ruby Sentry SDK"
    end
end

begin
  Sentry.capture_message("Testing capture_message fropm Ruby Sentry SDK")
  raise RubySdkTestError
  rescue RubySdkTestError => exception
    Sentry.capture_exception(exception)
end
