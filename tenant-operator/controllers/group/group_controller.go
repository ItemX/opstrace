/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Note(Arun): There are a lot of methods that should return wrapped errors.
//
//nolint:lll
package group

import (
	"context"
	"fmt"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	"github.com/fluxcd/pkg/runtime/patch"
	"github.com/fluxcd/pkg/ssa"
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	kerrors "k8s.io/apimachinery/pkg/util/errors"

	"sigs.k8s.io/cli-utils/pkg/kstatus/polling"
	ctrl "sigs.k8s.io/controller-runtime"

	"net/http"
	"net/url"

	jaegerv1 "github.com/jaegertracing/jaeger-operator/apis/v1"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	finalizerName = "group.opstrace.com/finalizer"
)

// +kubebuilder:rbac:groups=opstrace.com,resources=groups,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=opstrace.com,resources=groups/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=opstrace.com,resources=groups/finalizers,verbs=update
// +kubebuilder:rbac:groups=extensions;apps,resources=deployments;deployments/finalizers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=events,verbs=get;list;watch;create;patch
// +kubebuilder:rbac:groups="",resources=configmaps;secrets;serviceaccounts;services;persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=networking.k8s.io,resources=ingresses,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=monitoring.coreos.com,resources=servicemonitors,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=jaegertracing.io,resources=jaegers,verbs=get;list;watch;create;update;patch;delete

// SetupWithManager sets up the controller with the Manager.
func (r *ReconcileGroup) SetupWithManager(mgr ctrl.Manager) error {
	tenantHandler := func(tenant client.Object) []reconcile.Request {
		groups := &opstracev1alpha1.GroupList{}
		opts := &client.ListOptions{
			Namespace: tenant.GetNamespace(),
		}
		err := r.Client.List(context.TODO(), groups, opts)
		if err != nil {
			return nil
		}

		res := make([]reconcile.Request, len(groups.Items))
		for i, e := range groups.Items {
			res[i] = reconcile.Request{
				NamespacedName: types.NamespacedName{
					Name:      e.GetName(),
					Namespace: e.GetNamespace(),
				},
			}
		}

		return res
	}
	return ctrl.NewControllerManagedBy(mgr).
		For(&opstracev1alpha1.Group{}).
		Watches(
			&source.Kind{Type: &opstracev1alpha1.Tenant{}},
			handler.EnqueueRequestsFromMapFunc(tenantHandler),
		).
		Owns(&appsv1.Deployment{}).
		Owns(&netv1.Ingress{}).
		Owns(&v1.ConfigMap{}).
		Owns(&v1.Service{}).
		Owns(&v1.Secret{}).
		Owns(&v1.ServiceAccount{}).
		Owns(&monitoring.ServiceMonitor{}).
		Owns(&jaegerv1.Jaeger{}).
		Complete(r)
}

var _ reconcile.Reconciler = &ReconcileGroup{}

type ClientFactory func(groupId int64) (*common.ArgusClient, error)

// ReconcileGroup reconciles a Group object.
type ReconcileGroup struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	Client       client.Client
	Scheme       *runtime.Scheme
	Transport    *http.Transport
	Recorder     record.EventRecorder
	Log          logr.Logger
	StatusPoller *polling.StatusPoller
}

// Reconcile , The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileGroup) Reconcile(
	ctx context.Context,
	request reconcile.Request,
) (result ctrl.Result, err error) {
	group := &opstracev1alpha1.Group{}
	err = r.Client.Get(ctx, request.NamespacedName, group)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			r.Log.Info(
				"Group has been removed from API",
				"name", request.Name, "namespace", request.Namespace,
			)
			return reconcile.Result{}, nil
		}
		return reconcile.Result{}, err
	}

	cr := group.DeepCopy()

	// Initialize the runtime patcher with the current version of the object.
	patcher := patch.NewSerialPatcher(cr, r.Client)

	// Finalize the reconciliation
	defer func() {
		// Configure the runtime patcher.
		patchOpts := []patch.Option{
			patch.WithFieldOwner(constants.GroupFieldManagerIDString),
		}

		// Patch the object status, conditions and finalizers.
		if patchErr := patcher.Patch(ctx, cr, patchOpts...); patchErr != nil {
			if !cr.GetDeletionTimestamp().IsZero() {
				patchErr = kerrors.FilterOut(patchErr, apierrors.IsNotFound)
			}
			err = kerrors.NewAggregate([]error{err, patchErr})
		}
	}()

	// Add finalizer first if it doesn't exist to avoid the race condition
	// between init and delete.
	if !controllerutil.ContainsFinalizer(cr, finalizerName) {
		controllerutil.AddFinalizer(cr, finalizerName)
		return ctrl.Result{Requeue: true}, nil
	}

	if cr.Status.Inventory == nil {
		cr.Status.Inventory = make(map[string]*v1beta2.ResourceInventory)
	}

	teardown := !cr.ObjectMeta.DeletionTimestamp.IsZero()

	// Read current state
	currentState := NewGroupState()
	err = currentState.Read(ctx, cr, r.Client)
	if err != nil {
		r.Log.Error(err, "error reading state")
		r.manageError(cr, err)
		return reconcile.Result{}, err
	}
	// Get the actions required to reach the desired state
	desiredState := r.getDesiredState(teardown, currentState, cr)

	// Create the server-side apply manager.
	resourceManager := ssa.NewResourceManager(r.Client, r.StatusPoller, ssa.Owner{
		Field: constants.ClusterFieldManagerIDString,
		Group: cr.GetObjectKind().GroupVersionKind().Group,
	})

	// Run the actions to reach the desired state
	actionRunner := common.NewActionRunner(ctx, r.Client, r.Scheme, cr, resourceManager)
	err = actionRunner.RunAll(desiredState)
	if err != nil {
		r.manageError(cr, err)
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
	}

	// manage corresponding Argus group
	if teardown {
		if controllerutil.ContainsFinalizer(cr, finalizerName) {
			// Successfully deleted everything we care about.
			// Remove our finalizer from the list and update it
			controllerutil.RemoveFinalizer(cr, finalizerName)
		}
	} else {
		r.manageSuccess(cr)
	}

	return reconcile.Result{}, nil
}

func (r *ReconcileGroup) getDesiredState(teardown bool, currentState *GroupState, cr *opstracev1alpha1.Group) common.DesiredState {
	desiredState := common.DesiredState{}

	tracingReconciler := NewTracingReconciler(teardown, r.Log)
	desiredState = append(desiredState, tracingReconciler.Reconcile(currentState, cr)...)

	desiredState = append(desiredState, r.getArgusGroupsDesiredState(teardown, cr, currentState.Tenant))

	return desiredState
}

func (r *ReconcileGroup) getArgusGroupsDesiredState(
	teardown bool,
	cr *v1alpha1.Group,
	tenant *opstracev1alpha1.Tenant,
) common.Action {
	tenantReady := apimeta.IsStatusConditionTrue(tenant.Status.Conditions, common.ConditionTypeReady)
	if !tenantReady {
		return common.LogAction{
			Msg:   "tenant is not ready",
			Error: controllers.ErrGrafanaURLTenantNotReady,
		}
	}
	if tenant.Status.Argus.URL == nil {
		return common.LogAction{
			Msg:   "GOUI URL is empty",
			Error: controllers.ErrGrafanaURLTenantURLEmpty,
		}
	}
	argusURL := *tenant.Status.Argus.URL

	parsedUrl, err := url.Parse(argusURL)
	if err != nil {
		return common.LogAction{
			Msg:   "unable to parse argus URL",
			Error: fmt.Errorf("unable to parse argus URL: %w", err),
		}
	}
	username := parsedUrl.User.Username()
	password, _ := parsedUrl.User.Password()

	client, err := common.NewArgusClient(
		argusURL,
		username,
		password,
		r.Transport,
		cr.Spec.ID,
	)
	if err != nil {
		return common.LogAction{
			Msg:   "failed to create Argus client",
			Error: fmt.Errorf("failed to create Argus client: %w", err),
		}
	}

	if teardown {
		return common.DeleteArgusGroupAction{
			Ref:     client,
			Msg:     "delete GOUI group",
			GroupID: cr.Spec.ID,
		}
	}

	return common.CreateArgusGroupAction{
		Ref:       client,
		Msg:       "ensure GOUI group",
		GroupID:   cr.Spec.ID,
		GroupPath: cr.Spec.FullPath,
	}
}

// Handle success case.
func (r *ReconcileGroup) manageSuccess(group *opstracev1alpha1.Group) {
	condition := metav1.Condition{
		Status:             metav1.ConditionTrue,
		Reason:             common.ReconciliationSuccessReason,
		Message:            "All components are in ready state",
		Type:               common.ConditionTypeReady,
		ObservedGeneration: group.GetGeneration(),
	}
	apimeta.SetStatusCondition(&group.Status.Conditions, condition)

	r.Log.Info("group successfully reconciled", "groupId", group.Spec.ID)
}

// Handle error case: update group with error message and status.
func (r *ReconcileGroup) manageError(group *opstracev1alpha1.Group, issue error) {
	condition := metav1.Condition{
		Status:             metav1.ConditionFalse,
		Reason:             common.ReconciliationFailedReason,
		Message:            issue.Error(),
		Type:               common.ConditionTypeReady,
		ObservedGeneration: group.GetGeneration(),
	}
	apimeta.SetStatusCondition(&group.Status.Conditions, condition)
}
