# GKE cluster
resource "google_container_cluster" "primary" {
  name = var.instance_name

  # location - (Optional) The location (region or zone) in which the cluster
  # master will be created, as well as the default node location. If you specify
  # a zone (such as us-central1-a), the cluster will be a zonal cluster with a
  # single cluster master. If you specify a region (such as us-west1), the
  # cluster will be a regional cluster with multiple masters spread across zones
  # in the region, and with default node locations in those zones as well
  location = coalesce(var.zone, var.region)

  resource_labels = merge(var.global_labels, tomap({}))

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1

  network    = google_compute_network.vpc.name
  subnetwork = google_compute_subnetwork.subnet.name

  private_cluster_config {
    enable_private_nodes    = true
    enable_private_endpoint = false
    master_ipv4_cidr_block  = "172.16.0.16/28"
  }

  master_authorized_networks_config {
    cidr_blocks {
      display_name = "all"
      cidr_block   = "0.0.0.0/0"
    }
  }

  ip_allocation_policy {
    cluster_secondary_range_name  = google_compute_subnetwork.subnet.secondary_ip_range.0.range_name
    services_secondary_range_name = google_compute_subnetwork.subnet.secondary_ip_range.1.range_name
  }

  lifecycle {
    ignore_changes = [
      # Any changes to node_config must not replace the entire cluster
      node_config,
    ]
  }

  addons_config {
    network_policy_config {
      disabled = false
    }
  }

  network_policy {
    enabled  = true
    provider = "CALICO"
  }

  # See: https://cloud.google.com/kubernetes-engine/docs/concepts/release-channels
  release_channel {
    channel = var.release_channel
  }

  # See: https://cloud.google.com/kubernetes-engine/docs/how-to/maintenance-windows-and-exclusions
  maintenance_policy {
    recurring_window {
      start_time = var.maintenance_policy_recurring_window["start_time"]
      end_time   = var.maintenance_policy_recurring_window["end_time"]
      recurrence = var.maintenance_policy_recurring_window["recurrence"]
    }
  }
}

# Separately Managed Node Pool
resource "google_container_node_pool" "primary_nodes_hcpu" {
  name = "${google_container_cluster.primary.name}-node-pool-hcpu"

  cluster    = google_container_cluster.primary.name
  node_count = var.num_nodes

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]

    labels = merge(var.global_labels, tomap({
      "gitlab/env"      = var.project_id,
      "gitlab/nodepool" = "primary-nodes"
    }))

    # preemptible  = true
    machine_type = var.gke_machine_type
    # tags can be later used to reference resources (like while creating firewall rules for _these_ node tags)
    tags = ["gke-node", "${var.project_id}-gke", "${var.project_id}-${var.instance_name}-gke-node"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}

resource "google_container_node_pool" "clickhouse_nodes" {
  count = var.provision_clickhouse_node_pool ? 1 : 0
  name  = "${google_container_cluster.primary.name}-ch-node-pool"

  cluster    = google_container_cluster.primary.name
  node_count = var.clickhouse_num_nodes

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]

    labels = merge(var.global_labels, tomap({
      "gitlab/env"      = var.project_id,
      "gitlab/nodepool" = "clickhouse-nodes"
    }))

    taint = [{
      key    = "nodepool"
      value  = "clickhouse-nodes"
      effect = "NO_SCHEDULE"
    }]

    # preemptible  = true
    machine_type = "n1-standard-4"
    # tags can be later used to reference resources (like while creating firewall rules for _these_ node tags)
    tags = ["gke-node", "${var.project_id}-gke", "${var.project_id}-${var.instance_name}-gke-node"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}

# Defer reading the cluster data until the GKE cluster exists.
data "google_container_cluster" "primary" {
  name       = var.instance_name
  depends_on = [google_container_cluster.primary]
}

# Adds a firewall rule to allow connections on 8443/9443 from cluster master nodes
# to worker nodes. This is required for admission controllers/webhooks.
#
# For more cotnext, see:
#   * https://kubernetes.github.io/ingress-nginx/deploy/#gce-gke
#   * https://github.com/kubernetes/kubernetes/issues/79739#issuecomment-509813068
#   * https://github.com/elastic/cloud-on-k8s/issues/1437
#   * https://cloud.google.com/kubernetes-engine/docs/how-to/private-clusters#add_firewall_rules
resource "google_compute_firewall" "allow_ingress_rule" {

  name        = "gke-${var.instance_name}-allow-webhook-fw"
  network     = google_compute_network.vpc.name
  project     = var.project_id
  description = "Allow traffic from cluster master nodes to worker nodes on 8443/tcp and 9443/tcp"

  priority  = 1000
  direction = "INGRESS"

  allow {
    protocol = "tcp"
    ports    = [8443, 9443]
  }

  target_tags   = ["${var.project_id}-${var.instance_name}-gke-node"]
  source_ranges = [google_container_cluster.primary.private_cluster_config[0].master_ipv4_cidr_block]

  # turn on logging
  # log_config {
  #    metadata = "INCLUDE_ALL_METADATA"
  #  }
}
