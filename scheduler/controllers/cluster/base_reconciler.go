package cluster

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	ctrlcommon "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/common"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type BaseReconciler struct {
	Teardown         bool
	Log              logr.Logger
	initialManifests map[string][]byte

	inventoryID    string
	reconcilerName string
	getOverridesF  func(*v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec

	serviceAccountName string

	// This is a workaround for Go's "inheritance". The base object would
	// normally refer to its `applyOverrides` method instead of the subclass'
	// one. We force the subclass method here instead by linking to it and
	// calling it from the base class.
	subclassApplyMethod func(*v1alpha1.Cluster) (*kustomize.Kustomization, error)
}

func (i *BaseReconciler) Reconcile(cr *v1alpha1.Cluster) common.DesiredState {
	if i.Teardown {
		// We are cleaning up, no point in generating manifests as we will use
		// Inventory for that
		return common.DesiredState{
			common.ManifestsPruneAction{
				ComponentName: i.inventoryID,
				GetInventory: func() *v1beta2.ResourceInventory {
					return cr.Status.Inventory[i.inventoryID]
				},
				Msg: fmt.Sprintf("manifests/%s prune", i.reconcilerName),
				Log: i.Log,
			},
		}
	}

	overrideKustomization, err := i.subclassApplyMethod(cr)
	if err != nil {
		return common.DesiredState{
			common.LogAction{
				Msg:   fmt.Sprintf("[%s] failed to create overrides configuration", i.reconcilerName),
				Error: err,
			},
		}
	}
	i.applyOverrides(overrideKustomization, cr)
	manifests, err := ctrlcommon.ApplyKustomization(i.initialManifests, overrideKustomization)
	if err != nil {
		return common.DesiredState{
			common.LogAction{
				Msg:   fmt.Sprintf("[%s] failed to apply overrides to manifests", i.reconcilerName),
				Error: err,
			},
		}
	}

	return common.DesiredState{
		common.ManifestsEnsureAction{
			HealthcheckTimeout: 2 * time.Minute,
			Manifests:          manifests,
			ComponentName:      i.inventoryID,
			GetInventory: func() *v1beta2.ResourceInventory {
				return cr.Status.Inventory[i.inventoryID]
			},
			SetInventory: func(inv *v1beta2.ResourceInventory) {
				cr.Status.Inventory[i.inventoryID] = inv
			},
			Msg: fmt.Sprintf("manifests/%s ensure", i.reconcilerName),
			Log: i.Log,
		},
	}
}

func (i *BaseReconciler) applyOverrides(k *kustomize.Kustomization, cr *v1alpha1.Cluster) {
	overrides := i.getOverridesF(cr)

	if overrides == nil {
		return
	}

	k.CommonLabels = overrides.CommonLabels
	k.Labels = overrides.Labels
	k.CommonAnnotations = overrides.CommonAnnotations
	k.PatchesStrategicMerge = overrides.PatchesStrategicMerge
	k.PatchesJson6902 = append(k.PatchesJson6902, overrides.PatchesJson6902...)
	k.Images = append(k.Images, overrides.Images...)
	k.Replicas = overrides.Replicas
}

func (i *BaseReconciler) applyBaseConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res := &kustomize.Kustomization{
		TypeMeta: kustomize.TypeMeta{
			Kind:       kustomize.KustomizationKind,
			APIVersion: kustomize.KustomizationVersion,
		},
		Namespace: cr.Namespace(),
		Resources: []string{
			ctrlcommon.TmpResourceData,
		},
		PatchesJson6902: make([]types.Patch, 0),
	}

	if len(cr.Spec.ImagePullSecrets) > 0 {
		b, err := json.Marshal(cr.Spec.ImagePullSecrets)
		if err != nil {
			return nil, fmt.Errorf("unable to marshal image pull secrets: %w", err)
		}
		res.PatchesJson6902 = append(res.PatchesJson6902,
			types.Patch{
				Patch: fmt.Sprintf(`[{"op": "add", "path": "/imagePullSecrets", "value": %s}]`, string(b)),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "ServiceAccount",
						},
						Name: i.serviceAccountName,
					},
				},
			},
		)
	}

	return res, nil
}
