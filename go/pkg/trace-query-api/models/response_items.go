package query

import "time"

type SpanItem struct {
	Timestamp    time.Time `ch:"Timestamp" json:"timestamp"`
	SpanID       string    `ch:"spanID" json:"span_id"`
	TraceID      string    `ch:"traceID" json:"trace_id"`
	ServiceName  string    `ch:"serviceName" json:"service_name"`
	Operation    string    `ch:"operation" json:"operation"`
	DurationNano uint64    `ch:"durationNano" json:"duration_nano"`
	// HttpMethod         string    `ch:"httpMethod"`
	// Method             string    `json:"method"`
	StatusCode string `ch:"statusCode" json:"statusCode"`
	// RPCMethod          string    `ch:"rpcMethod"`
}

type TraceItem struct {
	Timestamp   time.Time `ch:"timestamp" json:"timestamp"`
	TraceID     string    `ch:"traceID" json:"trace_id"`
	ServiceName string    `ch:"serviceName" json:"service_name"`
	Operation   string    `ch:"operation" json:"operation"`
	StatusCode  string    `ch:"statusCode" json:"statusCode"`

	Spans      []SpanItem `json:"spans"`
	TotalSpans uint64     `json:"totalSpans"`
}
