variable "project_id" {
  type        = string
  description = "project id"
}

# region and zone used for gke_auth, one must be set.
variable "region" {
  type        = string
  default     = ""
  description = "The region to manage resources in. If not set, the zone will be used instead."
}

variable "zone" {
  type        = string
  default     = ""
  description = "The zone referencing the region to manage resources in. If not set, the region will be used instead."
}

variable "gke_cluster_name" {
  type        = string
  description = "Name of the GKE cluster"
}

// gitlab_oauth_client_id: string representing application client ID
variable "gitlab_oauth_client_id" {
  type      = string
  sensitive = true
  default   = "samplesecret"
}

// gitlab_oauth_client_secret: string representing application client secret
variable "gitlab_oauth_client_secret" {
  type      = string
  sensitive = true
  default   = "samplesecret"
}

// internal_endpoint_token: the token to talk to Gitlab internal auth API
variable "internal_endpoint_token" {
  type      = string
  sensitive = true
  default   = "samplesecret"
}

variable "cluster_secret_name" {
  type    = string
  default = "auth-secret"
}

variable "cluster_secret_namespace" {
  type    = string
  default = "default"
}

variable "cloudflare_secret_name" {
  type    = string
  default = "cloudflare-secret"
}

variable "cloudflare_secret_namespace" {
  type    = string
  default = "default"
}

variable "cloudflare_api_token" {
  type      = string
  sensitive = true
  default   = "sampletoken"
}

variable "disable_cluster_creation" {
  type    = bool
  default = false
}

variable "cluster_manifest" {
  type    = string
  default = ""
}

variable "create_gcs_secret" {
  type    = bool
  default = false
}

variable "gcs_secret_name" {
  type    = string
  default = "clickhouse-gcs-secret"
}

variable "gcs_access_key_id" {
  type      = string
  sensitive = true
  default   = "sample"
}

variable "gcs_access_key_secret" {
  type      = string
  sensitive = true
  default   = "samplesecret"
}
