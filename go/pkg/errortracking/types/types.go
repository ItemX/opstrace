package types

const (
	SupportedTypeException = "exception"
	SupportedTypeMessage   = "message"
	UnsupportedDataItem    = "unknown"

	EventType   = "event"
	SessionType = "session"
)

type ETLimits struct {
	APIReadsLimit  int `json:"et_api_reads"`
	APIWritesLimit int `json:"et_api_writes"`
}

type LimitsConfig struct {
	APIVersion uint64              `json:"api_version"`
	Sizings    map[string]ETLimits `json:"sizings"`
	Bindings   struct {
		Groups   map[uint64]string `json:"groups"`
		Projects map[uint64]string `json:"projects"`
	} `json:"bindings"`
}
