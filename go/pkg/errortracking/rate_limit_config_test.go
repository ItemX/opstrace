package errortracking

import (
	"os"
	"path"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Context("rate limiter config load", func() {
	var tmpDir string
	var tmpFile string

	BeforeEach(func() {
		var err error
		tmpDir, err = os.MkdirTemp("", "FileObserverTestFiles")
		Expect(err).NotTo(HaveOccurred())

		tmpFile = path.Join(tmpDir, "target")
	})

	AfterEach(func() {
		// Tmp dir is different on every run, we do not need to worry about errors
		// here.
		_ = os.RemoveAll(tmpDir)
	})

	It("fails if the config does not exist", func() {
		_, err := ReadConfig("/tmp/foobar.yaml")
		Expect(err).To(HaveOccurred())
	})

	It("fails if the config is not a valid yaml", func() {
		err := os.WriteFile(tmpFile, []byte("'asf2341--asdf"), 0600)
		Expect(err).NotTo(HaveOccurred())

		_, err = ReadConfig(tmpFile)
		Expect(err).To(HaveOccurred())
	})

	It("succeeds if the configuration is valid", func() {
		conf := []byte(`
---
api_version: 1
sizings:
  default:
    et_api_reads: 1001
    et_api_writes: 999
  xl:
    et_api_reads: 100000
    et_api_writes: 100000
bindings:
  groups:
    22: xl
  projects:
    1: xl
`)
		err := os.WriteFile(tmpFile, conf, 0600)
		Expect(err).NotTo(HaveOccurred())

		res, err := ReadConfig(tmpFile)
		Expect(err).ToNot(HaveOccurred())
		Expect(res.Bindings.Groups[22]).To(Equal("xl"))
		Expect(res.Sizings["default"].APIReadsLimit).To(Equal(1001))
		Expect(res.Sizings["default"].APIWritesLimit).To(Equal(999))
	})

	It("fails if the config has invalid api version", func() {
		conf := []byte(`
---
api_version: 2
sizings:
  default:
    et_api_reads: 1000
    et_api_writes: 1000
  xl:
    et_api_reads: 10000000
    et_api_writes: 100000000
bindings:
  groups:
    22: xl
  projects:
    1: xl
`)
		err := os.WriteFile(tmpFile, conf, 0600)
		Expect(err).NotTo(HaveOccurred())

		_, err = ReadConfig(tmpFile)
		Expect(err).Should(MatchError(ErrInvalidLimitsConfiguration))
	})

	It("fails if the config does not contain default limtis", func() {
		conf := []byte(`
---
api_version: 1
sizings:
  xl:
    et_api_reads: 10000000
    et_api_writes: 100000000
bindings:
  groups:
    22: xl
  projects:
    1: xl
`)
		err := os.WriteFile(tmpFile, conf, 0600)
		Expect(err).NotTo(HaveOccurred())

		_, err = ReadConfig(tmpFile)
		Expect(err).Should(MatchError(ErrInvalidLimitsConfiguration))
	})

	It("fails if the config has incomplete limits", func() {
		conf := []byte(`
---
api_version: 1
sizings:
  default:
    et_api_reads: 1000
  xl:
    et_api_writes: 100000000
bindings:
  groups:
    22: xl
  projects:
    1: xl
`)
		err := os.WriteFile(tmpFile, conf, 0600)
		Expect(err).NotTo(HaveOccurred())

		_, err = ReadConfig(tmpFile)
		Expect(err).Should(MatchError(ErrInvalidLimitsConfiguration))
	})

	It("fails if project bindings point to inexistant sizing", func() {
		conf := []byte(`
---
api_version: 2
sizings:
  default:
    et_api_reads: 1000
    et_api_writes: 1000
  xl:
    et_api_reads: 10000000
    et_api_writes: 100000000
bindings:
  groups:
    22: xl
  projects:
    1: xxl
`)
		err := os.WriteFile(tmpFile, conf, 0600)
		Expect(err).NotTo(HaveOccurred())

		_, err = ReadConfig(tmpFile)
		Expect(err).Should(MatchError(ErrInvalidLimitsConfiguration))
	})

	It("fails if group bindings point to inexistant sizing", func() {
		conf := []byte(`
---
api_version: 2
sizings:
  default:
    et_api_reads: 1000
    et_api_writes: 1000
  xl:
    et_api_reads: 10000000
    et_api_writes: 100000000
bindings:
  groups:
    22: xxl
  projects:
    1: xl
`)
		err := os.WriteFile(tmpFile, conf, 0600)
		Expect(err).NotTo(HaveOccurred())

		_, err = ReadConfig(tmpFile)
		Expect(err).Should(MatchError(ErrInvalidLimitsConfiguration))
	})
})
