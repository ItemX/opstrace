Cypress.Commands.add("loginViaUi", (username, password) => {
  cy.session([username, password], () => {
    expect(username).to.not.be.empty;
    expect(password).to.not.be.empty;
    cy.visit("/users/sign_in");
    cy.get("[data-testid=username-field]").type(username);
    cy.get("[data-testid=password-field]").type(password);
    cy.get("[data-testid=sign-in-button]").click();
    cy.url().should("eq", `https://${Cypress.env("GITLAB_DOMAIN")}/`);
  });
});
