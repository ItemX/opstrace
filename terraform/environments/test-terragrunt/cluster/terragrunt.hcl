terraform {
  source = "${get_terragrunt_dir()}/../../../modules//opstracecluster"
}

include {
  path = find_in_parent_folders()
}

dependency "gke" {
  config_path = "../gke"
  mock_outputs = {
    kubernetes_cluster_name     = "clustername"
    certmanager_service_account = "mock-sa"
    externaldns_service_account = "mock-sa"
  }
  mock_outputs_allowed_terraform_commands = ["init", "validate", "validate-inputs", "plan"]
}

dependency "install_gitlab" {
  config_path = "../install_gitlab"
  mock_outputs = {
    gitlab_url              = "https://gitlab.com"
    oauth_client_id         = "samplesecret"
    oauth_client_secret     = "samplesecret"
    internal_endpoint_token = "samplesecret"
  }
  mock_outputs_allowed_terraform_commands = ["init", "validate", "validate-inputs", "plan"]
}

dependency "opstrace" {
  config_path  = "../opstrace"
  skip_outputs = true
}

locals {
  common_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
}

inputs = {
  project_id                 = local.common_vars.inputs.project_id
  zone                       = local.common_vars.inputs.location
  gke_cluster_name           = dependency.gke.outputs.kubernetes_cluster_name
  cluster_secret_name        = local.common_vars.inputs.cluster_secret_name
  cluster_secret_namespace   = local.common_vars.inputs.cluster_secret_namespace
  gitlab_oauth_client_id     = dependency.install_gitlab.outputs.oauth_client_id
  gitlab_oauth_client_secret = dependency.install_gitlab.outputs.oauth_client_secret
  internal_endpoint_token    = dependency.install_gitlab.outputs.internal_endpoint_token
  cluster_manifest = yamlencode({
    "apiVersion" : "opstrace.com/v1alpha1"
    "kind" : "Cluster"
    "metadata" : {
      "name" : local.common_vars.inputs.instance_name,
    },
    "spec" : {
      "target" : "gcp",
      "goui" : {
        "image" : "registry.gitlab.com/gitlab-org/opstrace/opstrace-ui/gitlab-observability-ui:bac2ebe8"
      }
      "dns" : {
        "certificateIssuer" : local.common_vars.inputs.cert_issuer,
        "domain" : local.common_vars.inputs.domain,
        "acmeEmail" : local.common_vars.inputs.acme_email,
        "acmeServer" : local.common_vars.inputs.acme_server,
        "dns01Challenge" : {
          # Use google cloud DNS (where our domain is hosted).
          "cloudDNS" : {
            "project" : local.common_vars.inputs.project_id
          }
        },
        "externalDNSProvider" : {
          # Use google cloud DNS (where our domain is hosted).
          "gcp" : {
            "dnsServiceAccountName" : dependency.gke.outputs.externaldns_service_account
            "managedZoneName" : local.common_vars.inputs.cloud_dns_zone
          }
        },
        "firewallSourceIPsAllowed" : ["0.0.0.0/0"]
        # Use google cloud DNS (where our domain is hosted).
        "gcpCertManagerServiceAccount" : dependency.gke.outputs.certmanager_service_account
      },
      "gitlab" : {
        "instanceUrl" : dependency.install_gitlab.outputs.gitlab_url,
        "groupAllowedAccess" : "*",
        "groupAllowedSystemAccess" : "*",
        "authSecret" : {
          "name" : local.common_vars.inputs.cluster_secret_name
        }
      }
    }
  })
}
