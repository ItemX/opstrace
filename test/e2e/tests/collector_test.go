package tests

import (
	"context"
	"fmt"
	"net/url"
	"reflect"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gopkg.in/yaml.v3"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/tenant"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var _ = Describe("tenant collector provisioning", Ordered, Serial, func() {
	cluster := &schedulerv1alpha1.Cluster{}
	beforeAllEnsureCluster(cluster)

	gitlabObservabilityTenant := &schedulerv1alpha1.GitLabObservabilityTenant{}
	beforeAllEnsureGitLabObservabilityTenant(gitlabObservabilityTenant)

	Context("on creating an example tenant", func() {
		It("ensures it can be created successfully", func(ctx SpecContext) {
			expectGitLabObservabilityTenantReady(ctx, gitlabObservabilityTenant)
			expectNamespaceCreated(ctx, gitlabObservabilityTenant.Namespace())
			expectNamespacePodsReady(ctx, gitlabObservabilityTenant.Namespace())
		})
	})

	Context("ensures resources are configured for the tenant properly", func() {
		deployment := &appsv1.Deployment{}
		It("by checking collector deployment", func(ctx SpecContext) {
			Expect(k8sClient.Get(
				ctx,
				client.ObjectKey{
					Name:      constants.OtelCollectorComponentName,
					Namespace: gitlabObservabilityTenant.Namespace(),
				}, deployment),
			).To(Succeed())
			// we're expecting the deployment to have a single collector container
			Expect(deployment.Spec.Template.Spec.Containers).To(HaveLen(1))
		})

		It("by checking tenant ID", func() {
			tenantIDConfigured := false
			collectorContainer := deployment.Spec.Template.Spec.Containers[0]
			for _, args := range collectorContainer.Args {
				if args == fmt.Sprintf(
					"--set=exporters.gitlabobservability.tenant_id=%d",
					gitlabObservabilityTenant.Spec.TopLevelNamespaceID,
				) {
					tenantIDConfigured = true
					break
				}
			}
			Expect(tenantIDConfigured).To(BeTrue())
		})

		It("by checking ClickHouse credentials", func(ctx SpecContext) {
			secret := &corev1.Secret{}
			Expect(k8sClient.Get(
				ctx,
				client.ObjectKey{
					Name:      constants.GitLabObservabilityTenantCHCredentialsSecretName,
					Namespace: gitlabObservabilityTenant.Namespace(),
				},
				secret,
			)).To(Succeed())

			var nativeURL *url.URL
			if _, ok := secret.Data[constants.ClickHouseCredentialsNativeEndpointKey]; ok {
				nativeURL = common.MustParse(
					string(secret.Data[constants.ClickHouseCredentialsNativeEndpointKey]),
				)
			}
			Expect(nativeURL).ToNot(BeNil())

			chCredentialsConfigured := false
			collectorContainer := deployment.Spec.Template.Spec.Containers[0]
			for _, args := range collectorContainer.Args {
				if args == fmt.Sprintf(
					"--set=exporters.gitlabobservability.clickhouse_dsn=%s/tracing",
					nativeURL.String(),
				) {
					chCredentialsConfigured = true
					break
				}
			}
			Expect(chCredentialsConfigured).To(BeTrue())
		})

		It("by checking ingress", func(ctx SpecContext) {
			ingress := &netv1.Ingress{}
			Expect(k8sClient.Get(
				ctx,
				client.ObjectKey{
					Name:      constants.OtelCollectorComponentName,
					Namespace: gitlabObservabilityTenant.Namespace(),
				},
				ingress,
			)).To(Succeed())
			// expected ingress rule(s):
			/*
				[
				  {
				    "host": "abhtngr.dev",
				    "http": {
				      "paths": [
				        {
				          "backend": {
				            "service": {
				              "name": "otel-collector",
				              "port": {
				                "name": "otlp-http"
				              }
				            }
				          },
				          "path": "/v3/12345/(?<projectID>[0-9]+)/ingest/traces",
				          "pathType": "Prefix"
				        }
				      ]
				    }
				  }
				]
			*/
			Expect(ingress.Spec.Rules).To(HaveLen(1))
			ingressRule := ingress.Spec.Rules[0]
			Expect(ingressRule.HTTP.Paths).To(HaveLen(1))
			ingressPath := ingressRule.HTTP.Paths[0]
			Expect(ingressPath.Path).To(Equal(
				fmt.Sprintf(
					tenant.IngressPathTmpl,
					gitlabObservabilityTenant.Spec.TopLevelNamespaceID,
				),
			))
			Expect(*ingressPath.PathType).To(Equal(netv1.PathTypePrefix))
			backend := ingressPath.Backend
			Expect(backend.Service.Name).To(Equal(constants.OtelCollectorComponentName))
			Expect(backend.Service.Port.Name).To(Equal("otlp-http"))
		})

		It("by checking collector definition", func(ctx SpecContext) {
			configmap := &corev1.ConfigMap{}
			Expect(k8sClient.Get(
				ctx,
				client.ObjectKey{
					Name:      constants.OtelCollectorComponentName,
					Namespace: gitlabObservabilityTenant.Namespace(),
				},
				configmap,
			)).To(Succeed())
			// ensure config unmarshalling
			var config map[string]any
			Expect(yaml.Unmarshal([]byte(configmap.Data["config.yaml"]), &config)).To(Succeed())
			// ensure processor configuration
			processors, ok := config["processors"].(map[string]any)
			if ok {
				attributeProcessor := processors["attributes/pid"]
				Expect(
					reflect.DeepEqual(
						attributeProcessor,
						map[string]any{
							"actions": []any{
								map[string]any{
									"action":       "upsert",
									"key":          "gitlab.target_project_id",
									"from_context": "metadata.x-target-projectid",
								},
							},
						},
					),
				).To(BeTrue())
			}
		})
	})

	Context("on deleting the example tenant", func() {
		It("ensures it can be deleted successfully", func(ctx SpecContext) {
			deleteCustomResourceAndVerify(ctx, gitlabObservabilityTenant)
		})
	})
})

// Load example gitlabobservabilitytenant and ensure it's ready
func beforeAllEnsureGitLabObservabilityTenant(out *schedulerv1alpha1.GitLabObservabilityTenant) {
	BeforeAll(func(ctx SpecContext) {
		By("load GitLabObservabilityTenant config example")
		loadYaml("../../../scheduler/config/examples/GitLabObservabilityTenant.yaml", out)
		createOrUpdate(ctx, out)
	})
}

// check if the tenant namespace was created
func expectNamespaceCreated(
	ctx context.Context,
	namespace string,
) {
	By("check given k8s namespace is created")
	Eventually(func(g Gomega) {
		cr := &corev1.Namespace{}
		g.Expect(k8sClient.Get(ctx, client.ObjectKey{Name: namespace}, cr)).To(Succeed())
	}).WithTimeout(time.Second * 10).Should(Succeed())
}

// wait for the tenant to have a successful ready condition
func expectGitLabObservabilityTenantReady(
	ctx context.Context,
	tenant *schedulerv1alpha1.GitLabObservabilityTenant,
) {
	By("check GitLabObservabilityTenant CR has ready condition: True")
	Eventually(func(g Gomega) {
		cr := &schedulerv1alpha1.GitLabObservabilityTenant{}
		g.Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(tenant), cr)).To(Succeed())
		g.Expect(
			meta.IsStatusConditionTrue(cr.Status.Conditions, common.ConditionTypeReady),
		).To(BeTrue())
	}).WithTimeout(time.Minute * 2).Should(Succeed())
}
