package kind

import (
	"context"
	"fmt"

	gocommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/common"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
)

// Kind is mostly a noop implementation as we expect
// the kind cluster to be already running.
// It is equivalent of "bring your own kubeconfig" mode.
type Kind struct{}

func NewKind() *Kind {
	return &Kind{}
}

func (g *Kind) LoadConfiguration() (common.Configuration, error) {
	cfg := common.Configuration{}
	schedulerImage, err := common.GetEnv(common.TestEnvSchedulerImage)
	if err != nil {
		return cfg, err
	}

	gobHost, err := common.GetEnv("TEST_GOB_HOST")
	if err != nil {
		gobHost = "localhost"
	}

	gitlabHost, err := common.GetEnv("TEST_GITLAB_HOST")
	if err != nil {
		gitlabHost = "gitlab.com"
	}

	return common.Configuration{
		GOBHost:        gobHost,
		GitLabHost:     gitlabHost,
		InstanceName:   "kind",
		SchedulerImage: schedulerImage,
		// We're not deploying GitLab in this case
		GitLabImage: "",
		// TODO(joe): we can remove this once GOUI is gone
		GOUIImage: constants.ArgusRegistryName + "/" + constants.ArgusImageName + ":latest",
	}, nil
}

func (g *Kind) InternalConfiguration() (map[string]string, error) {
	// nothing to see here
	return map[string]string{}, nil
}

func (g *Kind) CreateGitLabInstance() error {
	return nil
}

func (g *Kind) DestroyGitLabInstance() error {
	return nil
}

func (g *Kind) CreateK8sCluster() error {
	return nil
}

// GetRestConfig returns the default go-client rest config.
// See https://pkg.go.dev/sigs.k8s.io/controller-runtime/pkg/client/config#GetConfig for more details.
func (g *Kind) GetRestConfig(context.Context) (*rest.Config, error) {
	c, err := config.GetConfig()
	if err != nil {
		return nil, fmt.Errorf("get rest config: %w", err)
	}
	return c, nil
}

func (g *Kind) DestroyK8sCluster() error {
	// do nothing right now
	return nil
}

//nolint:unparam
func (g *Kind) ConfigureCluster(out *schedulerv1alpha1.Cluster) error {
	out.Spec.Target = gocommon.KIND
	return nil
}

func (g *Kind) Close() error {
	// do nothing right now
	return nil
}
