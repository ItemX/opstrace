# error-tracking-test Angular

Install Angular:

```sh
npm install -g @angular/cli
```

edit main.ts and add your sentry dsn and then execute:

```sh
cd exceptions/angular/my-app
npm install
ng serve --open
```

Angular SDK sends error events using the POST envelope
