import https from "https";
import * as Sentry from "@sentry/node";

const unsafeHttpsModule = {
  request(options: any, callback: any) {
    const rejectUnauthorized = process.env.INSECURE_OK?.toString().toLowerCase() == "true" ? false : true;
    console.info("(Making HTTP request with unsafe https module with rejectUnauthorized: %s)", rejectUnauthorized);
    return https.request({ ...options, rejectUnauthorized: rejectUnauthorized }, callback);
  },
};

Sentry.init({
  dsn: process.env.SENTRY_DSN,
  debug: true,
  transportOptions: {
    httpModule: unsafeHttpsModule,
    url: process.env.SENTRY_DSN,
  },
});

Sentry.captureMessage('Testing captureMessage from JS Sentry SDK');

Sentry.captureException(new Error("Hello from Sentry Javascript SDK"));

console.log("About to crash!");
// This will generate a session with status = "crashed" since we are not capturing the error
// and this is the reason why the application exits. The application exits with a non 0 exit code.
// If we remove the following lines then we will receive a session with status = "exited" since
// the application will exit without an error.
setTimeout(function () {
  throw new Error('We crashed!!!!!');
}, 2);