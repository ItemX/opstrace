package datasource_test

import (
	"context"
	"crypto/tls"
	"fmt"
	"net/http"
	"sync"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	. "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/matchers"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
	tenantapiv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/datasource"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var _ = Context("datasource controller", func() {
	var toDelete []client.Object
	var ctrCtx context.Context
	var ctrCancelF context.CancelFunc
	var ctrWg *sync.WaitGroup

	BeforeEach(func() {
		toDelete = make([]client.Object, 0)
	})

	// NOTE(prozlach): Having controller started/stopped right before/after
	// tests allows us to remove the test objects without triggering
	// reconciliation. This in turn makes tests much more clean as during each
	// tet, the controller is reconciling only the single object, instead of
	// also reconciling objects from previous tests that we couldn't remove due
	// to finalizers
	JustBeforeEach(func() {
		ctrCtx, ctrCancelF = context.WithCancel(context.TODO())
		ctrWg = new(sync.WaitGroup)

		By("setting up controller-manager")
		k8sManager, err := ctrl.NewManager(restConfig,
			ctrl.Options{
				Scheme: scheme.Scheme,
				// disable metrics binding to prevent port clashes
				MetricsBindAddress: "0",
			})
		Expect(err).NotTo(HaveOccurred())
		err = (&datasource.DatasourceReconciler{
			Client:   k8sManager.GetClient(),
			Scheme:   k8sManager.GetScheme(),
			Log:      ctrl.Log.WithName("controllers").WithName("datasource"),
			Recorder: k8sManager.GetEventRecorderFor("controller"),
			/* #nosec G402 */
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					InsecureSkipVerify: true,
				},
			},
		}).SetupWithManager(k8sManager)
		Expect(err).NotTo(HaveOccurred())

		defer ctrWg.Add(1)
		go func() {
			defer GinkgoRecover()
			defer ctrWg.Done()
			err = k8sManager.Start(ctrCtx)
			Expect(err).ToNot(HaveOccurred(), "failed to run manager")
		}()
	})

	JustAfterEach(func() {
		By("stopping controller-manager")
		ctrCancelF()
		ctrWg.Wait()
	})

	AfterEach(func() {
		for _, obj := range toDelete {
			Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(obj), obj)).To(Succeed())
			obj.SetFinalizers([]string{})
			Expect(k8sClient.Update(ctx, obj)).Should(Succeed(), "unable to remove finalizers")
			Expect(client.IgnoreNotFound(k8sClient.Delete(ctx, obj))).Should(Succeed())
		}
	})

	It("handles Tenant CR not being ready", func() {
		namespace := createNs("tenant-not-ready")
		toDelete = append(toDelete, namespace)

		toDelete = append(toDelete, createTenant(false, "", namespace.Name))
		ds := createPlainDS("test-ds", namespace.Name)
		toDelete = append(toDelete, ds)

		Eventually(func(g Gomega) {
			g.Expect(
				k8sClient.Get(ctx, client.ObjectKeyFromObject(ds), ds),
			).To(Succeed())

			g.Expect(
				apimeta.IsStatusConditionTrue(ds.Status.Conditions, common.ConditionTypeReady),
			).To(BeFalse())

			g.Expect(ds.Status.Conditions).To(HaveLen(1))

			g.Expect(ds.Status.Conditions[0].Message).To(ContainSubstring("tenant is not ready"))
		}).Should(Succeed())
	})

	It("handles Tenant CR not having GOUI url set", func() {
		namespace := createNs("tenant-no-goui-url")
		toDelete = append(toDelete, namespace)

		toDelete = append(toDelete, createTenant(true, "", namespace.Name))
		ds := createPlainDS("test-ds", namespace.Name)
		toDelete = append(toDelete, ds)

		Eventually(func(g Gomega) {
			g.Expect(
				k8sClient.Get(ctx, client.ObjectKeyFromObject(ds), ds),
			).To(Succeed())

			g.Expect(
				apimeta.IsStatusConditionTrue(ds.Status.Conditions, common.ConditionTypeReady),
			).To(BeFalse())

			g.Expect(ds.Status.Conditions).To(HaveLen(1))

			g.Expect(ds.Status.Conditions[0].Message).To(ContainSubstring("GOUI URL is not set"))
		}).Should(Succeed())
	})

	It("requires GOUI Group to exist first", func() {
		namespace := createNs("tenant-no-goui-group")
		toDelete = append(toDelete, namespace)

		phs := testutils.NewProgrammableHTTPServer(logger, "admin:foo")
		phs.Start()
		DeferCleanup(phs.Stop)

		toDelete = append(toDelete, createTenant(true, phs.ServerURL(), namespace.Name))
		ds := createPlainDS("test-ds", namespace.Name)
		toDelete = append(toDelete, ds)
		groupQueryPath := fmt.Sprintf("/api/groups/%d", testGroupID)
		phs.SetResponse("GET", groupQueryPath, 404, `{"message":"Organization not found"}`)

		Eventually(func(g Gomega) {
			g.Expect(
				k8sClient.Get(ctx, client.ObjectKeyFromObject(ds), ds),
			).To(Succeed())

			g.Expect(
				apimeta.IsStatusConditionTrue(ds.Status.Conditions, common.ConditionTypeReady),
			).To(BeFalse())

			g.Expect(ds.Status.Conditions).To(HaveLen(1))

			g.Expect(ds.Status.Conditions[0].Message).To(ContainSubstring("GOUI group has not been provisioned yet"))
		}).Should(Succeed())
	})

	It("it handles GOUI being unavailable", func() {
		namespace := createNs("tenant-goui-borked")
		toDelete = append(toDelete, namespace)

		toDelete = append(toDelete, createTenant(true, "http://foo:bar@127.0.0.1:12212", namespace.Name))
		ds := createPlainDS("test-ds", namespace.Name)
		toDelete = append(toDelete, ds)

		Eventually(func(g Gomega) {
			g.Expect(
				k8sClient.Get(ctx, client.ObjectKeyFromObject(ds), ds),
			).To(Succeed())

			g.Expect(
				apimeta.IsStatusConditionTrue(ds.Status.Conditions, common.ConditionTypeReady),
			).To(BeFalse())

			g.Expect(ds.Status.Conditions).To(HaveLen(1))

			g.Expect(ds.Status.Conditions[0].Message).To(ContainSubstring("unable to check if group"))
		}).Should(Succeed())
	})

	It("it handles datasource with inconsistent groups", func() {
		namespace := createNs("tenant-ds-inconsist")
		toDelete = append(toDelete, namespace)

		toDelete = append(toDelete, createTenant(true, "http://foo:bar@127.0.0.1:12212", namespace.Name))
		ds := &tenantapiv1alpha1.DataSource{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-ds",
				Namespace: namespace.Name,
			},
			Spec: tenantapiv1alpha1.DataSourceSpec{
				Name: "jaeger.yaml",
				Datasources: []tenantapiv1alpha1.DataSourceFields{
					{
						Name: "Maryna",
						Type: "jaeger",
						Url:  "example.com/v1/jaeger/2",
						// Make this the default datasource that is
						// loaded automatically on the explore view
						IsDefault:       true,
						Access:          "proxy",
						Editable:        false,
						Version:         1,
						GroupId:         testGroupID,
						WithCredentials: false,
						JsonData: &tenantapiv1alpha1.DataSourceJsonData{
							KeepCookies: []string{
								constants.SessionCookieName,
							},
						},
					},
					{
						Name: "Boryna",
						Type: "jaeger",
						Url:  "example.com/v1/jaeger/2",
						// Make this the default datasource that is
						// loaded automatically on the explore view
						IsDefault:       true,
						Access:          "proxy",
						Editable:        false,
						Version:         1,
						GroupId:         testGroupID + 1,
						WithCredentials: false,
						JsonData: &tenantapiv1alpha1.DataSourceJsonData{
							KeepCookies: []string{
								constants.SessionCookieName,
							},
						},
					},
				},
			},
		}
		Expect(k8sClient.Create(ctx, ds)).To(Succeed())
		toDelete = append(toDelete, ds)

		Eventually(func(g Gomega) {
			g.Expect(
				k8sClient.Get(ctx, client.ObjectKeyFromObject(ds), ds),
			).To(Succeed())

			g.Expect(
				apimeta.IsStatusConditionTrue(ds.Status.Conditions, common.ConditionTypeReady),
			).To(BeFalse())

			g.Expect(ds.Status.Conditions).To(HaveLen(1))

			g.Expect(ds.Status.Conditions[0].Message).To(ContainSubstring("single Datasoruce CR contains configuration for mutiple groups"))
		}).Should(Succeed())
	})

	It("creates a datasource", func() {
		namespace := createNs("tenant-ds-create")
		toDelete = append(toDelete, namespace)

		phs := testutils.NewProgrammableHTTPServer(logger, "admin:foo")
		phs.Start()
		DeferCleanup(phs.Stop)

		toDelete = append(toDelete, createTenant(true, phs.ServerURL(), namespace.Name))
		ds := createPlainDS("test-ds", namespace.Name)
		toDelete = append(toDelete, ds)
		groupQueryPath := fmt.Sprintf("/api/groups/%d", testGroupID)
		phs.SetResponse("GET", groupQueryPath, 200, fmt.Sprintf(`{"id": %d, "name": "maryna" }`, testGroupID))
		checkDsQueryPath := fmt.Sprintf("/api/datasources/name/%s", ds.Spec.Datasources[0].Name)
		phs.SetResponse("GET", checkDsQueryPath, 404, `{"message":"Data source not found"}`)
		createDsQueryPath := "/api/datasources"
		phs.SetResponse("POST", createDsQueryPath, 200, `{"id": 10}`)

		Eventually(func(g Gomega) {
			g.Expect(
				k8sClient.Get(ctx, client.ObjectKeyFromObject(ds), ds),
			).To(Succeed())

			g.Expect(
				apimeta.IsStatusConditionTrue(ds.Status.Conditions, common.ConditionTypeReady),
			).To(BeTrue())
		}).Should(Succeed())

		requests := phs.GetRequests("POST", createDsQueryPath)
		Expect(requests).ToNot(BeEmpty())
	})

	It("updates a datasource", func() {
		namespace := createNs("tenant-ds-update")
		toDelete = append(toDelete, namespace)

		phs := testutils.NewProgrammableHTTPServer(logger, "admin:foo")
		phs.Start()
		DeferCleanup(phs.Stop)

		toDelete = append(toDelete, createTenant(true, phs.ServerURL(), namespace.Name))
		ds := createPlainDS("test-ds", namespace.Name)
		toDelete = append(toDelete, ds)
		groupQueryPath := fmt.Sprintf("/api/groups/%d", testGroupID)
		phs.SetResponse("GET", groupQueryPath, 200, fmt.Sprintf(`{"id": %d, "name": "maryna" }`, testGroupID))
		checkDsQueryPath := fmt.Sprintf("/api/datasources/name/%s", ds.Spec.Datasources[0].Name)
		phs.SetResponse("GET", checkDsQueryPath, 200, `{"id": 10}`)
		updateDsQueryPath := "/api/datasources/10"
		phs.SetResponse("PUT", updateDsQueryPath, 200, ``)

		Eventually(func(g Gomega) {
			g.Expect(
				k8sClient.Get(ctx, client.ObjectKeyFromObject(ds), ds),
			).To(Succeed())

			g.Expect(
				apimeta.IsStatusConditionTrue(ds.Status.Conditions, common.ConditionTypeReady),
			).To(BeTrue())
		}).Should(Succeed())

		requests := phs.GetRequests("PUT", updateDsQueryPath)
		Expect(requests).ToNot(BeEmpty())
	})

	It("removes a datasource", func() {
		namespace := createNs("tenant-ds-delete")
		toDelete = append(toDelete, namespace)

		phs := testutils.NewProgrammableHTTPServer(logger, "admin:foo")
		phs.Start()
		DeferCleanup(phs.Stop)

		toDelete = append(toDelete, createTenant(true, phs.ServerURL(), namespace.Name))
		ds := createPlainDS("test-ds", namespace.Name)
		groupQueryPath := fmt.Sprintf("/api/groups/%d", testGroupID)
		phs.SetResponse("GET", groupQueryPath, 200, fmt.Sprintf(`{"id": %d, "name": "maryna" }`, testGroupID))
		checkDsQueryPath := fmt.Sprintf("/api/datasources/name/%s", ds.Spec.Datasources[0].Name)
		phs.SetResponse("GET", checkDsQueryPath, 404, `{"message":"Data source not found"}`)
		createDsQueryPath := "/api/datasources"
		phs.SetResponse("POST", createDsQueryPath, 200, `{"id": 10}`)

		Eventually(func(g Gomega) {
			g.Expect(
				k8sClient.Get(ctx, client.ObjectKeyFromObject(ds), ds),
			).To(Succeed())

			g.Expect(
				apimeta.IsStatusConditionTrue(ds.Status.Conditions, common.ConditionTypeReady),
			).To(BeTrue())
		}).Should(Succeed())

		By("removing datasource")
		deleteDsQueryPath := fmt.Sprintf("/api/datasources/name/%s", ds.Spec.Datasources[0].Name)
		phs.SetResponse("DELETE", deleteDsQueryPath, 200, "")

		Expect(k8sClient.Delete(ctx, ds)).To(Succeed())

		By("waiting for controller to reconcile")
		Eventually(func(g Gomega) {
			requests := phs.GetRequests("DELETE", deleteDsQueryPath)
			g.Expect(requests).ToNot(BeEmpty())

			g.Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(ds), ds)).To(BeNotFound())
		}).Should(Succeed())
	})
})
