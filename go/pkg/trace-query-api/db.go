package query

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.uber.org/zap"

	models "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/trace-query-api/models"
)

type QueryDB interface {
	GetTraces(ctx context.Context, projectID int64) ([]*models.TraceItem, error)
	Close() error
}

type querier struct {
	db     clickhouse.Conn
	logger *zap.Logger
}

func NewQuerier(clickHouseDsn string, opts *clickhouse.Options, logger *zap.Logger) (QueryDB, error) {
	dbOpts, err := clickhouse.ParseDSN(clickHouseDsn)
	if err != nil {
		return nil, fmt.Errorf("failed to parse clickhouse DSN: %w", err)
	}
	if opts != nil {
		dbOpts.MaxOpenConns = opts.MaxOpenConns
		dbOpts.MaxIdleConns = opts.MaxIdleConns
	}
	dbOpts.ConnMaxLifetime = 1 * time.Hour
	if logger.Level() == zap.DebugLevel {
		dbOpts.Debug = true
	}

	db, err := clickhouse.Open(dbOpts)
	if err != nil {
		return nil, fmt.Errorf("open DB :%w", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	err = db.Ping(ctx)
	if err != nil {
		return nil, fmt.Errorf("connecting to clickhouse: %w", err)
	}
	return &querier{db: db, logger: logger}, nil
}

const baseQuery = `
SELECT
    traceID,
    any(start) AS trace_start_ts,
    length(spans) As total_spans,
    groupArray((spanID, start, serviceName, operation, statusCode, duration)) AS spans
FROM
(
    WITH
        toStartOfMinute(toDateTime(?)) AS r_start,
        toStartOfMinute(toDateTime(?)) AS r_end
    SELECT
        UUIDNumToString(TraceId) AS traceID,
        hex(SpanId) AS spanID,
        min(Timestamp) AS start,
        any(ServiceName) AS serviceName,
        any(SpanName) AS operation,
        any(StatusCode) AS statusCode,
        any(Duration) AS duration
--         any(if(ParentSpanId = '', '', hex(ParentSpanId))) AS parentSpanID,
--         max(Timestamp) AS end
    FROM gl_traces_main
    WHERE (Timestamp > r_start) AND (Timestamp < r_end) AND (ProjectId = ?)
    GROUP BY
        TraceId,
        SpanId
    ORDER BY start ASC
    LIMIT 100
)
GROUP BY traceID
ORDER BY trace_start_ts DESC
`

type DBItem struct {
	TraceID      string    `ch:"traceID" json:"trace_id"`
	TraceStartTS time.Time `ch:"trace_start_ts"`
	TotalSpans   uint64    `ch:"total_spans"`
	Values       [][]any   `ch:"spans"`
}

func (q *querier) GetTraces(ctx context.Context, projectID int64) ([]*models.TraceItem, error) {
	results := make([]*models.TraceItem, 0)

	builder := &queryBuilder{}
	builder.build(
		baseQuery,
		time.Now().Add(-time.Minute*30).UTC(),
		time.Now().UTC(), strconv.FormatInt(projectID, 10))

	rows, err := q.db.Query(ctx, builder.sql, builder.args...)
	if err != nil {
		return nil, fmt.Errorf("failed to query traces: %w", err)
	}

	for rows.Next() {
		item := &DBItem{}
		err = rows.ScanStruct(item)
		if err != nil {
			return nil, fmt.Errorf("failed to scan results from clickhouse: %w", err)
		}

		trace := models.TraceItem{}
		trace.TraceID = item.TraceID
		trace.TotalSpans = item.TotalSpans
		trace.Timestamp = item.TraceStartTS

		//nolint:errcheck
		for _, i := range item.Values {
			// TODO(Arun): Investigate if we can improve marshaling strategy.
			// This is a straight pass from DB results and should be fastest.
			spanItem := models.SpanItem{}
			spanItem.SpanID = i[0].(string)
			spanItem.Timestamp = i[1].(time.Time)
			spanItem.ServiceName = i[2].(string)
			spanItem.Operation = i[3].(string)
			spanItem.StatusCode = i[4].(string)
			spanItem.DurationNano = uint64(i[5].(int64))
			// spanItem. = i[6].(time.Time)
			// spanItem.IsRootSpan = i[7].(int)
			spanItem.TraceID = item.TraceID

			if len(trace.Spans) == 0 {
				// The first span will always be root span as they are ordered by start timestamp.
				trace.ServiceName = spanItem.ServiceName
				trace.Operation = spanItem.Operation
				trace.StatusCode = spanItem.StatusCode
				trace.Spans = append(trace.Spans, spanItem)
			} else {
				trace.Spans = append(trace.Spans, spanItem)
			}
		}

		// q.logger.Debug("scanned", zap.Any("value", trace))
		results = append(results, &trace)
	}

	return results, nil
}

func (q *querier) Close() error {
	err := q.db.Close()
	if err != nil {
		return fmt.Errorf("close DB: %w", err)
	}
	return nil
}
