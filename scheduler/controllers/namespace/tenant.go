package namespace

import (
	"fmt"
	"net"
	"net/url"
	"strconv"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	tenantOperator "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	networkingv1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func Tenant(
	postrgreSQLEndpoint *url.URL,
	cluster *v1alpha1.Cluster,
	cr *v1alpha1.GitLabNamespace,
) *tenantOperator.Tenant {
	return &tenantOperator.Tenant{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getTenantName(cr),
			Namespace:   cr.Namespace(),
			Labels:      getTenantLabels(cr),
			Annotations: getTenantAnnotations(cr, nil),
		},
		Spec: getTenantSpec(postrgreSQLEndpoint, cr, cluster),
	}
}

func TenantSelector(cr *v1alpha1.GitLabNamespace) client.ObjectKey {
	return client.ObjectKey{
		Name:      getTenantName(cr),
		Namespace: cr.Namespace(),
	}
}

func TenantMutator(
	postrgreSQLEndpoint *url.URL,
	cluster *v1alpha1.Cluster,
	cr *v1alpha1.GitLabNamespace,
	current *tenantOperator.Tenant,
) error {
	current.Labels = getTenantLabels(cr)
	current.Annotations = getTenantAnnotations(cr, current.Annotations)
	spec := getTenantSpec(postrgreSQLEndpoint, cr, cluster)

	current.Spec.Domain = spec.Domain
	current.Spec.ImagePullSecrets = spec.ImagePullSecrets
	current.Spec.GOUI = spec.GOUI

	return nil
}

func getTenantName(cr *v1alpha1.GitLabNamespace) string {
	return constants.TenantName
}

func getTenantLabels(cr *v1alpha1.GitLabNamespace) map[string]string {
	return map[string]string{}
}

func getTenantAnnotations(cr *v1alpha1.GitLabNamespace, existing map[string]string) map[string]string {
	return existing
}

func getTenantSpec(
	postrgreSQLEndpoint *url.URL,
	cr *v1alpha1.GitLabNamespace,
	cluster *v1alpha1.Cluster,
) tenantOperator.TenantSpec {
	domain := cluster.Spec.GetHost()
	gouiSpec := cluster.Spec.GetGOUISpec()
	var glURL *string
	if cluster.Spec.GitLab.InstanceURL != "" {
		glURL = &cluster.Spec.GitLab.InstanceURL
	}

	return tenantOperator.TenantSpec{
		Domain:           &domain,
		ImagePullSecrets: cluster.Spec.ImagePullSecrets,
		GOUI: tenantOperator.GOUISpec{
			Image:              gouiSpec.Image,
			EmbeddingParentURL: glURL,
			EgressRules:        getEgressRules(postrgreSQLEndpoint, cr, cluster),
		},
	}
}

func getEgressRules(
	postrgreSQLEndpoint *url.URL,
	cr *v1alpha1.GitLabNamespace,
	cluster *v1alpha1.Cluster,
) []networkingv1.NetworkPolicyEgressRule {
	egressRules := getDefaultEgressRules(cluster.Namespace())

	// postgres endpoint access - ip address only supported at the moment.
	// In managed environments, i.e. GCP, we'll get an ip address.
	if postrgreSQLEndpoint != nil {
		egressRules = append(egressRules, getPostgresEndpointEgressRule(postrgreSQLEndpoint))
	}

	// TODO(joe): support postgres pod selectors for self-managed users?
	// see https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1934

	if cluster.Spec.Target == common.KIND {
		egressRules = append(egressRules, getKINDEgressRules(cr)...)
	}

	return egressRules
}

func getDefaultEgressRules(clusterNamespace string) []networkingv1.NetworkPolicyEgressRule {
	tcp := corev1.ProtocolTCP
	udp := corev1.ProtocolUDP
	dnsPort := intstr.FromInt(53)
	return []networkingv1.NetworkPolicyEgressRule{
		// allow DNS queries to standard TCP/UDP ports
		{
			Ports: []networkingv1.NetworkPolicyPort{
				{
					Protocol: &tcp,
					Port:     &dnsPort,
				},
				{
					Protocol: &udp,
					Port:     &dnsPort,
				},
			},
		},
		// nginx-ingress access to handle routing for our public domains
		{
			To: []networkingv1.NetworkPolicyPeer{
				{
					PodSelector: &metav1.LabelSelector{
						MatchLabels: map[string]string{
							"app.kubernetes.io/component": "controller",
							"app.kubernetes.io/instance":  "ingress-nginx",
							"app.kubernetes.io/name":      "ingress-nginx",
						},
					},
					NamespaceSelector: &metav1.LabelSelector{
						MatchLabels: map[string]string{
							corev1.LabelMetadataName: clusterNamespace,
						},
					},
				},
			},
		},
		// internet access to allow connections to external data sources
		{
			To: []networkingv1.NetworkPolicyPeer{
				{
					IPBlock: &networkingv1.IPBlock{
						CIDR: "0.0.0.0/0",
						// block standard private network ranges to prevent internal cluster access
						// see https://datatracker.ietf.org/doc/html/rfc1918#section-3
						Except: []string{
							"10.0.0.0/8",
							"172.16.0.0/12",
							"192.168.0.0/16",
						},
					},
				},
			},
		},
	}
}

func getPostgresEndpointEgressRule(endpoint *url.URL) networkingv1.NetworkPolicyEgressRule {
	postgresPort := endpoint.Port()
	tcp := corev1.ProtocolTCP
	var postgresPorts []networkingv1.NetworkPolicyPort
	if postgresPort != "" {
		//nolint:errcheck // this error should not be possible, as url.Port will return empty string or a number.
		// intstr.FromString with a numeric port is invalid :(
		p, _ := strconv.Atoi(postgresPort)
		port := intstr.FromInt(p)
		postgresPorts = []networkingv1.NetworkPolicyPort{
			{
				Protocol: &tcp,
				Port:     &port,
			},
		}
	} else {
		// assume default postgres port
		postgresPorts = []networkingv1.NetworkPolicyPort{getDefaultPostgresPort()}
	}

	ip := net.ParseIP(endpoint.Hostname())
	if ip != nil {
		return networkingv1.NetworkPolicyEgressRule{
			To: []networkingv1.NetworkPolicyPeer{
				{
					IPBlock: &networkingv1.IPBlock{
						CIDR: fmt.Sprintf("%s/32", ip),
					},
				},
			},
			Ports: postgresPorts,
		}
	}

	// fall back to just limiting the postgres port when we
	// cannot infer an ip address.
	return networkingv1.NetworkPolicyEgressRule{
		Ports: postgresPorts,
	}
}

func getDefaultPostgresPort() networkingv1.NetworkPolicyPort {
	port := intstr.FromInt(5432)
	tcp := corev1.ProtocolTCP
	return networkingv1.NetworkPolicyPort{
		Protocol: &tcp,
		Port:     &port,
	}
}

// KIND specific egress rules for local dev.
// These will allow same namespace access, to allow local datasources to work.
// This also assumes a particular postgres setup, using scheduler/config/postgres
// as configured in the Makefile for the scheduler project.
func getKINDEgressRules(cr *v1alpha1.GitLabNamespace) []networkingv1.NetworkPolicyEgressRule {
	// Note(joe): this isn't ideal, but necessary for our kind deployment.
	// we could parameterize these selectors in the config, but I don't expect this to change.
	// e2e tests should cover this case when running in kind mode.
	return []networkingv1.NetworkPolicyEgressRule{
		{
			To: []networkingv1.NetworkPolicyPeer{
				{
					NamespaceSelector: &metav1.LabelSelector{
						MatchLabels: map[string]string{
							corev1.LabelMetadataName: "postgres",
						},
					},
				},
			},
			Ports: []networkingv1.NetworkPolicyPort{getDefaultPostgresPort()},
		},
		{
			To: []networkingv1.NetworkPolicyPeer{
				{
					NamespaceSelector: &metav1.LabelSelector{
						MatchLabels: map[string]string{
							corev1.LabelMetadataName: cr.Namespace(),
						},
					},
				},
			},
		},
	}
}
