package gitlabobservabilityexporter

import (
	"context"
	"fmt"
	"time"

	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/pdata/ptrace"
	conventions "go.opentelemetry.io/collector/semconv/v1.18.0"
	"go.uber.org/zap"
)

type tracesExporter struct {
	db     *database
	logger *zap.Logger
	cfg    *config
}

func newTracesExporter(logger *zap.Logger, cfg *config) (*tracesExporter, error) {
	db, err := newDB(cfg)
	if err != nil {
		return nil, err
	}
	return &tracesExporter{
		db:     db,
		logger: logger,
		cfg:    cfg,
	}, nil
}

func (e *tracesExporter) start(ctx context.Context, _ component.Host) error {
	pingCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	if err := e.db.conn.Ping(pingCtx); err != nil {
		return fmt.Errorf("pinging clickhouse backend: %w", err)
	}
	return nil
}

func (e *tracesExporter) shutdown(_ context.Context) error {
	if e.db != nil {
		if e.db.conn != nil {
			if err := e.db.conn.Close(); err != nil {
				return fmt.Errorf("closing underlying connection: %w", err)
			}
		}
	}
	return nil
}

const (
	insertSQL = `
INSERT INTO gl_traces_main (
	ProjectId,
	Timestamp,
	TraceId,
	SpanId,
	ParentSpanId,
	TraceState,
	SpanName,
	SpanKind,
	ServiceName,
	ResourceAttributes,
	ScopeName,
	ScopeVersion,
	SpanAttributes,
	Duration,
	StatusCode,
	StatusMessage,
	Events.Timestamp,
	Events.Name,
	Events.Attributes,
	Links.TraceId,
	Links.SpanId,
	Links.TraceState,
	Links.Attributes
)`
)

func (e *tracesExporter) pushTraceData(ctx context.Context, td ptrace.Traces) error {
	m := &ptrace.ProtoMarshaler{}
	tracedataSizeBytes.WithLabelValues(e.cfg.TenantID).Add(float64(m.TracesSize(td)))

	batch, err := e.db.conn.PrepareBatch(ctx, insertSQL)
	if err != nil {
		return fmt.Errorf("preparing traces batch for clickhouse backend: %w", err)
	}
	var batchSize float64

	for i := 0; i < td.ResourceSpans().Len(); i++ {
		spans := td.ResourceSpans().At(i)
		res := spans.Resource()
		resAttr := attributesToMap(res.Attributes())

		var serviceName string
		if v, ok := res.Attributes().Get(conventions.AttributeServiceName); ok {
			serviceName = v.Str()
		}

		for j := 0; j < spans.ScopeSpans().Len(); j++ {
			rs := spans.ScopeSpans().At(j).Spans()
			scopeName := spans.ScopeSpans().At(j).Scope().Name()
			scopeVersion := spans.ScopeSpans().At(j).Scope().Version()

			spansReceivedCounter.WithLabelValues(e.cfg.TenantID).Add(float64(rs.Len()))
			for k := 0; k < rs.Len(); k++ {
				r := rs.At(k)
				spanAttr := attributesToMap(r.Attributes())
				status := r.Status()

				eventTimes, eventNames, eventAttrs := convertEvents(r.Events())
				linksTraceIDs, linksSpanIDs, linksTraceStates, linksAttrs := convertLinks(r.Links())

				projectID := "unknown"
				if _, ok := spanAttr["gitlab.target_project_id"]; ok {
					projectID = spanAttr["gitlab.target_project_id"]
				}

				if err := batch.Append(
					projectID,
					r.StartTimestamp().AsTime(),
					TraceIDToFixedString(r.TraceID()),
					SpanIDToFixedString(r.SpanID()),
					SpanIDToFixedString(r.ParentSpanID()),
					r.TraceState().AsRaw(),
					r.Name(),
					SpanKindStr(r.Kind()),
					serviceName,
					resAttr,
					scopeName,
					scopeVersion,
					spanAttr,
					r.EndTimestamp().AsTime().Sub(r.StartTimestamp().AsTime()).Nanoseconds(),
					StatusCodeStr(status.Code()),
					status.Message(),
					eventTimes,
					eventNames,
					eventAttrs,
					linksTraceIDs,
					linksSpanIDs,
					linksTraceStates,
					linksAttrs,
				); err != nil {
					e.logger.Debug("appending span entry", zap.Error(err))
					continue
				}
				batchSize++
			}
		}
	}

	if err := batch.Send(); err != nil {
		return fmt.Errorf("sending traces batch to clickhouse backend: %w", err)
	}
	spansIngestedCounter.WithLabelValues(e.cfg.TenantID).Add(batchSize)
	return nil
}

func convertEvents(events ptrace.SpanEventSlice) ([]time.Time, []string, []map[string]string) {
	var (
		times []time.Time
		names []string
		attrs []map[string]string
	)
	for i := 0; i < events.Len(); i++ {
		event := events.At(i)
		times = append(times, event.Timestamp().AsTime())
		names = append(names, event.Name())
		attrs = append(attrs, attributesToMap(event.Attributes()))
	}
	return times, names, attrs
}

func convertLinks(links ptrace.SpanLinkSlice) ([]string, []string, []string, []map[string]string) {
	var (
		traceIDs []string
		spanIDs  []string
		states   []string
		attrs    []map[string]string
	)
	for i := 0; i < links.Len(); i++ {
		link := links.At(i)
		traceIDs = append(traceIDs, TraceIDToFixedString(link.TraceID()))
		spanIDs = append(spanIDs, SpanIDToFixedString(link.SpanID()))
		states = append(states, link.TraceState().AsRaw())
		attrs = append(attrs, attributesToMap(link.Attributes()))
	}
	return traceIDs, spanIDs, states, attrs
}
