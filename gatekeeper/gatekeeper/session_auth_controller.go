package gatekeeper

import (
	"context"
	"crypto/tls"
	stderrors "errors"
	"fmt"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	cache "github.com/go-redis/cache/v9"
	log "github.com/sirupsen/logrus"
	gitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// NOTE: It's really confusing until we rename all the things but an orgId is
// synonymous with a groupID and a groupID in GitLab is synonymous
// with a namespaceID.

type EmptyValue struct{}

func setSessionHeaders(ctx *gin.Context, userEmail, namespaceID string) {
	ctx.Header("X-WEBAUTH-EMAIL", userEmail)
	ctx.Header("X-GRAFANA-ORG-ID", namespaceID)
}

// Handles the token-based external auth request from nginx-ingress, both
// oauth-based session as well as Gitlab Access Token.
// See https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/#external-authentication
func HandleTokenAuth(ctx *gin.Context) {
	g := GetGitLabService(ctx)
	namespaceID := GetNamespace(ctx)

	if canAccess, accessLevel, err := g.CanAccessNamespace(namespaceID); canAccess {
		if err != nil {
			ctx.AbortWithError(http.StatusForbidden, fmt.Errorf("access namespace: %w", err))
			return
		}
		processRequest(
			ctx,
			accessLevel,
			namespaceID,
		)
		log.Debug("HandleTokenAuth decision OK")
		return
	}

	log.Debug("HandleTokenAuth decision deny")
	ctx.AbortWithStatus(http.StatusForbidden)
}

func HandleGroupAccessAuth(
	ginCtx *gin.Context,
	namespaceID, minAccessLevel int,
	supportLegacyGOUI bool,
) {
	g := GetGitLabService(ginCtx)

	if canAccess, accessLevel, err := g.CanAccessNamespace(strconv.Itoa(namespaceID)); canAccess {
		if err != nil {
			ginCtx.AbortWithError(http.StatusForbidden, fmt.Errorf("access namespace: %w", err))
			return
		}
		if accessLevel < gitlab.AccessLevelValue(minAccessLevel) {
			log.Debug("HandleGroupAccessAuth decision deny - insufficient perm")
			ginCtx.AbortWithStatus(http.StatusForbidden)
			return
		}
		if supportLegacyGOUI {
			processRequest(ginCtx, accessLevel, strconv.Itoa(namespaceID))
		}
		log.Debug("HandleGroupAccessAuth decision OK")
		return
	}

	log.Debug("HandleGroupAccessAuth decision deny")
	ginCtx.AbortWithStatus(http.StatusForbidden)
}

func SetTopLevelNamespaceHeader(ginCtx *gin.Context, projectID int) {
	g := GetGitLabService(ginCtx)

	pgs, err := g.ListProjectsGroups(projectID)
	if err != nil {
		ginCtx.AbortWithError(http.StatusForbidden, fmt.Errorf("access namespace: %w", err))
		return
	}

	if len(pgs) == 0 {
		log.Debugf("Project %d does not belong to any group", projectID)
		ginCtx.AbortWithStatus(http.StatusForbidden)
		return
	}

	ginCtx.Header("x-top-level-namespace", strconv.Itoa(pgs[0].ID))
}

func VerifyProjectGroupMembership(
	ginCtx *gin.Context,
	namespaceID, projectID int,
) {
	g := GetGitLabService(ginCtx)

	// NOTE(prozlach): We need to try to determine if the project is in the
	// group or a user-ns. In case it is a user-ns project, the response to
	// Gitlab API call to list project's ancestor groups will be empty.
	isGroupMember, err := g.IsProjectInGroup(projectID, namespaceID)
	if err != nil {
		ginCtx.AbortWithError(
			http.StatusForbidden,
			fmt.Errorf("error occurred, unable to determine if project belongs to the group: %w", err),
		)
		return
	}
	isUserNSMember, err := g.IsProjectInUserNamespace(projectID, namespaceID)
	if err != nil {
		ginCtx.AbortWithError(
			http.StatusForbidden,
			fmt.Errorf("error occurred, unable to determine if project belongs to a user ns: %w", err),
		)
		return
	}

	if !isGroupMember && !isUserNSMember {
		log.Debugf(
			"project %d does not belong to neirther a group nor namespace %d",
			projectID, namespaceID,
		)
		ginCtx.AbortWithStatus(http.StatusForbidden)
		return
	}
}

// Handles the token-based external auth request from nginx-ingress, both
// oauth-based session as well as Gitlab Access Token.
// See https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/#external-authentication
func HandleProjectAccessAuth(
	ginCtx *gin.Context,
	projectID, minAccessLevelKey int,
) {
	g := GetGitLabService(ginCtx)

	canAccess, err := g.CanAccessProject(projectID, minAccessLevelKey)
	if err != nil {
		ginCtx.AbortWithError(
			http.StatusForbidden,
			fmt.Errorf("error occurred, unable to determine project access: %w", err),
		)
		return
	}
	if !canAccess {
		log.Debug("HandleProjectAccessAuth decision deny")
		ginCtx.AbortWithStatus(http.StatusForbidden)
		return
	}

	log.Debug("HandleProjectAccessAuth decision OK")
}

// Processes the request by updating Argus and
// setting the headers before returning a 200.
// Fails with a 401 in all error circumstances because
// nginx-ingress external auth only accepts 2XX, 401 and 403
// or else it will show a 500 to the user.
func processRequest(
	ctx *gin.Context,
	accessLevel gitlab.AccessLevelValue,
	namespaceID string,
) {
	g := GetGitLabService(ctx)
	user, err := g.CurrentUser()
	if err != nil {
		ctx.AbortWithError(http.StatusForbidden, err)
		return
	}

	namespace, err := g.GetNamespace(namespaceID)
	if err != nil {
		ctx.AbortWithError(http.StatusForbidden, err)
		return
	}
	err = updateArgus(ctx, user, namespace, accessLevel)
	if err != nil {
		ctx.AbortWithError(http.StatusForbidden, err)
		return
	}

	setSessionHeaders(ctx, user.Email, namespaceID)

	ctx.String(200, "Success")
}

// Update the namespace and argus user. It would be great to find a way
// to offload some of this to an async thread so it doesn't block the request.
//
//nolint:funlen
func updateArgus(
	ctx *gin.Context,
	user *gitlab.User,
	namespace *GitLabNamespace,
	accessLevel gitlab.AccessLevelValue,
) error {
	// Get/Set a flag in our redis-backed Cache to signal that we've updated the system
	// with the latest namespace, user and user permissions. If this flag is present,
	// don't do the work again. The cache key will expire after 30 seconds so we only
	// update the system at most every 30 seconds if there are any changes to permissions
	// or to the user or namespace
	lastUpdatedFlag := fmt.Sprintf("updated:%d:%d", user.ID, namespace.ID)
	ca := GetCache(ctx)
	value := EmptyValue{}

	err := ca.Get(ctx, lastUpdatedFlag, &value)
	// Continue if the flag is not set because the key expired in the cache. If key still exists,
	// then we updated not long ago and shouldn't worry about doing the work again
	if err != nil && !stderrors.Is(err, cache.ErrCacheMiss) {
		return err
	}
	// No need to update again, updated recently.
	if err == nil {
		return nil
	}
	c := GetConfig(ctx)

	selector := client.ObjectKey{
		Name:      fmt.Sprint(namespace.ID),
		Namespace: c.Namespace,
	}
	obj := opstracev1alpha1.GitLabNamespace{}

	err = c.K8sClient.Get(context.TODO(), selector, &obj)
	if err != nil {
		return fmt.Errorf("failed to read namespace %s from kubernetes client cache: %w", namespace.FullPath, err)
	}
	// examine DeletionTimestamp to determine if object is under deletion
	if !obj.ObjectMeta.DeletionTimestamp.IsZero() {
		return fmt.Errorf("namespace %s is deleting", namespace.FullPath)
	}
	avatar := ""

	if namespace.AvatarURL != nil {
		// Prepend with gitlab instance host
		avatar = strings.Join([]string{c.GitlabAddr, *namespace.AvatarURL}, "")
	}

	current := obj.DeepCopy()
	current.Spec.Name = namespace.Name
	current.Spec.Path = namespace.Path
	current.Spec.FullPath = namespace.FullPath
	current.Spec.AvatarURL = avatar
	current.Spec.WebURL = namespace.WebURL

	if !reflect.DeepEqual(obj, current) {
		err := c.K8sClient.Update(context.TODO(), current)
		// Don't care if there is a conflict because it means another process succeeded with the update
		if err != nil && !errors.IsConflict(err) {
			return fmt.Errorf("update failed for namespace %s: %w", namespace.FullPath, err)
		}
		log.Info(fmt.Sprintf(
			"namespace %s [ID:%d] updated",
			namespace.FullPath,
			namespace.ID,
		))
	}

	if current.Status.ArgusURL == nil {
		return fmt.Errorf(
			"namespace %s [ID:%d] trying to update argus while waiting for argus url to propagate",
			namespace.FullPath,
			namespace.ID,
		)
	}

	group := int64(namespace.ID)

	argus, err := common.NewArgusClientFromURL(
		*current.Status.ArgusURL,
		group,
		/* #nosec G402 */
		&http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	)
	if err != nil {
		return fmt.Errorf("failed to create argus client for namespace %s: %w", namespace.FullPath, err)
	}
	// Prepend with gitlab instance host
	user.AvatarURL = strings.Join([]string{c.GitlabAddr, user.AvatarURL}, "")

	userID, err := argus.CreateOrUpdateUser(user)
	if err != nil {
		return fmt.Errorf("failed to update user in namespace %s: %w", namespace.FullPath, err)
	}
	user.ID = int(userID)

	err = argus.CreateOrUpdateGroupUser(group, user, common.GrafanaRoleFromGroupAccessLevel(accessLevel))
	if err != nil {
		return fmt.Errorf("failed to update user in namespace %s: %w", namespace.FullPath, err)
	}

	err = ca.Set(&cache.Item{
		Ctx:   context.TODO(),
		Key:   lastUpdatedFlag,
		Value: EmptyValue{},
		TTL:   30 * time.Second,
	})
	if err != nil {
		// just log it, it's not a critical error
		log.Errorf("lastUpdated flag set failure for key [%s]: %+v", lastUpdatedFlag, err)
	}
	return nil
}
