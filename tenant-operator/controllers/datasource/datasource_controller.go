/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package datasource

import (
	"context"
	"encoding/json"
	"errors"
	stdErr "errors"
	"fmt"
	"net/http"
	"time"

	"github.com/fluxcd/pkg/runtime/patch"
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/argusapi"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	kerrors "k8s.io/apimachinery/pkg/util/errors"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"

	k8s_runtime "k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const finalizerName = "datasource.opstrace.com/finalizer"

var ErrSingleDatasourceMultipleGroupIDs = fmt.Errorf("single Datasoruce CR contains configuration for mutiple groups")

// DatasourceReconciler reconciles a Datasource object.
type DatasourceReconciler struct {
	// This Client, initialized using mgr.Client() above, is a split Client
	// that reads objects from the cache and writes to the apiserver
	Client                  client.Client
	Scheme                  *k8s_runtime.Scheme
	Recorder                record.EventRecorder
	Transport               *http.Transport
	Log                     logr.Logger
	DriftPreventionInterval time.Duration
}

var _ reconcile.Reconciler = &DatasourceReconciler{}

// +kubebuilder:rbac:groups=opstrace.com,resources=datasources,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=opstrace.com,resources=datasources/status,verbs=get;update;patch

// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.7.0/pkg/reconcile
func (r *DatasourceReconciler) Reconcile(
	ctx context.Context,
	request ctrl.Request,
) (result ctrl.Result, err error) {
	cr := new(opstracev1alpha1.DataSource)
	err = r.Client.Get(ctx, request.NamespacedName, cr)
	if err != nil {
		if apierrors.IsNotFound(err) {
			r.Log.Info(
				"Datasource has been removed from API",
				"name", request.Name, "namespace", request.Namespace,
			)
			return reconcile.Result{}, nil
		}
		return reconcile.Result{}, err
	}

	// Initialize the runtime patcher with the current version of the object.
	patcher := patch.NewSerialPatcher(cr, r.Client)

	// Finalize the reconciliation
	defer func() {
		// Configure the runtime patcher.
		patchOpts := []patch.Option{
			patch.WithFieldOwner(constants.GroupFieldManagerIDString),
		}

		// Patch the object status, conditions and finalizers.
		if patchErr := patcher.Patch(ctx, cr, patchOpts...); patchErr != nil {
			if !cr.GetDeletionTimestamp().IsZero() {
				patchErr = kerrors.FilterOut(patchErr, apierrors.IsNotFound)
			}
			err = kerrors.NewAggregate([]error{err, patchErr})
		}
	}()

	// Add finalizer first if it doesn't exist to avoid the race condition
	// between init and delete.
	if !controllerutil.ContainsFinalizer(cr, finalizerName) {
		controllerutil.AddFinalizer(cr, finalizerName)
		return ctrl.Result{Requeue: true}, nil
	}

	groupID, err := r.determineGroupID(cr)
	if err != nil {
		r.manageError(cr, err)
		return reconcile.Result{}, err
	}
	argusClient, err := r.getArgusClient(ctx, request.Namespace, groupID)
	if err != nil {
		if stdErr.Is(err, controllers.ErrGrafanaURLTenantURLEmpty) ||
			stdErr.Is(err, controllers.ErrGrafanaURLTenantNotReady) {
			r.Log.Info(
				"dependencies required to create GOUI client are not ready yet",
				"reason", err.Error(),
				"name", request.Name, "namespace", request.Namespace,
			)
			r.manageError(cr, err)
			return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
		}
		r.Log.Error(err, "failed to create GOUI client")
		r.manageError(cr, err)
		return reconcile.Result{}, err
	}

	return r.reconcileDS(cr, argusClient, groupID)
}

// determineGroupID determines the group that datasources in the given CR
// belong to. It returns an error if all the datasources do not belong to the
// same group. It is not yet clear if there is a use case where this should be
// handled gracefully instead.
func (r *DatasourceReconciler) determineGroupID(ds *opstracev1alpha1.DataSource) (int64, error) {
	var prevGroupID int
	for i, dsc := range ds.Spec.Datasources {
		if i > 0 && dsc.GroupId != prevGroupID {
			return -1, ErrSingleDatasourceMultipleGroupIDs
		}
		prevGroupID = dsc.GroupId
	}

	// TODO(prozlach): adjust CRD to use int64 instead of int and remove
	// conversion here
	return int64(prevGroupID), nil
}

// Handle success case.
func (r *DatasourceReconciler) manageSuccess(ds *opstracev1alpha1.DataSource) {
	condition := metav1.Condition{
		Status:             metav1.ConditionTrue,
		Reason:             common.ReconciliationSuccessReason,
		Message:            "All Datasources have been successfully configured in GOUI",
		Type:               common.ConditionTypeReady,
		ObservedGeneration: ds.GetGeneration(),
	}
	apimeta.SetStatusCondition(&ds.Status.Conditions, condition)

	r.Log.Info("datasource successfully reconciled")
}

// Handle error case: update group with error message and status.
func (r *DatasourceReconciler) manageError(ds *opstracev1alpha1.DataSource, issue error) {
	condition := metav1.Condition{
		Status:             metav1.ConditionFalse,
		Reason:             common.ReconciliationFailedReason,
		Message:            issue.Error(),
		Type:               common.ConditionTypeReady,
		ObservedGeneration: ds.GetGeneration(),
	}
	apimeta.SetStatusCondition(&ds.Status.Conditions, condition)
}

// SetupWithManager sets up the controller with the Manager.
func (r *DatasourceReconciler) SetupWithManager(mgr ctrl.Manager) error {
	tenantHandler := func(tenant client.Object) []reconcile.Request {
		list := &opstracev1alpha1.DataSourceList{}
		opts := &client.ListOptions{
			Namespace: tenant.GetNamespace(),
		}
		err := r.Client.List(context.TODO(), list, opts)
		if err != nil {
			return nil
		}
		requests := make([]reconcile.Request, len(list.Items))
		for i, e := range list.Items {
			requests[i] = reconcile.Request{
				NamespacedName: types.NamespacedName{
					Namespace: e.GetNamespace(),
					Name:      e.GetName(),
				},
			}
		}
		return requests
	}
	return ctrl.NewControllerManagedBy(mgr).
		For(&opstracev1alpha1.DataSource{}).
		Watches(
			&source.Kind{Type: &opstracev1alpha1.Tenant{}},
			handler.EnqueueRequestsFromMapFunc(tenantHandler),
		).
		Complete(r)
}

// Get an authenticated grafana API client.
func (r *DatasourceReconciler) getArgusClient(
	ctx context.Context,
	namespace string,
	groupID int64,
) (*common.ArgusClient, error) {
	key := client.ObjectKey{
		Namespace: namespace,
		Name:      constants.TenantName,
	}
	tenant := new(v1alpha1.Tenant)

	err := r.Client.Get(ctx, key, tenant)
	if err != nil {
		return nil, fmt.Errorf("unable to fetch tenant %s/%s: %w", namespace, constants.TenantName, err)
	}

	tenantReady := apimeta.IsStatusConditionTrue(tenant.Status.Conditions, common.ConditionTypeReady)
	if !tenantReady {
		return nil, controllers.ErrGrafanaURLTenantNotReady
	}
	if tenant.Status.Argus.URL == nil {
		return nil, controllers.ErrGrafanaURLTenantURLEmpty
	}
	argusURL := *tenant.Status.Argus.URL
	return common.NewArgusClientFromURL(argusURL, groupID, r.Transport)
}

func (r *DatasourceReconciler) removeDatasources(
	groupID int64,
	cr *opstracev1alpha1.DataSource,
	argusClient *common.ArgusClient,
) error {
	for _, dsc := range cr.Spec.Datasources {
		err := argusClient.DeleteDataSourceByName(dsc.Name)
		if err != nil {
			e := new(argusapi.HTTPError)
			if stdErr.As(err, e) && e.StatusCode == http.StatusNotFound {
				r.Log.Info("Datasource has already been removed", "groupID", groupID, "name", dsc.Name)
			} else {
				return fmt.Errorf(
					"unable to remove Datasource %s in group %d: %w",
					dsc.Name, groupID, err,
				)
			}
		}
	}

	return nil
}

func (r *DatasourceReconciler) convertDSFormat(
	dsc opstracev1alpha1.DataSourceFields,
) (*argusapi.DataSource, error) {
	// To learn why we do the JSON dance here, please see the comment in
	// go/pkg/argusapi/datasource.go in the `DataSource` struct.
	blob, err := json.Marshal(dsc)
	if err != nil {
		return nil, fmt.Errorf("unable to marshal Datasource to JSON: %w", err)
	}
	dscGoui := new(argusapi.DataSource)
	err = json.Unmarshal(blob, dscGoui)
	if err != nil {
		return nil, fmt.Errorf("unable to unmarshal Datasource to JSON to GOUI struct: %w", err)
	}

	return dscGoui, nil
}

func (r *DatasourceReconciler) reconcileDS(
	cr *opstracev1alpha1.DataSource,
	argusClient *common.ArgusClient,
	groupID int64,
) (result ctrl.Result, err error) {
	teardown := !cr.ObjectMeta.DeletionTimestamp.IsZero()
	if teardown {
		if err = r.removeDatasources(groupID, cr, argusClient); err != nil {
			return reconcile.Result{}, err
		}
		if controllerutil.ContainsFinalizer(cr, finalizerName) {
			// Successfully deleted everything we care about.
			// Remove our finalizer from the list and update it
			controllerutil.RemoveFinalizer(cr, finalizerName)
		}
		return reconcile.Result{}, nil
	}

	exists, err := argusClient.GroupExists(groupID)
	if err != nil {
		r.manageError(cr, err)
		return reconcile.Result{}, err
	}
	if !exists {
		msg := "GOUI group has not been provisioned yet, delaying creation of the datasources"
		r.Log.Info(msg, "groupID", groupID, "name", cr.Name, "namespace", cr.Namespace)
		r.manageError(cr, errors.New(msg))
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
	}

	for _, dsc := range cr.Spec.Datasources {
		dscGouiNew, err := r.convertDSFormat(dsc)
		if err != nil {
			r.manageError(cr, err)
			return reconcile.Result{}, err
		}

		dscGouiOld, err := argusClient.DataSourceByName(dsc.Name)
		if err != nil {
			e := new(argusapi.HTTPError)
			if !stdErr.As(err, e) || e.StatusCode != http.StatusNotFound {
				r.manageError(cr, err)
				return reconcile.Result{}, err
			}

			// Datasource does not exist, create it:
			id, err := argusClient.NewDataSource(dscGouiNew)
			if err != nil {
				r.Log.Error(
					err, "unable to create Datasource via GOUI API",
					"groupID", groupID,
					"name", dsc.Name,
				)
				r.manageError(cr, err)
				return reconcile.Result{}, err
			}
			r.Log.Info("Datasource has been created via GOUI API", "groupID", groupID, "name", dsc.Name, "id", id)
			continue
		}

		// TODO(prozlach): In theory it is possible to compare the object we
		// got from the GOUI API and do the update only if there are changes.
		// For now we take the simple approach though.

		// Datasource already exists, update it:
		dscGouiNew.ID = dscGouiOld.ID
		err = argusClient.UpdateDataSource(dscGouiNew)
		if err != nil {
			r.manageError(cr, err)
			return reconcile.Result{}, err
		}
		r.Log.Info("Datasource updated via GOUI API", "groupID", groupID, "name", dsc.Name)
	}

	r.manageSuccess(cr)

	// Periodic reconciliation is meant to prevent drift between the GOUI
	// configuration and the CRs defined in k8s API.
	return reconcile.Result{RequeueAfter: r.DriftPreventionInterval}, nil
}
