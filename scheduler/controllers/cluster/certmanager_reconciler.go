package cluster

import (
	"encoding/json"
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type CertManagerReconciler struct {
	BaseReconciler
}

func NewCertManagerReconciler(
	initialManifests map[string][]byte,
	teardown bool,
) *CertManagerReconciler {
	res := &CertManagerReconciler{
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.CertManagerInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.CertManagerInventoryID,
			reconcilerName: "cert-manager",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.CertManager
			},

			serviceAccountName: constants.CertManagerName,
		},
	}

	res.subclassApplyMethod = res.applyConfiguration

	return res
}

//nolint:funlen
func (i *CertManagerReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := i.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}

	// Put cert-manager in its own, default namespace: `cert-manager`
	res.Namespace = ""

	var issuerRefName string
	if cr.Spec.Target != common.KIND {
		issuerRefName = cr.Spec.DNS.CertificateIssuer
	} else {
		issuerRefName = constants.SelfSignedCAIssuerName
	}

	res.PatchesJson6902 = append(
		res.PatchesJson6902,
		types.Patch{
			Patch: fmt.Sprintf(
				`[
					{"op": "replace", "path": "/metadata/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/dnsNames/0", "value": "%s" },
					{"op": "replace", "path": "/spec/issuerRef/name", "value": "%s" }
				]`,
				cr.Namespace(),
				cr.Spec.GetHost(),
				issuerRefName,
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Certificate",
					},
					Name: constants.HTTPSCertSecretName,
				},
			},
		},
	)

	if cr.Spec.Target != common.KIND {
		b, err := json.Marshal(cr.Spec.DNS.DNS01Challenge)
		if err != nil {
			return nil, fmt.Errorf("unable to marshal DNS01 challenge section: %w", err)
		}

		// TODO(prozlach): Make letsencrypt issuers provisioned basing on
		// cluster configuration instead of hardcoding them basing on cluster
		// type.
		i.Log.Info("Provisioning letsencrypt prod and staging issuers")
		res.PatchesJson6902 = append(
			res.PatchesJson6902,
			types.Patch{
				Patch: fmt.Sprintf(
					`[
						{"op": "replace", "path": "/spec/acme/server", "value": "%s" },
						{"op": "replace", "path": "/spec/acme/email", "value": "%s" },
						{"op": "replace", "path": "/spec/acme/solvers/0/dns01", "value": %s }
					]`,
					getProdDNSServer(cr),
					cr.Spec.DNS.ACMEEmail,
					string(b),
				),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "ClusterIssuer",
						},
						Name: constants.LetsEncryptProd,
					},
				},
			},
			types.Patch{
				Patch: fmt.Sprintf(
					`[
						{"op": "replace", "path": "/spec/acme/server", "value": "%s" },
						{"op": "replace", "path": "/spec/acme/email", "value": "%s" },
						{"op": "replace", "path": "/spec/acme/solvers/0/dns01", "value": %s }
					]`,
					getStagingDNSServer(cr),
					cr.Spec.DNS.ACMEEmail,
					string(b),
				),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "ClusterIssuer",
						},
						Name: constants.LetsEncryptStaging,
					},
				},
			},
		)
	} else {
		i.Log.Info("Letsencrypt issuers are not available on KIND cluster, using self-signed CA")
		res.PatchesStrategicMerge = []types.PatchStrategicMerge{
			"delete_clusterissuer_prod.yaml",
			"delete_clusterissuer_staging.yaml",
		}
	}

	return res, nil
}

func getProdDNSServer(cr *v1alpha1.Cluster) string {
	s := cr.Spec.DNS.ACMEserver
	if s != nil && *s != "" {
		return *s
	}
	return "https://acme-v02.api.letsencrypt.org/directory"
}

func getStagingDNSServer(cr *v1alpha1.Cluster) string {
	s := cr.Spec.DNS.ACMEserver
	if s != nil && *s != "" {
		return *s
	}
	return "https://acme-staging-v02.api.letsencrypt.org/directory"
}
