package cluster

import (
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type NginxIngressReconciler struct {
	BaseReconciler
}

func NewNginxIngressReconciler(
	initialManifests map[string][]byte,
	teardown bool,
) *NginxIngressReconciler {
	res := &NginxIngressReconciler{
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.NginxIngressInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.NginxIngressInventoryID,
			reconcilerName: "nginx-ingress",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.NginxIngress
			},

			serviceAccountName: constants.NginxIngressServiceAccountName,
		},
	}

	res.subclassApplyMethod = res.applyConfiguration

	return res
}

//nolint:funlen // This is just a long object definition with few ifs, not a complex function
func (i *NginxIngressReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := i.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}

	res.PatchesJson6902 = append(
		res.PatchesJson6902,
		types.Patch{
			Patch: fmt.Sprintf(
				`[
						{"op": "add", "path": "/metadata/annotations/external-dns.alpha.kubernetes.io~1hostname", "value": "%s" },
						{"op": "add", "path": "/metadata/annotations/external-dns.alpha.kubernetes.io~1ttl", "value": "30" }
					]`,
				cr.Spec.GetHost(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Service",
					},
					Name: constants.NginxIngressServiceName,
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "ServiceMonitor",
					},
					Name: constants.NginxIngressServiceMonitorName,
				},
			},
		},
		// Add the gatekeeper service as the default backend globally.
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "--default-backend-service=%s/%s"}]`,
				cr.Namespace(),
				constants.GatekeeperName,
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Deployment",
					},
					Name: constants.NginxIngressDeploymentName,
				},
			},
		},
	)

	if cr.Spec.Target == common.AWS {
		res.PatchesJson6902 = append(res.PatchesJson6902,
			// * Use an NLB type loadbalancer
			// * Ensure the ELB idle timeout is less than nginx keep-alive timeout. Because we're
			//   using WebSockets, the value will need to be
			//   increased to '3600' to avoid any potential issues. We set the NGINX timeout to 3700
			//   so that it's longer than the LB timeout.
			types.Patch{
				Patch: `[
					{"op": "add", "path": "/metadata/annotations/service.beta.kubernetes.io~1aws-load-balancer-type", "value": "nlb"},
					{"op": "add", "path": "/metadata/annotations/service.beta.kubernetes.io~1aws-load-balancer-backend-protocol", "value": "tcp"},
					{"op": "add", "path": "/metadata/annotations/service.beta.kubernetes.io~1aws-load-balancer-connection-idle-timeout", "value": "3600"}
				]`,
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Service",
						},
						Name: constants.NginxIngressServiceName,
					},
				},
			},
		)
	}

	if cr.Spec.Target == common.KIND {
		res.PatchesJson6902 = append(res.PatchesJson6902,
			// Use a NodePort type loadbalancer
			types.Patch{
				Patch: fmt.Sprintf(
					`[{"op": "replace", "path": "/spec/type", "value": "%s"}]`,
					v1.ServiceTypeNodePort,
				),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Service",
						},
						Name: constants.NginxIngressServiceName,
					},
				},
			},
			types.Patch{
				Patch: `[
					{"op": "add", "path": "/spec/template/spec/nodeSelector", "value": {"ingress-ready": "true" }},
					{"op": "replace", "path": "/spec/replicas", "value": 1},
					{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "--publish-status-address=localhost"}
					]`,
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Deployment",
						},
						Name: constants.NginxIngressDeploymentName,
					},
				},
			},
		)
	} else {
		res.PatchesJson6902 = append(res.PatchesJson6902,
			// Use an NLB type loadbalancer
			types.Patch{
				Patch: fmt.Sprintf(
					`[{"op": "replace", "path": "/spec/type", "value": "%s"}]`,
					v1.ServiceTypeLoadBalancer,
				),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Service",
						},
						Name: constants.NginxIngressServiceName,
					},
				},
			},
			types.Patch{
				Patch: `[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "--publish-service=$(POD_NAMESPACE)/ingress-nginx-controller"}]`,
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Deployment",
						},
						Name: constants.NginxIngressDeploymentName,
					},
				},
			},
		)
	}

	if len(cr.Spec.DNS.FirewallSourceIPsAllowed) > 0 {
		tmp := make([]string, len(cr.Spec.DNS.FirewallSourceIPsAllowed))
		for idx, s := range cr.Spec.DNS.FirewallSourceIPsAllowed {
			tmp[idx] = fmt.Sprintf(`"%s"`, s)
		}
		tmpJoined := strings.Join(tmp, ",")
		res.PatchesJson6902 = append(res.PatchesJson6902,
			// Use an NLB type loadbalancer
			types.Patch{
				Patch: fmt.Sprintf(
					`[{"op": "add", "path": "/spec/loadBalancerSourceRanges", "value": [%s]}]`,
					tmpJoined,
				),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Service",
						},
						Name: constants.NginxIngressServiceName,
					},
				},
			},
		)
	}

	return res, nil
}
