package gcp

import (
	"fmt"

	acmev1 "github.com/cert-manager/cert-manager/pkg/apis/acme/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/utils/pointer"
)

//nolint:unparam // interface method
func (g *GCP) ConfigureCluster(out *schedulerv1alpha1.Cluster) error {
	tfvars := g.config.tfVars

	out.ObjectMeta.Name = g.config.InstanceName

	out.Spec.Target = common.GCP // do not change

	out.Spec.GOUI.Image = pointer.String(g.config.GOUIImage)

	out.Spec.DNS.CertificateIssuer = tfvars.CertIssuer
	out.Spec.DNS.Domain = pointer.String(g.config.GOBHost)
	out.Spec.DNS.ACMEEmail = tfvars.AcmeEmail
	out.Spec.DNS.DNS01Challenge = acmev1.ACMEChallengeSolverDNS01{
		CloudDNS: &acmev1.ACMEIssuerDNS01ProviderCloudDNS{
			Project: tfvars.ProjectID,
		},
	}

	out.Spec.DNS.ExternalDNSProvider = schedulerv1alpha1.ExternalDNSProviderSpec{
		GCP: &schedulerv1alpha1.ExternalDNSGCPSpec{
			DNSServiceAccountName: g.config.tfOutputs.ExternalDNSServiceAccount,
			ManagedZoneName:       &tfvars.CloudDNSZone,
		},
	}
	out.Spec.DNS.FirewallSourceIPsAllowed = []string{"0.0.0.0/0"}

	out.Spec.DNS.GCPCertManagerServiceAccount = pointer.String(g.config.tfOutputs.CertManagerServiceAccount)

	out.Spec.GitLab.InstanceURL = fmt.Sprintf("https://%s", g.config.GitLabHost)
	out.Spec.GitLab.AuthSecret = v1.LocalObjectReference{
		Name: tfvars.ClusterSecretName,
	}

	return nil
}
