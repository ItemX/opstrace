-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS tracing.gl_traces_main ON CLUSTER '{cluster}' AS tracing.gl_traces_main_local
    ENGINE = Distributed('{cluster}', tracing, gl_traces_main_local, cityHash64(ProjectId, TraceId));
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd
