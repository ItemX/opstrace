package types

import (
	"bytes"
	"encoding/json"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
)

// Event is a representation of https://develop.sentry.dev/sdk/event-payloads/.
// Relevant schema on Gitlab App https://gitlab.com/gitlab-org/gitlab/-/blob/308867c700f2b614e2ee29c8fae8fd8a698e8203/app/validators/json_schemas/error_tracking_event_payload.json
//
//nolint:lll
type Event struct {
	Dist        string    `json:"dist,omitempty"`
	Environment string    `json:"environment,omitempty"`
	EventID     string    `json:"event_id,omitempty"`
	Level       string    `json:"level,omitempty"`
	Message     string    `json:"message,omitempty"`
	Platform    string    `json:"platform,omitempty"`
	Release     string    `json:"release,omitempty"`
	Timestamp   time.Time `json:"timestamp"`
	Transaction string    `json:"transaction,omitempty"`
	// Exception can be an object with the attribute values [] or a flat list of objects.
	// See https://develop.sentry.dev/sdk/event-payloads/exception/.
	// This is field is decoded in a special way
	Exception []*Exception `json:"-"`
	// The fields below are only relevant for transactions.
	Type      string    `json:"type,omitempty"`
	StartTime time.Time `json:"start_timestamp"`
	Spans     []*Span   `json:"spans,omitempty"`
	// This will hold a pointer to  the first exception that has a stacktrace
	// since the first exception may not provide adequate context (e.g. in the
	// Go SDK).
	exception *Exception `json:"-"`

	ServerName string `json:"server_name,omitempty"`
}

// ExceptionValues holds the exception in the values field.
// See https://develop.sentry.dev/sdk/event-payloads/exception/.
type ExceptionValues struct {
	Values []*Exception `json:"values"`
}

// Exception holds core attributes. See https://develop.sentry.dev/sdk/event-payloads/exception/#attributes.
type Exception struct {
	Type       string      `json:"type,omitempty"`
	Value      string      `json:"value,omitempty"`
	Module     string      `json:"module,omitempty"`
	ThreadID   interface{} `json:"thread_id,omitempty"`
	Stacktrace *Stacktrace `json:"stacktrace,omitempty"`
}

type Stacktrace struct {
	Frames        []Frame `json:"frames,omitempty"`
	FramesOmitted []uint  `json:"frames_omitted,omitempty"`
}

type Frame struct {
	Function    string                 `json:"function,omitempty"`
	Symbol      string                 `json:"symbol,omitempty"`
	Module      string                 `json:"module,omitempty"`
	Package     string                 `json:"package,omitempty"`
	Filename    string                 `json:"filename,omitempty"`
	AbsPath     string                 `json:"abs_path,omitempty"`
	Lineno      int                    `json:"lineno,omitempty"`
	Colno       int                    `json:"colno,omitempty"`
	PreContext  []string               `json:"pre_context,omitempty"`
	ContextLine string                 `json:"context_line,omitempty"`
	PostContext []string               `json:"post_context,omitempty"`
	InApp       bool                   `json:"in_app,omitempty"`
	Vars        map[string]interface{} `json:"vars,omitempty"`
}

type Span struct {
	TraceID      string                 `json:"trace_id"`
	SpanID       string                 `json:"span_id"`
	ParentSpanID string                 `json:"parent_span_id"`
	Op           string                 `json:"op,omitempty"`
	Description  string                 `json:"description,omitempty"`
	Status       string                 `json:"status,omitempty"`
	Tags         map[string]string      `json:"tags,omitempty"`
	StartTime    time.Time              `json:"start_timestamp"`
	EndTime      time.Time              `json:"timestamp"`
	Data         map[string]interface{} `json:"data,omitempty"`
}

// MessageObj is representation of https://develop.sentry.dev/sdk/event-payloads/message/.
type MessageObj struct {
	Formatted string `json:"formatted"`
}

// NewEventFrom parses a payload request to an Event object.
// Note: Currently we don't have limits on payload size when ingesting via this method.
func NewEventFrom(payload []byte) (*Event, error) {
	e := &Event{}

	err := e.UnmarshalJSON(payload)
	return e, err
}

// UnmarshalJSON parses the event payload and validates if the required attributes are present.
// For ref: see parsing in Gitlab app: https://gitlab.com/gitlab-org/gitlab/-/blob/308867c700f2b614e2ee29c8fae8fd8a698e8203/lib/error_tracking/collector/sentry_request_parser.rb
// and validation: https://gitlab.com/gitlab-org/gitlab/-/blob/308867c700f2b614e2ee29c8fae8fd8a698e8203/lib/error_tracking/collector/payload_validator.rb
//
//nolint:lll,gocyclo,cyclop,funlen
func (e *Event) UnmarshalJSON(data []byte) error {
	var objMap map[string]*json.RawMessage

	err := json.Unmarshal(data, &objMap)
	if err != nil {
		return fmt.Errorf("failed to unmarshal event into objMap: %w", err)
	}

	for key, val := range objMap {
		if val == nil {
			continue
		}
		switch key {
		case "dist":
			err = json.Unmarshal(*val, &e.Dist)
		case "environment":
			err = json.Unmarshal(*val, &e.Environment)
		case "event_id":
			err = json.Unmarshal(*val, &e.EventID)
		case "level":
			err = json.Unmarshal(*val, &e.Level)
		case "platform":
			err = json.Unmarshal(*val, &e.Platform)
		case "transaction":
			err = json.Unmarshal(*val, &e.Transaction)
		case "release":
			err = json.Unmarshal(*val, &e.Release)
		case "server_name":
			err = json.Unmarshal(*val, &e.ServerName)
		case "message":
			// message can be an object or a string. See https://develop.sentry.dev/sdk/event-payloads/message/.
			// Remove leading whitespace to correctly identify if the value is an object
			messageBts := bytes.TrimLeft(*val, " \t\r\n")
			isObject := len(messageBts) > 0 && messageBts[0] == '{'
			if isObject {
				var obj = &MessageObj{}
				err = json.Unmarshal(*val, obj)
				if err != nil {
					return fmt.Errorf("failed to unmarshal message into MessageObj: %w", err)
				}
				e.Message = obj.Formatted
			} else {
				err = json.Unmarshal(*val, &e.Message)
			}
		case "timestamp":
			var ts UnixOrRFC3339Time
			err = json.Unmarshal(*val, &ts)
			if err != nil {
				//nolint:wrapcheck
				return err
			}
			e.Timestamp = ts.Time().UTC()
		case "exception":
			// exception can be an object or an array. See https://develop.sentry.dev/sdk/event-payloads/exception/.
			// Remove leading whitespace to correctly identify it.
			exceptionBts := bytes.TrimLeft(*val, " \t\r\n")
			isObject := len(exceptionBts) > 0 && exceptionBts[0] == '{'
			isArray := len(exceptionBts) > 0 && exceptionBts[0] == '['

			if isArray {
				// directly unmarshall into []Exception values
				err = json.Unmarshal(*val, &e.Exception)
			} else if isObject {
				var obj = ExceptionValues{}
				err = json.Unmarshal(*val, &obj)
				if err != nil {
					return fmt.Errorf("failed to unmarshal exception into ExceptionValues: %w", err)
				}
				e.Exception = obj.Values
			}
		}
		if err != nil {
			//nolint:wrapcheck
			return err
		}
	}
	// In case where `timestamp` key is missing in the payload, fill it on server side
	if e.Timestamp.IsZero() {
		log.Info("missing 'timestamp' column in exception event payload, filling it on server")
		e.Timestamp = time.Now().UTC()
	}

	// Find pointer to the first exception that has a stacktrace since the first
	// exception may not provide adequate context (e.g. in the Go SDK).
	// Original ruby code:
	//nolint:lll
	// - https://gitlab.com/gitlab-org/gitlab/-/blob/cb84795fae22fa8d87bddda8810e636cd729030d/app/services/error_tracking/collect_error_service.rb#L41-48
	// - https://gitlab.com/gitlab-org/gitlab/-/blob/cb84795fae22fa8d87bddda8810e636cd729030d/app/services/error_tracking/collect_error_service.rb#L56-67
	for i, ex := range e.Exception {
		if ex.Stacktrace == nil {
			continue
		}
		e.exception = e.Exception[i]
		break
	}

	return nil
}

func (e *Event) ExtractDataItemType() string {
	if e.exception != nil {
		return SupportedTypeException
	}
	if e.Message != "" {
		return SupportedTypeMessage
	}
	return UnsupportedDataItem
}

func (e *Event) ValidateException() error {
	if e.exception == nil {
		return fmt.Errorf("invalid error event of exception type; exception key not present")
	}

	if e.Name() == "" {
		return fmt.Errorf("invalid error event of exception type: name is not set")
	}

	if e.Description() == "" {
		return fmt.Errorf("invalid error event of exception type: description is not set")
	}

	if e.ExceptionActor() == "" {
		return fmt.Errorf("invalid error event of exception type: actor is not set")
	}

	if e.Platform == "" {
		return fmt.Errorf("invalid error event of exception type: platform is not set")
	}

	return nil
}

// Actor for message returns first item in stacktrace that has a function and module name
// if stacktraces are not available then an empty string is returned.
// TODO: We need to implement message actor by parsing the stacktrace for various SDKs
// for now we will not be handling stacktraces

func (e Event) MessageActor(payload []byte) string {
	return ""
}

func (e *Event) ValidateMessage() error {
	if e.Platform == "" {
		return fmt.Errorf("invalid error event of message type: platform is not set")
	}

	if e.Message == "" {
		return fmt.Errorf("invalid error event of message type: message is not set")
	}
	return nil
}

// Validate Event has required fields set.
// Original ruby code:
// - https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/models/error_tracking/error.rb#L20-25
func (e *Event) Validate() error {
	// Note(Arun): Currently exception is a mandatory field as per our prior implementation.
	//nolint:lll
	// See https://gitlab.com/gitlab-org/gitlab/-/blob/308867c700f2b614e2ee29c8fae8fd8a698e8203/app/validators/json_schemas/error_tracking_event_payload.json#L4 for the list of required fields.
	if e.exception == nil {
		return fmt.Errorf("invalid error event; exception key not present")
	}

	if e.Name() == "" {
		return fmt.Errorf("invalid error event: name is not set")
	}

	if e.Description() == "" {
		return fmt.Errorf("invalid error event: description is not set")
	}

	if e.ExceptionActor() == "" {
		return fmt.Errorf("invalid error event: actor is not set")
	}

	if e.Platform == "" {
		return fmt.Errorf("invalid error event: platform is not set")
	}

	return nil
}

// Actor returns the transaction name. If the error does not have one set it
// returns the first item in stacktrace that has a function and module name.
// Original ruby code:
// - https://gitlab.com/gitlab-org/gitlab/-/blob/90b6bea21c74ccc98205aff69e0e0f2acfc7993f/app/services/error_tracking/collect_error_service.rb#L11
// - https://gitlab.com/gitlab-org/gitlab/-/blob/daa5796df89d775c635dc900a532efb657877b75/app/services/error_tracking/collect_error_service.rb#L66
//
//nolint:lll
func (e Event) ExceptionActor() string {
	if e.Transaction != "" {
		return e.Transaction
	}

	// If no exception is present return early
	if e.exception == nil {
		return ""
	}
	// This means there's no actor name and we should fail.
	l := len(e.exception.Stacktrace.Frames)
	if l == 0 {
		return ""
	}

	// Some SDKs do not have a transaction attribute. So we build it by
	// combining function name and module name from the last item in stacktrace.
	//nolint:lll
	// https://gitlab.com/gitlab-org/gitlab/-/blob/8565b9d4770baafa560915b6b06da6026ecab41c/app/services/error_tracking/collect_error_service.rb#L62

	f := e.exception.Stacktrace.Frames[l-1]
	return fmt.Sprintf("%s(%s)", f.Function, f.Module)
}

// Name returns the exception type.
// Original ruby code:
// https://gitlab.com/gitlab-org/gitlab/-/blob/cb84795fae22fa8d87bddda8810e636cd729030d/app/services/error_tracking/collect_error_service.rb#L9
//
//nolint:lll
func (e Event) Name() string {
	if e.exception != nil {
		return e.exception.Type
	}
	return ""
}

// Description returns the exception value.
// Original ruby code:
// - https://gitlab.com/gitlab-org/gitlab/-/blob/90b6bea21c74ccc98205aff69e0e0f2acfc7993f/app/services/error_tracking/collect_error_service.rb#L10
//
//nolint:lll
func (e Event) Description() string {
	if e.exception != nil {
		return e.exception.Value
	}
	return ""
}
