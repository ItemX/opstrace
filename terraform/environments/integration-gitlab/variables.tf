variable "instance_name" {
  description = "A unique name that will be appended to all managed resources."
  type        = string
}

variable "project_id" {
  description = "The ID of the project in which to provision resources."
  type        = string
}

variable "location" {
  description = "Google Cloud location (zone) where the instance will be created."
  type        = string
}

variable "domain" {
  description = "Domain for hosting gitlab"
  type        = string
}

variable "cloud_dns_zone" {
  description = "The name of the Cloud DNS managed zone to add the NS A record pointing at the instance"
  type        = string
}
