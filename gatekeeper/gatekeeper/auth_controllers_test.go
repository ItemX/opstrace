package gatekeeper_test

import (
	"bytes"
	"net/http"
	"net/http/httptest"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/memstore"
	"github.com/gin-gonic/gin"
	"gitlab.com/gitlab-org/opstrace/opstrace/gatekeeper/gatekeeper"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	kruntime "k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"
)

func newAuthTestRequest(path string, headers []Pair) *http.Request {
	GinkgoHelper()

	req, err := http.NewRequest(http.MethodGet, path, nil)
	Expect(err).NotTo(HaveOccurred())

	// add headers to test case request
	for _, h := range headers {
		req.Header.Set(h.Key, h.Value)
	}

	return req
}

var _ = Context("Webhook API authentication", func() {
	var (
		gitlabPHS *testutils.ProgrammableHTTPServer
		gouiPHS   *testutils.ProgrammableHTTPServer
		router    *gin.Engine
		logOutput *bytes.Buffer
	)

	var glatDummyTokens = map[string]struct{}{
		"glpat-rwrwrwrwrwrwrwrwrwrw": {},
		"glpat-rrrrrrrrrrrrrrrrrrrr": {},
		"glpat-wwwwwwwwwwwwwwwwwwww": {},
	}

	BeforeEach(func() {
		gouiPHS = testutils.NewProgrammableHTTPServer(logger, "admin:foo")
		configurePHSforGOUITokenAuthnz(gouiPHS)
		gouiPHS.Start()

		gitlabPHS = testutils.NewProgrammableHTTPServer(logger, "")
		configurePHSforGitlabTokenAuthnz(gitlabPHS, glatDummyTokens, testTokenTypeGroup)
		gitlabPHS.Start()

		gin.SetMode(gin.TestMode)
		gin.DefaultWriter = GinkgoWriter

		router = gin.Default()

		// logs capture
		logOutput = new(bytes.Buffer)
		GinkgoWriter.TeeTo(logOutput)

		// in-memory session storage
		store := memstore.NewStore([]byte("secret"))
		router.Use(sessions.Sessions("mysession", store))

		// templates needed to render error pages
		router.LoadHTMLGlob("../templates/*.gohtml")

		// setup fake k8s client
		scheme := kruntime.NewScheme()
		Expect(schedulerv1alpha1.AddToScheme(scheme)).To(Succeed())
		fakek8sCLient := fake.NewClientBuilder().WithScheme(scheme).WithRuntimeObjects(
			&schedulerv1alpha1.GitLabNamespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "1234",
					Namespace: "default",
				},
				Status: schedulerv1alpha1.GitLabNamespaceStatus{
					ArgusURL: stringRef(gouiPHS.ServerURL()),
				},
			},
			&schedulerv1alpha1.GitLabNamespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "13",
					Namespace: "default",
				},
				Status: schedulerv1alpha1.GitLabNamespaceStatus{
					ArgusURL: stringRef(gouiPHS.ServerURL()),
				},
			},
		).Build()

		router.Use(gatekeeper.ErrorLogger())
		// config middleware with mocks
		router.Use(gatekeeper.Config(&gatekeeper.ConfigOptions{
			Namespace:  "default",
			K8sClient:  fakek8sCLient,
			GitlabAddr: gitlabPHS.ServerURL(),
		}))
		router.Use(func(ctx *gin.Context) {
			ctx.Set(gatekeeper.CacheClientKey, NewMockRedis())
		})

		gatekeeper.SetRoutes(router)
	})

	AfterEach(func() {
		gouiPHS.Stop()
		gitlabPHS.Stop()
		GinkgoWriter.ClearTeeWriters()
	})

	Context("/v1/auth/webhook/ path", func() {
		Describe("common token based auth tests", func() {
			DescribeTable("should 404 if namespace is missing",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(404))

					requests := gouiPHS.GetRequests(http.MethodGet, "/api/group/")
					Expect(requests).To(BeEmpty())
				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/?min_accesslevel=20"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/?min_accesslevel=20&support_legacy_goui=true"),
			)

			DescribeTable("should 404 if action is missing",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(404))

					requests := gouiPHS.GetRequests(http.MethodGet, "/api/group/")
					Expect(requests).To(BeEmpty())
				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115?min_accesslevel=20"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13?min_accesslevel=20&support_legacy_goui=true"),
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115?min_accesslevel=20"),
			)

			DescribeTable("should 404 if project is missing",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(404))

					requests := gouiPHS.GetRequests(http.MethodGet, "/api/group/")
					Expect(requests).To(BeEmpty())
				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13?min_accesslevel=20"),
				Entry("Project Auth Handler", "/v1/auth/project/webhook?min_accesslevel=20"),
			)

			DescribeTable("should 401 if auth headers are missing",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(401))

					requests := gouiPHS.GetRequests(http.MethodGet, "/api/group/")
					Expect(requests).To(BeEmpty())
					Expect(logOutput.String()).To(ContainSubstring("handling session cookie-based auth"))
				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/read?min_accesslevel=20"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13/read?min_accesslevel=20&support_legacy_goui=true"),
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/read?min_accesslevel=20"),
			)

			DescribeTable("should 403 if namespace is invalid",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(http.StatusForbidden))

					requests := gouiPHS.GetRequests(http.MethodGet, "/api/group/")
					Expect(requests).To(BeEmpty())
					Expect(logOutput.String()).To(ContainSubstring("validation failed: strconv.ParseInt"))
				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/@/115/read?min_accesslevel=20"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/@/read?min_accesslevel=20&support_legacy_goui=true"),
			)

			DescribeTable("should 403 if project is invalid",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(http.StatusForbidden))

					requests := gouiPHS.GetRequests(http.MethodGet, "/api/group/")
					Expect(requests).To(BeEmpty())
					Expect(logOutput.String()).To(ContainSubstring("path parameters validation failed: strconv.ParseInt: parsing"))
				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/@/read?min_accesslevel=20"),
				Entry("Project Auth Handler", "/v1/auth/project/webhook/@/read?min_accesslevel=20"),
			)

			DescribeTable("should 403 if action is invalid",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(http.StatusForbidden))

					requests := gouiPHS.GetRequests(http.MethodGet, "/api/group/")
					Expect(requests).To(BeEmpty())
					Expect(logOutput.String()).To(ContainSubstring("Field validation for 'Action' failed on the 'oneof' tag"))
				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/foobar?min_accesslevel=20"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13/foobar?min_accesslevel=20&support_legacy_goui=true"),
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/foobar?min_accesslevel=20"),
			)

			DescribeTable("should 403 if min_accesslevel is invalid",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(http.StatusForbidden))

					requests := gouiPHS.GetRequests(http.MethodGet, "/api/group/")
					Expect(requests).To(BeEmpty())
					Expect(logOutput.String()).To(ContainSubstring(`uri parameters validation failed: strconv.ParseInt: parsing \"100s\": invalid syntax`))
				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/read?min_accesslevel=100s"),
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/read?min_accesslevel=100s"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13/read?min_accesslevel=100s&support_legacy_goui=true"),
			)

			DescribeTable("should 403 if min_accesslevel is missing",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(http.StatusForbidden))

					requests := gouiPHS.GetRequests(http.MethodGet, "/api/group/")
					Expect(requests).To(BeEmpty())
					Expect(logOutput.String()).To(ContainSubstring("Error:Field validation for 'MinAccessLevel' failed on the 'required' tag"))
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/read"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/read"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13/read?support_legacy_goui=true"),
			)

			It("should 403 if support_legacy_goui is invalid - group", func() {
				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest("/v1/auth/group/webhook/13/read?support_legacy_goui=foobar&min_accesslevel=20", nil)

				router.ServeHTTP(recorder, testReq)

				Expect(recorder.Code).To(Equal(http.StatusForbidden))

				requests := gouiPHS.GetRequests(http.MethodGet, "/api/group/")
				Expect(requests).To(BeEmpty())
				Expect(logOutput.String()).To(ContainSubstring("uri parameters validation failed: strconv.ParseBool"))
			})
		})

		Describe("Gitlab Access Token-based auth", func() {
			It("should 200 if token is valid - Group", func() {
				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					"/v1/auth/group/webhook/13/read?support_legacy_goui=true&min_accesslevel=20",
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusOK))

				// Check the authProxy headers
				headers := recorder.Result().Header
				proxyAuthEmail := headers.Get("X-WEBAUTH-EMAIL")
				Expect(proxyAuthEmail).To(Equal("group_115_bot1@noreply.gdk.devvm"))
				proxyAuthOrgID := headers.Get("X-GRAFANA-ORG-ID")
				Expect(proxyAuthOrgID).To(Equal("13"))

				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/namespaces/13")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/user")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/groups/13/members/all/61")
				verifyPHSRequestMade(gouiPHS, http.MethodGet, "/api/users/lookup")
				verifyPHSRequestMade(gouiPHS, "PUT", "/api/users/3")
				verifyPHSRequestMade(gouiPHS, http.MethodPatch, "/api/groups/13/users/3")

				// And finally - check if log message is produced
				Expect(logOutput.String()).To(ContainSubstring("HandleGroupAccessAuth decision OK"))
			})

			It("should not issue GOUI calls by default - Group", func() {
				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					"/v1/auth/group/webhook/13/read?min_accesslevel=20",
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusOK))

				// Check the authProxy headers
				headers := recorder.Result().Header
				proxyAuthEmail := headers.Get("X-WEBAUTH-EMAIL")
				Expect(proxyAuthEmail).To(BeEmpty())
				proxyAuthOrgID := headers.Get("X-GRAFANA-ORG-ID")
				Expect(proxyAuthOrgID).To(BeEmpty())

				verifyPHSRequestNotMade(gouiPHS, http.MethodGet, "/api/users/lookup")
				verifyPHSRequestNotMade(gouiPHS, "PUT", "/api/users/3")
				verifyPHSRequestNotMade(gouiPHS, http.MethodPatch, "/api/groups/13/users/3")

				// And finally - check if log message is produced
				Expect(logOutput.String()).To(ContainSubstring("HandleGroupAccessAuth decision OK"))
			})

			It("should not issue GOUI calls when support_legacy_goui=false - Group", func() {
				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					"/v1/auth/group/webhook/13/read?support_legacy_goui=false&min_accesslevel=20",
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusOK))

				// Check the authProxy headers
				headers := recorder.Result().Header
				proxyAuthEmail := headers.Get("X-WEBAUTH-EMAIL")
				Expect(proxyAuthEmail).To(BeEmpty())
				proxyAuthOrgID := headers.Get("X-GRAFANA-ORG-ID")
				Expect(proxyAuthOrgID).To(BeEmpty())

				verifyPHSRequestNotMade(gouiPHS, http.MethodGet, "/api/users/lookup")
				verifyPHSRequestNotMade(gouiPHS, "PUT", "/api/users/3")
				verifyPHSRequestNotMade(gouiPHS, http.MethodPatch, "/api/groups/13/users/3")

				// And finally - check if log message is produced
				Expect(logOutput.String()).To(ContainSubstring("HandleGroupAccessAuth decision OK"))
			})

			It("should 200 if token is valid - Project+Group", func() {
				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					"/v1/auth/both/webhook/13/115/read?min_accesslevel=20",
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusOK))
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/projects/115")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/user")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/projects/115/groups")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/projects/115/members/all/61")

				// And finally - check if log message is produced
				Expect(logOutput.String()).To(ContainSubstring("HandleProjectAccessAuth decision OK"))
			})

			It("should 200 if token is valid - Project", func() {
				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					"/v1/auth/project/webhook/115/read?min_accesslevel=20",
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusOK))

				headers := recorder.Result().Header
				toplevelNS := headers.Get("x-top-level-namespace")
				Expect(toplevelNS).To(Equal("13"))

				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/user")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/projects/115/members/all/61")

				// And finally - check if log message is produced
				Expect(logOutput.String()).To(ContainSubstring("HandleProjectAccessAuth decision OK"))
			})

			It("should 403 if gitlab namespace does not exist - Group", func() {
				// Namespace info query
				nsReply := func(r *http.Request) (int, string) {
					if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
						return 404, ``
					}
					return http.StatusForbidden, GitlabReplyUnauthorized
				}
				gitlabPHS.SetResponseFunc(
					http.MethodGet, "/api/v4/namespaces/13",
					nsReply,
				)

				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					"/v1/auth/group/webhook/13/read?support_legacy_goui=true&min_accesslevel=20",
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusForbidden))
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/namespaces/13")
			})

			DescribeTable("should 403 if gitlab project does not exist",
				func(urlPath string) {
					httpReply := func(r *http.Request) (int, string) {
						if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
							return 404, ``
						}
						return http.StatusForbidden, GitlabReplyUnauthorized
					}
					gitlabPHS.SetResponseFunc(
						http.MethodGet, "/api/v4/projects/115",
						httpReply,
					)

					httpReply = func(r *http.Request) (int, string) {
						if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
							return 404, ``
						}
						return http.StatusForbidden, GitlabReplyUnauthorized
					}
					gitlabPHS.SetResponseFunc(
						http.MethodGet, "/api/v4/projects/115/members/all/61",
						httpReply,
					)

					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
						},
					)

					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusForbidden))
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/read?min_accesslevel=20"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/read?min_accesslevel=20"),
			)

			It("should 403 if token group membership is inactive - Group", func() {
				membershipReply := func(r *http.Request) (int, string) {
					if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
						return http.StatusOK, `{
			                                "state": "inactive",
			                                "access_level": 30
			                              }`
					}
					return http.StatusForbidden, GitlabReplyUnauthorized
				}
				gitlabPHS.SetResponseFunc(
					http.MethodGet, "/api/v4/groups/13/members/all/61",
					membershipReply,
				)

				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					"/v1/auth/group/webhook/13/read?support_legacy_goui=true&min_accesslevel=20",
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusForbidden))
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/namespaces/13")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/user")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/groups/13/members/all/61")
			})

			DescribeTable("should 403 if token project membership is inactive",
				func(urlPath string) {
					projectGroups := func(r *http.Request) (int, string) {
						if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
							return http.StatusOK, `{
                                "state": "inactive",
                                "access_level": 30
                            }`
						}
						return http.StatusForbidden, GitlabReplyUnauthorized
					}
					gitlabPHS.SetResponseFunc(
						http.MethodGet, "/api/v4/projects/115/members/all/61",
						projectGroups,
					)

					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
						},
					)

					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusForbidden))
					verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
					verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/projects/115/members/all/61")
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/read?min_accesslevel=20"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/read?min_accesslevel=20"),
			)

			It("should 403 if token group membership is too low - Group", func() {
				membershipReply := func(r *http.Request) (int, string) {
					if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
						return http.StatusOK, `{
			                                "state": "active",
			                                "access_level": 20
			                              }`
					}
					return http.StatusForbidden, GitlabReplyUnauthorized
				}
				gitlabPHS.SetResponseFunc(
					http.MethodGet, "/api/v4/groups/13/members/all/61",
					membershipReply,
				)

				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					"/v1/auth/group/webhook/13/read?support_legacy_goui=true&min_accesslevel=50",
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusForbidden))
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/namespaces/13")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/user")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/groups/13/members/all/61")
			})

			DescribeTable("should 403 if token project membership is too low",
				func(urlPath string) {
					projectGroups := func(r *http.Request) (int, string) {
						if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
							return http.StatusOK, `{
                                "state": "active",
                                "access_level": 30
                            }`
						}
						return http.StatusForbidden, GitlabReplyUnauthorized
					}
					gitlabPHS.SetResponseFunc(
						http.MethodGet, "/api/v4/projects/115/members/all/61",
						projectGroups,
					)

					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
						},
					)

					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusForbidden))
					verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
					verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/projects/115/members/all/61")
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/read?min_accesslevel=50"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/read?min_accesslevel=50"),
			)

			DescribeTable("should 403 if token does not have correct scopes",
				func(urlPath string) {
					patSelfReply := func(r *http.Request) (int, string) {
						if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
							return http.StatusOK, GitlabTokenRegistryScopes
						}
						return http.StatusForbidden, GitlabReplyUnauthorized
					}
					gitlabPHS.SetResponseFunc(
						http.MethodGet, "/api/v4/personal_access_tokens/self",
						patSelfReply,
					)

					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
						},
					)

					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusForbidden))
					verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/read?min_accesslevel=50"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/read?min_accesslevel=50"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13/read?support_legacy_goui=true&min_accesslevel=30"),
			)

			DescribeTable("should 403 if token has only some of the scopes",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rrrrrrrrrrrrrrrrrrrr",
							},
						},
					)

					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusForbidden))
					verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/write?min_accesslevel=50"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/write?min_accesslevel=50"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13/write?support_legacy_goui=true&min_accesslevel=30"),
			)

			DescribeTable("should 200 if min_accesslevel=0",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
						},
					)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(http.StatusOK))
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/write?min_accesslevel=0"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/write?min_accesslevel=0"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13/write?support_legacy_goui=true&min_accesslevel=0"),
			)
		})
	})
})
