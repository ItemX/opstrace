package gcp

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/gruntwork-io/terratest/modules/logger"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/gruntwork-io/terratest/modules/testing"
	"golang.org/x/crypto/ssh"

	. "github.com/onsi/ginkgo/v2"
)

type gitLabProvider struct {
	config         *configuration
	logger         *logger.Logger
	testIdentifier string

	terragruntDir string
	publicIP      string
	sshUsername   string
	sshPrivateKey string
	client        *ssh.Client
}

func newGitLabProvider(l *logger.Logger, testIdentifier string, config *configuration) *gitLabProvider {
	return &gitLabProvider{
		config:         config,
		logger:         l,
		testIdentifier: testIdentifier,
		terragruntDir:  config.terragruntDir("environments/integration-gitlab"),
	}
}

func (g *gitLabProvider) provision(t testing.TestingT) error {
	vs, err := g.config.tfVars.encode()
	if err != nil {
		return err
	}

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		BackendConfig: g.config.tfVars.backendConfig(),
		TerraformDir:  g.terragruntDir,
		EnvVars:       vs,
		Logger:        g.logger,
		Reconfigure:   true,
	})

	_, err = terraform.InitAndApplyE(t, terraformOptions)
	if err != nil {
		return fmt.Errorf("terraform init and apply: %w", err)
	}
	return nil
}

func (g *gitLabProvider) deprovision(t testing.TestingT) error {
	vs, err := g.config.tfVars.encode()
	if err != nil {
		return err
	}

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		BackendConfig: g.config.tfVars.backendConfig(),
		TerraformDir:  g.terragruntDir,
		EnvVars:       vs,
		Logger:        g.logger,
		Reconfigure:   true,
	})

	_, err = terraform.InitE(t, terraformOptions)
	if err != nil {
		return fmt.Errorf("terraform init: %w", err)
	}

	_, err = terraform.DestroyE(t, terraformOptions)
	if err != nil {
		return fmt.Errorf("terraform destroy: %w", err)
	}
	return nil
}

func (g *gitLabProvider) readOutput(t testing.TestingT, outputKey string) (string, error) {
	vs, err := g.config.tfVars.encode()
	if err != nil {
		return "", err
	}

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		BackendConfig: g.config.tfVars.backendConfig(),
		TerraformDir:  g.terragruntDir,
		EnvVars:       vs,
		Logger:        logger.Discard, // do not log output commands
	})

	output, err := terraform.OutputE(t, terraformOptions, outputKey)
	if err != nil {
		return "", fmt.Errorf("terraform read output: %w", err)
	}
	return output, nil
}

func (g *gitLabProvider) setupSSH() error {
	var err error
	g.publicIP, err = g.readOutput(GinkgoT(), "public_ip")
	if err != nil {
		return err
	}
	g.sshUsername, err = g.readOutput(GinkgoT(), "ssh_username")
	if err != nil {
		return err
	}
	g.sshPrivateKey, err = g.readOutput(GinkgoT(), "ssh_private_key")
	if err != nil {
		return err
	}
	signer, err := ssh.ParsePrivateKey([]byte(g.sshPrivateKey))
	if err != nil {
		return fmt.Errorf("parse private key: %w", err)
	}
	sshConfig := &ssh.ClientConfig{
		User: g.sshUsername,
		Auth: []ssh.AuthMethod{ssh.PublicKeys(signer)},
		//#nosec G106
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}
	g.client, err = ssh.Dial("tcp", fmt.Sprintf("%s:22", g.publicIP), sshConfig)
	if err != nil {
		return fmt.Errorf("ssh dial: %w", err)
	}
	return nil
}

func (g *gitLabProvider) runCommand(cmd string) ([]byte, error) {
	s, err := g.client.NewSession()
	if err != nil {
		return nil, fmt.Errorf("ssh session: %w", err)
	}
	defer s.Close()
	o, err := s.CombinedOutput(cmd)
	if err != nil {
		// ensure bytes are returned even if there is an error
		return o, fmt.Errorf("ssh output: %w", err)
	}
	return o, nil
}

func (g *gitLabProvider) installGitLab() error {
	// docker login registry.gitlab.com --username $1 --password $2
	cmd := fmt.Sprintf(`
docker login registry.gitlab.com --username %s --password %s`,
		g.config.tfVars.RegistryUsername,
		g.config.tfVars.RegistryAuthToken,
	)
	output, err := g.runCommand(cmd)
	g.commandLogger(string(output))
	if err != nil {
		return err
	}

	cmd = `docker ps -aqf "name=gitlab"`
	output, err = g.runCommand(cmd)
	g.commandLogger(cmd)
	g.commandLogger(string(output))
	if err != nil {
		return err
	}

	if string(output) == "" {
		// no container running, lets provision one
		g.commandLogger("docker provisioning a new gitlab instance")
		//nolint:lll
		cmd = fmt.Sprintf(`
docker run --detach \
--env GITLAB_OMNIBUS_CONFIG="external_url 'https://%s/'; letsencrypt['enable'] = true; letsencrypt['contact_emails'] = ['%s@gitlab.com']" \
--hostname %s \
--publish 443:443 --publish 80:80 \
--name gitlab \
--restart always \
--volume /tmp/gitlab/config:/etc/gitlab \
--volume /tmp/gitlab/logs:/var/log/gitlab \
--volume /tmp/gitlab/data:/var/opt/gitlab \
--shm-size 256m \
%s`,
			g.config.tfVars.Domain,
			g.config.tfVars.RegistryUsername,
			g.config.tfVars.Domain,
			g.config.tfVars.GitLabImage,
		)
		output, err = g.runCommand(cmd)
		g.commandLogger(cmd)
		g.commandLogger(string(output))
		if err != nil {
			return err
		}
	}
	return nil
}

func (g *gitLabProvider) awaitGitLabReadiness() error {
	ticker := time.NewTicker(10 * time.Second)
	for range ticker.C {
		url := fmt.Sprintf("https://%s/users/sign_in", g.config.tfVars.Domain)
		request, err := http.NewRequest("GET", url, nil)
		if err != nil {
			return fmt.Errorf("sign in req: %w", err)
		}
		response, err := http.DefaultClient.Do(request)
		if err != nil {
			g.logger.Logf(GinkgoT(), "problems with GET %s: %v", url, err)
		} else {
			response.Body.Close()
			if response.StatusCode == 200 {
				g.logger.Logf(GinkgoT(), "connected to %s, instance should be ready", url)
				return nil
			} else {
				g.logger.Logf(GinkgoT(), "waiting for instance to get ready")
			}
		}
	}
	return nil
}

func (g *gitLabProvider) commandLogger(format string) {
	g.logger.Logf(GinkgoT(), format)
}

// TODO(Arun): Check if this function can be recomposed.
//
//nolint:funlen,cyclop
func (g *gitLabProvider) setupDependencies() error {
	var cmd string
	//nolint:lll
	cmd = `docker exec -i gitlab grep 'Password:' /etc/gitlab/initial_root_password | sed 's/^.\{10\}//' > /tmp/gitlab_initial_root_password`
	if _, err := g.runCommand(cmd); err != nil {
		return err
	}
	cmd = `cat /tmp/gitlab_initial_root_password`
	output, err := g.runCommand(cmd)
	if err != nil {
		return err
	}
	g.config.tfVars.GitLabRootPassword = string(output)

	// Bear in mind the steps are not idempotent by design, since
	// a personal access token cannot be fetched back. We just create
	// a new one and use that one to make requests!
	rootAuthToken := g.config.GitLabAdminToken

	tokenName := fmt.Sprintf("api-auth-token %s", time.Now().UTC().Format(time.RFC3339))
	//nolint:lll
	cmd = fmt.Sprintf(`
	docker exec -i gitlab gitlab-rails runner \
	"token = User.find_by_username('root').personal_access_tokens.create(scopes: [:api], name: '%s', expires_at: 2.day.from_now); \
	token.set_token('%s'); \
	token.save!"`,
		tokenName,
		rootAuthToken,
	)
	_, err = g.runCommand(cmd)
	if err != nil {
		return err
	}

	// enable feature flags
	cmd = `docker exec -i gitlab gitlab-rails runner \
	"Feature.enable(:integrated_error_tracking); \
	Feature.enable(:gitlab_error_tracking); \
	Feature.enable(:observability_group_tab);"`
	_, err = g.runCommand(cmd)
	if err != nil {
		return err
	}

	// get error tracking auth token
	cmd = `docker exec -i gitlab gitlab-rails runner \
	"puts Gitlab::CurrentSettings.error_tracking_access_token;"`
	output, err = g.runCommand(cmd)
	if err != nil {
		return err
	}
	g.config.tfVars.InternalEndpointToken = strings.TrimSpace(string(output))

	// create application
	type body struct {
		Name        string `json:"name"`
		RedirectURI string `json:"redirect_uri"`
		Scopes      string `json:"scopes"`
	}
	postBody := &body{
		Name:        "gitlab-observability",
		RedirectURI: fmt.Sprintf("https://%s/v1/auth/callback", g.config.GOBHost),
		Scopes:      "api",
	}
	postBodyJSON, err := json.Marshal(postBody)
	if err != nil {
		return fmt.Errorf("application post encode: %w", err)
	}

	url := fmt.Sprintf("https://%s/api/v4/applications", g.config.tfVars.Domain)
	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(postBodyJSON))
	if err != nil {
		return fmt.Errorf("applications req: %w", err)
	}
	request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", rootAuthToken))
	request.Header.Set("Content-Type", "application/json")

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return fmt.Errorf("http applications res: %w", err)
	}
	defer response.Body.Close()
	responseBody, err := io.ReadAll(response.Body)
	if err != nil {
		return fmt.Errorf("res read: %w", err)
	}
	if response.StatusCode >= 200 && response.StatusCode <= 300 {
		responseAttributes := make(map[string]interface{})
		if err := json.Unmarshal(responseBody, &responseAttributes); err != nil {
			return fmt.Errorf("json decode applications: %w", err)
		}
		// setup needed oath variables
		if v, ok := responseAttributes["application_id"].(string); ok {
			g.config.tfVars.OAuthClientID = v
		}
		if v, ok := responseAttributes["secret"].(string); ok {
			g.config.tfVars.OAuthClientSecret = v
		}
	}

	// Update Error tracking URL for ingestion
	url = fmt.Sprintf("https://%s/api/v4/application/settings", g.config.tfVars.Domain)
	request, err = http.NewRequest(http.MethodPut, url, nil)
	if err != nil {
		return fmt.Errorf("application/settings req: %w", err)
	}

	q := request.URL.Query()
	q.Add("error_tracking_enabled", "true")
	q.Add("error_tracking_api_url", fmt.Sprintf("https://%s", g.config.GOBHost))
	request.URL.RawQuery = q.Encode()
	request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", rootAuthToken))
	request.Header.Set("Content-Type", "application/json")

	response, err = http.DefaultClient.Do(request)
	if err != nil {
		return fmt.Errorf("http application/settings res: %w", err)
	}
	defer response.Body.Close()

	responseBody, err = io.ReadAll(response.Body)
	if err != nil {
		return fmt.Errorf("res read %w", err)
	}

	if response.StatusCode != http.StatusOK {
		return fmt.Errorf(
			"failed to update error tracking URL for gitlab: status(%v) - payload: %v",
			response.Status,
			string(responseBody),
		)
	}
	return nil
}
