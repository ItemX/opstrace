const { defineConfig } = require("cypress");

module.exports = defineConfig({
  fixturesFolder: false,
  defaultCommandTimeout: 10000,
  requestTimeout: 10000,
  retries: 2,
  e2e: {
    baseUrl: "https://" + process.env.GITLAB_DOMAIN,
    specPattern: "suite/specs/**/*.cy.{js,jsx,ts,tsx}",
    setupNodeEvents(on, config) {
      config.env = config.env || {};
      // setup configuration env
      config.env.GITLAB_DOMAIN = process.env.GITLAB_DOMAIN;
      config.env.GITLAB_USERNAME = process.env.GITLAB_USERNAME;
      config.env.GITLAB_PASSWORD = process.env.GITLAB_PASSWORD;
      config.env.SENTRY_DSN = process.env.SENTRY_DSN;
      config.env.ERROR_TRACKING_PROJECT_PATH =
        process.env.ERROR_TRACKING_PROJECT_PATH;
      return config;
    },
  },
});
