package tests

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	apiutil "github.com/cert-manager/cert-manager/pkg/api/util"
	certmanager "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	cmmeta "github.com/cert-manager/cert-manager/pkg/apis/meta/v1"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	redis "github.com/spotahome/redis-operator/api/redisfailover/v1"
	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	tenantv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/yaml"
)

// Load the infra configured cluster and ensure it's ready.
func beforeAllEnsureCluster(out *schedulerv1alpha1.Cluster) {
	BeforeAll(func(ctx SpecContext) {
		loadYaml("../../../scheduler/config/examples/Cluster.yaml", out)
		Expect(testInfra.ConfigureCluster(out)).To(Succeed())
		createOrUpdate(ctx, out)
		expectClusterReady(ctx, out)
		expectIngressCertificateReady(ctx)
	})
}

// Load the example namespace and ensure it's ready.
func beforeAllEnsureNamespace(out *schedulerv1alpha1.GitLabNamespace) {
	BeforeAll(func(ctx SpecContext) {
		By("load GitLabNamespace config example")
		loadYaml("../../../scheduler/config/examples/OpstraceNamespace.yaml", out)
		createOrUpdate(ctx, out)
		expectGitLabNamespaceReady(ctx, out)
	})
}

// wait for the cluster to have a successful ready condition.
func expectClusterReady(
	ctx context.Context,
	cluster *schedulerv1alpha1.Cluster,
) {
	By("check Cluster CR has ready condition: True")
	Eventually(func(g Gomega) {
		cr := &schedulerv1alpha1.Cluster{}
		g.Expect(defaultNSClient.Get(ctx, client.ObjectKeyFromObject(cluster), cr)).To(Succeed())

		g.Expect(
			meta.IsStatusConditionTrue(cr.Status.Conditions, common.ConditionTypeReady),
		).To(BeTrue())
	}).WithTimeout(time.Minute * 20).Should(Succeed())
}

// wait for ingress certificate to become ready
func expectIngressCertificateReady(ctx context.Context) {
	By("check ingress certificate is ready")
	Eventually(func(g Gomega) {
		cert := new(certmanager.Certificate)
		key := client.ObjectKey{
			// Values are hardcoded in
			// manifests/nginxIngress/kustomization.yaml as well.
			Namespace: "default",
			Name:      "https-cert",
		}
		g.Expect(defaultNSClient.Get(ctx, key, cert)).To(Succeed())

		g.Expect(
			apiutil.CertificateHasCondition(
				cert,
				certmanager.CertificateCondition{
					Type:   certmanager.CertificateConditionReady,
					Status: cmmeta.ConditionTrue,
				},
			),
		).To(BeTrue())
	}).WithTimeout(time.Minute * 3).Should(Succeed())
}

// wait for GitLabNamespace resource to have successful ready condition.
func expectGitLabNamespaceReady(
	ctx context.Context,
	ns *schedulerv1alpha1.GitLabNamespace,
) {
	By(fmt.Sprintf("check GitLabNamespace CR: %s has ready condition: True", ns.Namespace()))
	Eventually(func(g Gomega) {
		cr := &schedulerv1alpha1.GitLabNamespace{}
		g.Expect(defaultNSClient.Get(ctx, client.ObjectKeyFromObject(ns), cr)).To(Succeed())

		g.Expect(
			meta.IsStatusConditionTrue(cr.Status.Conditions, common.ConditionTypeReady)).
			To(BeTrue())
	}).Should(Succeed())
}

func expectNamespacePodsReady(
	ctx context.Context,
	namespace string,
) {
	By("check all Pods are ready in namespace: " + namespace)
	Eventually(func(g Gomega) {
		pods := &corev1.PodList{}
		g.Expect(k8sClient.List(ctx, pods, client.InNamespace(namespace))).To(Succeed())
		for _, p := range pods.Items {
			ready := false
			if p.Status.Phase == corev1.PodSucceeded {
				// For a pod where all containers have terminated in success,
				// consider it ready. This is usually the case with one-off
				// pods spawned as Job(s), e.g. when setting up nginx-ingress
				// admission webhook.
				ready = true
			} else {
				for _, c := range p.Status.Conditions {
					if c.Type == corev1.PodReady && c.Status == corev1.ConditionTrue {
						ready = true
						break
					}
				}
			}
			g.Expect(ready).To(BeTrue(), "all Pods should be ready")
		}
	}).Should(Succeed())
}

// gather resources owned by the resource and ensure they are all deleted.
func deleteCustomResourceAndVerify(
	ctx context.Context,
	cr client.Object,
	extraNamespaces ...string,
) {
	By("gather all resources that are owned by this resource")

	crUID := cr.GetUID()
	Expect(crUID).NotTo(Equal(""), "CR must have UID set from server")
	expectDeleted := []client.Object{}

	namespaces := append([]string{cr.GetNamespace()}, extraNamespaces...)

	for _, ns := range namespaces {
		for _, r := range potentiallyOwnedResources() {
			Expect(k8sClient.List(ctx, r, client.InNamespace(ns))).To(Succeed())
			v := reflect.ValueOf(r)
			// Note(joe): I can't find a nice way of doing this without reflection or lots of boilerplate for each type.
			// Every list type has the Items field.
			items := reflect.Indirect(v).FieldByName("Items")
			// all cleared up.
			if items.IsNil() {
				continue
			}
			for i := 0; i < items.Len(); i++ {
				item := items.Index(i)
				// list items can either be struct values or pointers
				var meta metav1.Object
				var ok bool
				if item.Kind() == reflect.Pointer {
					meta, ok = item.Interface().(metav1.Object)
				} else {
					meta, ok = item.Addr().Interface().(metav1.Object)
				}
				if !ok {
					continue
				}
				for _, o := range meta.GetOwnerReferences() {
					if o.UID == crUID {
						expectDeleted = append(expectDeleted, meta.(client.Object))
					}
				}
			}
		}
	}

	Expect(expectDeleted).NotTo(BeEmpty(), "at least one owned resource must exist")

	By(fmt.Sprintf("delete the CR: %s/%s", cr.GetNamespace(), cr.GetName()))
	Expect(k8sClient.Delete(ctx, cr)).To(Succeed())

	By(fmt.Sprintf("wait for %d owned resources to be deleted", len(expectDeleted)))
	Eventually(func(g Gomega) {
		for _, u := range expectDeleted {
			obj := u
			err := k8sClient.Get(ctx, client.ObjectKeyFromObject(obj), obj)
			g.Expect(err).To(HaveOccurred(), "want a not found error")
			g.Expect(client.IgnoreNotFound(err)).NotTo(HaveOccurred())
		}
		err := k8sClient.Get(ctx, client.ObjectKeyFromObject(cr), cr)
		g.Expect(err).To(HaveOccurred(), "want a not found error")
		g.Expect(client.IgnoreNotFound(err)).NotTo(HaveOccurred())
	}).Should(Succeed(), "check owned resources and cr are deleted")
}

// get potentially owned resource types for our CRs.
// Note(joe): It would be nice if there were a better way of inferring potentially created types.
// I tried using `k8sClient.Scheme().AllKnownTypes()` but the number of potential types is impractical.
func potentiallyOwnedResources() []client.ObjectList {
	return []client.ObjectList{
		&appsv1.DeploymentList{},
		&appsv1.StatefulSetList{},
		&netv1.IngressList{},
		&corev1.ConfigMapList{},
		&corev1.ServiceList{},
		&corev1.SecretList{},
		&corev1.ServiceAccountList{},
		&rbacv1.ClusterRoleList{},
		&rbacv1.ClusterRoleBindingList{},
		&rbacv1.RoleList{},
		&rbacv1.RoleBindingList{},
		&monitoring.ServiceMonitorList{},
		&monitoring.PrometheusRuleList{},
		&monitoring.PrometheusList{},
		&redis.RedisFailoverList{},
		&clickhousev1alpha1.ClickHouseList{},
		&certmanager.CertificateList{},
		&certmanager.IssuerList{},
		&tenantv1alpha1.TenantList{},
		&tenantv1alpha1.GroupList{},
	}
}

// wait for all pods to be in a ready state.
func expectClusterPodsReady(
	ctx context.Context,
) {
	expectNamespacePodsReady(ctx, "default")
}

func loadYaml(path string, out interface{}) {
	f, err := filepath.Abs(path)
	Expect(err).NotTo(HaveOccurred())
	secretBytes, err := os.ReadFile(f)
	Expect(err).NotTo(HaveOccurred())

	Expect(yaml.Unmarshal(secretBytes, out)).To(Succeed())
}

func createOrUpdate(
	ctx context.Context,
	obj client.Object,
) {
	cp, ok := obj.DeepCopyObject().(client.Object)
	Expect(ok).To(BeTrue(), "runtime object should support client.Object")
	err := defaultNSClient.Get(ctx, client.ObjectKeyFromObject(cp), cp)
	Expect(client.IgnoreNotFound(err)).To(Succeed())
	if err == nil {
		obj.SetResourceVersion(cp.GetResourceVersion())
		Expect(defaultNSClient.Update(ctx, obj)).To(Succeed())
	} else {
		Expect(defaultNSClient.Create(ctx, obj)).To(Succeed())
	}
}
