package errortracking

import (
	"encoding/json"
	"fmt"

	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/persistentqueue"
)

type Ingester interface {
	IngestEventData(uint64, string, []byte, *types.Event) error
	IngestMessageData(uint64, string, []byte, *types.Event) error
	IngestSessionData(uint64, string, []byte, *types.Session) error
}

type ClickhouseIngester struct {
	db Database
}

func NewClickhouseIngester(db Database) Ingester {
	return &ClickhouseIngester{db: db}
}

func (c *ClickhouseIngester) IngestEventData(
	projectID uint64, _ string, payload []byte, event *types.Event) error {
	err := c.db.InsertErrorTrackingErrorEvent(et.NewErrorTrackingErrorEvent(projectID, event, payload))
	if err != nil { // failure to insert
		return fmt.Errorf("failed to insert error event: %w", err)
	}

	var v2 *et.ErrorEventV2
	v2, err = et.NewErrorEventV2(projectID, event, payload)
	if err != nil { // failure to construct object
		return fmt.Errorf("creating error event V2: %w", err)
	}

	err = c.db.InsertErrorEventV2(v2)
	if err != nil { // failure to produce
		return fmt.Errorf("inserting error event V2: %w", err)
	}
	return nil
}

func (c *ClickhouseIngester) IngestMessageData(
	projectID uint64, _ string, payload []byte, event *types.Event) error {
	err := c.db.InsertErrorTrackingMessageEvent(et.NewErrorTrackingMessageEvent(projectID, event, payload))
	if err != nil {
		return fmt.Errorf("failed to insert message event to the database: %w", err)
	}

	return nil
}

func (c *ClickhouseIngester) IngestSessionData(
	projectID uint64, _ string, payload []byte, session *types.Session) error {
	err := c.db.InsertErrorTrackingSession(et.NewErrorTrackingSession(projectID, session, payload))
	if err != nil {
		return fmt.Errorf("failed to insert session to the database: %w", err)
	}
	return nil
}

type QueueIngester struct {
	queue persistentqueue.IQueue
}

func NewQueueIngester(queue *persistentqueue.Queue) Ingester {
	return &QueueIngester{queue: queue}
}

func (q *QueueIngester) IngestEventData(
	projectID uint64, payloadType string, payload []byte, _ *types.Event) error {
	return q.ingestData(projectID, payloadType, payload)
}

func (q *QueueIngester) IngestMessageData(
	projectID uint64, payloadType string, payload []byte, _ *types.Event) error {
	return q.ingestData(projectID, payloadType, payload)
}

func (q *QueueIngester) IngestSessionData(
	projectID uint64, payloadType string, payload []byte, session *types.Session) error {
	return q.ingestData(projectID, payloadType, payload)
}

func (q *QueueIngester) ingestData(projectID uint64, payloadType string, payload []byte) error {
	var b []byte
	b, err := json.Marshal(QueueItem{
		Type:      payloadType,
		Payload:   payload,
		ProjectID: projectID,
	})
	if err != nil {
		return fmt.Errorf("marshaling queue item: %w", err)
	}
	err = q.queue.Insert(b)
	if err != nil {
		return fmt.Errorf("enqueueing event: %w", err)
	}
	return nil
}
