package metrics

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	requestCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_requests_total",
			Help: "number of requests by status code, method and path",
		},
		[]string{"code", "method", "path"},
	)
	// TODO(Arun): Investigate request duration buckets with actual mean responses from our APIs.
	latencyHist = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "http_requests_duration_seconds",
			Help:    "duration of HTTP requests by status code, method and path",
			Buckets: []float64{0.05, 0.1, 0.25, 0.5, 1.0, 5.0},
		},
		[]string{"code", "method", "path"},
	)
	responseSize = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "http_response_size",
			Help:    "Size of HTTP response in bytes by status code, method and path",
			Buckets: []float64{512, 1024, 2048, 4096},
		},
		[]string{"code", "method", "path"},
	)
)

func ConfiguredCollectors() []prometheus.Collector {
	return []prometheus.Collector{
		requestCounter,
		latencyHist,
		responseSize,
	}
}

// StripFunc is a helper method to strip path such that it can be used as a label (satisfy low cardinality constraints).
type StripFunc func(str string) string

// AdditionalMetricsFunc should be a helper method to add additional metrics derived from the request.
type AdditionalMetricsFunc func(r *http.Request, code, method, path string)

func PrometheusMiddleware(next http.Handler, strip StripFunc, addMetrics AdditionalMetricsFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// delegate to capture response on the way back
		delegate := &responseWriterDelegator{ResponseWriter: w}
		rw := delegate

		begin := time.Now()
		next.ServeHTTP(rw, r)

		code := sanitizeCode(delegate.status)
		method := sanitizeMethod(r.Method)
		path := strip(r.URL.Path)

		requestCounter.WithLabelValues(code, method, path).Inc()
		latencyHist.WithLabelValues(
			code,
			method,
			path).Observe(float64(time.Since(begin)) / float64(time.Second))

		responseSize.WithLabelValues(
			code,
			method,
			path).Observe(float64(delegate.written))

		addMetrics(r, code, method, path)
	})
}

// NoOpStripFunc is a placeholder for strip function if path is to be kept as is
func NoOpStripFunc(str string) string {
	return str
}

func NoOpAdditionalMetricsFunc(_ *http.Request, _, _, _ string) {
}

type responseWriterDelegator struct {
	http.ResponseWriter
	status      int
	written     int64
	wroteHeader bool
}

func (r *responseWriterDelegator) WriteHeader(code int) {
	r.status = code
	r.wroteHeader = true
	r.ResponseWriter.WriteHeader(code)
}

func (r *responseWriterDelegator) Write(b []byte) (int, error) {
	if !r.wroteHeader {
		r.WriteHeader(http.StatusOK)
	}
	n, err := r.ResponseWriter.Write(b)
	if err != nil {
		return n, fmt.Errorf("responseWriteDelegator failed to write with %w", err)
	}
	r.written += int64(n)
	return n, nil
}

func sanitizeMethod(m string) string {
	return strings.ToUpper(m)
}

func sanitizeCode(s int) string {
	return strconv.Itoa(s)
}
