package argusapi

import (
	"testing"

	"github.com/gobs/pretty"
)

const (
	createdDataSourceJSON = `{"id":1,"uid":"myuid0001","message":"Datasource added", "name": "test_datasource"}`
)

func TestNewDataSource(t *testing.T) {
	server, client := gapiTestTools(t, 200, createdDataSourceJSON)
	defer server.Close()

	ds := &DataSource{
		Name:      "foo",
		Type:      "cloudwatch",
		URL:       "http://some-url.com",
		Access:    "access",
		IsDefault: true,
		JSONData: map[string]any{
			"assumeRoleArn":           "arn:aws:iam::123:role/some-role",
			"authType":                "keys",
			"customMetricsNamespaces": "SomeNamespace",
			"defaultRegion":           "us-east-1",
			"tlsSkipVerify":           true,
		},
		SecureJSONData: map[string]any{
			"accessKey": "123",
			"secretKey": "456",
		},
	}

	created, err := client.NewDataSource(ds)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(pretty.PrettyFormat(created))

	if created != 1 {
		t.Error("datasource creation response should return the created datasource ID")
	}
}

func TestNewPrometheusDataSource(t *testing.T) {
	server, client := gapiTestTools(t, 200, createdDataSourceJSON)
	defer server.Close()

	ds := &DataSource{
		Name:      "foo_prometheus",
		Type:      "prometheus",
		URL:       "http://some-url.com",
		Access:    "access",
		IsDefault: true,
		JSONData: map[string]any{
			"httpMethod":   "POST",
			"queryTimeout": "60s",
			"timeInterval": "1m",
		},
	}

	created, err := client.NewDataSource(ds)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(pretty.PrettyFormat(created))

	if created != 1 {
		t.Error("datasource creation response should return the created datasource ID")
	}
}

func TestNewPrometheusSigV4DataSource(t *testing.T) {
	server, client := gapiTestTools(t, 200, createdDataSourceJSON)
	defer server.Close()

	ds := &DataSource{
		Name:      "sigv4_prometheus",
		Type:      "prometheus",
		URL:       "http://some-url.com",
		Access:    "access",
		IsDefault: true,
		JSONData: map[string]any{
			"hTTPMethod":    "POST",
			"sigV4Auth":     true,
			"sigV4AuthType": "keys",
			"sigV4Region":   "us-east-1",
		},
		SecureJSONData: map[string]any{
			"sigV4AccessKey": "123",
			"sigV4SecretKey": "456",
		},
	}

	created, err := client.NewDataSource(ds)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(pretty.PrettyFormat(created))

	if created != 1 {
		t.Error("datasource creation response should return the created datasource ID")
	}
}

func TestNewElasticsearchDataSource(t *testing.T) {
	server, client := gapiTestTools(t, 200, createdDataSourceJSON)
	defer server.Close()

	ds := &DataSource{
		Name:      "foo_elasticsearch",
		Type:      "elasticsearch",
		URL:       "http://some-url.com",
		IsDefault: true,
		JSONData: map[string]any{
			"esVersion":                  "7.0.0",
			"timeField":                  "time",
			"interval":                   "1m",
			"logMessageField":            "message",
			"logLevelField":              "field",
			"maxConcurrentShardRequests": 8,
		},
	}

	created, err := client.NewDataSource(ds)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(pretty.PrettyFormat(created))

	if created != 1 {
		t.Error("datasource creation response should return the created datasource ID")
	}
}

func TestNewInfluxDBDataSource(t *testing.T) {
	server, client := gapiTestTools(t, 200, createdDataSourceJSON)
	defer server.Close()

	ds := &DataSource{
		Name:      "foo_influxdb",
		Type:      "influxdb",
		URL:       "http://some-url.com",
		IsDefault: true,
		JSONData: map[string]any{
			"defaultBucket":   "telegraf",
			"organization":    "acme",
			"version":         "Flux",
			"httpHeaderName1": "Authorization",
		},
		SecureJSONData: map[string]any{
			"httpHeaderValue1": "Token alksdjaslkdjkslajdkj.asdlkjaksdjlkajsdlkjsaldj==",
		},
	}

	created, err := client.NewDataSource(ds)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(pretty.PrettyFormat(created))

	if created != 1 {
		t.Error("datasource creation response should return the created datasource ID")
	}
}

func TestNewOpenTSDBDataSource(t *testing.T) {
	server, client := gapiTestTools(t, 200, createdDataSourceJSON)
	defer server.Close()

	ds := &DataSource{
		Name:      "foo_opentsdb",
		Type:      "opentsdb",
		URL:       "http://some-url.com",
		Access:    "access",
		IsDefault: true,
		JSONData: map[string]any{
			"tsdbResolution": 1,
			"tsdbVersion":    3,
		},
	}

	created, err := client.NewDataSource(ds)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(pretty.PrettyFormat(created))

	if created != 1 {
		t.Error("datasource creation response should return the created datasource ID")
	}
}

func TestNewAzureDataSource(t *testing.T) {
	server, client := gapiTestTools(t, 200, createdDataSourceJSON)
	defer server.Close()

	ds := &DataSource{
		Name:      "foo_azure",
		Type:      "grafana-azure-monitor-datasource",
		URL:       "http://some-url.com",
		Access:    "access",
		IsDefault: true,
		JSONData: map[string]any{
			"azureLogAnalyticsSameAs":      true,
			"clientID":                     "lorem-ipsum",
			"cloudName":                    "azuremonitor",
			"logAnalyticsClientID":         "lorem-ipsum",
			"logAnalyticsDefaultWorkspace": "lorem-ipsum",
			"logAnalyticsTenantID":         "lorem-ipsum",
			"subscriptionID":               "lorem-ipsum",
			"tenantID":                     "lorem-ipsum",
		},
		SecureJSONData: map[string]any{
			"clientSecret": "alksdjaslkdjkslajdkj.asdlkjaksdjlkajsdlkjsaldj==",
		},
	}

	created, err := client.NewDataSource(ds)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(pretty.PrettyFormat(created))

	if created != 1 {
		t.Error("datasource creation response should return the created datasource ID")
	}
}
