package tenant

import (
	"fmt"
	"time"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	ctrlcommon "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/common"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

const (
	IngressPathTmpl = `/v3/%d/(?<projectID>[0-9]+)/ingest/traces`
)

type OTELCollectorReconciler struct {
	Teardown         bool
	Log              logr.Logger
	initialManifests map[string][]byte
	State            *GitLabObservabilityTenantState
}

func NewOTELCollectorReconciler(
	initialManifests map[string][]byte,
	teardown bool,
	tenantID int64,
	state *GitLabObservabilityTenantState,
) *OTELCollectorReconciler {
	return &OTELCollectorReconciler{
		Teardown: teardown,
		Log: logf.Log.WithName(
			fmt.Sprintf("tenantmanifests/%s-%d", constants.OtelCollectorComponentName, tenantID),
		),
		initialManifests: initialManifests,
		State:            state,
	}
}

func (i *OTELCollectorReconciler) Reconcile(cr *v1alpha1.GitLabObservabilityTenant) common.DesiredState {
	if i.Teardown {
		// We are cleaning up, we'll just use the inventory to enlist things
		// to clean up
		return common.DesiredState{
			common.ManifestsPruneAction{
				ComponentName: constants.OtelCollectorComponentName,
				GetInventory: func() *v1beta2.ResourceInventory {
					return cr.Status.Inventory[constants.OtelCollectorInventoryID]
				},
				Msg: "[tenantmanifests/otelcollector] prune",
				Log: i.Log,
			},
		}
	}

	if i.State.TenantNamespace == nil {
		return common.DesiredState{
			common.LogAction{
				Msg:   "[tenantmanifests/otelcollector] failed to read tenant namespace, will retry",
				Error: fmt.Errorf("tenant namespace not available yet"),
			},
		}
	}

	if i.State.getTenantCHCredentials() == nil {
		return common.DesiredState{
			common.LogAction{
				Msg:   "[tenantmanifests/otelcollector] failed to read tenant CH credentials, will retry",
				Error: fmt.Errorf("tenant CH credentials not available yet"),
			},
		}
	}

	overrideKustomization, err := i.ApplyConfiguration(cr)
	if err != nil {
		return common.DesiredState{
			common.LogAction{
				Msg:   "[tenantmanifests/otelcollector] failed to apply configuration to manifests",
				Error: err,
			},
		}
	}
	i.ApplyOverrides(overrideKustomization, cr)

	manifests, err := ctrlcommon.ApplyKustomization(i.initialManifests, overrideKustomization)
	if err != nil {
		return common.DesiredState{
			common.LogAction{
				Msg:   "[tenantmanifests/otelcollector] failed to apply configuration to manifests",
				Error: err,
			},
		}
	}

	return common.DesiredState{
		common.ManifestsEnsureAction{
			HealthcheckTimeout: 2 * time.Minute,
			Manifests:          manifests,
			ComponentName:      constants.OtelCollectorComponentName,
			GetInventory: func() *v1beta2.ResourceInventory {
				return cr.Status.Inventory[constants.OtelCollectorInventoryID]
			},
			SetInventory: func(inv *v1beta2.ResourceInventory) {
				cr.Status.Inventory[constants.OtelCollectorInventoryID] = inv
			},
			Msg: "[tenantmanifests/otelcollector] ensure",
			Log: i.Log,
		},
	}
}

//nolint:unparam
func (i *OTELCollectorReconciler) ApplyConfiguration(
	cr *v1alpha1.GitLabObservabilityTenant,
) (*kustomize.Kustomization, error) {
	res := &kustomize.Kustomization{
		TypeMeta: kustomize.TypeMeta{
			Kind:       kustomize.KustomizationKind,
			APIVersion: kustomize.KustomizationVersion,
		},
		Namespace: cr.Namespace(),
		Resources: []string{
			ctrlcommon.TmpResourceData,
		},
	}

	res.Images = []kustomize.Image{
		{
			Name:    "otel-collector-image-placeholder",
			NewName: constants.DockerImageName(constants.OtelCollectorComponentName),
			NewTag:  constants.DockerImageTag,
		},
	}

	patches := []types.Patch{}

	// deployment
	// we set CH credentials & tenant ID here explicitly as CLI args to avoid them
	// from being overridden from other collector configs whenever applicable
	patches = append(patches, types.Patch{
		Target: deploymentSelector(),
		Patch: fmt.Sprintf(
			`[
				{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "%s"}
			]`,
			fmt.Sprintf(
				"--set=exporters.gitlabobservability.tenant_id=%d",
				cr.Spec.TopLevelNamespaceID,
			),
		),
	})
	if i.State.getTenantCHCredentials() != nil {
		patches = append(patches, types.Patch{
			Target: deploymentSelector(),
			Patch: fmt.Sprintf(
				`[
					{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "%s"}
				]`,
				fmt.Sprintf(
					"--set=exporters.gitlabobservability.clickhouse_dsn=%s/tracing",
					&i.State.getTenantCHCredentials().Native,
				),
			),
		})
	}

	// ingress
	if i.State.Cluster != nil {
		host := i.State.Cluster.Spec.GetHost()
		// setup nginx-ingress auth-url
		// e.g. http://GATEKEEPER_ENDPOINT/v1/auth/both/webhook/GROUP_ID/PROJECT_ID/write?min_accesslevel=0
		authURL := fmt.Sprintf(
			"%s/v1/auth/both/webhook/%d/$projectID/write?min_accesslevel=0",
			fmt.Sprintf("http://gatekeeper.%s.svc.cluster.local:3001", i.State.Cluster.Namespace()),
			cr.Spec.TopLevelNamespaceID,
		)
		patches = append(patches, types.Patch{
			Target: ingressSelector(),
			Patch: fmt.Sprintf(
				`[
					{"op": "replace", "path": "/spec/rules/0/host", "value": "%s"},
					{"op": "replace", "path": "/spec/rules/0/http/paths/0/path", "value": "%s"},
					{"op": "replace", "path": "/metadata/annotations/nginx.ingress.kubernetes.io~1auth-url", "value": "%s"},
					{"op": "replace", "path": "/spec/tls/0/hosts/0", "value": "%s" }
				]`,
				host,
				fmt.Sprintf(IngressPathTmpl, cr.Spec.TopLevelNamespaceID),
				authURL,
				host,
			),
		})
	}

	// podmonitor
	patches = append(patches, types.Patch{
		Target: podmonitorSelector(),
		Patch: fmt.Sprintf(
			`[
				{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}
			]`,
			cr.Namespace(),
		),
	})
	res.PatchesJson6902 = patches

	return res, nil
}

func (i *OTELCollectorReconciler) ApplyOverrides(k *kustomize.Kustomization, cr *v1alpha1.GitLabObservabilityTenant) {
	if cr.Spec.Overrides.OTELCollector == nil {
		return
	}

	k.CommonLabels = cr.Spec.Overrides.OTELCollector.CommonLabels
	k.Labels = cr.Spec.Overrides.OTELCollector.Labels
	k.CommonAnnotations = cr.Spec.Overrides.OTELCollector.CommonAnnotations
	k.PatchesStrategicMerge = cr.Spec.Overrides.OTELCollector.PatchesStrategicMerge
	k.PatchesJson6902 = append(k.PatchesJson6902, cr.Spec.Overrides.OTELCollector.PatchesJson6902...)
	k.Images = cr.Spec.Overrides.OTELCollector.Images
	k.Replicas = cr.Spec.Overrides.OTELCollector.Replicas
}

func deploymentSelector() *kustomize.Selector {
	return &types.Selector{
		ResId: resid.ResId{
			Gvk: resid.Gvk{
				Kind: "Deployment",
			},
			Name: constants.OtelCollectorComponentName,
		},
	}
}

func podmonitorSelector() *kustomize.Selector {
	return &types.Selector{
		ResId: resid.ResId{
			Gvk: resid.Gvk{
				Kind: "PodMonitor",
			},
			Name: constants.OtelCollectorComponentName,
		},
	}
}

func ingressSelector() *kustomize.Selector {
	return &types.Selector{
		ResId: resid.ResId{
			Gvk: resid.Gvk{
				Kind: "Ingress",
			},
			Name: constants.OtelCollectorComponentName,
		},
	}
}
