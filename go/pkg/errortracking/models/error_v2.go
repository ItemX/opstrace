package models

import "time"

type ErrorV2 struct {
	ProjectID uint64 `ch:"project_id"`
	// Currently this ID is same as the fingerprint calculated from the error event
	ID          string `json:"id,omitempty" ch:"_id"`
	Fingerprint []byte `ch:"fingerprint"`

	// Status is a code:
	//   0 - unresolved
	//   1 - resolved
	//   2 - ignored
	Status uint8  `ch:"status"`
	Actor  string `ch:"actor"`
	// TODO: Title needs to be computed
	Title string `ch:"title"`

	Count       uint64    `ch:"count"`
	UserCount   uint64    `ch:"user_count"`
	LastSeenAt  time.Time `ch:"_last_seen_at"`
	FirstSeenAt time.Time `ch:"_first_seen_at"`
}
