package testutils

import (
	"context"

	"github.com/anthhub/forwarder"
	"k8s.io/client-go/tools/portforward"

	. "github.com/onsi/gomega"
	"k8s.io/client-go/rest"
)

// PortForward will try to configure a port-forward proxy for the given opts.
// Uses gomega checks and will fail any calling test if the proxy cannot be made.
// Returns forwarded ports and a closer function to shut down the proxy.
func PortForward(restConfig *rest.Config, service *forwarder.Option, g Gomega) (
	[]portforward.ForwardedPort, func()) {
	var forward *forwarder.Result

	g.Eventually(func(g Gomega) {
		// forwarder uses context for both initial setup and as a closer for the port-forward.
		// We'll use a background context to simplify the test code.
		res, err := forwarder.WithRestConfig(context.Background(), []*forwarder.Option{service}, restConfig)
		g.Expect(err).NotTo(HaveOccurred())

		forward = res
	}).Should(Succeed())

	g.Expect(forward).NotTo(BeNil())

	var ports [][]portforward.ForwardedPort

	done := make(chan struct{})
	go func() {
		var err error
		ports, err = forward.Ready()
		Expect(err).NotTo(HaveOccurred())
		close(done)
	}()

	g.Eventually(done).Should(BeClosed(), "waiting for port-forward to be ready")
	g.Expect(ports).To(HaveLen(1))

	return ports[0], forward.Close
}
