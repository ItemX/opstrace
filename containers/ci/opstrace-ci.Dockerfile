# The image defined by this Dockerfile here is supposed to provide all dependencies
# required by CI
FROM debian:buster-slim

# - moreutils, for `chronic` and `ts`.
# - make, for running make targets in the container
# - git, for interacting with the current checkout.
# - gettext-base for envsubst
# - uuid-runtime for uuidgen used by ci_events
# - netcat for debugging
# - tree, because we love nature
RUN apt-get update && apt-get install -y -q --no-install-recommends \
    uuid-runtime rsync curl gnupg2 git make moreutils netcat-openbsd \
    build-essential gettext-base ca-certificates unzip less tree python3 \
    # Terraform dependencies
    software-properties-common openssh-server \
    # Frontend integration tests dependencies, see also https://docs.cypress.io/guides/continuous-integration/introduction#Linux
    libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb \
    npm

# install node/npm, see https://nodejs.org/en/download/releases
RUN npm install npm@9.5.0 -g && \
    npm install n -g && \
    n 18.15.0
RUN node --version
RUN npm --version

# gcloud CLI, for managing GCP
RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | \
    tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | \
    apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
RUN apt-get update && apt-get install -y -q --no-install-recommends google-cloud-sdk google-cloud-sdk-gke-gcloud-auth-plugin
RUN apt-get -y autoclean

RUN gcloud config set core/disable_usage_reporting true && \
    gcloud config set component_manager/disable_update_check true && \
    gcloud config set metrics/environment github_docker_image

# Install kubectl, discover current stable version dynamically.
RUN KVERSION=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt) && \
    echo "install kubectl ${KVERSION}" && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/${KVERSION}/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl

# Install terraform
RUN curl -L https://releases.hashicorp.com/terraform/1.4.6/terraform_1.4.6_linux_amd64.zip -o "terraform_linux_amd64.zip" && \
    unzip terraform_linux_amd64.zip && \
    chmod +x terraform && \
    mv terraform /usr/local/bin/terraform
# Check install succeeded
RUN terraform --version

# Install terragrunt
RUN curl -L "https://github.com/gruntwork-io/terragrunt/releases/download/v0.45.5/terragrunt_linux_amd64" -o "terragrunt_linux_amd64" && \
    mv terragrunt_linux_amd64 terragrunt && \
    chmod +x terragrunt && \
    mv terragrunt /usr/local/bin/terragrunt
# Check install succeeded
RUN terragrunt --version

# AWS CLI, for fetching data from S3. Mount this into the container: ~/.aws
# (the home dir has AWS credentials set up for accessing our S3 secrets bucket.)
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip -q awscliv2.zip && \
    ./aws/install && \
    rm awscliv2.zip

ENV KUSTOMIZE_VERSION=v5.0.0
RUN curl -sSfL https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2F${KUSTOMIZE_VERSION}/kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz | tar zxvf - -C /usr/local/bin

ENV IFACEMAKER_VERSION=1.1.0
RUN curl -sSfL https://github.com/vburenin/ifacemaker/releases/download/v${IFACEMAKER_VERSION}/ifacemaker_${IFACEMAKER_VERSION}_Linux_x86_64.tar.gz | tar zxvf - -C /usr/local/bin ifacemaker

# MOCKERY_VERSION=2.26.1 causes mockery to bork, see https://github.com/vektra/mockery/issues/391
ENV MOCKERYBIN_VERSION=2.26.1
RUN curl -sSfL https://github.com/vektra/mockery/releases/download/v${MOCKERYBIN_VERSION}/mockery_${MOCKERYBIN_VERSION}_Linux_x86_64.tar.gz | tar zxvf - -C /usr/local/bin mockery

# Set up the Docker binaries so that within the container we can manage
# containers running on the host with the `docker` CLI (the host's Docker
# socket is going to be mounted in).
ARG DOCKER_VERSION=20.10.14
RUN curl -fsSLO https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz && \
    tar --strip-components=1 -xvzf docker-${DOCKER_VERSION}.tgz -C /usr/local/bin && \
    rm -f docker-${DOCKER_VERSION}.tgz

# Set up golang.
ENV GOLANG_VERSION 1.20
RUN curl -fsSLO https://golang.org/dl/go${GOLANG_VERSION}.linux-amd64.tar.gz && \
    tar -xzf go${GOLANG_VERSION}.linux-amd64.tar.gz -C /usr/local/ && \
    rm -f go${GOLANG_VERSION}.linux-amd64.tar.gz

# Set up gojq.
ENV GOJQ_VERSION v0.12.13
RUN curl -fsSLO https://github.com/itchyny/gojq/releases/download/${GOJQ_VERSION}/gojq_${GOJQ_VERSION}_linux_amd64.tar.gz && \
    tar --strip-components=1 -xzf gojq_${GOJQ_VERSION}_linux_amd64.tar.gz -C /usr/local/bin/ gojq_${GOJQ_VERSION}_linux_amd64/gojq && \
    rm -f gojq_${GOJQ_VERSION}_linux_amd64.tar.gz

ENV GOPATH /go

ENV PATH /usr/local/go/bin:$GOPATH/bin:$PATH

# Register build args, set defaults. GID and UID are expected to be overridden
# in CI.
ARG CIUNAME=ciuser
ARG CIUID=1000
ARG CIGID=1000

RUN echo "set up user $CIUNAME / $CIUID in group $CIGID"
RUN groupadd -g $CIGID -o $CIUNAME
RUN useradd -m -u $CIUID -g $CIGID -o -s /bin/bash $CIUNAME
USER $CIUNAME
