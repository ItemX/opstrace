package tests

import (
	"regexp"
	"strings"

	gocommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/common"
)

// used to sanitize the TEST_IDENTIFIER env var
// replacing all non-alphanumeric characters with a dash
var identifierRegexp = regexp.MustCompile(`[[:^alnum:]]`)

// Configuration for the test suite, loaded from env vars.
type Configuration struct {
	TestIdentifier string
	TestTarget     gocommon.EnvironmentTarget
}

// LoadConfig from the environment or error.
func LoadConfig() (Configuration, error) {
	c := Configuration{}
	if tp, err := common.GetEnv("TEST_TARGET_PROVIDER"); err != nil {
		c.TestTarget = gocommon.KIND
	} else {
		c.TestTarget = gocommon.EnvironmentTarget(tp)
	}

	if c.TestTarget == gocommon.KIND {
		c.TestIdentifier = gocommon.RandStringRunes(8)
	} else {
		// use $TEST_IDENTIFIER as an identifier to help identify this
		// test run and all associated resources/dependencies
		ti, err := common.GetEnv("TEST_IDENTIFIER")
		if err != nil {
			return c, err
		}
		id := strings.ToLower(ti)
		c.TestIdentifier = identifierRegexp.ReplaceAllString(id, "-")
	}

	return c, nil
}
