package tests

import (
	"crypto/tls"
	"errors"
	"fmt"
	"net"
	"os/exec"
	"strings"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	gocommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

var _ = Describe("cluster provisioning", Ordered, Serial, func() {
	cluster := &schedulerv1alpha1.Cluster{}
	beforeAllEnsureCluster(cluster)

	Context("on creating an example cluster", func() {
		It("ensures it can be created successfully", func(ctx SpecContext) {
			expectClusterPodsReady(ctx)
		})
	})

	Context("ensures it is served over HTTPS", func() {
		It("by serving a valid certificate", func() {
			if testTarget == gocommon.KIND {
				Skip("skipping on kind clusters as we use self-signed certs")
			}

			conn, err := tls.Dial("tcp", fmt.Sprintf("%s:443", infraConfig.GOBHost), nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(conn).ToNot(BeNil())

			err = conn.VerifyHostname(infraConfig.GOBHost)
			Expect(err).ToNot(HaveOccurred())

			issuer := conn.ConnectionState().PeerCertificates[0].Issuer
			found := strings.Contains(issuer.String(), "O=Let's Encrypt")
			Expect(found).To(BeTrue())
		})
	})

	Context("ensures when accessing a new namespace", func() {
		It("the user is first authorized", func() {
			if testTarget == gocommon.KIND {
				Skip("skipping on kind clusters, as it may not be set up")
			}

			// access provision page for a group
			// TODO(joe): we don't need to exec curl here, we can use the go http client.
			//nolint:gosec
			cmd := exec.Command("curl", "-L", fmt.Sprintf("https://%s/-/1", infraConfig.GOBHost))
			out, err := cmd.Output()
			Expect(err).ToNot(HaveOccurred())
			Expect(out).ToNot(BeNil())
			// make sure we redirect for auth
			found := strings.Contains(string(out), "<title>Sign in · GitLab</title")
			Expect(found).To(BeTrue())
		})
	})

	Context("on deleting the example cluster", func() {
		It("ensures it can be deleted successfully", func(ctx SpecContext) {
			deleteCustomResourceAndVerify(ctx, cluster)
		})

		// Note(joe): this doesn't seem to work anymore.
		PIt("ensures cluster domain has been cleaned up properly", func() {
			if testTarget == gocommon.KIND {
				Skip("skipping on kind clusters, as they use localhost")
			}

			Eventually(func() bool {
				ips, err := net.LookupIP(infraConfig.GOBHost)
				if err != nil {
					var dnsErr *net.DNSError
					if errors.As(err, &dnsErr) {
						if dnsErr.IsNotFound {
							return true // host not found is good state
						}
					}
					return false
				}
				return len(ips) == 0
			}, time.Minute, 5*time.Second).Should(BeTrue())
		})
	})
})
