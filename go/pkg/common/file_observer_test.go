package common_test

import (
	"fmt"
	"os"
	"path"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
)

func atomicFileWrite(tmpDir, filename, content string) {
	b := []byte(content)
	tmpDst := path.Join(tmpDir, filename+".tmp")
	err := os.WriteFile(tmpDst, b, 0600)
	Expect(err).NotTo(HaveOccurred())

	dst := path.Join(tmpDir, filename)
	err = os.Rename(tmpDst, dst)
	Expect(err).NotTo(HaveOccurred())
}

func fileChangeFn(timeout time.Duration) (common.FileActionFn, func() int) {
	eventCh := make(chan struct{}, 16)

	counterFn := func(_ chan struct{}) error {
		eventCh <- struct{}{}
		return nil
	}

	summarizerFn := func() int {
		timeoutChan := time.After(timeout)
		counter := 0

		for {
			select {
			case <-timeoutChan:
				log.Debug("summarizer is done waiting")
				return counter
			case <-eventCh:
				log.Debug("count event received by the summarizer")
				counter++
			}
		}
	}

	return counterFn, summarizerFn
}

var _ = Context("file observer", func() {
	var tmpDir string
	var foFile string

	BeforeEach(func() {
		var err error
		tmpDir, err = os.MkdirTemp("", "FileObserverTestFiles")
		Expect(err).NotTo(HaveOccurred())

		for _, fileName := range []string{"dummy1", "dummy2"} {
			contents := []byte(fileName + " lorem ipsum")
			dstPath := path.Join(tmpDir, fileName)
			err = os.WriteFile(dstPath, contents, 0600)
			Expect(err).NotTo(HaveOccurred())
		}
		foFile = path.Join(tmpDir, "target")
		contents := []byte(foFile + " - initial content")
		err = os.WriteFile(foFile, contents, 0600)
		Expect(err).NotTo(HaveOccurred())
	})

	AfterEach(func() {
		// Tmp dir is different on every run, we do not need to worry about errors
		// here.
		_ = os.RemoveAll(tmpDir)
	})

	It("runs action function on the start", func() {
		counterF, summarizerF := fileChangeFn(2 * time.Second)

		fo := common.NewFileObserver(foFile, counterF)
		Expect(fo.Run()).To(Succeed())
		defer fo.Stop()

		callsNumber := summarizerF()
		Expect(callsNumber).To(Equal(1))
	})

	It("runs action function on file change", func() {
		counterF, summarizerF := fileChangeFn(2 * time.Second)

		fo := common.NewFileObserver(foFile, counterF)
		Expect(fo.Run()).To(Succeed())
		defer fo.Stop()

		atomicFileWrite(tmpDir, "target", "foo makes bar")

		callsNumber := summarizerF()
		Expect(callsNumber).To(Equal(2))
	})

	It("ignores files that it does not observe", func() {
		counterF, summarizerF := fileChangeFn(2 * time.Second)

		fo := common.NewFileObserver(foFile, counterF)
		Expect(fo.Run()).To(Succeed())
		defer fo.Stop()

		atomicFileWrite(tmpDir, "dummy1", "foo makes bar")

		callsNumber := summarizerF()
		Expect(callsNumber).To(Equal(1))
	})

	It("inexistant directory causes FailObserver to fail", func() {
		counterF, _ := fileChangeFn(2 * time.Second)

		fo := common.NewFileObserver("/tmp/foodir/foofile", counterF)
		Expect(fo.Run()).ToNot(Succeed())
	})

	It("action function's failure during initial run causes FailObserver to fail", func() {
		runCount := 0

		counterF := func(_ chan struct{}) error {
			log.Debug("file event received by the counterf")

			runCount++
			if runCount == 1 {
				return fmt.Errorf("dummy initial error")
			}

			return nil
		}

		fo := common.NewFileObserver(foFile, counterF)
		Expect(fo.Run()).ToNot(Succeed())
	})

	It("handles action function's failure during mail loop", func() {
		runCount := 0
		eventCh := make(chan struct{}, 1024)

		counterF := func(_ chan struct{}) error {
			log.Debug("file event received by the counterf")
			eventCh <- struct{}{}

			runCount++
			if runCount == 2 {
				return fmt.Errorf("dummy main loop error")
			}

			return nil
		}

		summarizerF := func() int {
			timeoutChan := time.After(2 * time.Second)
			counter := 0

			for {
				select {
				case <-timeoutChan:
					return counter
				case <-eventCh:
					counter++
				}
			}
		}

		fo := common.NewFileObserver(foFile, counterF)
		Expect(fo.Run()).To(Succeed())
		defer fo.Stop()

		atomicFileWrite(tmpDir, "target", "foo makes bar")

		callsNumber := summarizerF()
		Expect(callsNumber).To(Equal(2))
	})

})
