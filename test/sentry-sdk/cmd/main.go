package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"

	"gitlab.com/gitlab-org/opstrace/opstrace/test/sentry-sdk/internal/harness"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/sentry-sdk/internal/recorder"
)

const errBadCommand = "expected one of execute or recorder"

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	defer func() {
		signal.Stop(c)
		cancel()
	}()

	go func() {
		select {
		case <-c:
			cancel()
		case <-ctx.Done():
		}
	}()

	executeCmd := flag.NewFlagSet("execute", flag.ExitOnError)
	recorderCmd := flag.NewFlagSet("recorder", flag.ExitOnError)

	if len(os.Args) < 2 {
		fmt.Println(errBadCommand)
		return
	}

	switch os.Args[1] {
	case "execute":
		if err := executeCmd.Parse(os.Args[2:]); err != nil {
			panic(err)
		}
		h, err := harness.NewTestHarness(ctx)
		if err != nil {
			panic(err)
		}
		if err := h.Execute(ctx); err != nil {
			panic(err)
		}
		if err := h.DownloadArtifacts(ctx); err != nil {
			panic(err)
		}
	case "recorder":
		if err := recorderCmd.Parse(os.Args[2:]); err != nil {
			panic(err)
		}
		recorder := recorder.NewRecorder(8888)
		fmt.Println("run recorder server")
		if err := recorder.Start(); err != nil {
			panic(err)
		}
	default:
		fmt.Println(errBadCommand)
		return
	}
}
