data "google_client_config" "default" {}

provider "kubernetes" {
  host                   = var.cluster_endpoint
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = var.cluster_ca_certificate
}

provider "kubernetes-alpha" {
  host                   = "https://${var.cluster_endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = var.cluster_ca_certificate
}

provider "aws" {
  region = var.aws_region
}

# Additional provider configuration for a different region; resources can
# reference this as `aws.west`.
# See https://www.terraform.io/language/providers/configuration#multiple-provider-instances for details.
provider "aws" {
  alias  = "west"
  region = "us-west-2"
}
