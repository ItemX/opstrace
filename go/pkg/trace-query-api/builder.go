package query

import (
	"fmt"
	"strings"
)

// Helper struct to help construct a sql query.
type queryBuilder struct {
	sql  string
	args []interface{}
	idx  int
}

// build takes the given sql string replaces any ? with the equivalent $<idx>
// and appends elems to the args slice.
func (s *queryBuilder) build(stmt string, elems ...interface{}) {
	// add the query params to the args slice, if any
	s.args = append(s.args, elems...)
	q := stmt
	// replace ? with corresponding $<idx>
	for range elems {
		s.idx += 1
		// placeholder that builds the string, for example, $1 when idx is 1
		p := fmt.Sprintf("$%d", s.idx)
		// replace the first ? found in the string
		q = strings.Replace(q, "?", p, 1)
	}
	// add the sanitized query statement to the current sql query
	s.sql += q
}
