/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	// K8s Go types defined here:
	// https://github.com/kubernetes/kubernetes/blob/master/pkg/apis/core/types.go
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ClickHouseAdmin defines a managed admin user to be configured in the ClickHouse instance.
// This is used to define "bootstrap" admin users that can then create other limited users.
type ClickHouseAdmin struct {
	// Name is the username of the user.
	Name string `json:"name"`

	// SecretKeyRef points to the secret value where the admin user's password is held.
	// The secret must be in the same K8s namespace as the ClickHouse instance referencing it.
	// This only applies upon user creation and later changes are not automatically synchronized.
	SecretKeyRef corev1.SecretKeySelector `json:"secretKeyRef"`
}

// ClickhouseObjectStorageBackend defines the backend for the object storage to be used by Clickhouse.
// By Default it is S3.
// +kubebuilder:validation:Enum=S3;GCS
type ClickhouseObjectStorageBackend string

// ClickHouseObjectStorage describes configuration for applicable object storage backend(S3/GCS).
type ClickHouseObjectStorage struct {
	// Backend provides the object storage, for example S3/GCS.
	// Default is S3.
	Backend ClickhouseObjectStorageBackend `json:"backend"`

	// EndpointURL is currently S3 endpoint URL in path or virtual hosted styles.
	// Endpoint URL should contain a bucket and root path to store data.
	// Note(Arun): GCS specific URLs should adhere to the same specification.
	EndpointURL string `json:"endpointURL"`

	// AccessKeyIDSecret refers to the secret for the access key ID.
	// The secret must be in the same K8s namespace as the ClickHouse instance referencing it.
	// This only applies upon user creation and later changes are not automatically synchronized.
	AccessKeyIDSecret *corev1.SecretKeySelector `json:"accessKeyIDSecret"`

	// AccessKeySecret refers to the secret for the access key.
	// The secret must be in the same K8s namespace as the ClickHouse instance referencing it.
	// This only applies upon user creation and later changes are not automatically synchronized.
	AccessKeySecret *corev1.SecretKeySelector `json:"accessKeySecret"`

	// Region is the S3/GCS region name where the bucket is located.
	// +optional
	Region *string `json:"region,omitempty"`

	// CustomerKey refers to the secret for the customer key for server side encryption.
	// +optional
	CustomerKey *corev1.SecretKeySelector `json:"customerKey,omitempty"`
}

// ClickHouseSpec defines the desired state of ClickHouse.
type ClickHouseSpec struct {
	// Image is the ClickHouse docker image to be used for nodes.
	Image string `json:"image"`

	// KeeperImage is the ClickHouse keeper image to use.
	// Optional: keeper image is by default derived from the `Image` value.
	// +optional
	KeeperImage *string `json:"keeperImage,omitempty"`

	// Replicas is the number of desired nodes to run.
	// The instance will always be deployed in distributed mode, even if replicas=1.
	// Optional to discriminate between explicit zero and not set.
	// Defaults to 1.
	// +optional
	// +kubebuilder:default=1
	Replicas *int32 `json:"replicas,omitempty"`

	// ServerResourceRequirements is the description of resource requests & limits
	// for the ClickHouse server application container.
	// +optional
	ServerResourceRequirements *corev1.ResourceRequirements `json:"resources,omitempty"`

	// StorageSize is the amount of block storage capacity to allocate per replica.
	StorageSize resource.Quantity `json:"storageSize"`

	// NodeSelector applies pod template NodeSelector to the ClickHouse StatefulSets.
	// More info: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/
	// +optional
	// +mapType=atomic
	NodeSelector map[string]string `json:"nodeSelector,omitempty"`

	// Affinity has any restrictions on replica placement in the cluster.
	// This is copied through to the nodes.
	// +optional
	Affinity *corev1.Affinity `json:"affinity,omitempty"`

	// Tolerations are optional pod template Tolerations for ClickHouse StatefulSets.
	// More info: https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/
	// +optional
	Tolerations []corev1.Toleration `json:"tolerations,omitempty"`

	// StorageClass is the storage class to use for data volumes
	// +optional
	StorageClass *string `json:"storageClass,omitempty"`

	// InternalSSL is a flag to enable ssl between server/clients of the Clickhouse deployment.
	// Defaults to false. Assumes that certmanager is installed in the cluster.
	// +optional
	// +kubebuilder:default=false
	InternalSSL *bool `json:"internalSSL,omitempty"`

	// AdminUsers contains one or more admin users (access_management=1) to create in the instance.
	// Additional non-admin users may be added via ClickHouse APIs using these credentials.
	// +optional
	AdminUsers []ClickHouseAdmin `json:"adminUsers,omitempty"`

	// ObjectStorage is the configuration for the object storage that will be used by ClickHouse.
	// +optional
	ObjectStorage *ClickHouseObjectStorage `json:"objectStorage,omitempty"`
}

// ClickHouseClusterStatus gives general state of cluster setup.
type ClickHouseClusterStatus string

// ClickHouseStatus defines the observed state of ClickHouse.
type ClickHouseStatus struct {
	// Status is general status of cluster availability.
	// +optional
	Status ClickHouseClusterStatus `json:"status,omitempty"`

	// ReadyShards is the number of StatefulSet shards that have a ready condition.
	// A Single shard is comprised of a number of replicas.
	// +optional
	ReadyShards int32 `json:"readyShards"`

	// ReadyStorage is the number of PersistentVolumeClaims which finished all
	// resizing operations and are in sync with storage defined in the CR.
	// +optional
	ReadyStorage int32 `json:"readyStorage"`

	// ReadyReplicas is the number of StatefulSet cluster replicas that have a ready condition.
	// Considered ready when the StatefulSet readyReplicas equals its spec.replicas.
	// +optional
	ReadyReplicas int32 `json:"readyReplicas"`

	// ObservedGeneration is the value of the object's Generation field when
	// controller was reconiling the object the last time
	// +optional
	ObservedGeneration int64 `json:"observedGeneration"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:printcolumn:name="Status",type=string,JSONPath=`.status.status`
//+kubebuilder:printcolumn:name="Replicas",type=integer,JSONPath=`.spec.replicas`
//+kubebuilder:printcolumn:name="Ready Replicas",type=integer,JSONPath=`.status.readyReplicas`

// ClickHouse is the Schema for the clickhouses API.
type ClickHouse struct {
	metav1.TypeMeta `json:",inline"`

	// Standard object's metadata.
	// +optional
	metav1.ObjectMeta `json:"metadata,omitempty"`

	// Spec of desired behavior.
	// +optional
	Spec ClickHouseSpec `json:"spec,omitempty"`

	// Status of the ClickHouse cluster
	// +optional
	Status ClickHouseStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// ClickHouseList contains a list of ClickHouse.
type ClickHouseList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ClickHouse `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ClickHouse{}, &ClickHouseList{})
}
