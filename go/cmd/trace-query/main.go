package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync/atomic"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/metrics"
	query "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/trace-query-api"
)

var (
	metricsAddr   = common.GetEnv("METRICS_ADDR", ":8081")
	addr          = common.GetEnv("ADDR", ":8080")
	clickHouseDsn = common.GetEnv("CLICKHOUSE_DSN", "tcp://localhost:9000/tracing")
	logLevel      = common.GetEnv("LOG_LEVEL", "debug")
)

//nolint:funlen
func main() {
	config := zap.NewProductionConfig()
	level, err := zapcore.ParseLevel(logLevel)
	if err != nil {
		panic(fmt.Errorf("failed to parse log level: %w", err))
	}
	config.Level = zap.NewAtomicLevelAt(level)
	logger, err := config.Build(zap.Development())
	if err != nil {
		panic(fmt.Errorf("failed to setup logger: %w", err))
	}
	defer func(logger *zap.Logger) {
		err := logger.Sync()
		if err != nil {
			panic(fmt.Errorf("failed to flush logger: %w", err))
		}
	}(logger)

	prometheus.MustRegister(metrics.ConfiguredCollectors()...)
	mux := http.NewServeMux()
	isReady := &atomic.Bool{}
	mux.Handle("/metrics", promhttp.Handler())
	mux.Handle("/readyz", common.ReadyHandler(isReady))

	q, err := query.NewQuerier(clickHouseDsn, nil, logger)
	if err != nil {
		logger.Fatal("failed to init query :%w", zap.Error(err))
	}
	controller := &query.Controller{
		Q: q, Logger: logger,
	}
	if config.Level.Level() >= zapcore.InfoLevel {
		gin.SetMode(gin.ReleaseMode)
	}
	routerMain := gin.Default()
	query.SetRoutes(controller, routerMain)

	metricServer := http.Server{
		Addr:              metricsAddr,
		Handler:           mux,
		ReadHeaderTimeout: time.Second * 3,
	}

	mainServer := http.Server{
		Addr:              addr,
		Handler:           routerMain,
		ReadHeaderTimeout: time.Second * 3,
	}

	go func() {
		logger.Info("starting the metrics server")
		if err := metricServer.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			logger.Fatal("failed to setup metrics server", zap.Error(err))
		}
	}()

	go func() {
		logger.Info("starting the API server")
		if err := mainServer.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			logger.Fatal("failed to setup main server", zap.Error(err))
		}
	}()

	isReady.Store(true)

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGTERM)
	<-sigChan

	isReady.Store(false)
	// This is a guess estimate on how long will it take for pod's endpoint is removed from service/LB.
	// Note: Nginx ingress controller uses endpoints (podIP:port) instead of a service, so it makes more sense
	// to continue serving the requests till the pod is kicked out.
	// See https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/#service-upstream
	// and https://github.com/kubernetes/ingress-nginx/issues/257 for more details.
	time.Sleep(time.Second * 10)

	// Ideally code path shouldn't reach this but, shutdown the server to make sure we cover cases when sleep
	// seconds isn't enough.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	err = metricServer.Shutdown(ctx)
	if err != nil {
		logger.Error("failed to shutdown the metric server", zap.Error(err))
	}
	ctx2, cancel2 := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel2()
	err = mainServer.Shutdown(ctx2)
	if err != nil {
		logger.Error("failed to shutdown the server", zap.Error(err))
	}
	err = controller.Q.Close()
	if err != nil {
		logger.Error("failed to shutdown Database connection", zap.Error(err))
	}
}
