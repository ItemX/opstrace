package constants

import (
	_ "embed"
	"encoding/json"
	"fmt"
	"time"
)

//go:embed docker-images.json
var images []byte

type ImagesFromJSON struct {
	ArgusPluginsInitContainerImage string `json:"argusPluginsInit"`
	JaegerAllInOneImage            string `json:"jaegerAllInOne"`
	JaegerClickHouseImage          string `json:"jaegerClickHouse"`
	JaegerOperatorImage            string `json:"jaegerOperator"`
	ClickHouseImage                string `json:"clickHouse"`
}

func OpstraceImages() ImagesFromJSON {
	var opstraceImages = ImagesFromJSON{}
	err := json.Unmarshal(images, &opstraceImages)
	if err != nil {
		panic(fmt.Sprintf("unable to unmarshal embedded images.json: %s", err))
	}

	return opstraceImages
}

// DockerImageTag is set by Makefiles via ldflags, needs to be exported.
// same of 2 other variables below.
var DockerImageTag string
var DockerImageRegistry string

func DockerImageName(name string) string {
	return fmt.Sprintf("%s/%s", DockerImageRegistry, name)
}

func DockerImageFullName(name string) string {
	return fmt.Sprintf("%s/%s:%s", DockerImageRegistry, name, DockerImageTag)
}

func DockerImageFullNameWithRegistry(registry, name string) string {
	return fmt.Sprintf("%s/%s:%s", registry, name, DockerImageTag)
}

// Controller.
const (
	RequeueDelay                   = time.Second
	DefaultDriftPreventionInterval = 5 * time.Minute
	SelectorLabelName              = "app"
	HTTPSCertSecretName            = "https-cert" // #nosec G101
	SelfSignedCACertName           = "self-signed-ca"
	SelfSignedCACertSecretName     = "self-signed-ca-secret" // #nosec G101
	SelfSignedCAIssuerName         = "self-signed-ca-issuer"
	TenantLabelIdentifier          = "opstrace.com/tenant"
	StorageClassName               = "pd-ssd"
	StorageInventoryID             = "storage"
	SelfSignedIssuer               = "selfsigned-issuer"
	LetsEncryptProd                = "letsencrypt-prod"
	LetsEncryptStaging             = "letsencrypt-staging"
	IngressControllerName          = "k8s.io/ingress-nginx"
	IngressClassName               = "nginx"
	// legacy placement, we should move to same namespace as cluster
	PostgresCredentialNamespace                   = "kube-system"
	PostgresCredentialName                        = "postgres-secret"
	PostgresEndpointKey                           = "endpoint"
	ClusterFieldManagerIDString                   = "scheduler-cluster"
	GroupFieldManagerIDString                     = "scheduler-group"
	GitlabNamespaceFieldManagerIDString           = "scheduler-gns"
	GitLabObservabilityTenantFieldManagerIDString = "scheduler-obs-tenant"
	TenantFieldManagerIDString                    = "scheduler-tenant"
	// #nosec
	AuthSecretOAuthClientIDKey = "gitlab_oauth_client_id"
	// #nosec
	AuthSecretOAuthClientSecretKey = "gitlab_oauth_client_secret"

	// Cloudflare external-dns auth credentials.
	// #nosec
	CloudflareAPITokenKey = "CF_API_TOKEN"
	// #nosec
	CloudflareAPIKeyKey = "CF_API_KEY"
	// #nosec
	CloudflareAPIEmailKey = "CF_API_EMAIL"
)

// Clickhouse.
const (
	ClickHouseOperatorName               = "clickhouse-operator"
	ClickHouseOperatorInventoryID        = "clickhouse-operator"
	ClickHouseOperatorServiceMonitorName = "clickhouse-operator"
	ClickHouseOperatorServiceAccountName = "clickhouse-operator"
	ClickHouseOperatorRoleBindingName    = "clickhouse-operator"

	ClickHouseImageName                      = "clickhouse-operator"
	ClickHouseOperatorCredentialsSecretName  = "clickhouse-operator-credentials" // #nosec G101
	ClickHouseOperatorUsername               = "clickhouse_operator"
	ClickHouseClusterServiceName             = "cluster"
	ClickHouseServiceMonitorName             = "clickhouse"
	ClickHouseSchedulerUsername              = "opstrace_scheduler"
	ClickHouseSchedulerCredentialsSecretName = "opstrace-scheduler-clickhouse-credentials" // #nosec G101
	ClickHouseCredentialsUserKey             = "user"
	ClickHouseCredentialsPasswordKey         = "password" // #nosec G101
	ClickHouseCredentialsHTTPEndpointKey     = "http-endpoint"
	ClickHouseCredentialsNativeEndpointKey   = "native-endpoint"
)

// Argus.
const (
	ArgusRegistryName                     = "registry.gitlab.com/gitlab-org/opstrace/opstrace-ui"
	ArgusImageName                        = "gitlab-observability-ui"
	ArgusDefaultClientTimeoutSeconds      = 5
	ArgusSecretsMountDir                  = "/etc/argus-secrets/" // #nosec G101
	ArgusConfigMapsMountDir               = "/etc/argus-configmaps/"
	ArgusConfigDashboardsSynced           = "argus.dashboards.synced"
	ArgusJsonnetBasePath                  = "/opt/jsonnet"
	ArgusDataPath                         = "/var/lib/argus"
	ArgusLogsPath                         = "/var/log/argus"
	ArgusPluginsPath                      = "/var/lib/argus/plugins"
	ArgusProvisioningPath                 = "/etc/argus/provisioning/"
	ArgusProvisioningPluginsPath          = "/etc/argus/provisioning/plugins"
	ArgusProvisioningDashboardsPath       = "/etc/argus/provisioning/dashboards"
	ArgusProvisioningNotifiersPath        = "/etc/argus/provisioning/notifiers"
	ArgusPluginsURL                       = "https://grafana.com/api/plugins/%s/versions/%s"
	ArgusServiceAccountName               = "argus"
	ArgusServiceName                      = "argus"
	ArgusDataStorageName                  = "argus-pvc"
	ArgusConfigName                       = "argus-config"
	ArgusConfigFileName                   = "grafana.ini"
	ArgusIngressName                      = "argus-ingress"
	ArgusStatefulSetName                  = "argus-sts"
	ArgusPluginsVolumeName                = "argus-plugins"
	ArgusInitContainerName                = "argus-plugins-init"
	ArgusProvisionPluginVolumeName        = "argus-provision-plugins"
	ArgusProvisionDashboardVolumeName     = "argus-provision-dashboards"
	ArgusProvisionNotifierVolumeName      = "argus-provision-notifiers"
	ArgusLogsVolumeName                   = "argus-logs"
	ArgusDataVolumeName                   = "argus-data"
	ArgusHealthEndpoint                   = "/api/health"
	ArgusPodLabel                         = "argus"
	LastConfigAnnotation                  = "last-config"
	LastConfigEnvVar                      = "LAST_CONFIG"
	ArgusAdminSecretName                  = "argus-admin-credentials"    // #nosec G101
	ArgusPostgresSecretName               = "argus-postgres-credentials" // #nosec G101
	ArgusPostgresURLKey                   = "GF_DATABASE_URL"
	DefaultAdminUser                      = "admin"
	ArgusAdminUserEnvVar                  = "GF_SECURITY_ADMIN_USER"
	ArgusAdminPasswordEnvVar              = "GF_SECURITY_ADMIN_PASSWORD" // #nosec G101
	ArgusDataPathsEnvVar                  = "GF_PATHS_DATA"
	ArgusLogsPathsEnvVar                  = "GF_PATHS_LOGS"
	ArgusPluginPathsEnvVar                = "GF_PATHS_PLUGINS"
	ArgusProvisioningPathsEnvVar          = "GF_PATHS_PROVISIONING"
	ArgusHTTPPortEnvVar                   = "GF_SERVER_HTTP_PORT"
	ArgusHTTPPort                     int = 3000
	ArgusHTTPPortName                     = "argus"
	ArgusSuccessMsg                       = "success"
	InstalledDashboardsDataKey            = "installed_dashboards.json"
	InstalledDashboardsConfigMapName      = "installed-dashboards"
)

// Jaeger.
const (
	JaegerOperatorInventoryID          = "jaeger-operator"
	JaegerOperatorName                 = "jaeger-operator"
	JaegerPluginConfigPrefix           = "jaeger-plugin-config"
	JaegerNamePrefix                   = "jaeger"
	JaegerOperatorServiceCertName      = "jaeger-operator-service-cert"
	JaegerClickhouseSecretName         = "jaeger-clickhouse-credentials" // #nosec G101
	JaegerClickhouseQuotaConfigmapName = "jaeger-clickhouse-quotas"
	JaegerDatabaseName                 = "tracing"
	JaegerGCSDatabaseName              = "tracing_gcs"
	JaegerS3DatabaseName               = "tracing_s3"
)

// Otel.
const (
	OtelImageName               = "tracing-api"
	OtelIngressPortName         = "otlp-http"
	OtelJaegerIngressPortName   = "otlp-jaeger"
	OtelJaegerIngressNamePrefix = "opentelemetry-jaeger"
	OtelDeploymentNamePrefix    = "opentelemetry"
	OtelDeploymentConfigPrefix  = "opentelemetry-config"
	OtelPodLabel                = "opentelemetry"
)

// CertManager.
const (
	CertManagerName        = "cert-manager"
	CainjectorName         = "cert-manager-cainjector"
	CertManagerInventoryID = "cert-manager"
)

// Redis.
const (
	RedisOperatorInventoryID        = "redis-operator"
	RedisOperatorName               = "redis-operator"
	RedisName                       = "redis"
	RedisOperatorServiceMonitorName = "redis"
	RedisOperatorServiceAccountName = "redis-operator"
)

// ExternalDNS.
const (
	ExternalDNSInventoryID = "external-dns"
)

// Gatekeeper.
const (
	GatekeeperInventoryID            = "gatekeeper"
	GatekeeperImageName              = "gatekeeper"
	GatekeeperName                   = "gatekeeper"
	GatekeeperOperatorName           = "gatekeeper"
	GatekeeperServiceMonitorName     = "gatekeeper"
	GatekeeperServiceAccountName     = "gatekeeper"
	GatekeeperClusterRoleBindingName = "gatekeeper-clusteradmin-binding"
	SessionCookieSecretName          = "session-cookie-secret" // #nosec G101
)

const SessionCookieName = "gob.sid"

// NginxIngress.
const (
	NginxIngressDeploymentName     = "ingress-nginx-controller"
	NginxIngressServiceName        = "ingress-nginx-controller"
	NginxIngressServiceAccountName = "ingress-nginx"
	NginxIngressServiceMonitorName = "ingress-nginx"
	NginxIngressInventoryID        = "nginx-ingress"
)

// Reloader.
const (
	ReloaderInventoryID        = "reloader"
	ReloaderServiceAccountName = "reloader-reloader"
)

// Prometheus.
const (
	PrometheusInventoryID           = "prometheus"
	PrometheusServiceAccountName    = "prometheus"
	PrometheusServiceMonitorName    = "prometheus"
	PrometheusClusterRoleBidingName = "prometheus"
	PrometheusRoleBidingName        = "prometheus"
)

// Prometheus-operator.
const (
	PrometheusOperatorName               = "prometheus-operator"
	PrometheusOperatorInventoryID        = "prometheus-operator"
	PrometheusOperatorServiceAccountName = "prometheus-operator"
	PrometheusOperatorServiceMonitorName = "prometheus-operator"
)

// TenantOperator.
const (
	TenantImageName    = "tenant-operator"
	TenantOperatorName = "tenant-operator"
	TenantName         = "opstrace-tenant"
)

// GitLabObservabilityTenant
const (
	GitLabObservabilityDatabaseName = "tracing"
	// #nosec
	GitLabObservabilityTenantCHCredentialsSecretName = "tenant-clickhouse-credentials"
	GitLabObservabilityTenantNamespaceInventoryID    = "tenant-namespace"
)

// GitLabObservabilityTenant operator
const (
	OtelCollectorNamespaceComponentName = "namespace"
	OtelCollectorComponentName          = "otel-collector"
	OtelCollectorInventoryID            = "otel-collector"
)

// Error Tracking API.
const (
	ErrortrackingAPIInventoryID  = "errortracking-api"
	ErrorTrackingOperatorName    = "errortracking-api"
	ErrorTrackingImageName       = "errortracking-api"
	ErrorTrackingAPIName         = "errortracking-api"
	ErrorTrackingAPIDatabaseName = "errortracking_api"

	// https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1783
	// https://gitlab.com/gitlab-org/gitlab/-/merge_requests/91928
	GitlabInternalErrorTrackingEndpoint = "/api/v4/internal/error_tracking/allowed"

	///nolint
	GitlabErrorTrackingTokenHeader = "Gitlab-Error-Tracking-Token"
)

// Error tracking.
const (
	ErrorTrackingateLimitingConfigMap  = "et-ratelimits-config"
	ErrorTrackingateLimitingVolumeName = "rate-limits-configuration"

	ErrorTrackingAPIQueueVolumeName       = "errortracking-queue"
	ErrorTrackingAPIQueueDataPVCName      = "errortracking-queue-data"
	ErrorTrackingAPIQueueDataMountPath    = "/etc/errortracking/queue"
	ErrorTrackingAPIQueueStorageClassName = "pd-ssd"
)

// Monitoring.
const (
	MonitoringInventoryID = "monitoring"
)

const (
	TraceQueryAPIInventoryID = "trace-query-api"
	TraceQueryAPIName        = "trace-query-api"
	TraceQueryAPIImageName   = "trace-query-api"
	TraceQueryAPIDBName      = "tracing"
)
