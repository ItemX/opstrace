package tenant

import (
	"context"
	"errors"
	"net/http"
	"time"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	"github.com/fluxcd/pkg/runtime/patch"
	"github.com/fluxcd/pkg/ssa"
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	kerrors "k8s.io/apimachinery/pkg/util/errors"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/cli-utils/pkg/kstatus/polling"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

const (
	finalizerName = "gitlabobservabilitytenant.opstrace.com/finalizer"
)

type ReconcileTenant struct {
	Client                  client.Client
	Scheme                  *runtime.Scheme
	Transport               *http.Transport
	Recorder                record.EventRecorder
	Log                     logr.Logger
	InitialManifests        map[string]map[string][]byte
	StatusPoller            *polling.StatusPoller
	DriftPreventionInterval time.Duration
	LogLevel                string
}

var _ reconcile.Reconciler = &ReconcileTenant{}

var errMigrationsNotApplied = errors.New("necessary migrations not applied yet")

// +kubebuilder:rbac:groups=opstrace.com,resources=gitlabobservabilitytenant,verbs=get;list;watch
// +kubebuilder:rbac:groups=opstrace.com,resources=gitlabobservabilitytenant/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=opstrace.com,resources=gitlabobservabilitytenant/finalizers,verbs=update

func (r *ReconcileTenant) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		WithOptions(controller.Options{MaxConcurrentReconciles: 10}).
		For(&opstracev1alpha1.GitLabObservabilityTenant{}).
		Complete(r)
}

func (r *ReconcileTenant) Reconcile(
	ctx context.Context,
	request reconcile.Request,
) (result ctrl.Result, err error) {
	tenant := &opstracev1alpha1.GitLabObservabilityTenant{}
	err = r.Client.Get(ctx, request.NamespacedName, tenant)
	if err != nil {
		if apierrors.IsNotFound(err) {
			r.Log.Info("Tenant has been removed from the API", "name", request.Name)
			return reconcile.Result{}, nil
		}
		return reconcile.Result{}, err
	}

	cr := tenant.DeepCopy()

	// Initialize the runtime patcher with the current version of the object.
	patcher := patch.NewSerialPatcher(cr, r.Client)

	// Finalize the reconciliation
	defer func() {
		// Configure the runtime patcher.
		patchOpts := []patch.Option{
			patch.WithFieldOwner(constants.GitLabObservabilityTenantFieldManagerIDString),
		}

		// Patch the object status, conditions and finalizers.
		if patchErr := patcher.Patch(ctx, cr, patchOpts...); patchErr != nil {
			if !cr.GetDeletionTimestamp().IsZero() {
				patchErr = kerrors.FilterOut(patchErr, apierrors.IsNotFound)
			}
			err = kerrors.NewAggregate([]error{err, patchErr})
		}
	}()

	// Add finalizer first if it doesn't exist to avoid the race condition
	// between init and delete.
	if !controllerutil.ContainsFinalizer(cr, finalizerName) {
		controllerutil.AddFinalizer(cr, finalizerName)
		return ctrl.Result{Requeue: true}, nil
	}

	if cr.Status.Inventory == nil {
		cr.Status.Inventory = make(map[string]*v1beta2.ResourceInventory)
	}

	// Read current state
	currentState := NewGitLabObservabilityTenantState()
	err = currentState.Read(ctx, cr, r.Client)
	if err != nil {
		r.Log.Error(err, "error reading state")
		r.manageError(cr, err)
		return reconcile.Result{}, err
	}

	// Check if the scheduler managed to apply all necessary migrations successfully, abort
	// early here and requeue for later otherwise
	var migrationsApplied bool
	clusterStatus := currentState.Cluster.Status
	if clusterStatus.LastMigrationApplied != nil {
		migrationsApplied = (*clusterStatus.LastMigrationApplied == constants.DockerImageTag)
	}
	if !migrationsApplied {
		r.Log.Error(errMigrationsNotApplied, "error checking migrations")
		r.manageError(cr, errMigrationsNotApplied)
		return reconcile.Result{RequeueAfter: r.DriftPreventionInterval}, nil
	}

	teardown := !cr.ObjectMeta.DeletionTimestamp.IsZero()
	desiredState := r.getDesiredState(teardown, cr, currentState)

	// Create the server-side apply manager.
	resourceManager := ssa.NewResourceManager(r.Client, r.StatusPoller, ssa.Owner{
		Field: constants.GitLabObservabilityTenantFieldManagerIDString,
		Group: cr.GetObjectKind().GroupVersionKind().Group,
	})

	// Run the actions to reach the desired state
	actionRunner := common.NewActionRunner(ctx, r.Client, r.Scheme, cr, resourceManager)
	err = actionRunner.RunAll(desiredState)
	if err != nil {
		r.Log.Error(err, "error executing action")
		r.manageError(cr, err)
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
	}

	if teardown {
		if controllerutil.ContainsFinalizer(cr, finalizerName) {
			// Successfully deleted everything we care about.
			// Remove our finalizer from the list and update it
			controllerutil.RemoveFinalizer(cr, finalizerName)
		}
	} else {
		r.manageSuccess(cr)
	}

	return reconcile.Result{RequeueAfter: r.DriftPreventionInterval}, nil
}

func (r *ReconcileTenant) getDesiredState(
	teardown bool,
	cr *opstracev1alpha1.GitLabObservabilityTenant,
	currentState *GitLabObservabilityTenantState,
) common.DesiredState {
	desiredState := common.DesiredState{}
	tenantID := cr.Spec.TopLevelNamespaceID

	if teardown {
		// OTEL collector
		otelcollector := NewOTELCollectorReconciler(
			r.InitialManifests[constants.OtelCollectorComponentName],
			teardown,
			tenantID,
			currentState,
		)
		desiredState = append(desiredState, otelcollector.Reconcile(cr)...)
		// CH credentials
		clickhouse := NewClickHouseReconciler(teardown, currentState, tenantID)
		desiredState = append(desiredState, clickhouse.Reconcile(cr)...)
		// Tenant namespace
		namespace := NewNamespaceReconciler(
			r.InitialManifests[constants.OtelCollectorNamespaceComponentName],
			teardown,
			tenantID,
		)
		desiredState = append(desiredState, namespace.Reconcile(cr)...)
	} else {
		// Tenant namespace
		namespace := NewNamespaceReconciler(
			r.InitialManifests[constants.OtelCollectorNamespaceComponentName],
			teardown,
			tenantID,
		)
		desiredState = append(desiredState, namespace.Reconcile(cr)...)
		// CH credentials
		clickhouse := NewClickHouseReconciler(teardown, currentState, tenantID)
		desiredState = append(desiredState, clickhouse.Reconcile(cr)...)
		// OTEL collector
		otelcollector := NewOTELCollectorReconciler(
			r.InitialManifests[constants.OtelCollectorComponentName],
			teardown,
			tenantID,
			currentState,
		)
		desiredState = append(desiredState, otelcollector.Reconcile(cr)...)
	}

	return desiredState
}

// Handle success case
func (r *ReconcileTenant) manageSuccess(tenant *opstracev1alpha1.GitLabObservabilityTenant) {
	condition := metav1.Condition{
		Status:             metav1.ConditionTrue,
		Reason:             common.ReconciliationSuccessReason,
		Message:            "All components are in ready state",
		Type:               common.ConditionTypeReady,
		ObservedGeneration: tenant.GetGeneration(),
	}
	apimeta.SetStatusCondition(&tenant.Status.Conditions, condition)
	r.Log.Info("tenant successfully reconciled", "tenant", tenant.Name)
}

// Handle error case: update tenant with error message and status
func (r *ReconcileTenant) manageError(tenant *opstracev1alpha1.GitLabObservabilityTenant, issue error) {
	condition := metav1.Condition{
		Status:             metav1.ConditionFalse,
		Reason:             common.ReconciliationFailedReason,
		Message:            issue.Error(),
		Type:               common.ConditionTypeReady,
		ObservedGeneration: tenant.GetGeneration(),
	}
	apimeta.SetStatusCondition(&tenant.Status.Conditions, condition)
}
