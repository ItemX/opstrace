package e2e

import (
	"strconv"
	"time"

	"github.com/davecgh/go-spew/spew"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
)

var _ = PDescribe("Cluster rollout", func() {
	var clickhouseCluster *clickhousev1alpha1.ClickHouse

	// test migrating a minor version of clickhouse
	// see also https://clickhouse.com/docs/en/operations/update#multiple-clickhouse-server-versions-in-a-cluster
	const startImage = "clickhouse/clickhouse-server:23.4-alpine"
	const endImage = "clickhouse/clickhouse-server:23.5-alpine"

	beforeEachCreateCluster(&clickhouseCluster, clusterConfig{
		image:        startImage,
		generateName: "e2e-rollout-test-",
	})

	Context("when updating a server image", func() {

		BeforeEach(func(ctx SpecContext) {
			assertClusterReady(ctx, clickhouseCluster)
		})

		// TODO(joe): check cluster stability during rollout.
		// Maybe see if we can keep inserting data and querying it during the rollout?
		It("rolls out one by one", MustPassRepeatedly(3), func(ctx SpecContext) {
			clickhouseCluster.Spec.Image = endImage
			Expect(k8sClient.Update(ctx, clickhouseCluster)).Should(Succeed())

			By("wait for image rollout")
			replicaReadyOrder := map[string]int64{}
			Eventually(ctx, func(g Gomega) {
				walkClusterStatefulSets(ctx, clickhouseCluster, func(s *appsv1.StatefulSet) {
					if replicaReadyOrder[s.GetLabels()["replica"]] != 0 {
						return
					}
					ready, _, err := stsReady(s)
					g.Expect(err).NotTo(HaveOccurred())
					if ready && s.Spec.Template.Spec.Containers[0].Image == endImage {
						replicaReadyOrder[s.GetLabels()["replica"]] = time.Now().Unix()
					}
				})
				g.Expect(replicaReadyOrder).To(HaveLen(int(*clickhouseCluster.Spec.Replicas)))
			}).Should(Succeed())
			By("observed rollout stats" + spew.Sdump(replicaReadyOrder))
			By("check rollout order is correct")
			// replica readiness should be in time order
			for i := 0; i < int(*clickhouseCluster.Spec.Replicas)-1; i++ {
				Expect(replicaReadyOrder[strconv.Itoa(i)]).To(
					BeNumerically("<", replicaReadyOrder[strconv.Itoa(i+1)]),
					"replica %d should have been ready before replica %d", i, i+1)
			}

			assertClusterReady(ctx, clickhouseCluster)

			By("verify all images were updated")
			Eventually(ctx, func(g Gomega) {
				walkClusterStatefulSets(ctx, clickhouseCluster, func(sts *appsv1.StatefulSet) {
					g.Expect(sts.Spec.Template.Spec.Containers[0].Image).To(Equal(endImage))
				})
			}).Should(Succeed())
		})
	})
})
