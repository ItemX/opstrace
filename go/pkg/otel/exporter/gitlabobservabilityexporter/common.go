package gitlabobservabilityexporter

import (
	"go.opentelemetry.io/collector/pdata/pcommon"
)

func attributesToMap(attributes pcommon.Map) map[string]string {
	m := make(map[string]string, attributes.Len())
	attributes.Range(func(k string, v pcommon.Value) bool {
		m[k] = v.AsString()
		return true
	})
	return m
}
