package tenant

import (
	"fmt"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/gstruct"
	"github.com/onsi/gomega/types"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	networkingv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// ResourceMatcher is used to verify resources in k8s.
// The Object can be verified to exist and likewise cleaned up.
// The gomega gstruct Fields matcher is used to verify specific fields.
type ResourceMatcher struct {
	client.Object
	fields gstruct.Fields
}

func (r ResourceMatcher) MatchResource() types.GomegaMatcher {
	// matching type should always be a pointer, as it should be a k8s Object
	return gstruct.PointTo(MatchFields(r.fields))
}

func MatchFields(fields gstruct.Fields) types.GomegaMatcher {
	return gstruct.MatchFields(gstruct.IgnoreMissing|gstruct.IgnoreExtras, fields)
}

var _ = Describe("Tenant controller", func() {
	domain := "example.com"
	var tenantCR *opstracev1alpha1.Tenant
	var defaultK8sClient client.Client
	// generic expected resources, by default these should always be present.
	var expectedResources []ResourceMatcher

	BeforeEach(func() {
		defaultK8sClient = client.NewNamespacedClient(k8sClient, "default")
	})

	JustBeforeEach(func() {
		Expect(tenantCR).ToNot(BeNil(), "setup tenant CR in your context")

		Expect(defaultK8sClient.Create(ctx, tenantCR)).To(Succeed())

		argusImage := constants.DockerImageFullNameWithRegistry(
			constants.ArgusRegistryName,
			constants.ArgusImageName,
		)
		if tenantCR.Spec.GOUI.Image != nil {
			argusImage = *tenantCR.Spec.GOUI.Image
		}

		expectedResources = []ResourceMatcher{
			{Object: &corev1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name: constants.ArgusAdminSecretName,
				},
			}},
			{Object: &corev1.Service{
				ObjectMeta: metav1.ObjectMeta{
					Name: constants.ArgusServiceName,
				},
			}},
			{Object: &corev1.ServiceAccount{
				ObjectMeta: metav1.ObjectMeta{
					Name: constants.ArgusServiceAccountName,
				},
			}},
			{
				Object: &corev1.ConfigMap{
					ObjectMeta: metav1.ObjectMeta{
						Name: constants.ArgusConfigName,
					},
				},
				fields: gstruct.Fields{
					"Data": &gstruct.KeysMatcher{
						Keys: gstruct.Keys{
							constants.ArgusConfigFileName: matchArgusInit(tenantCR),
						},
					},
				}},
			{Object: &networkingv1.Ingress{
				ObjectMeta: metav1.ObjectMeta{
					Name: constants.ArgusIngressName,
				},
			}},
			{Object: &monitoring.ServiceMonitor{
				ObjectMeta: metav1.ObjectMeta{
					Name: constants.ArgusStatefulSetName,
				},
			}},
			{
				&appsv1.StatefulSet{
					ObjectMeta: metav1.ObjectMeta{
						Name: constants.ArgusStatefulSetName,
					},
				}, gstruct.Fields{
					"Spec": MatchFields(
						gstruct.Fields{
							"Replicas": gstruct.PointTo(BeNumerically("==", 2)),
							"Template": MatchFields(
								gstruct.Fields{
									"Containers": SatisfyAll(HaveLen(1), ContainElement(MatchFields(
										gstruct.Fields{
											"Image": Equal(argusImage),
										},
									))),
								},
							),
						},
					),
				},
			},
		}
	})

	AfterEach(func() {
		Expect(defaultK8sClient.Delete(ctx, tenantCR)).To(Succeed())

		By("check all expected resources are deleted")
		Eventually(func(g Gomega) {
			for _, r := range expectedResources {
				err := defaultK8sClient.Get(ctx, client.ObjectKeyFromObject(r), r.DeepCopyObject().(client.Object))
				g.Expect(err).To(HaveOccurred())
				g.Expect(errors.IsNotFound(err)).To(BeTrue(),
					fmt.Sprintf("%s/%s should not be found", r.GetObjectKind().GroupVersionKind().Kind, r.GetName()))
			}
		}).Should(Succeed())
	})

	// general function to match desired state based on tenant CR setup
	verifyTenantResources := func() {
		By("checking all tenant resources exist")
		Eventually(func(g Gomega) {
			for _, r := range expectedResources {
				obj := r.DeepCopyObject().(client.Object)
				g.Expect(defaultK8sClient.Get(ctx, client.ObjectKeyFromObject(r), obj)).To(Succeed())
				g.Expect(obj).To(r.MatchResource())
			}
		}).Should(Succeed())
	}

	Context("Creating default tenant CR", func() {
		BeforeEach(func() {
			tenantCR = &opstracev1alpha1.Tenant{
				ObjectMeta: metav1.ObjectMeta{
					Name: "default-tenant",
				},
				Spec: opstracev1alpha1.TenantSpec{
					Domain: &domain,
				},
			}
		})

		It("creates tenant resources and waits for status to complete", func() {
			verifyTenantResources()
			// TODO: verify more about the resources
		})

		It("should not create any network policies by default", func() {
			verifyTenantResources()

			policies := &networkingv1.NetworkPolicyList{}
			Expect(defaultK8sClient.List(ctx, policies)).To(Succeed())
			Expect(policies.Items).To(BeEmpty())
		})
	})

	Context("Creating tenant CR with GOUI image override", func() {
		BeforeEach(func() {
			image := "myimage:latest"
			tenantCR = &opstracev1alpha1.Tenant{
				ObjectMeta: metav1.ObjectMeta{
					Name: "override-image",
				},
				Spec: opstracev1alpha1.TenantSpec{
					Domain: &domain,
					GOUI: opstracev1alpha1.GOUISpec{
						Image: &image,
					},
				},
			}
		})

		It("overrides the image in the Argus statefulset", func() {
			verifyTenantResources()
		})
	})

	Context("Creating tenant CR with GOUI embedding parent URL", func() {
		BeforeEach(func() {
			url := "https://gitlab.com"
			tenantCR = &opstracev1alpha1.Tenant{
				ObjectMeta: metav1.ObjectMeta{
					Name: "embedding-url",
				},
				Spec: opstracev1alpha1.TenantSpec{
					Domain: &domain,
					GOUI: opstracev1alpha1.GOUISpec{
						EmbeddingParentURL: &url,
					},
				},
			}
		})

		It("sets the embedding URL in the ini config", func() {
			verifyTenantResources()
		})
	})

	Context("Creating tenant CR with GOUI network egress rules", func() {
		BeforeEach(func() {
			port := intstr.FromInt(52)
			protocol := corev1.Protocol("TCP")
			egressRules := []networkingv1.NetworkPolicyEgressRule{
				{
					Ports: []networkingv1.NetworkPolicyPort{
						{
							Port:     &port,
							Protocol: &protocol,
						},
					},
				},
				{
					To: []networkingv1.NetworkPolicyPeer{
						{
							IPBlock: &networkingv1.IPBlock{
								CIDR: "0.0.0.0/0",
							},
						},
					},
				},
			}
			tenantCR = &opstracev1alpha1.Tenant{
				ObjectMeta: metav1.ObjectMeta{
					Name: "goui-egress-rules",
				},
				Spec: opstracev1alpha1.TenantSpec{
					Domain: &domain,
					GOUI: opstracev1alpha1.GOUISpec{
						EgressRules: egressRules,
					},
				},
			}
			expectedResources = append(expectedResources, ResourceMatcher{
				Object: &networkingv1.NetworkPolicy{
					ObjectMeta: metav1.ObjectMeta{
						Name: constants.ArgusServiceName,
					},
				},
				fields: gstruct.Fields{
					"Spec": Equal(networkingv1.NetworkPolicySpec{
						PodSelector: metav1.LabelSelector{
							MatchLabels: map[string]string{
								"app": constants.ArgusPodLabel,
							},
						},
						PolicyTypes: []networkingv1.PolicyType{
							networkingv1.PolicyTypeEgress,
						},
						Egress: egressRules,
					}),
				}})
		})

		It("creates the network policy in the same namespace", func() {
			By("check pod selector and egress rules are accurate")

			verifyTenantResources()
		})

		It("deletes the network policy if the egress rules are removed", func() {
			By("first verify networkpolicy exists")
			verifyTenantResources()

			mod := tenantCR.DeepCopy()
			mod.Spec.GOUI.EgressRules = nil
			Expect(defaultK8sClient.Patch(ctx, mod, client.MergeFrom(tenantCR))).To(Succeed())

			By("check network policy has been removed")

			Eventually(func(g Gomega) {
				obj := &networkingv1.NetworkPolicy{
					ObjectMeta: metav1.ObjectMeta{
						Name: constants.ArgusServiceName,
					},
				}
				err := defaultK8sClient.Get(ctx, client.ObjectKeyFromObject(obj), obj)
				g.Expect(err).To(HaveOccurred())
				g.Expect(errors.IsNotFound(err)).To(BeTrue(),
					fmt.Sprintf("NetworkPolicy/%s should not be found", obj.GetName()))
			}).Should(Succeed())
		})
	})
})

func matchArgusInit(cr *opstracev1alpha1.Tenant) types.GomegaMatcher {
	embeddingParentURL := ""
	if cr.Spec.GOUI.EmbeddingParentURL != nil {
		embeddingParentURL = fmt.Sprintf("\nembedding_parent_url = %s\n", *cr.Spec.GOUI.EmbeddingParentURL)
	}

	expectedSections := []string{
		`[auth]
disable_login_form = true
disable_signout_menu = true

`,
		`[auth.proxy]
auto_sign_up = false
enabled = true
header_name = X-WEBAUTH-EMAIL
header_property = email

`,
		`[paths]
data = /var/lib/argus
logs = /var/log/argus
plugins = /var/lib/argus/plugins
provisioning = /etc/argus/provisioning/

`,
		`[security]
allow_embedding = true
content_security_policy = true
cookie_secure = true` + embeddingParentURL + "\n",
		`[server]
domain = example.com
root_url = https://%(domain)s/` + cr.Namespace + `/
serve_from_sub_path = true

`,
		`[users]
allow_org_create = false
auto_assign_org = true
auto_assign_org_id = 1

`,
	}

	ms := make([]types.GomegaMatcher, len(expectedSections))
	for i, e := range expectedSections {
		ms[i] = ContainSubstring(e)
	}

	return SatisfyAll(ms...)
}
