package datasource_test

import (
	"context"
	"path/filepath"
	"testing"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	uberzap "go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	corev1 "k8s.io/api/core/v1"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/envtest"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	tenantapiv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
)

var (
	k8sClient  client.Client
	testEnv    *envtest.Environment
	restConfig *rest.Config

	ctx     context.Context
	cancelF context.CancelFunc

	logger *uberzap.SugaredLogger
)

const testGroupID = 22

func TestDatasourceController(t *testing.T) {
	RegisterFailHandler(Fail)
	suiteConfig, reporterConfig := GinkgoConfiguration()
	SetDefaultEventuallyTimeout(30 * time.Second)
	SetDefaultEventuallyPollingInterval(2 * time.Second)

	RunSpecs(t, "Datasource Test Suite", suiteConfig, reporterConfig)
}

var _ = BeforeSuite(func() {
	var err error

	setTimeEncoderOpt := func(o *zap.Options) {
		o.TimeEncoder = zapcore.RFC3339TimeEncoder
	}

	logf.SetLogger(
		zap.New(
			zap.WriteTo(GinkgoWriter),
			zap.UseDevMode(true),
			setTimeEncoderOpt,
		),
	)
	logger = zap.NewRaw(
		zap.WriteTo(GinkgoWriter),
		zap.UseDevMode(true),
		setTimeEncoderOpt,
	).Sugar()

	ctx, cancelF = context.WithCancel(context.TODO())

	By("bootstrapping test environment")
	testEnv = &envtest.Environment{
		CRDDirectoryPaths:     []string{filepath.Join("../../", "config", "crd", "bases")},
		ErrorIfCRDPathMissing: true,
	}
	restConfig, err = testEnv.Start()
	Expect(err).NotTo(HaveOccurred())
	Expect(restConfig).NotTo(BeNil())

	By("setting up k8s client")
	Expect(scheme.AddToScheme(scheme.Scheme)).To(Succeed())
	Expect(tenantapiv1alpha1.AddToScheme(scheme.Scheme)).To(Succeed())
	k8sClient, err = client.New(restConfig, client.Options{Scheme: scheme.Scheme})
	Expect(err).NotTo(HaveOccurred())
	Expect(k8sClient).NotTo(BeNil())

})

var _ = AfterSuite(func() {
	By("tearing down the test environment")
	Expect(testEnv.Stop()).To(Succeed())

	cancelF()
})

func createTenant(isReady bool, argusURL, namespace string) *tenantapiv1alpha1.Tenant {
	GinkgoHelper()

	res := &tenantapiv1alpha1.Tenant{
		ObjectMeta: metav1.ObjectMeta{
			Name:       constants.TenantName,
			Namespace:  namespace,
			Generation: 1,
		},
	}
	Expect(k8sClient.Create(ctx, res)).To(Succeed())

	var condition metav1.Condition
	if isReady {
		condition = metav1.Condition{
			Status:             metav1.ConditionTrue,
			Reason:             common.ReconciliationSuccessReason,
			Message:            "All components are in ready state",
			Type:               common.ConditionTypeReady,
			ObservedGeneration: 1,
		}
	} else {
		condition = metav1.Condition{
			Status:             metav1.ConditionFalse,
			Reason:             common.ReconciliationFailedReason,
			Message:            "foo error prevents tenant from getting ready",
			Type:               common.ConditionTypeReady,
			ObservedGeneration: 1,
		}
	}
	apimeta.SetStatusCondition(&res.Status.Conditions, condition)
	if argusURL != "" {
		res.Status.Argus = tenantapiv1alpha1.ArgusStatus{
			URL: &argusURL,
		}
	}
	Expect(k8sClient.Status().Update(ctx, res)).To(Succeed())

	return res
}

func createNs(name string) *corev1.Namespace {
	res := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
	}

	Expect(k8sClient.Create(ctx, res)).To(Succeed())
	return res
}

func createPlainDS(name, namespace string) *tenantapiv1alpha1.DataSource {
	GinkgoHelper()

	res := &tenantapiv1alpha1.DataSource{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: tenantapiv1alpha1.DataSourceSpec{
			Name: "jaeger.yaml",
			Datasources: []tenantapiv1alpha1.DataSourceFields{
				{
					Name: "Tracing",
					Type: "jaeger",
					Url:  "example.com/v1/jaeger/2",
					// Make this the default datasource that is
					// loaded automatically on the explore view
					IsDefault:       true,
					Access:          "proxy",
					Editable:        false,
					Version:         1,
					GroupId:         testGroupID,
					WithCredentials: false,
					JsonData: &tenantapiv1alpha1.DataSourceJsonData{
						KeepCookies: []string{
							constants.SessionCookieName,
						},
					},
				},
			},
		},
	}
	Expect(k8sClient.Create(ctx, res)).To(Succeed())
	return res
}
