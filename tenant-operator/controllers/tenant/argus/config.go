package argus

import (
	"fmt"

	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/config"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// Default config for Argus.
func NewArgusDefaultConfig(cr *v1alpha1.Tenant) v1alpha1.ArgusConfig {
	yes := true
	no := false

	embeddingParentURL := ""
	if cr.Spec.GOUI.EmbeddingParentURL != nil {
		embeddingParentURL = *cr.Spec.GOUI.EmbeddingParentURL
	}

	return v1alpha1.ArgusConfig{
		Users: &v1alpha1.ArgusConfigUsers{
			DefaultTheme:   "light",
			AllowOrgCreate: &no,
			// Argus will fallback to creating an org with the user's email/login
			// if we don't specify the org to auto assign when creating new users.
			// This adds all new users to the root namespace org by default, and then
			// they will also have other memberships to other orgs within argus that map
			// to their membership to gitlab namespaces within the root namespace.
			AutoAssignOrg: &yes,
			// Set to 1 which is the Argus "Main Org." and it always exists.
			// Setting to something other than 1 can cause a problem when Argus starts up
			// because it can't add the admin user to the org because the org doesn't yet
			// exist: service init failed: failed to create admin user: could not create user: organization ID XX does not exist
			AutoAssignOrgId: "1",
		},
		Auth: &v1alpha1.ArgusConfigAuth{
			DisableLoginForm:   &yes,
			DisableSignoutMenu: &yes,
		},
		AuthProxy: &v1alpha1.ArgusConfigAuthProxy{
			Enabled:        &yes,
			HeaderName:     "X-WEBAUTH-EMAIL",
			HeaderProperty: "email",
			// Don't allow autosignup since we orchestrate signup in gatekeeper
			AutoSignUp: &no,
		},
		Security: &v1alpha1.ArgusConfigSecurity{
			AllowEmbedding:        &yes,
			EmbeddingParentURL:    embeddingParentURL,
			CookieSecure:          &yes,
			ContentSecurityPolicy: &yes,
		},
		Server: &v1alpha1.ArgusConfigServer{
			Domain: *cr.Spec.Domain,
			// Note(joe): Specifying https here as the ingress will always be served over https.
			// The standard config template "%%(protocol)s://%%(domain)s" will result in http://...
			// We keep the protocol as http as we're not serving over https internally.
			RootUrl:          fmt.Sprintf("https://%%(domain)s/%s/", cr.Namespace),
			ServeFromSubPath: &yes,
		},
	}
}

// Provide default configuration for Argus and then merge in the overrides
// from the CR
func getArgusConfig(cr *v1alpha1.Tenant) (*v1alpha1.ArgusConfig, error) {
	defaults := NewArgusDefaultConfig(cr)
	err := utils.PatchObject(&defaults, &cr.Spec.Overrides.Argus.Config)

	return &defaults, err
}

func Config(cr *v1alpha1.Tenant) (*v1.ConfigMap, error) {
	configMap := &v1.ConfigMap{}
	configMap.ObjectMeta = metav1.ObjectMeta{
		Name:      constants.ArgusConfigName,
		Namespace: cr.Namespace,
	}

	defaultsWithOverrides, err := getArgusConfig(cr)
	if err != nil {
		return configMap, err
	}
	ini := config.NewGrafanaIni(defaultsWithOverrides)
	conf, hash, err := ini.Write()
	if err != nil {
		return configMap, err
	}

	// Store the hash of the current configuration for later
	// comparisons
	configMap.Annotations = map[string]string{
		constants.LastConfigAnnotation: hash,
	}

	data := map[string]string{
		constants.ArgusConfigFileName: conf,
	}
	configMap.Data = data

	return configMap, nil
}

func ConfigMutator(cr *v1alpha1.Tenant, current *v1.ConfigMap) error {
	defaultsWithOverrides, err := getArgusConfig(cr)
	if err != nil {
		return err
	}
	ini := config.NewGrafanaIni(defaultsWithOverrides)
	conf, hash, err := ini.Write()
	if err != nil {
		return err
	}
	current.Annotations = map[string]string{
		constants.LastConfigAnnotation: hash,
	}
	data := map[string]string{
		constants.ArgusConfigFileName: conf,
	}
	current.Data = data

	return nil
}

func ConfigSelector(cr *v1alpha1.Tenant) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      constants.ArgusConfigName,
	}
}
