package v1alpha1

import (
	"fmt"
	"strings"

	cmacme "github.com/cert-manager/cert-manager/pkg/apis/acme/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"sigs.k8s.io/kustomize/api/types"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// ClusterSpec defines the desired state of Cluster.
type ClusterSpec struct {
	// Optional namespace for deploying cluster resources (tenants will still have separate namespaces).
	// Defaults to the default namespace
	// +optional
	Namespace string `json:"namespace"`
	// Target environment (aws, gcp, kind)
	Target common.EnvironmentTarget `json:"target"`
	// GitLab instance integration config
	GitLab GitLabSpec `json:"gitlab"`
	// Cluster-wide image pull secrets (NOTE: Not plumbed to all components yet)
	ImagePullSecrets []v1.LocalObjectReference `json:"imagePullSecrets,omitempty"`
	// DNS configuration
	// +optional
	DNS DNSSpec `json:"dns"`
	// Gitlab Observability UI configuration
	GOUI GOUISpec `json:"goui"`
	// Set overrides for components and config
	// +optional
	Overrides ClusterOverridesSpec `json:"overrides"`
	// Setup features for cluster and its resources
	// +optional
	Features map[string]string `json:"features,omitempty"`
}

type GOUISpec struct {
	// Image path for which image to install for Observability UI.
	// This value eventually gets plumbed down into each Tenant object when we
	// create one using which we spin up a corresponding instance of the UI.
	// Should there be need to override the image specific to a tenant, it
	// should be done as an override on the corresponding deployment instead.
	Image *string `json:"image,omitempty"`
}

type DNSSpec struct {
	// Certificate Issuer (letsencrypt-prod or letsencrypt-staging)
	CertificateIssuer string `json:"certificateIssuer,omitempty"`
	// Cluster domain e.g. observe.gitlab.com
	Domain *string `json:"domain,omitempty"`
	// Used to configure a DNS01 challenge provider to be used when solving DNS01
	// challenges.
	// Only one DNS provider may be configured per solver.
	DNS01Challenge cmacme.ACMEChallengeSolverDNS01 `json:"dns01Challenge"`
	// ACMEEmail is the email address to be associated with the ACME account.
	// It will be used to contact you in case of issues with your account or certificates,
	// including expiry notification emails. This field may be updated after the account is
	// initially registered
	ACMEEmail string `json:"acmeEmail"`
	// ACMEServer is the URL used to access the ACME server's 'directory' endpoint.
	// For example, for Let's Encrypt's staging endpoint,
	// you would use: "https://acme-staging-v02.api.letsencrypt.org/directory".
	// Only ACME v2 endpoints (i.e. RFC 8555) are supported.
	ACMEserver *string `json:"acmeServer,omitempty"`
	// ExternalDNS provider config
	// +optional
	ExternalDNSProvider ExternalDNSProviderSpec `json:"externalDNSProvider"`
	// GCPCertManagerServiceAccount is the service account for managing DNS on GCP.
	// This field is only relevant if the Domain specified is hosted in GCP.
	// +optional
	GCPCertManagerServiceAccount *string `json:"gcpCertManagerServiceAccount,omitempty"`
	// If specified and supported by the platform, this will restrict traffic through
	// the cloud-provider load-balancer will be restricted to the specified client IPs.
	// This field will be ignored if the cloud-provider does not support the feature."
	// More info: https://kubernetes.io/docs/tasks/access-application-cluster/configure-cloud-provider-firewall/
	// +optional
	FirewallSourceIPsAllowed []string `json:"firewallSourceIPsAllowed"`
}

type ExternalDNSProviderSpec struct {
	// +optional
	GCP *ExternalDNSGCPSpec `json:"gcp,omitempty"`
	// +optional
	Cloudflare *ExternalDNSCloudflareSpec `json:"cloudflare,omitempty"`
	// We can support more here... (see https://github.com/kubernetes-sigs/external-dns#the-latest-release)
}

type ExternalDNSGCPSpec struct {
	// Service account name in GCP IAM to use for managing dns zone
	DNSServiceAccountName string `json:"dnsServiceAccountName"`

	// ManagedZoneName is the name of the managed zone to use for DNS records.
	// If not specified, the zone name will be derived from the cluster domain.
	// +optional
	ManagedZoneName *string `json:"managedZoneName,omitempty"`
}

type ExternalDNSCloudflareSpec struct {
	// Reference to a Secret object containing auth credentials for your Cloudflare DNS setup
	// (see: https://github.com/kubernetes-sigs/external-dns/blob/master/docs/tutorials/cloudflare.md#creating-cloudflare-credentials)
	CFAuthSecret v1.LocalObjectReference `json:"cloudflareSecret"`
	// Zone ID to restrict management to a specific zone
	// +optional
	ZoneID *string `json:"zoneId,omitempty"`
}

type GitLabSpec struct {
	InstanceURL string                  `json:"instanceUrl"`
	AuthSecret  v1.LocalObjectReference `json:"authSecret"`
}

// Trims trailing slash from InstanceURL
func (g *GitLabSpec) TrimInstanceURL() string {
	if strings.HasSuffix(g.InstanceURL, "/") {
		return g.InstanceURL[:len(g.InstanceURL)-1]
	}
	return g.InstanceURL
}

type ClusterOverridesSpec struct {
	// +optional
	ClickHouseOperator *KustomizeOverridesSpec `json:"clickhouseOperator"`
	// +optional
	ClickHouse ClickHouseOverridesSpec `json:"clickhouse"`
	// +optional
	JaegerOperator *KustomizeOverridesSpec `json:"jaegerOperator"`
	// +optional
	CertManager *KustomizeOverridesSpec `json:"certManager"`
	// +optional
	PrometheusOperator *KustomizeOverridesSpec `json:"prometheusOperator"`
	// +optional
	Prometheus *KustomizeOverridesSpec `json:"prometheus"`
	// +optional
	RedisOperator *KustomizeOverridesSpec `json:"redisOperator"`
	// +optional
	Redis *KustomizeOverridesSpec `json:"redis"`
	// +optional
	ExternalDNS *KustomizeOverridesSpec `json:"externalDNS"`
	// +optional
	Gatekeeper *KustomizeOverridesSpec `json:"gatekeeper"`
	// +optional
	NginxIngress *KustomizeOverridesSpec `json:"nginxIngress"`
	// +optional
	ErrorTrackingAPI *KustomizeOverridesSpec `json:"errorTrackingAPI"`
	// +optional
	Reloader *KustomizeOverridesSpec `json:"reloader"`
	// +optional
	Monitoring *KustomizeOverridesSpec `json:"monitoring"`
	// +optional
	Storage *KustomizeOverridesSpec `json:"storage"`
	// +optional
	TraceQueryAPI *KustomizeOverridesSpec `json:"traceQueryAPI"`
}

type GeneralOverridesSpec struct {
	// +optional
	Components ComponentsSpec `json:"components"`
}

type KustomizeOverridesSpec struct {
	// CommonLabels to add to all objects and selectors.
	CommonLabels map[string]string `json:"commonLabels,omitempty" yaml:"commonLabels,omitempty"`

	// Labels to add to all objects but not selectors. Follows schema of
	// sigs.k8s.io/kustomize/api/types/labels.Label
	Labels []types.Label `json:"labels,omitempty" yaml:"labels,omitempty"`

	// CommonAnnotations to add to all objects.
	CommonAnnotations map[string]string `json:"commonAnnotations,omitempty" yaml:"commonAnnotations,omitempty"`

	// PatchesStrategicMerge specifies the relative path to a file
	// containing a strategic merge patch.  Format documented at
	// https://github.com/kubernetes/community/blob/master/contributors/devel/sig-api-machinery/strategic-merge-patch.md
	// URLs and globs are not supported.
	PatchesStrategicMerge []types.PatchStrategicMerge `json:"patchesStrategicMerge,omitempty" yaml:"patchesStrategicMerge,omitempty"`

	// JSONPatches is a list of JSONPatch for applying JSON patch.
	// Format documented at https://tools.ietf.org/html/rfc6902
	// and http://jsonpatch.com
	PatchesJson6902 []types.Patch `json:"patchesJson6902,omitempty" yaml:"patchesJson6902,omitempty"`

	// Images is a list of (image name, new name, new tag or digest)
	// for changing image names, tags or digests. This can also be achieved with a
	// patch, but this operator is simpler to specify.
	Images []types.Image `json:"images,omitempty" yaml:"images,omitempty"`

	// Replicas is a list of {resourcename, count} that allows for simpler replica
	// specification. This can also be done with a patch.
	Replicas []types.Replica `json:"replicas,omitempty" yaml:"replicas,omitempty"`
}

type ClickHouseOverridesSpec struct {
	// +optional
	Components ClickHouseComponentsSpec `json:"components,omitempty"`
}

type ClickHouseComponentsSpec struct {
	// +optional
	Service Service `json:"service,omitempty"`
	// +optional
	Deployment ClickHouseCRSpec `json:"deployment,omitempty"`
	// +optional
	ServiceMonitor ServiceMonitor `json:"serviceMonitor,omitempty"`
}

type ClickHouseCRSpec struct {
	// +optional
	Labels map[string]string `json:"labels,omitempty"`
	// +optional
	Annotations map[string]string `json:"annotations,omitempty"`
	// This spec is actually a copy of the one provided in Clickhouse operator with changes made for optional fields.
	// +optional
	Spec *ClickHouseSpec `json:"spec,omitempty"`
}

// ClusterStatus defines the observed state of Cluster.
type ClusterStatus struct {
	Conditions []metav1.Condition `json:"conditions,omitempty"`

	// Inventory contains the list of Kubernetes resource object references that have been successfully applied.
	// +optional
	Inventory map[string]*v1beta2.ResourceInventory `json:"inventory,omitempty"`

	// LastMigrationApplied refers to the version of the migrations last applied successfully
	// +optional
	LastMigrationApplied *string `json:"lastMigrationApplied,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:scope="Cluster"
// Cluster is the Schema for the Clusters API.
type Cluster struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ClusterSpec   `json:"spec,omitempty"`
	Status ClusterStatus `json:"status,omitempty"`
}

// Cluster is a cluster-scoped resource so this method returns
// the namespace for cluster resources to be deployed to.
func (c *Cluster) Namespace() string {
	n := c.Spec.Namespace
	if n == "" {
		return "default"
	}
	return n
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// ClusterList contains a list of Cluster.
type ClusterList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Cluster `json:"items"`
}

func (t *ClusterSpec) GetHost() string {
	if t.DNS.Domain != nil {
		return *t.DNS.Domain
	}
	return "localhost"
}

func (t *ClusterSpec) GetHostURL() string {
	if t.DNS.Domain != nil {
		return fmt.Sprintf("https://%s", *t.DNS.Domain)
	}
	return "http://localhost"
}

// check if we should configure externalDNS.
func (t *ClusterSpec) UseExternalDNS() bool {
	return t.Target != common.KIND
}

// get GOUI-specific configuration.
func (t *ClusterSpec) GetGOUISpec() GOUISpec {
	return t.GOUI
}

func init() {
	SchemeBuilder.Register(&Cluster{}, &ClusterList{})
}
