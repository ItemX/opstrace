# Development environment

The reference build environment is the "CI container" as defined in `containers/ci`.
A native host environment probably deviates from this reference environment in various minor or major ways.
This document attempts to help setting up a local development environment that resembles that of the CI container.

Overview of requirements:

* Terraform 1.2+
* Go 1.20+
* Docker
* GCP and/or AWS CLIs (`gcloud`/`aws`)
* `git`
* `golangci-lint`
* `markdownlint`
* Build tooling essentials such as `make` and `g++` (among others, for building binary extensions to some npm packages like `snappy`)

The sections below hopefully help with setting up some of these!

## Golang and dependencies

We are currently using Golang 1.20.

We use [golangci-lint](https://golangci-lint.run) to lint the Go codebase.
You'll need to [install](https://golangci-lint.run/usage/install/#local-installation) it and have it available in your PATH to run the pre-commit hooks locally.

You may need to set `GOPRIVATE` to be able to build the project in Visual Studio Code.
Also, you may want to use the following   the following in your `preferences.json` to configure the Go plugin.

```json
"go.toolsEnvVars": {
  "GOPRIVATE": "github.com/opstrace",
},
```

## Required commit sign-off

[Git commit sign-off](https://git-scm.com/docs/git-commit#Documentation/git-commit.txt---signoff) is required for all commits.

GitLab Observability is following the [Developer Certificate of Origin](https://developercertificate.org) process instead of requiring a CLA in order to reduce friction toward contributions. All git commits must be have an explicit sign-off asserting the contents of the DCO to pass our CI checks.
