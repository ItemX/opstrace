package tenant

import (
	"fmt"
	"net/url"
	"strings"

	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
)

type ClickHouseReconciler struct {
	Teardown bool
	Log      logr.Logger
	State    *GitLabObservabilityTenantState
}

func NewClickHouseReconciler(
	teardown bool,
	state *GitLabObservabilityTenantState,
	tenantID int64,
) *ClickHouseReconciler {
	return &ClickHouseReconciler{
		Teardown: teardown,
		Log: logf.Log.WithName(
			fmt.Sprintf("tenantmanifests/%s-%d", "clickhouse", tenantID),
		),
		State: state,
	}
}

func (i *ClickHouseReconciler) Reconcile(
	cr *opstracev1alpha1.GitLabObservabilityTenant,
) common.DesiredState {
	actions := []common.Action{}

	user := fmt.Sprintf("tenant_%d", cr.Spec.TopLevelNamespaceID)
	databaseName := constants.GitLabObservabilityDatabaseName

	if i.Teardown {
		// teardown CH user
		actions = append(actions,
			common.ClickHouseAction{
				Msg:      fmt.Sprintf("clickhouse SQL drop user %s if exists", user),
				SQL:      fmt.Sprintf("DROP USER IF EXISTS %s ON CLUSTER '{cluster}'", user),
				URL:      i.State.getSchedulerCHCredentials().Native,
				Database: databaseName,
			},
		)
		// teardown secret
		if i.State.TenantCHCredentials != nil {
			actions = append(actions,
				common.GenericDeleteAction{
					Ref: i.State.TenantCHCredentials,
					Msg: "clickhouse credentials",
				},
			)
		}
		return actions
	}

	if i.State.TenantCHCredentials == nil {
		// we've likely not provisioned tenant-specific CH credentials yet, lets
		credentialsData := getCredentialsData(cr)
		secret := &v1.Secret{}
		secret.ObjectMeta = metav1.ObjectMeta{
			Name:      constants.GitLabObservabilityTenantCHCredentialsSecretName,
			Namespace: cr.Namespace(),
		}
		secret.Data = credentialsData
		// provision secret
		actions = append(actions,
			common.GenericCreateOrUpdateAction{
				Ref: secret,
				Msg: "tenant clickhouse credentials",
				Mutator: func() error {
					return nil // don't mutate ever
				},
			},
		)
		// provision CH user & their grants
		actions = append(actions, ClickHouseUsersProvision(
			user,
			string(credentialsData[constants.ClickHouseCredentialsPasswordKey]),
			[]string{"SELECT", "INSERT", "ALTER", "CREATE"},
			databaseName,
			i.State.getSchedulerCHCredentials().Native,
		)...)
	}

	return actions
}

func getCredentialsData(
	cr *v1alpha1.GitLabObservabilityTenant,
) map[string][]byte {
	user := fmt.Sprintf("tenant_%d", cr.Spec.TopLevelNamespaceID)
	pass := common.RandStringRunesRaw(10)
	hostname := fmt.Sprintf("%s.%s.svc.cluster.local", constants.ClickHouseClusterServiceName, "default")
	nativeEndpoint := url.URL{
		Host:   fmt.Sprintf("%s:9000", hostname),
		User:   url.UserPassword(user, pass),
		Scheme: "tcp",
	}
	httpEndpoint := url.URL{
		Host:   fmt.Sprintf("%s:8123", hostname),
		User:   url.UserPassword(user, pass),
		Scheme: "http",
	}

	return map[string][]byte{
		constants.ClickHouseCredentialsHTTPEndpointKey:   []byte(httpEndpoint.String()),
		constants.ClickHouseCredentialsNativeEndpointKey: []byte(nativeEndpoint.String()),
		constants.ClickHouseCredentialsPasswordKey:       []byte(pass),
	}
}

func ClickHouseUsersProvision(
	username string,
	password string,
	grants []string,
	databaseName string,
	chEndpoint url.URL,
) []common.Action {
	actions := []common.Action{}

	actions = append(actions,
		common.ClickHouseAction{
			Msg: fmt.Sprintf("clickhouse SQL create user %s if not exists with password [...]", username),
			//nolint:lll
			SQL: fmt.Sprintf("CREATE USER IF NOT EXISTS %s ON CLUSTER '{cluster}' IDENTIFIED WITH plaintext_password BY '%s' HOST ANY DEFAULT DATABASE default",
				username,
				password,
			),
			URL:      chEndpoint,
			Database: "default",
		},
		// REMOTE source privilege required for Distributed tables
		// This can't be specified on the database level
		common.ClickHouseAction{
			Msg: fmt.Sprintf("clickhouse SQL grant remote to user %s", username),
			SQL: fmt.Sprintf("GRANT ON CLUSTER '{cluster}' REMOTE ON *.* TO %s",
				username,
			),
			URL:      chEndpoint,
			Database: "default",
		},
	)

	// add grants as requested
	if len(grants) > 0 {
		grantsStr := strings.Join(grants, ",")
		actions = append(actions,
			common.ClickHouseAction{
				Msg: fmt.Sprintf("clickhouse SQL grant permissions to user %s", username),
				SQL: fmt.Sprintf("GRANT ON CLUSTER '{cluster}' %s ON %s.* TO %s",
					grantsStr,
					databaseName,
					username,
				),
				URL:      chEndpoint,
				Database: "default",
			},
		)
	}

	return actions
}
