package metrics

import (
	"time"

	"github.com/gin-gonic/gin"
)

// InstrumentHandlerFuncWithOpts is similar to PrometheusMiddleware but returns a gin specific middleware.
// This is only to be used as a middleware and uses the same metrics in use by its http.Handler counterpart.
func InstrumentHandlerFuncWithOpts(strip StripFunc, addMetrics AdditionalMetricsFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		now := time.Now()

		r := c.Request

		c.Next()

		elapsed := float64(time.Since(now)) / float64(time.Second)

		path := strip(r.URL.Path)
		method := sanitizeMethod(r.Method)
		code := sanitizeCode(c.Writer.Status())

		requestCounter.WithLabelValues(code, method, path).Inc()
		latencyHist.WithLabelValues(code, method, path).Observe(elapsed)
		responseSize.WithLabelValues(code, method, path).Observe(float64(c.Writer.Size()))

		addMetrics(r, code, method, path)
	}
}
