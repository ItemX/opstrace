package infra

import (
	"context"
	"fmt"

	gocommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/gcp"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/kind"
	"k8s.io/client-go/rest"
)

type ConfigurationBuilder interface {
	// LoadConfiguration sets up config and returns standard configuration settings.
	LoadConfiguration() (common.Configuration, error)
	// ConfigureCluster configures the cluster with infra specific overrides
	ConfigureCluster(*schedulerv1alpha1.Cluster) error

	// InternalConfiguration returns the internal configuration for the provider.
	// used for debugging purposes, e.g. TF_VARs for GCP.
	InternalConfiguration() (map[string]string, error)
}

type GitLabInstanceBuilder interface {
	CreateGitLabInstance() error
	DestroyGitLabInstance() error
}

type K8sClusterBuilder interface {
	CreateK8sCluster() error
	DestroyK8sCluster() error
	// GetRestConfig returns the rest config for the cluster.
	// This is used for creating clients.
	GetRestConfig(ctx context.Context) (*rest.Config, error)
}

type InfrastructureBuilder interface {
	ConfigurationBuilder
	GitLabInstanceBuilder
	K8sClusterBuilder
	Close() error
}

// NewInfrastructure returns a new InfrastructureBuilder based on the provider.
// The identifier is used as a unique reference for any created resources.
func NewInfrastructure(provider gocommon.EnvironmentTarget, identifier string) (InfrastructureBuilder, error) {
	var builder InfrastructureBuilder
	switch provider {
	case gocommon.KIND:
		builder = kind.NewKind()
	case gocommon.GCP:
		builder = gcp.NewGCP(identifier)
	default:
		return nil, fmt.Errorf("unknown provider: %s", provider)
	}
	return builder, nil
}
