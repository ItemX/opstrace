package main

import (
	"context"
	"fmt"
	"net/http"
	"os/signal"
	"sync/atomic"
	"syscall"

	stdErrors "errors"
	"net/http/pprof"
	"net/url"
	"os"
	"strconv"
	"time"

	"github.com/go-openapi/loads"
	"github.com/go-redis/redis_rate/v10"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"

	_ "go.uber.org/automaxprocs"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors_v2"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/events"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/messages"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/projects"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/persistentqueue"
)

const (
	ErrorTrackingQueueName    string = "errortracking"
	ErrortrackingQueueDirPath string = "/etc/errortracking/queue"
)

var (
	port                   = getEnvInt("PORT", 8080)
	metricsPort            = getEnvInt("METRICS_PORT", 8081)
	clickHouseDsn          = getEnv("CLICKHOUSE_DSN", "tcp://localhost:9000")
	apiBaseURL             = getEnv("API_BASE_URL", "")
	clickhouseMaxOpenConns = getEnvInt("CLICKHOUSE_MAX_OPEN_CONNS", 100)
	clickhouseMaxIdleConns = getEnvInt("CLICKHOUSE_MAX_IDLE_CONNS", 20)
	// TODO: Adjust error tracking API deployment to override log level from cluster spec.
	logLevel = getEnv("LOG_LEVEL", "debug")

	gatekeeperURL = getEnv("GATEKEEPER_URL", "https://gatekeeper.svc.cluster.local")

	debugEnabled               = getEnvBool("DEBUG_ENABLED", false)
	useRemoteStorage           = getEnv("USE_REMOTE_STORAGE", "")
	dbCompressionEnabled       = getEnvBool("DB_USE_COMPRESSION", false)
	clientSideBufferingEnabled = getEnvBool("CLIENT_SIDE_BUFFERING_ENABLED", false)
	maxBatchSize               = getEnvInt("MAX_BATCH_SIZE", 100)
	maxProcessorCount          = getEnvInt("MAX_PROCESSOR_COUNT", 1)

	// redis addr is typically sentinel address (USE_REDIS_SENTINEL).
	redisAddr     = getEnv("REDIS_ADDRESS", "127.0.0.1:6379")
	redisPassword = getEnv("REDIS_PASSWORD", "")
	// redis HA is the default via sentinel client.
	useRedisSentinel        = getEnvBool("USE_REDIS_SENTINEL", true)
	redisConnectionPoolSize = getEnvInt("REDIS_CONNECTION_POOL_SIZE", 100)
	rateLimitingConfigFile  = getEnv("RATE_LIMITING_CONFIG_FILE", "")
)

func NewDebugServer(addr string) *http.Server {
	mux := http.NewServeMux()
	mux.HandleFunc("/debug/pprof/", pprof.Index)
	mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	mux.HandleFunc("/debug/pprof/trace", pprof.Trace)

	return &http.Server{
		Addr:              addr,
		Handler:           mux,
		ReadHeaderTimeout: 3 * time.Second,
	}
}

func getEnvBool(key string, fallback bool) bool {
	if _, ok := os.LookupEnv(key); ok {
		if v, err := strconv.ParseBool(os.Getenv(key)); err == nil {
			return v
		}
	}
	return fallback
}

func getEnvInt(key string, fallback int) int {
	s := os.Getenv(key)
	if len(s) == 0 {
		return fallback
	}

	v, err := strconv.Atoi(s)
	if err != nil {
		return fallback
	}

	return v
}

func getEnv(key string, fallback string) string {
	s := os.Getenv(key)
	if s == "" {
		return fallback
	}
	return s
}

//nolint:gocyclo
func main() {
	level, err := log.ParseLevel(logLevel)
	if err != nil {
		log.Fatalf("failed to parse %v as log level", logLevel)
	}
	log.SetLevel(level)
	log.SetFormatter(&log.TextFormatter{
		DisableColors: true,
	})

	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.WithError(err).Fatal("failed to load embedded swagger")
	}

	if clickHouseDsn == "" {
		log.Fatal("CLICKHOUSE_DSN env variable not set")
	}
	if apiBaseURL == "" {
		log.Fatal("API_BASE_URL env variable not set")
	}
	if gatekeeperURL == "" {
		log.Fatal("GATEKEEPER_URL env variable not set")
	}

	baseURL, err := url.Parse(apiBaseURL)
	if err != nil {
		log.WithError(err).Fatal("invalid base domain url")
	}

	gkURL, err := url.Parse(gatekeeperURL)
	if err != nil {
		log.WithError(err).Fatal("invalid gatekeeper URL")
	}

	opts := &errortracking.DatabaseOptions{
		MaxOpenConns:     clickhouseMaxOpenConns,
		MaxIdleConns:     clickhouseMaxIdleConns,
		UseCompression:   dbCompressionEnabled,
		UseRemoteStorage: useRemoteStorage,
	}

	db, err := errortracking.NewDB(clickHouseDsn, opts)
	if err != nil {
		log.WithError(err).Fatal("failed to set up clickhouse")
	}

	var queue *persistentqueue.Queue
	if clientSideBufferingEnabled {
		queue, err = persistentqueue.NewQueue(
			persistentqueue.WithQueueName(ErrorTrackingQueueName),
			persistentqueue.WithDirPath(ErrortrackingQueueDirPath),
			persistentqueue.WithBatchSize(maxBatchSize),
			// processor is configured when errortracking controller is contructed
			persistentqueue.WithProcessorCount(maxProcessorCount),
		)
		if err != nil {
			log.WithError(err).Fatal("failed to setup buffer")
		}
	}

	var rateLimiter errortracking.RateLimiter

	if rateLimitingConfigFile != "" {
		redisClient := errortracking.GetRedis(
			redisAddr, redisPassword, useRedisSentinel, redisConnectionPoolSize,
		)
		redisRateLimiter := redis_rate.NewLimiter(redisClient)
		rateLimiter = errortracking.NewRateLimiter(redisRateLimiter)

		actionFn := func(_ chan struct{}) error {
			newLimits, err := errortracking.ReadConfig(rateLimitingConfigFile)
			if err != nil {
				return fmt.Errorf(
					"unable to load rate-limiting config %s: %w", rateLimitingConfigFile, err)
			}
			rateLimiter.SetLimits(newLimits)
			log.Info("New limits configuration has been set")
			return nil
		}

		fo := common.NewFileObserver(rateLimitingConfigFile, actionFn)
		err = fo.Run()
		if err != nil {
			log.WithError(err).Fatal("failed to set up rate-limiting config reloading")
		}
		defer fo.Stop()
	} else {
		log.Warn("rate-limiting config file path is not set, rate-limiting is disabled")
		rateLimiter = errortracking.NewNullRateLimiter()
	}

	ctrl := errortracking.NewController(baseURL, db, gkURL, queue, rateLimiter)

	api := operations.NewErrorTrackingAPI(swaggerSpec)
	// Use logrus for logging.
	api.Logger = log.Infof

	// Set up the ingestion endpoints at the
	// /projects/{projectId}/(store|envelope) routes.
	api.EventsPostProjectsAPIProjectIDEnvelopeHandler = events.PostProjectsAPIProjectIDEnvelopeHandlerFunc(
		ctrl.PostEnvelopeHandlerRateLimited,
	)
	api.EventsPostProjectsAPIProjectIDStoreHandler = events.PostProjectsAPIProjectIDStoreHandlerFunc(
		ctrl.PostStoreHandlerRateLimited,
	)

	// Set up the /projects/{projectId}/errors route that returns a list of
	// errors.
	api.ErrorsListErrorsHandler = errors.ListErrorsHandlerFunc(
		ctrl.ListErrorsRateLimited,
	)

	// Set up the GET /projects/{projectId}/errors/{fingerprint} route handler
	// that returns information about an error.
	api.ErrorsGetErrorHandler = errors.GetErrorHandlerFunc(
		ctrl.GetErrorRateLimited,
	)

	// Set up the PUT /projects/{projectId}/errors/{fingerprint} route handler
	// that updates information about an error.
	api.ErrorsUpdateErrorHandler = errors.UpdateErrorHandlerFunc(
		ctrl.UpdateErrorRateLimited,
	)

	// Set up the GET /projects/{projectID}/errors/{fingerprint}/events router
	// handle that returns information about the events related to an error.
	api.ErrorsListEventsHandler = errors.ListEventsHandlerFunc(
		ctrl.ListEventsRateLimited,
	)

	// Set up the DELETE /projects/{projectId} route handle that deletes errors
	// from the given project.
	api.ProjectsDeleteProjectHandler = projects.DeleteProjectHandlerFunc(
		ctrl.DeleteProjectRateLimited,
	)

	api.ErrorsV2ListErrorsV2Handler = errors_v2.ListErrorsV2HandlerFunc(
		ctrl.ListErrorsV2,
	)

	api.ErrorsV2ListProjectsHandler = errors_v2.ListProjectsHandlerFunc(
		ctrl.ListProjects,
	)

	api.MessagesListMessagesHandler = messages.ListMessagesHandlerFunc(
		ctrl.ListMessagesRateLimited,
	)

	// Set up base metrics before we instrument the middleware
	prometheus.MustRegister(metrics.ConfiguredCollectors()...)
	// Set up per-project metrics
	prometheus.MustRegister(errortracking.ConfiguredCollectors()...)
	// Setup additional metrics
	prometheus.MustRegister(restapi.ConfiguredCollectors()...)

	server := restapi.NewServer(api)
	server.ConfigureAPI()

	server.Port = port
	server.EnabledListeners = []string{"http"}

	// Set up metrics, readiness server
	var (
		metricsServ *http.Server
		isReady     = &atomic.Bool{}
		mux         = http.NewServeMux()
	)
	mux.Handle("/metrics", promhttp.Handler())
	mux.HandleFunc("/ready", common.ReadyHandler(isReady))
	metricsAddr := fmt.Sprintf("0.0.0.0:%d", metricsPort)
	metricsServ = &http.Server{
		Addr:              metricsAddr,
		Handler:           mux,
		ReadHeaderTimeout: time.Second * 3,
	}
	go func() {
		log.Debugf("metrics server starting at %s", metricsAddr)
		if err := metricsServ.ListenAndServe(); err != nil && !stdErrors.Is(err, http.ErrServerClosed) {
			if err != http.ErrServerClosed {
				log.WithError(err).Error("unable to start metric server")
			}
		}
	}()

	// Set up debug server
	if debugEnabled {
		debugServer := NewDebugServer("localhost:8082")
		go func() {
			if err := debugServer.ListenAndServe(); err != nil && !stdErrors.Is(err, http.ErrServerClosed) {
				log.WithError(err).Error("unable to start debug server")
			}
		}()
	}

	go func() {
		if err := server.Serve(); err != nil && !stdErrors.Is(err, http.ErrServerClosed) {
			log.WithError(err).Fatal("server failed")
		}
	}()

	isReady.Store(true)

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGTERM)
	<-sigChan

	// Instead of shutting down the server, just mark this server as not ready and continue processing server
	// till a termination period. Similar to `preStop` hook to pod.
	isReady.Store(false)

	// This is a guess estimate on how long will it take for pod's endpoint is removed from service/LB.
	// Note: Nginx ingress controller uses endpoints (podIP:port) instead of a service, so it makes more sense
	// to continue serving the requests till the pod is kicked out.
	// See https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/#service-upstream
	// and https://github.com/kubernetes/ingress-nginx/issues/257 for more details.
	time.Sleep(time.Second * 10)

	// Ideally code path shouldn't reach this but, shutdown the server to make sure we cover cases when sleep
	// seconds isn't enough.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	err = metricsServ.Shutdown(ctx)
	if err != nil {
		log.WithError(err).Fatal("failed to shutdown the metric server")
	}
	err = server.Shutdown()
	if err != nil {
		log.WithError(err).Fatal("failed to shutdown the server")
	}
	err = db.Close()
	if err != nil {
		log.WithError(err).Fatal("failed to close DB connections")
	}
}
