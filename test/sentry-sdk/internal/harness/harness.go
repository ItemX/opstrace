package harness

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/olekukonko/tablewriter"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
	harnesstypes "gitlab.com/gitlab-org/opstrace/opstrace/test/sentry-sdk/internal/types"
)

var (
	downloadDir string = filepath.Join(".", "testdata", "payloads")
)

type TestHarness struct {
	networkName string
	network     testcontainers.Network

	recorderContainer testcontainers.Container
	sdkContainers     []testcontainers.Container
}

func NewTestHarness(ctx context.Context) (*TestHarness, error) {
	networkCtx, cancel := context.WithTimeout(ctx, time.Second*60)
	defer cancel()

	networkName := "test-harness-network"
	network, err := testcontainers.GenericNetwork(networkCtx, testcontainers.GenericNetworkRequest{
		NetworkRequest: testcontainers.NetworkRequest{
			Name:           networkName,
			CheckDuplicate: false,
		},
	})
	if err != nil {
		return nil, fmt.Errorf("building testcontainer network: %w", err)
	}

	harness := &TestHarness{
		networkName: networkName,
		network:     network,

		sdkContainers: make([]testcontainers.Container, 0),
	}

	if err := harness.createRecordingServer(ctx); err != nil {
		return nil, err
	}

	if err := harness.createSDKClientContainers(ctx); err != nil {
		return nil, err
	}

	return harness, nil
}

func (th *TestHarness) Execute(ctx context.Context) error {
	// start client SDK containers
	for _, container := range th.sdkContainers {
		// start container
		if err := container.Start(ctx); err != nil {
			fmt.Println(err)
		}
		// fetch logs
		logs, err := container.Logs(ctx)
		if err != nil {
			return fmt.Errorf("fetching container logs: %w", err)
		}
		readLogs, err := io.ReadAll(logs)
		if err != nil {
			return fmt.Errorf("reading container logs: %w", err)
		}
		fmt.Println(string(readLogs))
		// check container state
		state, err := container.State(ctx)
		if err != nil {
			return fmt.Errorf("getting container state: %w", err)
		}
		fmt.Printf("exit code: %v\n", state.ExitCode)
	}

	return nil
}

type requests []harnesstypes.ParsedRequest

func (r requests) Len() int           { return len(r) }
func (r requests) Less(i, j int) bool { return r[i].ClientVersion < r[j].ClientVersion }
func (r requests) Swap(i, j int)      { r[i], r[j] = r[j], r[i] }

func (th *TestHarness) DownloadArtifacts(ctx context.Context) error {
	port, err := th.recorderContainer.MappedPort(ctx, "8888")
	if err != nil {
		return fmt.Errorf("getting mapped port: %w", err)
	}

	req, err := http.NewRequest("GET", fmt.Sprintf("http://localhost:%s/download", port.Port()), nil)
	if err != nil {
		return fmt.Errorf("building download request: %w", err)
	}
	req.Header.Add("Accept", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("performing download request: %w", err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("reading download response: %w", err)
	}

	var response map[string]harnesstypes.ParsedRequest
	if err := json.Unmarshal(body, &response); err != nil {
		return fmt.Errorf("unmarshalling download response: %w", err)
	}

	if err := downloadPayloads(ctx, response); err != nil {
		return fmt.Errorf("downloading payloads: %w", err)
	}

	if err := printCompatibilityMatrix(ctx, response); err != nil {
		return fmt.Errorf("printing compatibility matrix: %w", err)
	}

	return nil
}

func (th *TestHarness) createRecordingServer(ctx context.Context) error {
	recorderContainer, err := testcontainers.GenericContainer(
		ctx,
		testcontainers.GenericContainerRequest{
			ContainerRequest: testcontainers.ContainerRequest{
				Networks:     []string{th.networkName},
				Name:         "recorder",
				Image:        "registry.gitlab.com/gitlab-org/opstrace/opstrace/sentry-test-harness:latest",
				Cmd:          []string{"recorder"},
				ExposedPorts: []string{"8888"},
				WaitingFor:   wait.ForLog("run recorder server"),
			},
		},
	)
	if err != nil {
		return fmt.Errorf("creating recorder testcontainer: %w", err)
	}
	th.recorderContainer = recorderContainer

	// start recorder container
	if err := th.recorderContainer.Start(ctx); err != nil {
		return fmt.Errorf("starting recorder testcontainer: %w", err)
	}
	// check container state
	state, err := th.recorderContainer.State(ctx)
	if err != nil {
		return fmt.Errorf("fetching recorder testcontainer status: %w", err)
	}
	if state.Running {
		fmt.Println("recorder server running")
	}

	return nil
}

func (th *TestHarness) createSDKClientContainers(ctx context.Context) error {
	containerIP, err := th.recorderContainer.ContainerIP(ctx)
	if err != nil {
		return fmt.Errorf("fetching containerIP from recorder container: %w", err)
	}

	// When SENTRY_DSN is configured as an environment variable, we use the
	// Sentry instance pointed by it. Otherwise, we configure the apps to
	// use a locally-run "proxy server" which emulates the Sentry API to
	// capture and examine received payloads.
	var dsn string
	if _, ok := os.LookupEnv("SENTRY_DSN"); ok {
		dsn = os.Getenv("SENTRY_DSN")
	} else {
		dsn = fmt.Sprintf("http://random@%s:%s/projects/%s", containerIP, "8888", "12345")
	}

	if dsn == "" {
		return fmt.Errorf("SENTRY_DSN cannot be empty")
	}

	containerEnv := map[string]string{
		"SENTRY_DSN":  dsn,
		"INSECURE_OK": strconv.FormatBool(false),
	}

	tcs, err := GetTestContainers(containerEnv)
	if err != nil {
		return err
	}

	for _, tc := range tcs {
		tc.Networks = []string{th.networkName}
		container, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
			ContainerRequest: tc,
		})
		if err != nil {
			fmt.Println(err)
			continue
		}
		th.sdkContainers = append(th.sdkContainers, container)
	}
	return nil
}

func downloadPayloads(_ context.Context, response map[string]harnesstypes.ParsedRequest) error {
	if err := os.MkdirAll(downloadDir, os.ModePerm); err != nil {
		return fmt.Errorf("creating directory: %w", err)
	}
	// nuke old contents, just in case
	contents, err := os.ReadDir(downloadDir)
	if err != nil {
		return fmt.Errorf("reading directory: %w", err)
	}
	for _, d := range contents {
		temp := path.Join([]string{downloadDir, d.Name()}...)
		fmt.Printf("deleting old payload: %s ...\n", temp)
		os.RemoveAll(temp)
	}
	for k, v := range response {
		file, err := json.MarshalIndent(v, "", " ")
		if err != nil {
			return fmt.Errorf("marshaling payload: %w", err)
		}
		fileName := fmt.Sprintf("%s/%s.json", downloadDir, k)
		if err := os.WriteFile(fileName, file, 0600); err != nil {
			return fmt.Errorf("writing payload file: %w", err)
		}
	}
	return nil
}

func printCompatibilityMatrix(_ context.Context, response map[string]harnesstypes.ParsedRequest) error {
	r := make(requests, 0)
	for _, v := range response {
		r = append(r, v)
	}
	sort.Sort(r)

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"CLIENT", "ENDPOINT", "SUPPORTED ITEM TYPES", "PAYLOAD"})

	for _, v := range r {
		var row []string
		// client
		row = append(row, v.ClientVersion)
		// endpoint
		row = append(row, v.Endpoint)
		// body
		var itemTypes []string
		//nolint:nestif
		if v.Endpoint == "envelope" {
			envelope, err := types.NewEnvelopeFrom([]byte(v.Body))
			if err != nil {
				return err
			}
			for _, item := range envelope.Items {
				itemTypes = append(itemTypes, item.Type.Type)
				if item.Type.Type == "event" {
					event, err := types.NewEventFrom(item.Payload)
					if err != nil {
						return err
					}
					if event.Message != "" {
						itemTypes = append(itemTypes, "message")
					}
					if event.Exception != nil {
						itemTypes = append(itemTypes, "exception")
					}
				}
			}
		} else {
			event, err := types.NewEventFrom([]byte(v.Body))
			if err != nil {
				return err
			}
			if event.Message != "" {
				itemTypes = append(itemTypes, "message")
			}
			if event.Exception != nil {
				itemTypes = append(itemTypes, "exception")
			}
		}
		row = append(row, strings.Join(itemTypes, ","))
		row = append(row, fmt.Sprintf("%s/%s.json", downloadDir, v.UUID))
		table.Append(row)
	}
	table.Render()
	return nil
}
