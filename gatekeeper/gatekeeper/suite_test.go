package gatekeeper_test

import (
	"context"
	"encoding/base64"
	"fmt"
	"net/http"
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"

	"github.com/go-redis/cache/v9"
	"github.com/sirupsen/logrus"
	uberzap "go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
)

const GitlabReplyUnauthorized = `{"message":"Unauthorized"}`

//nolint:gosec
const GitlabTokenRegistryScopes = `{ "scopes": [ "registry" ] }`

var (
	logger *uberzap.SugaredLogger
)

func stringRef(s string) *string {
	return &s
}

func TestGatekeeper(t *testing.T) {
	RegisterFailHandler(Fail)

	// TODO(prozlach): Unify logging in GOB - https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2130
	// Let's use two loggers for the time being.
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetFormatter(&logrus.TextFormatter{FullTimestamp: true})
	logrus.SetOutput(GinkgoWriter)

	setTimeEncoderOpt := func(o *zap.Options) {
		o.TimeEncoder = zapcore.RFC3339TimeEncoder
	}
	logger = zap.NewRaw(
		zap.WriteTo(GinkgoWriter),
		zap.UseDevMode(true),
		setTimeEncoderOpt,
	).Sugar()
	// TODO ends

	RunSpecs(t, "Gatekeeper Test Suite")
}

type MockRedis struct {
	store map[string]interface{}
}

func (m *MockRedis) Set(item *cache.Item) error {
	m.store[item.Key] = item.Value
	return nil
}

func (m *MockRedis) Get(ctx context.Context, key string, value interface{}) error {
	return cache.ErrCacheMiss
}

func NewMockRedis() *MockRedis {
	return &MockRedis{
		store: make(map[string]interface{}),
	}
}

func verifyPHSRequestMade(
	phs *testutils.ProgrammableHTTPServer,
	method string,
	path string,
) {
	GinkgoHelper()

	issuedRequests := phs.GetRequests(method, path)
	description := fmt.Sprintf("%s %s has not been issued while it should", method, path)
	Expect(issuedRequests).ToNot(BeEmpty(), description)
}

func verifyPHSRequestNotMade(
	phs *testutils.ProgrammableHTTPServer,
	method string,
	path string,
) {
	GinkgoHelper()

	issuedRequests := phs.GetRequests(method, path)
	description := fmt.Sprintf("%s %s has been issued while it shouldn't", method, path)
	Expect(issuedRequests).To(BeEmpty(), description)
}

type testTokenType string

const testTokenTypeProject = "project"
const testTokenTypeGroup = "group"
const testTokenTypeUnknown = "invalid"

// configurePHSforGitlabTokenAuthnz configures the ProgrammableHTTPServer in a
// way that it mimics Gitlab instance's API.
//
// The styructure of projects/groups is as follows:
// * project with ID 115,
//   - ancestor groups of this project: 13
//
// * user with id 61
//   - member of group 13
//
// * namespace with id 13
func configurePHSforGitlabTokenAuthnz(
	phs *testutils.ProgrammableHTTPServer,
	glatDummyTokens map[string]struct{},
	tokenType testTokenType,
) {
	// GL Access Token info about itself - we are interested here in
	// the scopes this token has
	patSelfReply := func(r *http.Request) (int, string) {
		switch r.Header.Get("private-token") {
		case "glpat-rwrwrwrwrwrwrwrwrwrw":
			return http.StatusOK, `{ "scopes": ["read_observability", "write_observability", "read_api"] }`
		case "glpat-rrrrrrrrrrrrrrrrrrrr":
			return http.StatusOK, `{ "scopes": ["read_observability", "read_api"] }`
		case "glpat-wwwwwwwwwwwwwwwwwwww":
			return http.StatusOK, `{ "scopes": ["write_observability", "read_api"] }`
		default:
			return http.StatusForbidden, GitlabReplyUnauthorized
		}
	}
	phs.SetResponseFunc(
		http.MethodGet, "/api/v4/personal_access_tokens/self",
		patSelfReply,
	)

	// Namespace info query
	nsReply := func(r *http.Request) (int, string) {
		if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
			return http.StatusOK, `{
                                   "id" : 13,
                                   "name": "Gitlab Org",
                                   "path": "gitlab-org",
                                   "full_path": "gitlab-org",
                                   "web_url": "https://gdk.devvm:3443/groups/gitlab-org",
                                   "kind": "group"
                                 }`
		}
		return http.StatusForbidden, GitlabReplyUnauthorized
	}
	phs.SetResponseFunc(
		http.MethodGet, "/api/v4/namespaces/13",
		nsReply,
	)

	// User info query
	userReply := func(r *http.Request) (int, string) {
		if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
			var botName string
			switch tokenType {
			case testTokenTypeProject:
				botName = "project_115_bot1"
			case testTokenTypeGroup:
				botName = "group_115_bot1"
			case testTokenTypeUnknown:
			}
			return http.StatusOK, fmt.Sprintf(`{
                                   "id": 61,
								   "bot": true,
								   "username": "%s",
                                   "email": "%s@noreply.gdk.devvm"
                                 }`, botName, botName)
		}
		return http.StatusForbidden, GitlabReplyUnauthorized
	}
	phs.SetResponseFunc(
		http.MethodGet, "/api/v4/user",
		userReply,
	)

	// Project info query
	projectInfo := func(r *http.Request) (int, string) {
		if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
			return http.StatusOK, `{
                                   "id" : 115,
                                   "name": "Foo Project",
                                   "name_with_namespace": "Gitlab Org / Foo Project",
                                   "path": "fooproj",
                                   "path_with_namespace": "gitlab-org/fooproj",
                                   "namespace": {
                                     "id": 13
                                   }
                                 }`
		}
		return http.StatusForbidden, GitlabReplyUnauthorized
	}
	phs.SetResponseFunc(
		http.MethodGet, "/api/v4/projects/115",
		projectInfo,
	)

	// project groups
	projectGroupMemberships := func(r *http.Request) (int, string) {
		if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
			return http.StatusOK, `[
                            {
							  "id": 13,
                              "full_path": "gitlab-org"
							}
                        ]`
		}
		return http.StatusForbidden, GitlabReplyUnauthorized
	}
	phs.SetResponseFunc(
		http.MethodGet, "/api/v4/projects/115/groups",
		projectGroupMemberships,
	)

	// project groups
	projectGroups := func(r *http.Request) (int, string) {
		if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
			return http.StatusOK, `{
                                "state": "active",
                                "access_level": 30
                            }`
		}
		return http.StatusForbidden, GitlabReplyUnauthorized
	}
	phs.SetResponseFunc(
		http.MethodGet, "/api/v4/projects/115/members/all/61",
		projectGroups,
	)

	// group membership
	groupMembership := func(r *http.Request) (int, string) {
		if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
			return http.StatusOK, `{
                                   "state": "active",
                                   "access_level": 20
                                 }`
		}
		return http.StatusForbidden, GitlabReplyUnauthorized
	}
	phs.SetResponseFunc(
		http.MethodGet, "/api/v4/groups/13/members/all/61",
		groupMembership,
	)
}

func configurePHSforGOUITokenAuthnz(phs *testutils.ProgrammableHTTPServer) {
	// Simulate that the user in GOUI already exists, and that the
	// udpate is needed
	userQuery := func(r *http.Request) (int, string) {
		if r.Header.Get("Authorization") != "Basic "+base64.StdEncoding.EncodeToString([]byte("admin:foo")) {
			return http.StatusForbidden, GitlabReplyUnauthorized
		}

		login := r.URL.Query().Get("loginOrEmail")
		if login != "group_115_bot1@noreply.gdk.devvm" {
			return http.StatusForbidden, GitlabReplyUnauthorized
		}

		return http.StatusOK, `{  "id": 3,
                                    "email": "group_115_bot1@noreply.gdk.devvm",
                                    "name": "test-scope-group",
                                    "login": "group_13_bot1",
                                    "avatarUrl": ""
                                  }`
	}
	phs.SetResponseFunc(
		http.MethodGet, "/api/users/lookup",
		userQuery,
	)

	userUpdate := func(r *http.Request) (int, string) {
		if r.Header.Get("Authorization") != "Basic "+base64.StdEncoding.EncodeToString([]byte("admin:foo")) {
			return http.StatusForbidden, GitlabReplyUnauthorized
		}

		return http.StatusOK, ""
	}
	phs.SetResponseFunc(
		"PUT", "/api/users/3",
		userUpdate,
	)

	groupuserUpdate := func(r *http.Request) (int, string) {
		if r.Header.Get("Authorization") != "Basic "+base64.StdEncoding.EncodeToString([]byte("admin:foo")) {
			return http.StatusForbidden, GitlabReplyUnauthorized
		}

		return http.StatusOK, ""
	}
	phs.SetResponseFunc(
		http.MethodPatch, "/api/groups/13/users/3",
		groupuserUpdate,
	)
}
