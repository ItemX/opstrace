package argusapi

import (
	"bytes"
	"encoding/json"
	"fmt"
)

// DataSource represents a Grafana data source.
//
//nolint:lll
type DataSource struct {
	ID     int64  `json:"id,omitempty"`
	UID    string `json:"uid,omitempty"`
	Name   string `json:"name"`
	Type   string `json:"type"`
	URL    string `json:"url"`
	Access string `json:"access"`

	// This is only returned by the API. It can only be set through the
	// `editable` attribute of provisioned data sources.
	ReadOnly bool `json:"readOnly"`

	Database string `json:"database,omitempty"`
	User     string `json:"user,omitempty"`
	// Deprecated: Use secureJsonData.password instead.
	Password string `json:"password,omitempty"`

	GroupID   int64 `json:"groupId,omitempty"`
	IsDefault bool  `json:"isDefault"`

	BasicAuth     bool   `json:"basicAuth"`
	BasicAuthUser string `json:"basicAuthUser,omitempty"`
	// Deprecated: Use secureJsonData.basicAuthPassword instead.
	BasicAuthPassword string `json:"basicAuthPassword,omitempty"`
	WithCredentials   bool   `json:"withCredentials"`

	/*
		NOTE(prozlach):

		Grafana Golang Client[1] and Grafana Operator[2] are not in sync wrt.
		the contents of `JsonData` and `SecureJsonData`. One such example could
		be `tlsConfigurationMethod` field - it is present in Grafana APIs
		`JsonData` but not in the operator. I did a check and there are lots of
		other such fields too. The GOUI(Grafana) API code treats both
		`JsonData` and `SecureJsonData` fields in a type agnostic way[3]
		solving the problem in an elegant way of letting plugins treat these
		two fields as a map internally:

		```go
		JsonData          *simplejson.Json  `json:"jsonData"`
		SecureJsonData    map[string]string `json:"secureJsonData"`
		```

		same with Grafana API library [4]:

		```go
		JSONData       map[string]interface{} `json:"jsonData,omitempty"`
		SecureJSONData map[string]interface{} `json:"secureJsonData,omitempty"`
		```

		I wanted to limit the scope of changes, but at the same time I had to
		find a way to be able to submit the contents of Datasource CRD to the
		GOUI API. The idea is to make our fork of API also type-agnostic for
		`JsonData` and `SecureJsonData` too, while keeping operator AS IS, plus
		few small changes to update the API library itself. I am not aware of
		the code that uses the API library ATM so this should be safe. In order
		to not confuse people, I have removed the "Adapter" code we had in
		the library so far and simplified it to bare minimum.

		[1] https://github.com/grafana/grafana-api-golang-client
		[2] https://github.com/grafana-operator/grafana-operator/
		[3] https://gitlab.com/gitlab-org/opstrace/opstrace-ui/blob/23133c620c0f794837b9af4f3f184ea7a42b3570/pkg/models/datasource.go#L176-177
		[4] https://github.com/grafana/grafana-api-golang-client/blob/2afa96c2a9e24a7c743741a301f6a086213a1fe4/datasource.go#L34-L35
	*/
	JSONData       map[string]interface{} `json:"jsonData,omitempty"`
	SecureJSONData map[string]interface{} `json:"secureJsonData,omitempty"`
}

// NewDataSource creates a new Grafana data source.
func (c *Client) NewDataSource(s *DataSource) (int64, error) {
	data, err := json.Marshal(s)
	if err != nil {
		// TODO: Fix error check here to be wrapped before returning
		//nolint:wrapcheck
		return 0, err
	}

	result := struct {
		ID int64 `json:"id"`
	}{}

	err = c.request("POST", "/api/datasources", nil, bytes.NewBuffer(data), &result)
	if err != nil {
		return 0, err
	}

	return result.ID, err
}

// UpdateDataSource updates a Grafana data source.
func (c *Client) UpdateDataSource(s *DataSource) error {
	path := fmt.Sprintf("/api/datasources/%d", s.ID)
	data, err := json.Marshal(s)
	if err != nil {
		// TODO: Fix error check here to be wrapped before returning
		//nolint:wrapcheck
		return err
	}

	return c.request("PUT", path, nil, bytes.NewBuffer(data), nil)
}

// DataSource fetches and returns the Grafana data source whose ID it's passed.
func (c *Client) DataSource(id int64) (*DataSource, error) {
	path := fmt.Sprintf("/api/datasources/%d", id)
	result := &DataSource{}
	err := c.request("GET", path, nil, nil, result)
	if err != nil {
		return nil, err
	}

	return result, err
}

// DataSourceByUID fetches and returns the Grafana data source whose UID is passed.
func (c *Client) DataSourceByUID(uid string) (*DataSource, error) {
	path := fmt.Sprintf("/api/datasources/uid/%s", uid)
	result := &DataSource{}
	err := c.request("GET", path, nil, nil, result)
	if err != nil {
		return nil, err
	}

	return result, err
}

// DataSourceByUID fetches and returns the Grafana data source whose UID is passed.
func (c *Client) DataSourceByName(name string) (*DataSource, error) {
	path := fmt.Sprintf("/api/datasources/name/%s", name)
	result := new(DataSource)
	err := c.request("GET", path, nil, nil, result)
	if err != nil {
		return nil, err
	}

	return result, err
}

// DeleteDataSource deletes the Grafana data source whose ID it's passed.
func (c *Client) DeleteDataSourceByID(id int64) error {
	path := fmt.Sprintf("/api/datasources/%d", id)

	return c.request("DELETE", path, nil, nil, nil)
}

// DeleteDataSource deletes the Grafana data source whose ID it's passed.
func (c *Client) DeleteDataSourceByName(name string) error {
	path := fmt.Sprintf("/api/datasources/name/%s", name)

	return c.request("DELETE", path, nil, nil, nil)
}
