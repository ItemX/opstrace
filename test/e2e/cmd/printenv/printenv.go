package main

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/tests"
	"go.uber.org/zap"
)

// Set up infra provider and print it's internal configuration in .env format.
func main() {
	log, err := zap.NewDevelopment()
	if err != nil {
		panic(err)
	}
	defer func() {
		log.Sync()
	}()
	logger := log.Sugar()

	c, err := tests.LoadConfig()
	if err != nil {
		logger.Fatalf("failed to load tests config: %v", err)
	}

	logger.Infof("getting internal %s env for %s", c.TestTarget, c.TestIdentifier)

	infra, err := infra.NewInfrastructure(c.TestTarget, c.TestIdentifier)
	if err != nil {
		logger.Fatalf("failed to init infrastructure: %v", err)
	}

	if _, err := infra.LoadConfiguration(); err != nil {
		logger.Fatalf("failed to load infra config: %v", err)
	}

	// print internal env
	kvs, err := infra.InternalConfiguration()
	if err != nil {
		logger.Fatalf("failed to get internal configuration: %v", err)
	}

	for k, v := range kvs {
		fmt.Println(k + "=" + v)
	}
}
