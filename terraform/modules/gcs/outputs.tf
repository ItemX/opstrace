output "bucket_name" {
  value       = google_storage_bucket.clickhouse_data.name
  description = "Name of the GCS bucket"
}

output "service_account_hmac_access_id" {
  value     = google_storage_hmac_key.key.access_id
  sensitive = true
}

output "service_account_hmac_secret" {
  value     = google_storage_hmac_key.key.secret
  sensitive = true
}
