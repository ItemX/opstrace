# kubernetes-postgresql

Kubernetes StatefulSet for PostgreSQL.

These are applied when running GOB on a KIND cluster (for local development).

Do not use this configuration for production as the credentials are hardcoded.

Generated from [Bitnami PostgreSQL chart](https://github.com/bitnami/charts/tree/main/bitnami/postgresql) with the following overrides:

* image.tag=11-debian-11
* auth.postgresPassword=dev
* primary.persistence.size=1Gi
