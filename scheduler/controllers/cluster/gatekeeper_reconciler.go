package cluster

import (
	"fmt"
	"time"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	ctrlcommon "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/common"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type GatekeeperReconciler struct {
	BaseReconciler
	clusterState *ClusterState
}

func NewGatekeeperReconciler(
	initialManifests map[string][]byte,
	teardown bool,
	clusterState *ClusterState,
) *GatekeeperReconciler {
	return &GatekeeperReconciler{
		clusterState: clusterState,

		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.GatekeeperInventoryID)),
			initialManifests: initialManifests,

			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.Gatekeeper
			},
			serviceAccountName: constants.GatekeeperServiceAccountName,
		},
	}
}

func (i *GatekeeperReconciler) reconcileTeardown(cr *v1alpha1.Cluster) common.DesiredState {
	// We are cleaning up, no point in generating manifests as we will use
	// Inventory for that
	return common.DesiredState{
		common.ManifestsPruneAction{
			ComponentName: constants.GatekeeperInventoryID + "/deployment",
			GetInventory: func() *v1beta2.ResourceInventory {
				return cr.Status.Inventory[constants.GatekeeperInventoryID+"/deployment"]
			},
			Msg: "manifests/gatekeeper-deployment prune",
			Log: i.Log,
		},
		i.getSessionCookieDesiredState(cr),
		common.ManifestsPruneAction{
			ComponentName: constants.GatekeeperInventoryID + "/ingress",
			GetInventory: func() *v1beta2.ResourceInventory {
				return cr.Status.Inventory[constants.GatekeeperInventoryID+"/ingress"]
			},
			Msg: "manifests/gatekeeper-ingress prune",
			Log: i.Log,
		},
	}
}

func (i *GatekeeperReconciler) reconcileState(cr *v1alpha1.Cluster) common.DesiredState {
	psqlEndpoint, err := i.clusterState.GetPostgresEndpoint()
	if err != nil {
		msg := "psql endpoint is not available yet"
		return common.DesiredState{
			common.LogAction{
				Msg:   "[gatekeeper/deployment] " + msg,
				Error: err,
			},
			common.LogAction{
				Msg:   "[gatekeeper/ingress] " + msg,
				Error: err,
			},
		}
	}

	overrideKustomization, err := i.applyConfiguration(cr, psqlEndpoint.String())
	if err != nil {
		msg := "failed to apply configuration to the manifests"
		return common.DesiredState{
			common.LogAction{
				Msg:   "[gatekeeper/deployment] " + msg,
				Error: err,
			},
			common.LogAction{
				Msg:   "[gatekeeper/ingress] " + msg,
				Error: err,
			},
		}
	}
	i.applyOverrides(overrideKustomization, cr)
	manifests, err := ctrlcommon.ApplyKustomization(i.initialManifests, overrideKustomization)
	if err != nil {
		return common.DesiredState{
			common.LogAction{
				Msg:   "[gatekeeper/deployment] failed to apply overrides to the manifests",
				Error: err,
			},
			common.LogAction{
				Msg:   "[gatekeeper/ingress] failed to apply overrides to the manifests",
				Error: err,
			},
		}
	}

	// NOTE(prozlach): ingress-nginx has a circular dependency on gatekeeper -
	// gatekeeper relies on ingress-nginx for his ingress object to transition
	// to "Ready" state, while ingress-nginx relies on gatekeeper for the
	// default backend.
	// The solution is to produce two sets of actions/split the set we get from
	// built-in manifests and then reconcile gatekeeper deployment, then
	// ingress-nginx and then gatekeeper-ingress.
	manifestsAll, manifestIngress := i.extractIngressManifest(manifests)

	return common.DesiredState{
		i.getSessionCookieDesiredState(cr),
		common.ManifestsEnsureAction{
			HealthcheckTimeout: 2 * time.Minute,
			Manifests:          manifestsAll,
			ComponentName:      constants.GatekeeperInventoryID + "/deployment",
			GetInventory: func() *v1beta2.ResourceInventory {
				return cr.Status.Inventory[constants.GatekeeperInventoryID+"/deployment"]
			},
			SetInventory: func(inv *v1beta2.ResourceInventory) {
				cr.Status.Inventory[constants.GatekeeperInventoryID+"/deployment"] = inv
			},
			Msg: "manifests/gatekeeper-deployment ensure",
			Log: i.Log,
		},
		common.ManifestsEnsureAction{
			HealthcheckTimeout: 2 * time.Minute,
			Manifests:          manifestIngress,
			ComponentName:      constants.GatekeeperInventoryID + "/ingress",
			GetInventory: func() *v1beta2.ResourceInventory {
				return cr.Status.Inventory[constants.GatekeeperInventoryID+"/ingress"]
			},
			SetInventory: func(inv *v1beta2.ResourceInventory) {
				cr.Status.Inventory[constants.GatekeeperInventoryID+"/ingress"] = inv
			},
			Msg: "manifests/gatekeeper-ingress ensure",
			Log: i.Log,
		},
	}
}

func (i *GatekeeperReconciler) Reconcile(cr *v1alpha1.Cluster) common.DesiredState {
	if i.Teardown {
		return i.reconcileTeardown(cr)
	}

	return i.reconcileState(cr)
}

func (i *GatekeeperReconciler) extractIngressManifest(in []*unstructured.Unstructured) ([]*unstructured.Unstructured, []*unstructured.Unstructured) {
	resAll := make([]*unstructured.Unstructured, 0, len(in)-1)
	resIngress := make([]*unstructured.Unstructured, 0, 1)

	for _, obj := range in {
		if obj.GetName() == constants.GatekeeperName && obj.GetKind() == "Ingress" {
			resIngress = append(resIngress, obj)
		} else {
			resAll = append(resAll, obj)
		}
	}
	// NOTE(prozlach): no ingress objects in the gatekeeper manifests set is a
	// programming error that needs attention.
	if len(resIngress) != 1 {
		panic(
			fmt.Sprintf("%d!=1 ingresses disovered in Gatekeeper manifests, "+
				"please doublecheck the code and manifests", len(resIngress)),
		)
	}
	return resAll, resIngress
}

func (i *GatekeeperReconciler) getSessionCookieDesiredState(cr *v1alpha1.Cluster) common.Action {
	secret := &v1.Secret{}
	secret.ObjectMeta = metav1.ObjectMeta{
		Name:      constants.SessionCookieSecretName,
		Namespace: cr.Namespace(),
	}
	secret.Data = map[string][]byte{
		"COOKIE_SECRET": []byte(utils.RandStringRunes(50)),
	}

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: secret,
			Msg: "gatekeeper session cookie",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: secret,
		Msg: "gatekeeper session cookie",
		Mutator: func() error {
			// Don't mutate to ensure we don't change the cookie secret after
			// it's first created
			return nil
		},
	}
}

//nolint:funlen // this is just a bunch of ifs, no point is splitting it.
func (i *GatekeeperReconciler) applyConfiguration(
	cr *v1alpha1.Cluster,
	psqlEndpoint string,
) (*kustomize.Kustomization, error) {
	skipTLSInsecureVerify := "false"
	if cr.Spec.Target == common.KIND {
		// running on localhost domain and the loopback interface is not trusted
		skipTLSInsecureVerify = "true"
	}

	res, err := i.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}

	res.Images = []kustomize.Image{
		{
			Name:    "gatekeeper-image-placeholder",
			NewName: constants.DockerImageName(constants.GatekeeperImageName),
			NewTag:  constants.DockerImageTag,
		},
	}
	res.PatchesJson6902 = append(res.PatchesJson6902,
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "ServiceMonitor",
					},
					Name: constants.GatekeeperServiceMonitorName,
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/subjects/0/namespace", "value": "%s" }]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "ClusterRoleBinding",
					},
					Name: constants.GatekeeperClusterRoleBindingName,
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[
						{"op": "replace", "path": "/spec/rules/0/host", "value": "%s" },
						{"op": "replace", "path": "/spec/tls/0/hosts/0", "value": "%s" },
						{"op": "replace", "path": "/metadata/annotations/nginx.ingress.kubernetes.io~1custom-http-errors", "value": "%s" }
					  ]`,
				cr.Spec.GetHost(),
				cr.Spec.GetHost(),
				common.NginxCustomHTTPErrors,
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Ingress",
					},
					Name: constants.GatekeeperName,
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[
						{"op": "replace", "path": "/spec/template/spec/containers/0/env/3/value", "value": "%s" },
						{"op": "replace", "path": "/spec/template/spec/containers/0/env/4/value", "value": "%s" },
						{"op": "replace", "path": "/spec/template/spec/containers/0/env/5/value", "value": "%s" },
						{"op": "replace", "path": "/spec/template/spec/containers/0/env/6/valueFrom/secretKeyRef/name", "value": "%s" },
						{"op": "replace", "path": "/spec/template/spec/containers/0/env/7/valueFrom/secretKeyRef/name", "value": "%s" },
						{"op": "replace", "path": "/spec/template/spec/containers/0/env/8/valueFrom/secretKeyRef/name", "value": "%s" },
						{"op": "replace", "path": "/spec/template/spec/containers/0/env/9/value", "value": "%s" },
						{"op": "replace", "path": "/spec/template/spec/containers/0/env/10/value", "value": "%s" },
						{"op": "replace", "path": "/spec/template/spec/containers/0/env/11/value", "value": "%s" },
						{"op": "replace", "path": "/spec/template/spec/containers/0/env/12/value", "value": "%s" }
					  ]`,
				psqlEndpoint,
				cr.Namespace(),
				fmt.Sprintf("rfs-%s.%s.svc.cluster.local:26379", constants.RedisName, cr.Namespace()),
				cr.Spec.GitLab.AuthSecret.Name,
				cr.Spec.GitLab.AuthSecret.Name,
				cr.Spec.GitLab.AuthSecret.Name,
				cr.Spec.GetHostURL(),
				skipTLSInsecureVerify,
				cr.Spec.GitLab.InstanceURL,
				i.clusterState.LogLevel,
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Deployment",
					},
					Name: constants.GatekeeperName,
				},
			},
		},
	)

	return res, nil
}
