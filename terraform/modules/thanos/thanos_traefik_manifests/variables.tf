
variable "traefik_address" {
  type        = string
  description = "traefik IP address"
}

variable "namespace" {
  type        = string
  default     = "thanos"
  description = "Namespace for thanos components"
}

variable "kubeconfig_path" {
  type    = string
  default = ".kubeconfig"
}

variable "cluster_endpoint" {
  type        = string
  description = "Cluster endpoint"
}

variable "cluster_ca_certificate" {
  type        = string
  description = "Cluster CA certificate"
}