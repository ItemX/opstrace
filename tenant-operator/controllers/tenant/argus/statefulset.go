package argus

import (
	"fmt"
	"strconv"

	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"

	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
)

const (
	InitMemoryRequest = "128Mi"
	InitCpuRequest    = "250m"
	InitMemoryLimit   = "512Mi"
	InitCpuLimit      = "1000m"
	MemoryRequest     = "256Mi"
	CpuRequest        = "100m"
	MemoryLimit       = "1024Mi"
	CpuLimit          = "500m"
)

var Replicas int32 = 2

func getInitResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(InitMemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(InitCpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(InitMemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(InitCpuLimit),
		},
	}
}

func getResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getPodLabels() map[string]string {
	return map[string]string{
		"app": constants.ArgusPodLabel,
	}
}

func getVolumes() []corev1.Volume {
	var volumes []corev1.Volume

	volumes = append(volumes, corev1.Volume{
		Name: constants.ArgusProvisionPluginVolumeName,
		VolumeSource: corev1.VolumeSource{
			EmptyDir: &corev1.EmptyDirVolumeSource{},
		},
	})

	volumes = append(volumes, corev1.Volume{
		Name: constants.ArgusProvisionDashboardVolumeName,
		VolumeSource: corev1.VolumeSource{
			EmptyDir: &corev1.EmptyDirVolumeSource{},
		},
	})

	volumes = append(volumes, corev1.Volume{
		Name: constants.ArgusProvisionNotifierVolumeName,
		VolumeSource: corev1.VolumeSource{
			EmptyDir: &corev1.EmptyDirVolumeSource{},
		},
	})

	// Volume to mount the config file from a config map
	volumes = append(volumes, corev1.Volume{
		Name: constants.ArgusConfigName,
		VolumeSource: corev1.VolumeSource{
			ConfigMap: &corev1.ConfigMapVolumeSource{
				LocalObjectReference: corev1.LocalObjectReference{
					Name: constants.ArgusConfigName,
				},
			},
		},
	})

	// Volume to store the logs
	volumes = append(volumes, corev1.Volume{
		Name: constants.ArgusLogsVolumeName,
		VolumeSource: corev1.VolumeSource{
			EmptyDir: &corev1.EmptyDirVolumeSource{},
		},
	})

	volumes = append(volumes, corev1.Volume{
		Name: constants.ArgusDataVolumeName,
		VolumeSource: corev1.VolumeSource{
			EmptyDir: &corev1.EmptyDirVolumeSource{},
		},
	})

	volumes = append(volumes, corev1.Volume{
		Name: constants.ArgusPluginsVolumeName,
		VolumeSource: corev1.VolumeSource{
			EmptyDir: &corev1.EmptyDirVolumeSource{},
		},
	})

	return volumes
}

func getVolumeMounts() []corev1.VolumeMount {
	var mounts []corev1.VolumeMount

	mounts = append(mounts, corev1.VolumeMount{
		Name:      constants.ArgusConfigName,
		MountPath: "/etc/argus/",
	})

	mounts = append(mounts, corev1.VolumeMount{
		Name:      constants.ArgusDataVolumeName,
		MountPath: constants.ArgusDataPath,
	})

	mounts = append(mounts, corev1.VolumeMount{
		Name:      constants.ArgusPluginsVolumeName,
		MountPath: constants.ArgusPluginsPath,
	})

	mounts = append(mounts, corev1.VolumeMount{
		Name:      constants.ArgusProvisionPluginVolumeName,
		MountPath: constants.ArgusProvisioningPluginsPath,
	})

	mounts = append(mounts, corev1.VolumeMount{
		Name:      constants.ArgusProvisionDashboardVolumeName,
		MountPath: constants.ArgusProvisioningDashboardsPath,
	})

	mounts = append(mounts, corev1.VolumeMount{
		Name:      constants.ArgusProvisionNotifierVolumeName,
		MountPath: constants.ArgusProvisioningNotifiersPath,
	})

	mounts = append(mounts, corev1.VolumeMount{
		Name:      constants.ArgusLogsVolumeName,
		MountPath: constants.ArgusLogsPath,
	})

	return mounts
}

func getContainers(cr *v1alpha1.Tenant, configHash string) []corev1.Container {
	var argusImage string
	if cr.Spec.GOUI.Image != nil {
		argusImage = *cr.Spec.GOUI.Image
	} else {
		// e.g. registry.gitlab.com/gitlab-org/opstrace/opstrace-ui/gitlab-observability-ui:<TAG>
		argusImage = constants.DockerImageFullNameWithRegistry(
			constants.ArgusRegistryName,
			constants.ArgusImageName,
		)
	}

	return []corev1.Container{{
		Name:  "argus",
		Image: argusImage,
		Args:  []string{"-config=/etc/argus/grafana.ini"},
		Ports: []corev1.ContainerPort{
			{
				Name:          "argus-http",
				ContainerPort: int32(GetArgusPort(cr)),
				Protocol:      "TCP",
			},
		},
		Env:          getEnv(configHash),
		Resources:    getResources(),
		VolumeMounts: getVolumeMounts(),
		LivenessProbe: &corev1.Probe{
			ProbeHandler: corev1.ProbeHandler{
				HTTPGet: &corev1.HTTPGetAction{
					Path:   constants.ArgusHealthEndpoint,
					Port:   intstr.FromInt(GetArgusPort(cr)),
					Scheme: corev1.URISchemeHTTP,
				},
			},
			InitialDelaySeconds: 60,
			TimeoutSeconds:      30,
			PeriodSeconds:       10,
			SuccessThreshold:    1,
			FailureThreshold:    10,
		},
		ReadinessProbe: &corev1.Probe{
			ProbeHandler: corev1.ProbeHandler{
				HTTPGet: &corev1.HTTPGetAction{
					Path:   constants.ArgusHealthEndpoint,
					Port:   intstr.FromInt(GetArgusPort(cr)),
					Scheme: corev1.URISchemeHTTP,
				},
			},
			InitialDelaySeconds: 5,
			TimeoutSeconds:      3,
			PeriodSeconds:       10,
			SuccessThreshold:    1,
			FailureThreshold:    1,
		},
		TerminationMessagePath:   "/dev/termination-log",
		TerminationMessagePolicy: "File",
		ImagePullPolicy:          "IfNotPresent",
	}}
}

func getEnv(configHash string) []corev1.EnvVar {
	return []corev1.EnvVar{
		{
			Name:  constants.ArgusDataPathsEnvVar,
			Value: "/var/lib/argus/",
		},
		{
			Name:  constants.ArgusLogsPathsEnvVar,
			Value: "/var/log/argus/",
		},
		{
			Name:  constants.ArgusPluginPathsEnvVar,
			Value: "/var/lib/argus/plugins/",
		},
		{
			Name:  constants.ArgusProvisioningPathsEnvVar,
			Value: "/etc/argus/provisioning/",
		},
		{
			Name:  constants.LastConfigEnvVar,
			Value: configHash,
		},
		{
			Name: constants.ArgusAdminUserEnvVar,
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: constants.ArgusAdminSecretName,
					},
					Key: constants.ArgusAdminUserEnvVar,
				},
			},
		},
		{
			Name: constants.ArgusAdminPasswordEnvVar,
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: constants.ArgusAdminSecretName,
					},
					Key: constants.ArgusAdminPasswordEnvVar,
				},
			},
		},
		{
			Name: constants.ArgusPostgresURLKey,
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: constants.ArgusPostgresSecretName,
					},
					Key: constants.ArgusPostgresURLKey,
				},
			},
		},
		{
			Name:  constants.ArgusHTTPPortEnvVar,
			Value: strconv.Itoa(constants.ArgusHTTPPort),
		},
	}
}

func getInitContainers(plugins string) []corev1.Container {
	return []corev1.Container{
		{
			Name:  constants.ArgusInitContainerName,
			Image: constants.OpstraceImages().ArgusPluginsInitContainerImage,
			Env: []corev1.EnvVar{
				{
					Name:  "GRAFANA_PLUGINS",
					Value: plugins,
				},
			},
			Resources: getInitResources(),
			VolumeMounts: []corev1.VolumeMount{
				{
					Name:      constants.ArgusPluginsVolumeName,
					ReadOnly:  false,
					MountPath: "/opt/plugins",
				},
			},
			TerminationMessagePath:   "/dev/termination-log",
			TerminationMessagePolicy: "File",
			ImagePullPolicy:          "IfNotPresent",
		},
	}
}

func StatefulSet(cr *v1alpha1.Tenant) *v1.StatefulSet {
	return &v1.StatefulSet{
		ObjectMeta: metav1.ObjectMeta{
			Name:      constants.ArgusStatefulSetName,
			Namespace: cr.Namespace,
		},
	}
}

func getStatefulSetSpec(
	cr *v1alpha1.Tenant,
	configHash, plugins string,
	current v1.StatefulSetSpec,
) v1.StatefulSetSpec {
	return v1.StatefulSetSpec{
		Replicas: &Replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: getPodLabels(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        constants.ArgusStatefulSetName,
				Labels:      getPodLabels(),
				Annotations: getStatefulSetAnnotations(cr, current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Volumes:            getVolumes(),
				InitContainers:     getInitContainers(plugins),
				Containers:         getContainers(cr, configHash),
				ServiceAccountName: constants.ArgusServiceAccountName,
			},
		},
	}
}

func StatefulSetMutator(cr *v1alpha1.Tenant, current *v1.StatefulSet, configHash, plugins string) error {
	currentSpec := &current.Spec
	spec := getStatefulSetSpec(
		cr, configHash, plugins, current.Spec)

	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return fmt.Errorf("error while patching statefulset spec: %w", err)
	}

	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.Argus.Components.StatefulSet.Spec,
	); err != nil {
		return fmt.Errorf("error while patching statefulset spec: %w", err)
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getStatefulSetAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.Argus.Components.StatefulSet.Annotations,
	)
	current.Labels = utils.MergeMap(
		getStatefulSetLabels(),
		cr.Spec.Overrides.Argus.Components.StatefulSet.Labels,
	)

	return nil
}

func StatefulSetSelector(cr *v1alpha1.Tenant) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      constants.ArgusStatefulSetName,
	}
}

func getStatefulSetLabels() map[string]string {
	return map[string]string{
		"app": constants.ArgusPodLabel,
	}
}

func getStatefulSetAnnotations(_ *v1alpha1.Tenant, existing map[string]string) map[string]string {
	return existing
}
