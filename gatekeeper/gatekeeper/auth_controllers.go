package gatekeeper

import (
	"errors"
	"fmt"
	"net/url"
	"regexp"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

// NOTE(prozlach): The caveat here is that our integration points to GitLab
// already require `read_api` scopes as we use it to perform some membership
// checks. We need to investigate whether we can simply extend
// `*_observability` scopes a bit so that `read_api` is no longer needed.

// ReadAccessTokenScopes returns scope sets. Token needs to have one or
// more of them in order for request to be granted.
func ReadAccessTokenScopes() []string {
	return []string{"read_api", "read_observability"}
}

func WriteAccessTokenScopes() []string {
	return []string{"read_api", "write_observability"}
}

func ReadWriteAccessTokenScopes() []string {
	return []string{"read_api", "read_observability", "write_observability"}
}

func actionToTokenScopes(action string) []string {
	switch action {
	case "read":
		return ReadAccessTokenScopes()
	case "write":
		return WriteAccessTokenScopes()
	case "readwrite":
		return ReadWriteAccessTokenScopes()
	default:
		// This is a programming error, not input error. The Gin's path params
		// input validation should have handled it before we reached this
		// point.
		panic("Unknown action passed")
	}
}

type ArgusAuthWebhookURIParams struct {
	RootID string `uri:"root_namespace_id" binding:"required"`
}

type GroupAuthHandlerFormParams struct {
	MinAccessLevel    *int `json:"min_accesslevel" form:"min_accesslevel" binding:"required,numeric,min=0,max=50"`
	SupportLegacyGOUI bool `json:"support_legacy_goui" form:"support_legacy_goui"`
}

type GroupAuthHandlerPathParams struct {
	NamespaceID int    `uri:"namespace_id" binding:"required,numeric,min=1"`
	Action      string `uri:"action" binding:"required,oneof=read write readwrite"`
}

type ProjectAuthHandlerFormParams struct {
	MinAccessLevel *int `json:"min_accesslevel" form:"min_accesslevel" binding:"required,numeric,min=0,max=50"`
}

type ProjectAuthHandlerPathParams struct {
	ProjectID int    `uri:"project_id" binding:"required,numeric,min=1"`
	Action    string `uri:"action" binding:"required,oneof=read write readwrite"`
}

type NamespacePathParam struct {
	NamespaceID int `uri:"namespace_id" binding:"required,numeric,min=1"`
}

type AuthWebhookQuery struct {
	URI string `form:"uri"  binding:"required"`
}

type AuthWebhookHeader struct {
	ArgusNamespaceID string `header:"x-grafana-org-id"`
	NamespaceID      string `header:"x-namespace-id"`
}

// NamespaceIDs are required to be alphanumeric in order for the query hostname to work below.
// Gitlab namespaces are numeric, but special case "system" namespace must also be allowed through.
var namespaceIDPattern = regexp.MustCompile(`^[0-9-]+$`)

// Specific middleware for Argus ingress.
// Returns an auth handler that will handle several cases of auth.
// First it will check if there is an Authorization header
// and then seek to verify the token if the header exists.
// If the header does not exist, the controller will fall
// back to requiring an authenticated session and will verify
// auth against the session.
// Only supports argus auth tokens.
//
//nolint:funlen,gocyclo,cyclop
func ArgusAuthHandlerFactory(autoAuth bool) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var header AuthWebhookHeader
		if err := ctx.ShouldBindHeader(&header); err != nil {
			ctx.AbortWithError(403, fmt.Errorf("invalid header: %w", err))
			return
		}
		var params ArgusAuthWebhookURIParams
		if err := ctx.ShouldBindUri(&params); err != nil {
			ctx.AbortWithError(403, fmt.Errorf("invalid params: %w", err))
			return
		}
		var query AuthWebhookQuery
		if err := ctx.ShouldBindQuery(&query); err != nil {
			ctx.AbortWithError(403, fmt.Errorf("invalid query: %w", err))
			return
		}

		uri, err := url.Parse(query.URI)
		if err != nil {
			ctx.AbortWithError(400, fmt.Errorf("uri query parameter: %w", err))
			return
		}
		// Let Argus static assets through.
		if strings.HasPrefix(uri.Path, fmt.Sprintf("/%s/build/", params.RootID)) ||
			strings.HasPrefix(uri.Path, fmt.Sprintf("/%s/public/", params.RootID)) ||
			strings.HasPrefix(uri.Path, fmt.Sprintf("/%s/robots.txt", params.RootID)) ||
			strings.HasPrefix(uri.Path, fmt.Sprintf("/%s/avatar/", params.RootID)) {
			ctx.String(200, "Success")
			return
		}

		// Use the root level namespace.
		// Admin uris in Argus, e.g.
		//
		// 		/<rootNamespaceID>/datasources
		//		/<rootNamespaceID>/plugins
		// 		/<rootNamespaceID>/preferences
		//
		// will not send a header containing x-grafana-org-id and will send
		// a query param of groupId=-1 if the browser page is refreshed. This presents
		// an issue where under these circumstances, we'll have to do a little more
		// work to get the namespaceID.
		rootNamespaceID := params.RootID
		namespaceID := ""

		// Use groupId if it exists. This query param is sent by the Argus UI.
		// Argus UI uses a mix of sending groupId param or setting the header
		// that we check for below.
		if uri.Query().Has("groupId") && uri.Query().Get("groupId") != "" {
			namespaceID = uri.Query().Get("groupId")
		}
		// Allow users to set an Opstrace specific header for selecting namespace
		// x-namespace-id
		if header.NamespaceID != "" {
			namespaceID = header.NamespaceID
		}
		// Use standard Argus header if it exists (x-grafana-org-id)
		// This header is sent in requests from the Argus UI.
		if header.ArgusNamespaceID != "" {
			namespaceID = header.ArgusNamespaceID
		}

		// Make sure the rootNamespaceID is legit.
		if !namespaceIDPattern.MatchString(rootNamespaceID) {
			ctx.AbortWithError(404, fmt.Errorf("rootNamespaceID='%s' is invalid", rootNamespaceID))
			return
		}
		// Make sure the namespaceID is legit if it exists at this point.
		if namespaceID != "" && !namespaceIDPattern.MatchString(namespaceID) {
			ctx.AbortWithError(404, fmt.Errorf("namespaceID='%s' is invalid", namespaceID))
			return
		}

		bearerToken := ctx.GetHeader("private-token")
		// Request using a token:
		//nolint:cyclop,nestif // required by the business logic
		if bearerToken != "" {
			if namespaceID == "" {
				gitLabService, err := NewGitLabServiceFromAccessToken(ctx, bearerToken)
				if err != nil {
					ctx.AbortWithError(403, fmt.Errorf("create GitlabService: %w", err))
					return
				}
				namespaceID, err = GetNamespaceFromGitlabForToken(ctx, gitLabService)
				if err != nil {
					ctx.AbortWithError(403, fmt.Errorf("GetNamespaceFromGitlabForToken: %w", err))
					return
				}
			}
			if namespaceID == "" {
				ctx.AbortWithError(403, errors.New("target namespace not discovered"))
				return
			}
			// Set the namespaceID on the context for downstream handlers
			SetNamespace(ctx, namespaceID)

			HandleGLAccessTokenAuth(ctx, bearerToken, ReadAccessTokenScopes())
			if !ctx.IsAborted() {
				HandleTokenAuth(ctx)
			}
			return
		}

		// Fallback to authorization via session cookie. This request comes from the browser.
		AuthenticatedSessionRequired(autoAuth, func(_ *gin.Context) {
			if namespaceID == "" {
				// Now that we have a session, we can retrieve the user and
				// attempt to retrieve their last known namespace that they used
				// in this Argus instance.
				var lastUsedNamespace = &LastNamespace{}
				found := GetLastUsedNamespaceForUser(ctx, rootNamespaceID, lastUsedNamespace)
				if !found {
					ctx.AbortWithError(403, errors.New("the last used NS for user was not found"))
					return
				}
				if lastUsedNamespace.ID == "" {
					ctx.AbortWithError(403, errors.New("the last NS used retrieved from cache is empty"))
					return
				}
				namespaceID = lastUsedNamespace.ID
			} else {
				// Update the last known namespaceID in the cache
				err := SetLastUsedNamespaceForUser(ctx, rootNamespaceID, LastNamespace{ID: namespaceID})
				if err != nil {
					// This probably doesn't need to block auth moving forward,
					// since it's not critical to operation but it will ensure we catch
					// any service errors that we should resolve.
					ctx.AbortWithError(403, errors.New("failed to update last known namespace in cache"))
					return
				}
			}

			// Set the namespaceID on the context for downstream handlers
			SetNamespace(ctx, namespaceID)
			HandleTokenAuth(ctx)
		})(ctx)
	}
}

// ProjectAuthHandlerFactory returns an auth handler that will handle several
// cases of auth for project membership.
//
//nolint:funlen // This is mostly empty lines, and comments plus
func ProjectAuthHandlerFactory(autoAuth bool, namespacedPath bool) gin.HandlerFunc {
	return func(ginCtx *gin.Context) {
		var pathParams ProjectAuthHandlerPathParams
		if err := ginCtx.ShouldBindUri(&pathParams); err != nil {
			ginCtx.AbortWithError(403, fmt.Errorf("path parameters validation failed: %w", err))
			return
		}

		namespaceID := -1
		if namespacedPath {
			var tmp NamespacePathParam
			if err := ginCtx.ShouldBindUri(&tmp); err != nil {
				ginCtx.AbortWithError(403, fmt.Errorf("namespace parameter validation failed: %w", err))
				return
			}
			namespaceID = tmp.NamespaceID
		}

		var formParams ProjectAuthHandlerFormParams
		if err := ginCtx.ShouldBindQuery(&formParams); err != nil {
			ginCtx.AbortWithError(403, fmt.Errorf("uri parameters validation failed: %w", err))
			return
		}

		// Check for Authorization header first
		bearerToken := ginCtx.GetHeader("private-token")
		if bearerToken != "" {
			// NOTE(prozlach): we differentiate "action" only for token
			// access/auth, user acesssing via cookie/browser is assumed to
			// have full access. There are only three possible actions as there
			// are only 2 scopes available for us:
			// * `read_observability`
			// * `write_observability`
			// Individual ingress objects may map these to URIs, but from the
			// perspective of Gatekeeper URIs have no meaning as they would be
			// mapped to one of these three "actions", hence we keep things
			// simple and configure this in the ingress object itself.
			HandleGLAccessTokenAuth(
				ginCtx,
				bearerToken,
				actionToTokenScopes(pathParams.Action),
			)

			if ginCtx.IsAborted() {
				return
			}

			if namespacedPath {
				VerifyProjectGroupMembership(
					ginCtx,
					namespaceID,
					pathParams.ProjectID,
				)
			} else {
				// If the request path does not contain the namespace of the
				// projects, we fetch the top-level namespace that the project
				// belongs to and set it as a HTTP header for downstream's
				// components consuption.
				SetTopLevelNamespaceHeader(ginCtx, pathParams.ProjectID)
			}

			if ginCtx.IsAborted() {
				return
			}

			HandleProjectAccessAuth(
				ginCtx,
				pathParams.ProjectID,
				*formParams.MinAccessLevel,
			)
			return
		}

		// Fallback to authorization via session cookie. This request comes
		// from the browser and allows users to interact with the API in the
		// browser
		log.Debug("handling session cookie-based auth")
		AuthenticatedSessionRequired(autoAuth, func(_ *gin.Context) {
			if namespacedPath {
				VerifyProjectGroupMembership(
					ginCtx,
					namespaceID,
					pathParams.ProjectID,
				)
			} else {
				// If the request path does not contain the namespace of the
				// projects, we fetch the top-level namespace that the project
				// belongs to and set it as a HTTP header for downstream's
				// components consuption.
				SetTopLevelNamespaceHeader(ginCtx, pathParams.ProjectID)
			}

			if ginCtx.IsAborted() {
				return
			}

			HandleProjectAccessAuth(
				ginCtx,
				pathParams.ProjectID,
				*formParams.MinAccessLevel,
			)
		})(ginCtx)
	}
}

// GroupAuthHandlerFactory returns an auth handler that will handle several
// cases of authn/authz for Group/Namespace membership.
func GroupAuthHandlerFactory(autoAuth bool) gin.HandlerFunc {
	return func(ginCtx *gin.Context) {
		var pathParams GroupAuthHandlerPathParams
		if err := ginCtx.ShouldBindUri(&pathParams); err != nil {
			ginCtx.AbortWithError(403, fmt.Errorf("path parameters validation failed: %w", err))
			return
		}

		var formParams GroupAuthHandlerFormParams
		if err := ginCtx.ShouldBindQuery(&formParams); err != nil {
			ginCtx.AbortWithError(403, fmt.Errorf("uri parameters validation failed: %w", err))
			return
		}

		// Check for Authorization header first
		bearerToken := ginCtx.GetHeader("private-token")
		if bearerToken != "" {
			// NOTE(prozlach): we differentiate "action" only for token
			// access/auth, user acesssing via cookie/browser is assumed to
			// have full access. There are only three possible actions as there
			// are only 2 scopes available for us:
			// * `read_observability`
			// * `write_observability`
			// Individual ingress objects may map these to URIs, but from the
			// perspective of Gatekeeper URIs have no meaning as they would be
			// mapped to one of these three "actions", hence we keep things
			// simple and configure this in the ingress object itself.
			HandleGLAccessTokenAuth(
				ginCtx,
				bearerToken,
				actionToTokenScopes(pathParams.Action),
			)
			if ginCtx.IsAborted() {
				return
			}

			HandleGroupAccessAuth(
				ginCtx,
				pathParams.NamespaceID,
				*formParams.MinAccessLevel,
				formParams.SupportLegacyGOUI,
			)
			return
		}

		// Fallback to authorization via session cookie. This request comes
		// from the browser and allows users to interact with the API in the
		// browser.
		log.Debug("handling session cookie-based auth")
		AuthenticatedSessionRequired(autoAuth, func(_ *gin.Context) {
			HandleGroupAccessAuth(
				ginCtx,
				pathParams.NamespaceID,
				*formParams.MinAccessLevel,
				formParams.SupportLegacyGOUI,
			)
		})(ginCtx)
	}
}

// GetNamespaceFromGitlabForToken tries to guess the group assigned to given
// Access Token. It is not bulletproof, treat it as workaroudn that we can iterate on
// if necessary. Hopefully Gitlab API starts providing a way to do it no more
// elegant way someway.
//
//nolint:funlen // business logic and lots of comments
func GetNamespaceFromGitlabForToken(ctx *gin.Context, gitLabService *GitLabService) (string, error) {
	// There are three possible cases:
	// 1. Group Access Token
	// 2. Project Access Token
	// 3. Personal Access Token
	//
	// GitLab API does not offer an easy way to determine the token type, so we
	// use an educated guess - we check the name of the user:
	// * in case of Group Access Token: `group_22_bot` and `bot` attribute set
	// * in case of Project Access Token: `project_1_bot1 and `bot` attribute set`
	// * personal access token - `bot` attribute not set

	user, err := gitLabService.CurrentUser()
	if err != nil {
		ctx.AbortWithError(403, fmt.Errorf("current user: %w", err))
		return "", nil
	}

	if !user.Bot {
		// NOTE(prozlach): Seems like Personal Access Token. In such a case we
		// can't really determine the namespace as there can be more than one
		// assigned. In some cases, when there is only one group to which user
		// belongs, we could return this group, but this seems like
		// overcomplication.
		return "", nil
	}

	// There is no API guarantee that the user will always follow this pattern,
	// but this is the best we have :(
	re := regexp.MustCompile(`^(project|group)_(\d+)_bot.*$`)
	matches := re.FindStringSubmatch(user.Username)

	if len(matches) < 3 {
		log.Warn(fmt.Sprintf(
			"bot account name %q does not conform to the pattern, "+
				"please verify that the bot naming format has not changed",
			user.Username,
		))
		return "", nil
	}

	switch matches[1] {
	case "group":
		groupID, err := strconv.Atoi(matches[2])
		if err != nil {
			return "", fmt.Errorf(
				"unable to determine group ID basing on the bot account name %q: %w",
				user.Username, err,
			)
		}
		// Double-check that the GroupID from the bot's name is accompanied by
		// the correct membership
		groupMember, err := gitLabService.GetInheritedGroupMember(groupID, user.ID)
		if err != nil {
			return "", fmt.Errorf("failed to retrieve group membership: %w", err)
		}
		if groupMember == nil {
			log.Warn(fmt.Sprintf(
				"bot account name %q points to a group that the bot is not member of, "+
					"please verify that the bot naming format has not changed",
				user.Username,
			))
			return "", nil
		}

		// Bot is a member of this group, we should be good to go.
		return strconv.Itoa(groupID), nil
	case "project":
		projectID, err := strconv.Atoi(matches[2])
		if err != nil {
			return "", fmt.Errorf(
				"unable to determine project ID basing on the bot account name %q: %w",
				user.Username, err,
			)
		}
		projectMember, err := gitLabService.GetInheritedProjectMember(projectID, user.ID)
		if err != nil {
			return "", fmt.Errorf("failed to check if project is a member of a group: %w", err)
		}
		if projectMember == nil {
			log.Warn(fmt.Sprintf(
				"bot account name %q points to a project that the bot is not member of, "+
					"please verify that the bot naming format has not changed",
				user.Username,
			))
			return "", nil
		}

		projectGroups, err := gitLabService.ListProjectsGroups(projectID)
		if err != nil {
			return "", fmt.Errorf("failed to check list project's groups: %w", err)
		}

		// NOTE(prozlach) - project should belong to at least one group
		return strconv.Itoa(projectGroups[0].ID), nil
	default:
		log.Warn(fmt.Sprintf(
			"bot account name %q has an unexpected prefix, "+
				"please verify that the bot naming format has not changed",
			user.Username,
		))
	}
	return "", nil
}
