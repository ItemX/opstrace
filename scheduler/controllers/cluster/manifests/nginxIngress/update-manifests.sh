#!/usr/bin/env bash

set -eou pipefail

# https://github.com/kubernetes/ingress-nginx#changelog
VER=v1.7.1
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
README_DIR=$(realpath $SCRIPT_DIR)

curl -s\
    https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-${VER}/deploy/static/provider/cloud/deploy.yaml \
    -o ${SCRIPT_DIR}/bundle.yaml

echo "Please make sure to read the README.md file in the $README_DIR directory as well."
