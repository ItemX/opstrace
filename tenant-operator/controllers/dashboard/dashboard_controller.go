/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package dashboard

import (
	"context"
	"fmt"
	"math"
	"net/http"
	"net/url"

	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/config"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/source"

	"time"

	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

const (
	ControllerName         = "controller_dashboard"
	DefaultDashboardFolder = "managed-dashboards"
)

type ClientFactory func(groupId int64) (*common.ArgusClient, error)

// DashboardReconciler reconciles a Dashboard object.
type DashboardReconciler struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver

	Client    client.Client
	Scheme    *runtime.Scheme
	Transport *http.Transport
	Config    *config.ControllerConfig
	Recorder  record.EventRecorder
	Log       logr.Logger
}

// +kubebuilder:rbac:groups=opstrace.com,resources=dashboards,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=opstrace.com,resources=dashboards/status,verbs=get;update;patch

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Dashboard object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.7.0/pkg/reconcile
func (r *DashboardReconciler) Reconcile(ctx context.Context, request ctrl.Request) (ctrl.Result, error) {
	getClient, err := r.getClientFactory(ctx, request)
	if err != nil {
		r.Log.Error(err, "failed to create client")
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, err
	}

	// Fetch the Dashboard instance
	instance := &opstracev1alpha1.Dashboard{}

	err = r.Client.Get(ctx, request.NamespacedName, instance)

	if err != nil && !k8serrors.IsNotFound(err) {
		r.Log.Error(err, "failed to read Dashboard")
		// Error reading the object - requeue the request.
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, err
	}
	// Otherwise always re sync all dashboards
	return r.reconcileDashboards(ctx, request, getClient)
}

var _ reconcile.Reconciler = &DashboardReconciler{}

// Check if a given dashboard (by name) is present in the list of
// dashboards in the Tenant.
func inTenant(existingTenantDashboards *opstracev1alpha1.DashboardList, item *opstracev1alpha1.DashboardRef) bool {
	for _, d := range existingTenantDashboards.Items {
		if d.Name == item.Name && d.Spec.GroupID == item.GroupID {
			return true
		}
	}
	return false
}

// Returns the hash of a dashboard if it is known.
func findHash(knownDashboards []*opstracev1alpha1.DashboardRef, item *opstracev1alpha1.Dashboard) string {
	for _, d := range knownDashboards {
		if item.Name == d.Name && item.Spec.GroupID == d.GroupID {
			return d.Hash
		}
	}
	return ""
}

// Returns the UID of a dashboard if it is known.
func findUid(knownDashboards []*opstracev1alpha1.DashboardRef, item *opstracev1alpha1.Dashboard) string {
	for _, d := range knownDashboards {
		if item.Name == d.Name && item.Spec.GroupID == d.GroupID {
			return d.UID
		}
	}
	return ""
}

func (r *DashboardReconciler) reconcileDashboards( //nolint
	ctx context.Context,
	request reconcile.Request,
	grafanaClientFactory ClientFactory,
) (reconcile.Result, error) {
	retryReconcile := false
	// Collect known and existing dashboards
	knownDashboards := r.Config.GetDashboards()
	existingTenantDashboards := &opstracev1alpha1.DashboardList{}

	opts := &client.ListOptions{
		Namespace: request.Namespace,
	}

	err := r.Client.List(ctx, existingTenantDashboards, opts)
	if err != nil {
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, err
	}

	// Prepare lists
	var dashboardsToDelete []*opstracev1alpha1.DashboardRef

	// Dashboards to delete: dashboards that are known but not found
	// any longer in the namespace
	for _, dashboard := range knownDashboards {
		if !inTenant(existingTenantDashboards, dashboard) {
			dashboardsToDelete = append(dashboardsToDelete, dashboard)
		}
	}

	// Process new/updated dashboards
	for _, dashboard := range existingTenantDashboards.Items {
		dashboard := dashboard
		folderName := DefaultDashboardFolder
		groupId := dashboard.Spec.GroupID
		if dashboard.Spec.CustomFolderName != "" {
			folderName = dashboard.Spec.CustomFolderName
		}

		if dashboard.Status.Error != nil && dashboard.Status.Error.Code == 429 {
			backoffDuration := 30 * time.Second * time.Duration(math.Pow(2, float64(dashboard.Status.Error.Retries)))

			if dashboard.Status.ContentTimestamp.Add(backoffDuration).After(time.Now()) {
				log.Log.Info("still awaiting rate limit for dashboard", "folder", folderName, "dashboard", request.Name)
				retryReconcile = true
				continue
			}
		}
		c, err := grafanaClientFactory(groupId)
		if err != nil {
			r.manageError(&dashboard, err)
			retryReconcile = true
			continue
		}

		folder, err := c.CreateOrUpdateFolder(folderName)

		if err != nil {
			r.manageError(&dashboard, err)
			retryReconcile = true
			continue
		}

		// Process the dashboard. Use the known hash of an existing dashboard
		// to determine if an update is required
		knownHash := findHash(knownDashboards, &dashboard)
		knownUid := findUid(knownDashboards, &dashboard)
		pipeline := NewDashboardPipeline(r.Client, &dashboard)
		processed, err := pipeline.ProcessDashboard(ctx, knownHash, folder.UID, folderName, false)

		// Check known dashboards exist on grafana instance and recreate if not
		if knownUid != "" {
			response, err := c.GetDashboard(knownUid)
			if err != nil {
				log.Log.Error(err, "Failed to search Grafana for dashboard")
			}

			if response == nil {
				log.Log.Info(fmt.Sprintf("Dashboard %v does not exist. Recreating.", dashboard.ObjectMeta.Name))
				processed, err = pipeline.ProcessDashboard(ctx, knownHash, folder.UID, folderName, true)

				if err != nil {
					r.manageError(&dashboard, err)
					retryReconcile = true
					continue
				}
			}
		}

		if err != nil {
			r.manageError(&dashboard, err)
			retryReconcile = true
			continue
		}

		if processed == nil {
			r.Config.SetPluginsFor(&dashboard)
			continue
		}

		_, err = c.CreateOrUpdateDashboard(processed, folder.ID)
		if err != nil {
			r.manageError(&dashboard, err)
			retryReconcile = true
			continue
		}

		r.manageSuccess(&dashboard, &folder.ID, folderName)
	}

	for _, dashboard := range dashboardsToDelete {
		groupId := dashboard.GroupID
		c, err := grafanaClientFactory(groupId)
		if err != nil {
			log.Log.Error(err, "failed to create grafana client for group", "groupId", groupId)
			retryReconcile = true
			continue
		}

		err = c.DeleteDashboardByUID(dashboard.UID)
		if err != nil {
			log.Log.Error(err, "error deleting dashboard")
			retryReconcile = true
			continue
		}

		log.Log.Info(fmt.Sprintf("deleted dashboard %s/%s", dashboard.FolderName, dashboard.Name), "groupId", groupId)

		r.Config.RemovePluginsFor(dashboard)
		r.Config.RemoveDashboard(dashboard.UID, groupId)

		safe, err := c.SafeToDelete(dashboard.FolderId)
		if err != nil {
			log.Log.Error(err, "failed to search folder contents", "dashboard.folderId", *dashboard.FolderId)
			retryReconcile = true
		}
		if safe {
			if err = c.DeleteFolderByID(dashboard.FolderId); err != nil {
				log.Log.Error(err, "delete dashboard folder failed", "dashboard.FolderId", dashboard.FolderId)
				retryReconcile = true
			} else {
				log.Log.Info("deleted empty folder", "dashboard.FolderId", dashboard.FolderId, "groupId", groupId)
			}
		}
	}
	// Update InstalledDashboards in cluster
	op, err := r.Config.PersistKnownDashboards(r.Client, ctx, request.Namespace)
	if err != nil {
		log.Log.Error(err, "failed to persist known dashboards to cluster")
		retryReconcile = true
	} else {
		log.Log.Info(fmt.Sprintf("persist known dashboards to cluster [%s]", op))
	}

	if retryReconcile {
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
	}

	return reconcile.Result{}, nil
}

// Get an authenticated grafana API client.
func (r *DashboardReconciler) getClientFactory(
	ctx context.Context,
	request ctrl.Request,
) (ClientFactory, error) {
	key := client.ObjectKey{
		Namespace: request.Namespace,
		Name:      constants.TenantName,
	}
	tenant := new(v1alpha1.Tenant)

	err := r.Client.Get(ctx, key, tenant)
	if err != nil {
		return nil, fmt.Errorf("unable to fetch tenant %s/%s: %w", request.Namespace, constants.TenantName, err)
	}

	tenantReady := apimeta.IsStatusConditionTrue(tenant.Status.Conditions, common.ConditionTypeReady)
	if !tenantReady {
		return nil, controllers.ErrGrafanaURLTenantNotReady
	}
	if tenant.Status.Argus.URL == nil {
		return nil, controllers.ErrGrafanaURLTenantURLEmpty
	}
	argusURL := *tenant.Status.Argus.URL
	parsedUrl, err := url.Parse(argusURL)
	if err != nil {
		return nil, fmt.Errorf("unable to parse argus URL %s: %w", argusURL, err)
	}
	username := parsedUrl.User.Username()
	password, _ := parsedUrl.User.Password()

	return func(groupId int64) (*common.ArgusClient, error) {
		return common.NewArgusClient(argusURL, username, password, r.Transport, groupId)
	}, nil
}

// Handle success case: update dashboard metadata (id, uid) and update the list
// of plugins.
func (r *DashboardReconciler) manageSuccess(dashboard *opstracev1alpha1.Dashboard, folderId *int64, folderName string) {
	msg := fmt.Sprintf("dashboard %v/%v successfully submitted",
		dashboard.Namespace,
		dashboard.Name)
	r.Recorder.Event(dashboard, "Normal", "Success", msg)

	log.Log.Info("dashboard successfully submitted", "name", dashboard.Name, "groupId", dashboard.Spec.GroupID)

	r.Config.AddDashboard(dashboard, folderId, folderName)
	r.Config.SetPluginsFor(dashboard)
}

// Handle error case: update dashboard with error message and status.
func (r *DashboardReconciler) manageError(dashboard *opstracev1alpha1.Dashboard, issue error) {
	r.Recorder.Event(dashboard, "Warning", "ProcessingError", issue.Error())
	// Ignore conflicts. Resource might just be outdated, also ignore if grafana isn't available.
	if k8serrors.IsConflict(issue) || k8serrors.IsServiceUnavailable(issue) {
		return
	}
	log.Log.Error(issue, "error during dashboard reconcile", "name", dashboard.Name, "groupId", dashboard.Spec.GroupID)
}

func (r *DashboardReconciler) SetupWithManager(mgr manager.Manager) error {
	tenantHandler := func(tenant client.Object) []reconcile.Request {
		list := &opstracev1alpha1.DashboardList{}
		opts := &client.ListOptions{
			Namespace: tenant.GetNamespace(),
		}
		err := r.Client.List(context.TODO(), list, opts)
		if err != nil {
			return nil
		}
		requests := make([]reconcile.Request, len(list.Items))
		for i, e := range list.Items {
			requests[i] = reconcile.Request{
				NamespacedName: types.NamespacedName{
					Namespace: e.GetNamespace(),
					Name:      e.GetName(),
				},
			}
		}
		return requests
	}
	return ctrl.NewControllerManagedBy(mgr).
		For(&opstracev1alpha1.Dashboard{}).
		Watches(
			&source.Kind{Type: &opstracev1alpha1.Tenant{}},
			handler.EnqueueRequestsFromMapFunc(tenantHandler),
		).
		Complete(r)
}
