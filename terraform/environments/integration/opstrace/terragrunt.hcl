terraform {
  source = "${get_terragrunt_dir()}/../../../..//terraform/modules/opstrace"
}

include {
  path = find_in_parent_folders()
}

dependency "gke" {
  config_path = "../gke"
  mock_outputs = {
    kubernetes_cluster_name = "mock"
    vpc_id                  = "mock"
    vpc_name                = "mock"
  }
  mock_outputs_merge_with_state           = true
  mock_outputs_merge_strategy_with_state  = "shallow"
  mock_outputs_allowed_terraform_commands = ["validate", "init"]
}

dependency "sql" {
  config_path = "../sql"
  mock_outputs = {
    postgres_dsn_endpoint = "mock"
  }
  mock_outputs_merge_with_state           = true
  mock_outputs_merge_strategy_with_state  = "shallow"
  mock_outputs_allowed_terraform_commands = ["validate", "init"]
}

inputs = {
  project_id                     = get_env("TF_VAR_project_id")
  zone                           = get_env("TF_VAR_location")
  gke_cluster_name               = dependency.gke.outputs.kubernetes_cluster_name
  provision_clickhouse_node_pool = false
  ch_nodepool_nodes_number       = 3
  ch_nodepool_machine_type       = "n1-standard-4"
  postgres_dsn_endpoint          = dependency.sql.outputs.postgres_dsn_endpoint
  scheduler_image                = get_env("TF_VAR_scheduler_image")
}